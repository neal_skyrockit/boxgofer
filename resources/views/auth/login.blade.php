<!DOCTYPE html>
<html lang='en'>
    <head>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title> Login  | Administrator</title>
        <link href="{{ asset('bk/css/bootstrap.min.css') }}" rel="stylesheet">

    </head>
    <body>
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-lg-4 col-lg-offset-4'>
				    @if ($errors->has())
				        @foreach ($errors->all() as $error)
				            <div class='bg-danger alert'>{{ $error }}</div>
				        @endforeach
				    @endif

				    <h1><i class='fa fa-lock'></i> Administrator Login</h1>

				    <form class="form-signin" action="/auth/login" method="post">

				    	{!! csrf_field() !!}

					    <div class='form-group'>
					        <label for="inputEmail" class="sr-only">Email address</label>
					        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="">
					    </div>

					    <div class='form-group'>
					        <label for="inputPassword" class="sr-only">Password</label>
					        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required="">
					    </div>

					    <div class='form-group'>
					        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
					    </div>

				    </form>

				</div>

            </div>
        </div>
    </body>
</html>