<div id="myModalCharge" class="modal fade themodel" role="dialog">
    <div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body model-deliver">
                    <form class="deliverform" ng-submit="chargeCustomer()">
                        <h4>Charge informations</h4>

                        <div class="grey-section margin1400">
                            <div class="form-group row nomarginbot">
                                <div class="col-md-3 col-sm-3 input-table padding5">
                                    <input id="charge-amount" ng-model="charge.amount" type="number" step="0.01" min="0" max="1000" class="form-control" placeholder="Amount ($)" required />
                                    <!-- <span class="glyphicon glyphicon-usd" aria-hidden="true"></span> -->
                                </div>

                                <div class="col-md-9 col-sm-9 input-table padding5">
                                    <input id="charge-des" ng-model="charge.des" type="text" class="form-control" placeholder="Description" required />
                                </div>
                            </div>
                        </div>

                        <!-- Error message -->
                        <div class="row text-danger" ng-show="charge.mess != ''">
                        	<div class="col-xs-12">@{{ charge.mess }}</div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-primary submit-btn pull-right">Submit</button>
                            </div>
                        </div>
                    </form>
			</div>
		</div>
	</div>
</div>