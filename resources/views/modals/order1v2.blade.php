<!-- row 1-->
<div class="row">
    <div class="col-xs-3">
        <p class="color">Bins Drop-off</p>
        <p>When are you free to receive your empty bins?</p>
    </div>
    <div class="col-xs-9">
        <div class="grey-section">
            <div class="row">
                <div class="col-sm-12 col-lg-6">
                    <div class="form-group">
                        <div class="input-table">
                            <input id="datepicker" type="text" class="form-control w165" placeholder="Date" />
                            <label for="datepicker" class="icon green icon-calendar"></label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6">
                    <div class="form-group">
                        <div class="input-table">
                            <input id="timepicker" ng-click="isShowTime = (! isShowTime) && (availableTime != null)" readonly="readonly" type="text" class="form-control w165" placeholder="Time" />
                            <input id="timepickerval" type="hidden" value="" />
                            <div ng-show="isShowTime" class="dropdown-menu bottom order-time">
                            	<div ng-repeat="foo in availableTime" class="@{{ foo.text }}" ng-click="onSelect(foo.time, foo.text)">@{{ foo.time[1] }}</div>
                            </div>
                            <label for="timepicker" class="icon green icon-clock"></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row 1 -->
<!-- row 2-->
<div class="row">
    <div class="col-xs-3">
        <p class="color">Address</p>
        <p>Where will all deliveries take place?</p>
    </div>
    <div class="col-xs-9">
        <div class="grey-section">

            @if (Auth::guard('user')->user())
                @if ($user->physicalAddress)
            	<div class="row">
            		<div class="col-sm-12">
            			<div class="grey-section nopadding">
                            <div class="current-address">{{ $user->physicalAddress }}, {{ $address }}</div>
                        </div>
                        <p>or change address</p>
                    </div>
            	</div>
                @endif
            @endif

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <input id="form-address" class="form-control" placeholder="Address" ng-model="order.address" />
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <input id="form-apt" class="form-control" placeholder="Apt/Suite" ng-model="order.aptsuite" style="font-size: 14px;"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    @if (Auth::guard('user')->user())
                        <div class="form-group" ng-init="city='{{ $user->city }}';zipcode='{{ $user->zipcode }}';order.physicalAddress='{{ $user->physicalAddress }}';$scope.order.zipcode='{{ $user->zipcode }}';addressend='{{ $address }}'">
                            <p style="height:38px;line-height: 38px;">{{ $address }}</p>
                    @else
                        <div class="form-group" ng-init="city='{{ $address->name }}';zipcode='{{ $address->zipcodes[0] }}';order.physicalAddress='';$scope.order.zipcode='{{ $address->zipcodes[0] }}';addressend='{{ $address->full[$address->zipcodes[0]] }}'">
                            <p style="height:38px;line-height: 38px;">{{ $address->full[$address->zipcodes[0]] }}</p>
                    @endif
                        </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input id="form-phone" class="form-control" placeholder="(123) 456-7890" ng-model="order.phone" maxlength="14" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row 2 -->
<!-- row 3-->
<div class="row">
    <div class="col-xs-3">
        <p class="color">Special Request</p>
        <p>Is there any more information you’d like to tell us about pick up times, your storage request or how your day is going?</p>
    </div>
    <div class="col-xs-9">
        <div class="grey-section">
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <textarea class="form-control" rows="5" ng-model="order.specialRequest"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>