<div class="modal fade" id="warehouseModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Warehouse Location
                </h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">

                <form class="form-horizontal" role="form">
                  <div class="form-group">
                    <label class="col-sm-3 control-label" for="ware-select-city">City code</label>
                    <div class="col-sm-9">
						<input type="text" ng-model="storage.warehouse_city" class="form-control" id="ware-select-city" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label" for="ware-select-code" >Warehouse code</label>
                    <div class="col-sm-9">
						<input type="text" ng-model="storage.warehouse_code" class="form-control" id="ware-select-code" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label" for="aisle-inp">Aisle number</label>
                    <div class="col-sm-9">
                        <input type="text" ng-model="storage.warehouse_aisle" class="form-control" id="aisle-inp" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label" for="section-inp">Section number</label>
                    <div class="col-sm-9">
                        <input type="text" ng-model="storage.warehouse_section" class="form-control" id="section-inp" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label" for="shelf-inp">Shelf number</label>
                    <div class="col-sm-9">
                        <input type="text" ng-model="storage.warehouse_shelf" class="form-control" id="shelf-inp" />
                    </div>
                  </div>
                </form>

                <p ng-repeat="error in warehouseErrors" ng-bind="error" class="error"></p>
          </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" ng-click="editWarehouse()" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>