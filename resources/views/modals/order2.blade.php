<!-- row 1-->
<div class="row">
    <div class="col-xs-3">
        <p class="color">Ordered Items:</p>
    </div>
    <div class="col-xs-9">
        <div class="grey-section nopadding">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <span class="icon icon-smallpackage"></span>
                    <span class="text" ng-bind="order.large"></span>
                    <span>Large</span>
                </div>
                <p class="margin0300 visible-xs col-xs-12"></p>
                <div class="col-xs-12 col-sm-6">
                    <span class="icon icon-bigpackage"></span>
                    <span class="text" ng-bind="order.extlarge"></span>
                    <span>Extra Large</span>
                </div>
            </div>
        </div>
        <div class="grey-section nopadding">
            <div class="row">
                <div class="col-xs-12">
                    <span class="icon icon-bicycle"></span>
                    <span class="text" ng-bind="order.otherstuff"></span>
                    <span>Other Stuff</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row 1 -->
<!-- row 2-->
<div class="row">
    <div class="col-xs-3">
        <p class="color">Drop-off Date and Time:</p>
    </div>
    <div class="col-xs-9">
        <div class="grey-section nopadding">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <span class="icon icon-calendarV2"></span>
                    <span class="text" ng-bind="order.formatdropoffDate()"></span>
                </div>
                <p class="margin0300 visible-xs col-xs-12"></p>
                <div class="col-xs-12 col-sm-6">
                    <span class="icon icon-clockV2"></span>
                    <span class="text" ng-bind="order.dropoffTimeShow"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row 2 -->
<!-- row 3-->
<div class="row">
    <div class="col-xs-3">
        <p class="color">Address Where Items will be Delivered:</p>
    </div>
    <div class="col-xs-9">
        <div class="grey-section nopadding">
            <div class="row">
                <div class="col-xs-12">
                    <span class="icon icon-home"></span>
                    <span class="text" ng-bind="order.addressfull"></span>
                </div>
            </div>
        </div>
        <div class="grey-section nopadding">
            <div class="row">
                <div class="col-xs-12">
                    <span class="icon icon-contact-phone"></span>
                    <span class="text" ng-bind="order.phone"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row 3 -->
<!-- row 4-->
<div class="row">
    <div class="col-xs-3">
        <p class="color" style="padding-top:18px;">Total Monthly Price</p>
    </div>
    <div class="col-xs-9">
        <div class="grey-section">
            <div class="row">
                <div class="col-xs-12">
                    <p class="color" style="padding:0 0 0 50px;"><span class="currency" ng-bind="order.totalMonthly()"></span>/month</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row 4 -->
<!--row 5-->

<!-- end row 5 -->