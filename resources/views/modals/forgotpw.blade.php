<div id="myModalForgotpw" class="modal fade themodel" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
            	<div ng-show="emailsuccess == ''">
	                <h3>Enter your email address and we'll send over your password.</h3>
	                <form class="frm-referFriend">
			            <div class="form-group">
		                    <div class="col-md-7 col-sm-7">
		                        <div class="form-group">
		                            <input type="email" ng-model="fgpwemail" class="form-control" id="fgpw-email" placeholder="Email" />
		                        </div>
		                    </div>
		                    <div class="col-md-2 col-sm-2">
		                        <button type="submit" class="btn btn-save btn-submit" ng-click="forgotpw()">Submit</button>
		                    </div>
		                </div>
		                <p class="text-danger" ng-show="emailerror != ''">@{{ emailerror }}</p>
		            </form>
	            </div>
	            <div ng-show="emailsuccess != ''">@{{ emailsuccess }}</div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>