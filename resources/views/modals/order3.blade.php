<!-- billing information -->
<div class="row">
    <div class="col-xs-12 col-sm-3">
        <p class="color fs24">Billing Details</p>
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="grey-section billing-step">
            <!-- row -->
            <div class="row">
                <div class="col-xs-12">
                    <p class="color">Account Information</p>
                </div>
            </div>
            <!-- row -->
            <div class="row">
                <div class="col-xs-4">
                    <span>Name</span>
                </div>
                <div class="col-xs-8">
                    @if (Auth::guard('user')->user())
                        <p style="border: 1px solid #ccc;border-radius: 4px;padding: 6px 12px;">{{ $user->name }}</p>
                    @else
                         <input type="text" class="form-control" id="full_name" name="full_name" placeholder="Name" ng-model="order.account.full_name" />
                    @endif
                </div>
            </div>
            <!-- row -->
            <!-- row -->
            <div class="row">
                <div class="col-xs-4">
                    <span>Email Address</span>
                </div>
                <div class="col-xs-8">
                    @if (Auth::guard('user')->user())
                        <p style="border: 1px solid #ccc;border-radius: 4px;padding: 6px 12px;">{{ $user->email }}</p>
                    @else
                        <input type="email" name="email" class="form-control" id="email" placeholder="Email Address" ng-model="order.account.email" />
                    @endif
                </div>
            </div>
            <!-- row -->
        </div>
    </div>
</div>
<!-- end of billing information -->
<!-- row 1-->
<div class="row">
    <div class="col-xs-12 col-sm-3">
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="grey-section billing-step">
            <!-- row -->
            <div class="row">
                <div class="col-xs-12">
                    <p class="color">Secure Payment</p>
                </div>
            </div>
            <!-- row -->
            <div class="row">
                <div class="col-xs-4">
                    <span>Credit Card</span>
                </div>
                <div class="col-xs-8">
                    <div class="payment payment-credit"></div>
                </div>
                <!-- <div class="col-xs-4">
                    <label class="payment payment-paypal">
                        <input ng-click="paypalPayment()" src="{{ asset('public/web/images/paypalbuynow.gif') }}" name="submit" alt="PayPal - The safer, easier way to pay online!" border="0" type="image">
                    </label>
                </div> -->
            </div>
            <!-- row -->
            <div class="row">
                <div class="col-xs-4">
                    <span>Card Number</span>
                </div>
                <div class="col-xs-8">
                    <input class="form-control" maxlength="16" id="cardNumber" placeholder="Card number" ng-model="order.card.card_number" />
                </div>
            </div>
            <!-- row -->
            <div class="row">
                <div class="col-xs-4">
                    <span>Expiration</span>
                </div>
                <div class="col-xs-3">
                    <input class="form-control" maxlength="5" id="cardExpiration" placeholder="MM / YY" ng-model="order.card.exp" />
                </div>
                <div class="col-xs-2">
                    <span>CVC</span>
                </div>
                <div class="col-xs-3">
                    <input class="form-control" maxlength="4" id="cardCVC" placeholder="CVC" ng-model="order.card.cvc" />
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row 1 -->
<!-- row 2-->
<div class="row">
    <div class="col-xs-12 col-sm-3">
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="grey-section billing-step">
            <div class="row">
                <div class="col-xs-12">
                    <p>Monthly cost :
                        <span class="color fs24 currency" ng-bind="order.totalMonthly()"></span>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <span>Promotion Code</span>
                </div>
                <div class="col-xs-3">
                	<input class="form-control" id="order-promo-code" placeholder="Code" ng-model="order.promo" />
                </div>
                <div class="col-xs-3 hidden order-promo-checking">
                	<img src="/public/web/images/Processing1.gif" width="20px" height="20px" />&nbsp;&nbsp;<span>checking...</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row 2 -->
<div class="row">
	<div class="col-xs-12 col-sm-3"></div>
    <div class="col-xs-12 col-sm-9">
		<div class="text-danger" style="display: none;" id="payment-mess">Error create token</div>
	</div>
</div>