<div id="myModalCancel" class="modal fade themodel" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body model-deliver">
				<div class="before-submit" ng-show="!isShow">
                    <form class="deliverform cus-deliverform" ng-submit="cancelRequesting(storage._id)">
                        <h2>Are you sure cancel this Item to your Doostep ?</h2>
                        <p>Current Address</p>
                        <div class="grey-section nopadding">
                            <div class="current-address">@{{ current.addressca }}</div>
                        </div>
                        <p>or change address</p>
                        <div class="grey-section margin1400">
                             <div class="row">
                                <div class="form-group col-xs-4 padding5" id="select-city">
                                	<input type="text" name="address" ng-model="form.addressca" placeholder="New Address" class="form-control">
				                </div>
								<div class="form-group nomarginbot col-xs-3 padding5">
                                    <select class="form-control" name="cityca" id="cityca">
				                       
				                        @foreach($places as $place)
				                        <option value="{{ $place->name }}">{{ $place->name }}</option>
				                        @endforeach
				                    </select>
                                </div>
                                <div class="form-group nomarginbot col-xs-2 padding5">
                                    <select id="zipcodeca" class="form-control" name="zipcodeca" data-init="{{$storage[0]->relationUser->zipcode}}">
                                    	<!-- <option value='@{{ storage.zipcode }}'>@{{ storage.zipcode }}</option> -->
                                    </select>
                                </div>
                                <div class="form-group nomarginbot col-xs-3 padding5">
                                    <input id="form-phoneca" type="text" name="phone" ng-model="form.phoneca" placeholder="(123) 456-7890" class="form-control" maxlength="14">
                                </div>
                             </div>
                        </div>
                        <div class="grey-section margin1400">
                            <div class="form-group row nomarginbot">
                                <div class="col-md-6 col-sm-6 input-table padding5">
                                    <input id="datepickerca" ng-model="form.dateca" type="text" class="form-control w120" placeholder="Date" />
                                    <label for="datepickerca" class="icon green icon-calendar"></label>
                                </div>
                                <div class="col-md-6 col-sm-6 input-table padding5">
                                    <input id="timepickerca" ng-click="processTime('timepickerca')" readonly="readonly" type="text" class="form-control w120" placeholder="Time" />
                                    <input id="timepickercaval" type="hidden" val="" />
		                            <div ng-show="isShowTimeca" class="dropdown-menu bottom order-time">
		                            	<div ng-repeat="foo in availableTimeca" class="@{{ foo.text }}" ng-click="onSelect(foo.time, foo.text, 'timepickerca')">@{{ foo.time[1] }}</div>
		                            </div>
                                    <label for="timepickerca" class="icon green icon-clock"></label>
                                </div>
                            </div>
                        </div>
                        
                        <!-- 
                        <div class="grey-section margin1400" ng-if="form.addressca != ''">
                            <div class="form-group row nomarginbot">
                                <div class="col-sm-6 padding5">
                                	<span class="text-danger">Password Confirmation</span>
                                    <input type="password" class="form-control" ng-model="form.passwordca" placeholder="Password confirm" />
                                </div>
                            </div>
                        </div>
                        -->

                        <!-- Error message -->
                        <div class="row text-danger" ng-show="errormessageca != ''">
                        	<div class="col-xs-12">@{{ errormessageca }}</div>
                        </div>

                        <img class="hidden loading-img" style="float: right;" width="50" height="50" src="{{ asset('web/images/Processing1.gif') }}" />
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-save submit-btn pull-right">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="after-submit" ng-show="isShow">
                    <img src="{{ asset('web/images/section-4-image3.png') }}" />
                    <span>Our Gofers are on the ways.</span>
                </div>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>