<div id="myModalDeliver" class="modal fade themodel" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body model-deliver">
                <div class="before-submit" ng-show="!isShow">
                    <form class="deliverform cus-deliverform" ng-submit="deliver(storagedkey)">
                        <h2>Deliver to</h2>
                        <p>Current Address</p>
                        <div class="grey-section nopadding">
                            <div class="current-address">@{{ current.address }}</div>
                        </div>
                        <p>or change address</p>
                        <div class="grey-section margin1400">
                             <div class="row">
                                <div class="form-group col-xs-4 padding5" id="select-city">
                                	<input type="text" name="address" ng-model="form.address" placeholder="New Address" class="form-control">
				                </div>
								<div class="form-group nomarginbot col-xs-3 padding5">
                                    <select class="form-control" name="city" id="city">
				                        @if ($user->place_id == '')
				                        <option value="">City</option>
				                        @endif
				                        @foreach($places as $place)
				                        <option value="{{ $place->name }}">{{ $place->name }}</option>
				                        @endforeach
				                    </select>
                                </div>
                                <div class="form-group nomarginbot col-xs-2 padding5">
                                    <select id="zipcode" class="form-control" name="zipcode" data-init="{{$user->zipcode}}"></select>
                                </div>
                                <div class="form-group nomarginbot col-xs-3 padding5">
                                    <input id="form-phone" type="text" name="phone" ng-model="form.phone" placeholder="(123) 456-7890" class="form-control" maxlength="14">
                                </div>
                             </div>
                        </div>
                        <div class="grey-section margin1400">
                            <div class="form-group row nomarginbot">
                                <div class="col-md-6 col-sm-6 input-table padding5">
                                    <input id="datepicker" type="text" class="form-control w120" placeholder="Drop-off date" />
                                    <label for="datepicker" class="icon green icon-calendar"></label>
                                </div>
                                <div class="col-md-6 col-sm-6 input-table padding5">
                                    <!-- <input id="timepicker" ng-model="$parent.form.time" type="text" class="form-control w120" placeholder="Time" /> -->
                                    <input id="timepicker" ng-click="processTime('timepicker')" readonly="readonly" type="text" class="form-control w120" placeholder="Drop-off time" />
                                    <input id="timepickerval" type="hidden" value="" />
		                            <div ng-show="isShowTime" class="dropdown-menu bottom order-time">
		                            	<div ng-repeat="foo in availableTime" class="@{{ foo.text }}" ng-click="onSelect(foo.time, foo.text, 'timepicker')">@{{ foo.time[1] }}</div>
		                            </div>
                                    <label for="timepicker" class="icon green icon-clock"></label>
                                </div>
                            </div>
                        </div>
                        <div class="grey-section margin1400" ng-if="form.address != ''">
                            <div class="form-group row nomarginbot">
                                <div class="col-sm-6 padding5">
                                	<span class="text-danger">Password Confirmation</span>
                                    <input type="password" class="form-control" ng-model="form.password" placeholder="Password confirm" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12" id="drop-off-options">
                                <input type="radio" ng-model="form.option" name="drop-pick-option" id="drop-pick-option-1" class="hidden drop-pick-option" value="1" />
                                <label for="drop-pick-option-1" class="drop-pick-option-trigger">Drop-off your stuff while we wait. Our Gofer will wait up to 20 minutes for you to either empty your bin(s) or take a few things out (or add more stuff!) and we'll take 'em back to our warehouse.</label>
                                <br>
                                <input type="radio" ng-model="form.option" name="drop-pick-option" id="drop-pick-option-2" class="hidden drop-pick-option" value="2" />
                                <label for="drop-pick-option-2" class="drop-pick-option-trigger">Need more time? We'll drop them off and come back at the time below to pick them up - full or empty, your choice.</label>
                                <div class="grey-section pickuptime">
                                    <div class="form-group row nomarginbot">
                                        <div class="col-md-6 col-sm-6 input-table padding5">
                                            <input id="datepicker2" type="text" class="form-control w120" placeholder="Pick-up date" />
                                            <label for="datepicker2" class="icon green icon-calendar"></label>
                                        </div>
                                        <div class="col-md-6 col-sm-6 input-table padding5">
                                            <input id="timepicker2" ng-click="processTime('timepicker2')" readonly="readonly" type="text" class="form-control w120" placeholder="Pick-up time" />
                                            <input id="timepicker2val" type="hidden" value="" />
				                            <div ng-show="isShowTime2" class="dropdown-menu bottom order-time">
				                            	<div ng-repeat="foo in availableTime2" class="@{{ foo.text }}" ng-click="onSelect(foo.time, foo.text, 'timepicker2')">@{{ foo.time[1] }}</div>
				                            </div>
                                            <label for="timepicker2" class="icon green icon-clock"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<!-- Error message -->
                        <div class="row text-danger" ng-show="errormessage != ''">
                        	<div class="col-xs-12">@{{ errormessage }}</div>
                        </div>

                        <img class="hidden loading-img" style="float: right;" width="50" height="50" src="{{ asset('web/images/Processing1.gif') }}" />
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-save submit-btn pull-right">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="after-submit" ng-show="isShow">
                    <img src="{{ asset('web/images/section-4-image3.png') }}" />
                    <span>Our Gofers are on their way.</span>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>