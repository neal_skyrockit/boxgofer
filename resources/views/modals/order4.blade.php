<!--row 1-->
<div class="row">
    <div class="col-xs-12 col-sm-3">
        <p class="color fs24">Confirmation</p>
    </div>
    <div class="col-xs-12 col-sm-9">
        <p class="color">Thank you for ordering Storage at Your Doorstep</p>
        <p>Your Appointment Confirmation Number : <span class="color fs18" ng-bind="order.trancode"></span></p>
    </div>
</div>
<!--end row 1-->
<!-- row 1-->
<div class="row">
    <div class="col-xs-12 col-sm-3">
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="grey-section nopadding">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <span class="icon icon-smallpackage"></span>
                    <span class="text" ng-bind="order.large"></span><span> Large</span>
                </div>
                <p class="margin0300 visible-xs col-xs-12"></p>
                <div class="col-xs-12 col-sm-6">
                    <span class="icon icon-bigpackage"></span>
                    <span class="text" ng-bind="order.extlarge"></span><span> Extra Large</span>
                </div>
            </div>
        </div>
        <div class="grey-section nopadding">
            <div class="row">
                <div class="col-xs-12">
                    <span class="icon icon-bicycle"></span>
                    <span class="text" ng-bind="order.otherstuff"></span><span> Other Stuff</span>
                </div>
            </div>
        </div>
        <div class="grey-section nopadding">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <span class="icon icon-calendarV2"></span>
                    <span class="text" ng-bind="order.formatdropoffDate()"></span>
                </div>
                <p class="margin0300 visible-xs col-xs-12"></p>
                <div class="col-xs-12 col-sm-6">
                    <span class="icon icon-clockV2"></span>
                    <span class="text" ng-bind="order.dropoffTimeShow"></span>
                </div>
            </div>
        </div>
        <div class="grey-section nopadding">
            <div class="row">
                <div class="col-xs-12">
                    <span class="icon icon-home"></span>
                    <span class="text" ng-bind="order.addressfull"></span>
                </div>
            </div>
        </div>
        <div class="grey-section nopadding">
            <div class="row">
                <div class="col-xs-12">
                    <span class="icon icon-contact-phone"></span>
                    <span class="text" ng-bind="order.phone"></span>
                </div>
            </div>
        </div>
        <div class="grey-section nopadding">
            <div class="row">
                <div class="col-xs-12">
                    <p style="padding:12px 0 12px 66px;margin:0">Monthly cost :
                        <span class="color fs24 currency" ng-bind="order.totalMonthly()"></span>

                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row 1 -->