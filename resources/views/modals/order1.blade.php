<!-- row 1-->
<div class="row">
    <div class="col-xs-3">
        <p class="color">How many bins do you need?</p>
    </div>
    <div class="col-xs-9">
        <div class="grey-section">
            <div class="row bin-type-container">
                <div class="col-md-12 col-lg-6 nowrap">
                    <div class="form-group">
                        <span class="type-title">Large<span class="description" ng-bind="items.large.price"></span></span>
                        <groupnumber amount="order.large" max="items.max('large')" ></groupnumber>
                        <div class="tooltip-group">
                            <input id="tooltip-large" class="hidden trigger-tooltip" type="checkbox" />
                            <label for="tooltip-large" class="guide" data-tooltip="@{{ removeSlashes(items.large.description) }}"></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 nowrap">
                    <div class="form-group">
                        <span class="type-title">Extra Large<span class="description" ng-bind="items.extlarge.price"></span></span>
                        <groupnumber amount="order.extlarge" max="items.max('extlarge')" ></groupnumber>
                        <div class="tooltip-group">
                            <input id="tooltip-ex-large" class="hidden trigger-tooltip" type="checkbox" />
                            <label for="tooltip-ex-large" class="guide" data-tooltip="@{{ removeSlashes(items.extlarge.description) }}"></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row 1 -->
<!--row 2-->
<div class="row">
    <div class="col-xs-3">
        <p class="color">Have other items to store?</p>
    </div>
    <div class="col-xs-9">
        <div class="grey-section">
            <div class="row bin-type-container">
                <div class="col-md-12 col-lg-6 nowrap">
                    <span class="type-title">Other Stuff<span class="description" ng-bind="items.otherstuff.price"></span></span>
                    <groupnumber amount="order.otherstuff" max="items.max('otherstuff')" ></groupnumber>
                    <!--<div id="Item-OtherStuff" class="number-group">
                        <button class="decrease"></button>
                        <input class="amount" value="0" />
                        <button class="increase"></button>
                    </div>-->
                    <div class="tooltip-group">
                        <input id="tooltip-large-item" class="hidden trigger-tooltip" type="checkbox" />
                        <label for="tooltip-large-item" class="guide" data-tooltip="@{{ removeSlashes(items.otherstuff.description) }}"></label>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 nowrap">
                	<div class="form-group mar-top-8">
                        <input id="form-other-des" class="form-control" placeholder="Other stuff description" ng-model="order.otherDes" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row 2 -->
<!--row 3-->
<div class="row">
    <div class="col-xs-3">
        <p class="color">Total Monthly Price</p>
    </div>
    <div class="col-xs-9">
        <div class="grey-section">
            <div class="row">
                <div class="col-md-12 nowrap">
                    <p class="label-price currency" ng-bind="order.totalMonthly()"></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row 3 -->
@include('modals.order1v2')

