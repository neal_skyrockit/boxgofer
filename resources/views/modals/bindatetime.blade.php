<div id="myModalBin" class="modal fade themodel" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body model-deliver">
            		<h2>Change @{{currentStorageStatus}} Date</h2>
                    <form class="deliverform cus-deliverform" ng-submit="changeStorageDateTime()">
                        
                        <div class="grey-section margin1400">
                            <div class="form-group row nomarginbot">
                                <div class="col-md-6 col-sm-6 input-table padding5">
                                    <input id="datepicker" type="text" class="form-control w120" placeholder="@{{currentStorageStatus}} date" />
                                    <label for="datepicker" class="icon green icon-calendar"></label>
                                </div>
                                <div class="col-md-6 col-sm-6 input-table padding5">
                                    <input id="timepicker" ng-click="processTime('timepicker')" readonly="readonly" type="text" class="form-control w120" placeholder="@{{currentStorageStatus}} time" />
                                    <input id="timepickerval" type="hidden" value="" />
		                            <div ng-show="isShowTime" class="dropdown-menu bottom order-time">
		                            	<div ng-repeat="foo in availableTime" class="@{{ foo.text }}" ng-click="onSelect(foo.time, foo.text, 'timepicker')">@{{ foo.time[1] }}</div>
		                            </div>
                                    <label for="timepicker" class="icon green icon-clock"></label>
                                </div>
                            </div>
                        </div>
                        
						<!-- Error message -->
                        <div class="row text-danger" ng-show="errormessage != ''">
                        	<div class="col-xs-12">@{{ errormessage }}</div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-save submit-btn pull-right">Submit</button>
                            </div>
                        </div>
                    </form>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>