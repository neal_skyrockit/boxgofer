@extends('layouts.web') @section('title') Home @stop

@section('content')

<section id="section-1" class="section">
	<div class="content-container">
		<div class="content">
			<h4>STORAGE AT YOUR DOORSTEP</h4>
			<p>We pick up, store, and bring back your stuff.</p>
			<button onclick="window.location.href='/order'" class="btn">Get started</button>
		</div>
	</div>
</section>
<section id="section-2" class="section">
	<div id="how-it-work"></div>
	<h3 class="title-section">How it Works</h3>
	<div class="in-section">
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<div class="image-circle image-circle-1"></div>
				<p class="text">Free durable bins to your doorstep for you to pack</p>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="image-circle image-circle-2"></div>
				<p class="text">We take it to our secure storage facility.</p>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="image-circle image-circle-3"></div>
				<p class="text">We bring it back whenever you want.</p>
			</div>
		</div>
	</div>
</section>
<section id="section-3" class="section">
	<h3 class="title-section">Simple Pricing</h3>
	<div class="in-section">
		<div class="row">
			<!--<div class="col-md-9">
				<div>
					<img src="{{ asset('web/images/section-3-image1.png') }}" style="max-width: 100%" />
				</div>
			</div>
			<div class="col-md-3">
				<h5>Lorem Ipsum is simply dummy text of the printing and typesetting
					industry. Lorem Ipsum has been the industry's standard dummy text
					ever since the 1500s,</h5>
			</div>-->
            <div class="col-md-4 col-sm-4">
				<div class="image image-1"></div>
				<p class="text">Large: <span class="currency color">{{ $items[0]->price }}/month</span></p>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="image image-2"></div>
				<p class="text">Extra Large: <span class="currency color">{{ $items[1]->price }}/month</span></p>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="image image-3"></div>
				<p class="text">Other Stuff: <span class="currency color">{{ $items[2]->price }}/month</span></p>
			</div>
		</div>
	</div>
</section>
<section id="section-4" class="section">
	<div class="in-section">
		<span class="image1">Barcode Scanned</span> <span class="image2">Fast
			& Free Pickup</span> <span class="image3">Durable Bins</span> <span
			class="image4">Safe & Secure Storage</span> <span class="image5">Attach
			Pictures</span>
	</div>
</section>
<section id="section-5" class="section">
	<div class="content">
		<h3 class="title-section">Now Serving</h3>
	@foreach($places as $place)
		<p>{{ $place->name }}</p>
	@endforeach
	</div>
</section>
<section id="section-6" class="section">
	<div class="content">
		<h5>Schedule storage at your doorstep</h5>
		<a href="/order">Get Started</a>
	</div>
</section>

@stop