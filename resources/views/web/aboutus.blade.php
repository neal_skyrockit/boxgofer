@extends('layouts.web') @section('title') About Us @stop

@section('content')
<div class="text-center" style="margin-top: 120px;"><img style="width: 85%;" src="{{ asset('public/web/images/logo-about-us.jpg') }}"></div>
<div class="container">
	<section class="page static-page">
		<h2 style="margin-bottom: 30px; font-size: 40px;">About Us</h2>
		<div class="text-justify">
			<p>Box Gofer provides an easy, convenient and affordable alternative to brick-and-mortar “traditional” self-storage.  We are ‘Storage at Your Doorstep.’  Created in 2015 by three friends that were tired of renting trailers and hauling stuff to tired, dusty, dark rooms, our goal is to de-clutter your home or office with minimal hassle, stress and sweat.  To do that, we deliver storage bins to your doorstep for you to fill with the stuff that you still need, just not right away.  We’ll come pick up your packed bins when you’re ready, store them in our secure, climate-controlled facility and deliver them back to you at the click of a button.  We’ll even store your other items that won’t fit into our standard size bins, such as bicycles, sports gear, home furnishings and anything else you can think of that is adding to the clutter in your space, subject to some (minor) restrictions.  You can even catalog your items online with your photos and personalized descriptions!</p>
			<p>You no longer have to go through the do-it-yourself self-storage process to de-clutter your space - we can save you over 8 hours of your life when you want to store your stuff – see the difference below!</p>
			<div class="aboutus-title">Your time to de-clutter your home or office:</div>
			<div class="table-responsive border-none">
			  <table class="table aboutus-table" cellspacing="0">
					<tr>
						<th colspan="2" class="border-right-black">Traditional Self Storage</th>
						<th colspan="2"><div class="text-center"><img width="130px" src="{{ asset('public/web/images/logo.png') }}"></div></th>
					</tr>
					<tr>
						<td style="width: 30%;">Call/visit/locate/price self-storage unit</td>
						<td class="border-right-black" style="width: 20%;"><div class="about-label">2 hours</div></td>
						<td style="width: 20%;"><div class="about-label">10 seconds</div></td>
						<td style="width: 30%;">Visit <a href="http://boxgofer.com" target="_blank">www.boxgofer.com</a></td>
					</tr>
					<tr>
						<td>Coordinate move timing and labor<br />(or rent a trailer)
						</td>
						<td class="border-right-black"><div class="about-label">2 hours</div></td>
						<td><div class="about-label">2 minutes</div></td>
						<td>Schedule bin drop-off on website</td>
					</tr>
					<tr>
						<td>Pack your stuff</td>
						<td class="border-right-black"><div class="about-label">1 hour</div></td>
						<td><div class="about-label">1 hour</div></td>
						<td>Pack your stuff</td>
					</tr>
					<tr>
						<td>Load your stuff & deliver to your self-storage unit</td>
						<td class="border-right-black"><div class="about-label">2 hours</div></td>
						<td><div class="about-label">15 minutes</div></td>
						<td>Box Gofer arrives to pick up your bins and other stuff, load into our van and let you get back to your life</td>
					</tr>
					<tr class="tr-bold">
						<td>Time spent to store your stuff</td>
						<td class="border-right-black"><div class="about-label-2">~ 7 hours</div></td>
						<td><div class="about-label-2">~ 1.5 hours</div></td>
						<td></td>
					</tr>
			  </table>
			</div>

			<div class="aboutus-title">Your time to get your stuff back:</div>
			<div class="table-responsive border-none">
			  <table class="table aboutus-table" cellspacing="0">
					<tr>
						<th colspan="2" class="border-right-black">Traditional Self Storage</th>
						<th colspan="2"><div class="text-center"><img width="190px" src="{{ asset('public/web/images/logo.png') }}"></div></th>
					</tr>
					<tr>
						<td style="width: 30%;">Coordinate move timing and labor<br />(or rent a trailer)</td>
						<td class="border-right-black" style="width: 20%;"><div class="about-label">1 hour</div></td>
						<td style="width: 20%;"><div class="about-label">2 minutes</div></td>
						<td style="width: 30%;">Schedule drop-off at your doorstep at<br /><a href="http://boxgofer.com" target="_blank">www.boxgofer.com</a></td>
					</tr>
					<tr>
						<td>Get your stuff and move it back home</td>
						<td class="border-right-black"><div class="about-label">1 hour</div></td>
						<td><div class="about-label">15 minutes</div></td>
						<td>Our Gofers arrive to deliver your bins or other stuff</td>
					</tr>
					<tr>
						<td>Unpack your stuff</td>
						<td class="border-right-black"><div class="about-label">1 hour</div></td>
						<td><div class="about-label">1 hour</div></td>
						<td>Unpack your stuff</td>
					</tr>
					<tr>
						<td>Break down storage boxes and dispose or store in attic/elsewhere</td>
						<td class="border-right-black"><div class="about-label">30 minutes</div></td>
						<td><div class="about-label">2 minutes</div></td>
						<td>Schedule bin pick-up at<br /><a href="http://boxgofer.com" target="_blank">www.boxgofer.com</a></td>
					</tr>
					<tr>
						<td>Cancel self-storage contract via phone or in-person visit</td>
						<td class="border-right-black"><div class="about-label">15 minutes</div></td>
						<td><div class="about-label">5 minutes</div></td>
						<td>Gofer picks up bins</td>
					</tr>
					<tr class="tr-bold">
						<td>Time spent to get your stuff back</td>
						<td class="border-right-black"><div class="about-label-2">~ 4 hours</div></td>
						<td><div class="about-label-2">~ 1.5 hours</div></td>
						<td></td>
					</tr>
			  </table>
			</div>

			<div class="table-responsive border-none">
			  <table class="table aboutus-table" cellspacing="0" style="padding: 20px 0px;">
					<tr>
						<td style="width: 30%;" class="color-green">Total Time Spent</td>
						<td class="border-right-black" style="width: 20%;"><div class="about-label-3">Approximately<br />11 hours</div></td>
						<td style="width: 20%;"><div class="about-label-3">Approximately<br />3 hours</div></td>
						<td style="width: 30%;" class="color-green">Total Time Spent</td>
					</tr>
				</table>
			</div>

			<p style="text-align:center;">Thanks for visiting our website and let us know how we can help simplify your storage needs!</p>
		</div>
	</section>
</div>

@stop