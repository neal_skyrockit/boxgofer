@extends('layouts.web') @section('title') T&C @stop

@section('content')

<div class="container">
	<section class="page static-page static-page-term">
		<div class="text-center"><img src="{{ asset('public/web/images/logo.png') }}"></div>
		<h2>Terms and Conditions of Use</h2>
		<div class="text-justify">

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;These terms and conditions (collectively, the “<u><em>Agreement</em></u>”) constitute a legal agreement

between you and Box Gofer, LLC, a Texas limited liability company (the “<u><em>Company</em></u>”) and govern

all use of the Service or the Software (including this website), or both. If you are using the Service

on behalf of an organization, you represent and warrant that you have the authority to bind such

organization to this Agreement and you agree to be bound by this Agreement on behalf of such

organization. Each reference to “You”, “Your” or other similar pronoun, shall in that case refer to

the business or organization that you represent. By using or receiving any services supplied to

you by the Company (the “<u><em>Service</em></u>” as is further defined below), or downloading, installing or using

this website or any associated software supplied by the Company to use the Service (collectively,

the “<u><em>Software</em></u>”), you hereby expressly acknowledge and agree to be bound by the terms and

conditions of the Agreement, and any future amendments and additions to this Agreement as

published from time to time at <a class="text-primary" href="/terms">www.boxgofer.com/terms</a>.
			</p>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Company reserves the right, in its sole discretion, to modify the terms and conditions

of this Agreement or its policies relating to the Service or Software at any time, effective upon

posting of an updated version of this Agreement on the Service or Software. You are responsible

for regularly reviewing this Agreement. Continued use of the Service or Software after any such

changes shall constitute your consent to such changes.
			</p>

			<h4>The Service</h4>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Company provides pick-up, storage and delivery services in certain designated

geographic areas listed on this website. The Company may contract with third parties to offer

some of its services on its behalf in certain areas. Pricing for different areas may vary, at the

Company’s sole discretion. The Company is under no obligation to deliver your stored contents

to a location other than your original pickup service area (by zip code) or outside of the Company’s

covered service areas, however, the Company will endeavor to accommodate you. Delivery of

your stored goods to a location other than your original pickup service area or within the

Company’s covered service areas will be at your sole cost and will be at a rate determined by the

Company in advance, which will likely be greater (and in some cases materially greater), than the

Company’s standard delivery charge. You understand and agree that the Company may store

your contents in any of its storage locations and is under no obligation to store your property in a

specific location. The Company may also, from time to time, move your bins to another storage

location, at its sole discretion, without your consent and without notice.
			</p>

			<h4>The Service</h4>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><em>Generally.</em></u>&nbsp;&nbsp;&nbsp;&nbsp;The Company will provide you with the storage bins in which you will store

your property. Except for “Other Items”, which must weigh 35 lbs. or less and be able to be carried

by one Company personnel, all of your contents should be placed entirely within the bins. The

bins will remain, at all times, the sole property of the Company and will be leased to you as part

of the Service. You will be charged for use of the Company provided storage bins based on the

size of and number of bins the Company provides to you. In the event that you lose, destroy or

otherwise fail to return any of the Company’s bins, in the same condition as they were provided

to you (normal wear and tear excepted), you will be charged the current retail value of such

unreturned or damaged bins. All bins must be returned to the Company by scheduling a pickup

within 10 days of delivery of such bins to you.
			</p>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><em>Packing Your Bins / Risk of Loss.</em></u>&nbsp;&nbsp;&nbsp;&nbsp;You are solely responsible for packing your bins and

protecting the items you place in the bins. This includes packing your items so that they will not

be damaged during transit and storage. The Company is NOT responsible for the contents of

your bins, except as otherwise expressly set forth in this Agreement. The Company will not pack

or repack your bin. You agree that the Company is not responsible for any damage caused by or

arising from your failure to properly pack your bins. The Company does not and will not maintain

insurance on your bins or their contents. You are solely responsible to insure the contents of your

bins against damage; provided, that the Company will be responsible for damages it causes to

the contents of your bins, up to $250 per bin. Any insurance carried by the Company or you is

for the sole benefit of the party carrying the insurance. Each party waives its right to make any

claim against the other for loss or damage in the event of casualty and will cause its respective

insurance policies to be endorsed so as to waive that right of their respective insurers. You

expressly waive all rights of subrogation that your carrier may have against the Company or its

affiliates or agent. This clause is a specific bargained for condition of this Agreement and the

Company would not have entered into this Agreement without it.
			</p>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><em>Refusal to Accept Bins.</em></u>&nbsp;&nbsp;&nbsp;&nbsp;The Company may refuse to accept or store any bins you provide,

in its sole discretion. Common reasons for refusing your bins are that they are over-packed,

weigh more than 35 pounds (bins should not weigh more than this), appear to be poorly packed

(i.e. failure to use reasonable packing materials), or the Company suspects you have packed

prohibited items in the bins. THE COMPANY RESERVES THE RIGHT TO OPEN AND INSPECT

ANY BIN OR OTHER ITEM YOU PROVIDE IF IT: (i) SUSPECTS THAT THE BINS OR OTHER

ITEMS CONTAIN PROHIBITED ITEMS, (ii) SUSPECTS THAT YOU HAVE OTHERWISE

VIOLATED THIS AGREEMENT, (iii) SUSPECTS CRIMINAL ACTIVITY, (iv) IS REQUIRED BY

COURT ORDER OR OTHER LEGAL DEMAND, or (v) SUSPECTS OR ENCOUNTERS ANY

EMERGENCY SITUATION INVOLVING THE BIN. If the Company receives a search warrant

from a governmental agency, it may, without incurring any liability to you, immediately allow your

bins to be searched, and, if applicable, the contents to be seized. Should the Company receive

a subpoena, or a law officer or governmental agency requests documents or information about

bins or the contents therein, you agree that the Company may provide such information or

documents without incurring liability to you.
			</p>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><em>Personal Identification / Fragile Items.</em></u>&nbsp;&nbsp;&nbsp;&nbsp;While not prohibited, you are strongly advised that

you should not place into the bins, any official personal documents (such as passports, driver

licenses, green cards, visas, birth or marriage certificates, etc.) or anything that contains personally identifiable information, such as date of birth, social security number, mortgage

number, or bank account numbers, or anything that contains information that can be used to steal

someone’s identity or a document that you may urgently need. You understand and agree that if

you store official documents or items containing personally identifiable information in a Company

bin, you and not the Company are fully responsible for any damages caused by the disclosure of

such information.
			</p>

			<p>In addition, you should not store fragile or breakable items in bins. While the Company takes

precautions to treat the bins with due care, the bins will be moved around from time to time and

the contents will be subject to jarring and being bumped and the contents, especially if fragile, will

be subject to damage. Such incidental damage is your sole responsibility. You waive all rights

and claims against the Company should your fragile items break or be damaged during the pick-
up, storage or delivery process.
			</p>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><em>Prohibited Items.</em></u>&nbsp;&nbsp;&nbsp;&nbsp;You are prohibited from storing any of the following items in the bins, or

in connection with the Service:
				<ul>
					<li><p>hazardous materials (including but not limited to any hazardous or toxic chemical,

gas, liquid, substance, material, waste, or any other substance whose storage is or

becomes regulated under any applicable local, state or federal law or regulation);</p></li>
					<li><p>food or other perishable goods;</p></li>
					<li><p>any item that is likely to attract vermin or creates a nuisance;</p></li>
					<li><p>flammable materials, such as gas, kerosene, paint, oil, etc;</p></li>
					<li><p>items that, in the Company’s determination, have a noxious odor;</p></li>
					<li><p>items that endanger the safety and health of people of the environment;</p></li>
					<li><p>explosives, fireworks, or other inherently dangerous materials;</p></li>
					<li><p>animals, pets, or any living creature;</p></li>
					<li><p>any item which is illegal to possess, including, illegal drugs, stolen property;</li>
					<li><p>firearms, ammunition, or any weapon;</p></li>
					<li><p>counterfeit goods; or</p></li>
					<li><p>personal property which would result in the violation of any law or regulation of any

governmental authority, including without limitation, all laws and regulations relating

to hazardous materials, waste disposal and other environmental matters.</p></li>

				</ul>
			</p>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><em>Photos / Inventory.</em></u>&nbsp;&nbsp;&nbsp;&nbsp;The Company recommends that you inventory the contents of each

of your bins and take photos of the contents, so that you are able to track each bin and your

possessions for later use. You will be able to upload any photographs you take to the Company’s

website.
			</p>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><em>Storage.</em></u>&nbsp;&nbsp;&nbsp;&nbsp;The Company intends to store your bins in climate controlled facilities that

maintain an ambient temperature of between 50°F and 90°F, although temperature variations,

including above or below these levels, are possible. The air flow and moisture level however will

not be regulated. The Company will take commercially reasonable steps to protect your items

from the growth of mold or similar microorganisms. However, there is no guarantee that mold or

similar microorganisms may not develop on your property. The Company is not liable for the

growth of mold, or mildew or similar microorganisms on your property. You assume the risk that

mold or similar microorganisms could develop under these circumstances.
			</p>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><em>Pick-up and Delivery.</em></u>&nbsp;&nbsp;&nbsp;&nbsp;It is your responsibility to be home (or at the designated location of

your appointment) to deliver and turn over to the Company your packed bins, or to receive return

of your stored bins, when you request a pick-up or delivery. Fees for late appointment

cancellations and no shows may apply.
			</p>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><em>Ownership Disputes.</em></u>&nbsp;&nbsp;&nbsp;&nbsp;The Company will not knowingly release your bins or their contents

to anyone but you or your representative or agent. However, the Company is not responsible for

adjudicating ownership of the bins or their contents, if a dispute arises. To the extent anyone else

claims that they are the true owner of some or all of your bins or their contents, the Company will

request proof of ownership and other information. As part of submitting information, the claiming

person must agree to indemnify you and the Company for all costs that result from their claim if it

is wrong, and they must agree to offer you the option to arbitrate (without the Company as a party)

to resolve the claim, with the loser to pay the winner’s attorney’s fees and costs.
			</p>

			<h4>User Account / Electronic Communications</h4>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In order to use the Service, you must establish an account through this website. When

you register for an account, you will be required to provide the Company with certain information

about yourself including, without limitation, your name, address, telephone number, credit card

information, e-mail address or other contact information. You agree that the information provided

to the Company is and will be accurate and up-to-date at all times during which you use the

Service. You are solely responsible for your failure to update your account, billing, contact and

other information. When you register, you will be asked to provide a password and certain other

information necessary for you to communicate with the Company and the Company to

communicate with you in connection with the Service and other matters. You are solely

responsible for maintaining the confidentiality of your account and password, as well as any

activities occurring under your account and with your password. If you have reason to believe

that your account is no longer secure, then you agree to immediately notify the Company at

<a class="text-primary" href="mailto:support@boxgofer.com">support@boxgofer.com</a>. You hereby consent to receive communications from the Company

electronically. The Company will communicate with you by e-mail or by posting notices on this or

associated websites. You hereby agree that all agreements, notices, disclosures and other

communications that the Company provides to you electronically satisfy any legal requirement

that such communications be in writing.
			</p>

			<h4>Representations and Warranties</h4>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;By using the Service or the Software, you expressly represent and warrant that you are

legally entitled to enter this Agreement. If you reside in a jurisdiction that restricts the use of the

Service because of age, or restricts the ability to enter into agreements such as this one due to age, you must abide by such age limits and you must not use the Service or the Software. Without

limiting the foregoing, the Service and Software is not available to minors (persons under the age

of 18). By using the Service or the Software, you represent and warrant that you are at least 18

years old and that you have the right, authority and capacity to enter into, perform and be bound

by this Agreement. Your participation in using the Service or Software, or both, is for your sole

use and may not be used for your commercial purposes, to provide services to others, or for a

profit. You may not authorize others to use your account and you may not assign or otherwise

transfer your user account to any other person or entity. When using the Service or the Software,

or both, you agree to comply with all applicable laws from your home nation, the country, state

and city in which you are present while using the Software or Service.
			</p>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You may only access the Service using authorized means. It is your responsibility to check

to ensure you download the correct Software for your device. The Company is not liable if you

do not have a compatible handset or if you have downloaded the wrong version of the Software

for your handset. The Company reserves the right to terminate this Agreement

using the Service or Software with an incompatible or unauthorized device.
			</p>

			<h4>Use Restrictions</h4>

			<p>By using the Service or the Software, or both, you agree that:
				<ul>
					<li><p>You will only use the Service or Software for lawful purposes and you will not use the

Services for sending or storing any unlawful items, materials or for fraudulent or

unlawful purposes.</p></li>
					<li><p>You will not use the Service or Software to cause nuisance, annoyance or

inconvenience.</p></li>
					<li><p>You will not copy, or distribute the Software or other content without written

permission from the Company.</p></li>
					<li><p>You will keep secure and confidential your account password or any identification the

Company provides you which allows access to the Service.</p></li>
					<li><p>You will not try to harm the Service or Software in any way whatsoever, including

using any device, software or routine to interfere or attempt to interfere with the proper

working of the Service or any transaction being conducted on the Service, or with any

other person’s use of the Service.</p></li>
					<li><p>You will report any errors, bugs, unauthorized access methodologies or any breach

of the Company’s intellectual property rights that you uncover in your use of the

Service.</p></li>
					<li><p>You may not use, or cause or enable to be used, any deep-link, page-scrape, robot,

spider or other automatic device, program, algorithm or methodology, or any similar

or equivalent manual process, to access, acquire, copy or monitor any portion of the

Service, content of any of the Company’s websites (including this website), or in any

way reproduce or circumvent the navigational structure or presentation of the Service or this website, to obtain or attempt to obtain any data, materials, documents or

information through any means not purposely made available through the Service.</p></li>
					<li><p>You may not attempt to or gain unauthorized access to any portion or feature of the

Service or the Software, or any other systems or networks connected to the Service

or to any Company server, or to any of the services offered on or through the Service,

by hacking, password mining, or any other illegitimate means.</p></li>
				</ul>
			</p>

			<h4>Intellectual Property Ownership</h4>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Company (and its licensors, where applicable) owns all right, title and interest,

including all related intellectual property rights, in and to the Software, the Service, the content on

the Company’s websites and any suggestions, ideas, enhancement requests, feedback,

recommendations or other information provided by you or any other party relating to the Software

or the Service. This Agreement does not convey to you any rights of ownership in or related to

the Software, any content on the Company’s website or the Service, or any intellectual property

rights owned by the Company. The Company name, the Company logo, and the product names

associated with the Software and Service are trademarks of the Company or third parties, and no

right or license is granted to use them. You hereby grant to the Company, a perpetual, non-
terminable right and license to all information, pictures, diagrams or other data you provide to the

Company, whether through electronic upload or otherwise.
			</p>

			<h4>Privacy</h4>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In order to provide the Services, the Company will collect, use and disclose certain

information from you, including personally identifiable information. That information is subject to

the Company’s Privacy Policy, which is available at this website at <a class="text-primary" href="/privacy">www.boxgofer.com/privacy</a>

and is incorporated by this reference. The Company strongly recommends that you review the

Privacy Policy closely.
			</p>

			<h4>Payment Terms</h4>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><em>Generally.</em></u>&nbsp;&nbsp;&nbsp;&nbsp;You are responsible for all applicable storage fees and other costs and

expenses, which are set forth in this website, in connection with the Service. Storage charges

will begin on the earlier of: (i) the date the Company picks up your loaded bins, or (ii) fourteen

(14) days from the date the empty bins are first delivered to you. You will be billed for storage

and other ancillary costs, if any, on the same date each month, coinciding with the date the

storage charges started (“Monthly Billing Day”), and will continue as long as you use the

Service. The cost of additional bins will be pro-rated for month in which they are delivered to you

until the next following Monthly Billing Day. You will continue to be billed for storage fees until the

bins are picked up by the Company after you unload them and schedule a pick-up. With respect

to pick-up dates, the bins will be charged for the full month, regardless of the pick-up date, with

no refunds or credits given for partial month returns. If you fail to schedule a pick-up of empty

bins within one month after the full bins are delivered to you, you will automatically be charged

$65 for each unreturned bin. Any bins that are returned damaged will be evaluated by the

Company and could result in a charge of up to $65 per bin depending on the extent of the damage. You may keep the bin(s) for any reason, including material damage, and will be charged

the $65 fee for all unreturned bins.
			</p>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Payment for the Service shall be made by credit or debit card, unless otherwise agreed

in advance by the Company. The Company will save or cause to be saved, your credit or debit

card information and use it for all future charges, which will automatically be charged to the saved

card, unless you notify the Company or change your card information or method of payment

through your user account. YOU HEREBY AUTHORIZE THE COMPANY TO CHARGE YOUR

CREDIT OR DEBIT CARD FOR ALL SERVICES PROVIDED TO YOU UNDER THIS

AGREEMENT. The Company reserves the right to determine and change the fee schedule for

Service from time to time, in its sole discretion. Current pricing will be published on this website.

Payments for the Service are non-refundable. By using the Service you are agreeing to pay the

prevailing price for the Service as posted by the Company. The Company, at its sole discretion,

MAY make promotional offers for the Service with different features and different rates to any of

its customers or prospective customers. These promotional offers, unless made to you, shall

have no bearing whatsoever on your agreement to pay the then prevailing and posted fees. Late

fees may be assessed to the extent allowed by applicable law.
			</p>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><em>Failure to Pay / Lien.</em></u>&nbsp;&nbsp;&nbsp;&nbsp;If you fail to pay storage and other fees on time, you will be in default

under this Agreement. If you are in default, the Company may, at its discretion, refuse to pick-up

or deliver your bins and their contents until you have paid all amounts due. YOU

ACKNOWLDEGE AND AGREE THAT BY VIRTUE OF THE BUSINESS RELATIONSHIP

BETWEEN YOU AND THE COMPANY, THE COMPANY HAS A COLLATERAL LIEN ON ALL

PROPERTY STORED BY YOU IN THE BINS. THE LIEN SECURES YOUR PAYMENT OF

OUTSTANDING STORAGE FEES, PICK-UP AND DELIVERY FEES AND ALL OTHER

CHARGES, PRESENT OR FUTURE, IN RELATION TO THE PROPERTY, AND FOR

EXPENSES NECESSARY FOR THE COMPANY’S PRESERVATION, OR EXPENSES

REASONABLY INCURRED IN ITS SALE OR OTHER DISPOSITION AS PERMITTED BY

APPLICABLE LAW. <u><strong>YOU FURTHER ACKNOWLEDGE THAT THE COMPANY MAY ENFORCE

ITS LIEN BY SELLING THE PROPERTY YOU PROVIDE TO IT FOR STORAGE AT A LIEN

SALE IN ACCORDANCE WITH APPLICABLE LAW.</strong></u> THE COMPANY WILL PROVIDE YOU

WITH DUE AND PROPER NOTICE OF THE LIEN SALE PRIOR TO ITS OCCURRENCE. YOU

WILL BE RESPONSIBLE FOR ALL COSTS INCURRED BY THE COMPANY IN CONNECTDION

WITH ITS LIEN FORECLOSURE, PROPERTY SALE AND OTHER EFFORTS TO COLLECT

AMOUNTS DUE FROM YOU. </p>

			<h4>Third Party Interactions</h4>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;During use of the Service and Software, you may enter into correspondence with,

purchase goods and/or services from, or participate in promotions of third party service providers,

advertisers or sponsors showing their goods and/or services through the Service or Software. Any

such activity, and any terms, conditions, warranties or representations associated with such

activity, is solely between you and the applicable third-party. The Company and its licensors shall

have no liability, obligation or responsibility for any such correspondence, purchase, transaction

or promotion between you and any such third-party. The Company does not endorse any sites on

the Internet that are linked through the Service or Software, and in no event shall the Company

or its licensors be responsible for any content, products, services or other materials on or available

from such sites or third party providers. The Company provides the Service to you pursuant to the terms and conditions of this Agreement. You recognize, however, that certain third-party

providers of goods and services may require your agreement to additional or different terms and

conditions prior to your use of or access to such goods or services, and the Company disclaims

any and all responsibility or liability arising from such agreements between you and the third party

providers.
			</p>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Company may rely on third party advertising and marketing supplied through the

Software or Service and other mechanisms to subsidize the Software or Service. By agreeing to

these terms and conditions you agree to receive such advertising and marketing. The Company

may compile and release information regarding you and your use of the Software or Service on

an anonymous basis as part of a customer profile or similar report or analysis. You agree that it

is your responsibility to take reasonable precautions in all actions and interactions with any third

party you interact with through the Service. </p>

			<h4>Indemnification</h4>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You agree to defend, indemnify and hold harmless the Company and its officers, directors,

employees, agents and affiliates (each, an “<u><em>Indemnified Party</em></u>”), from and against any losses,

liabilities, claims, actions, costs, damages, penalties, fines and expenses, including without

limitation attorneys’ fees and expenses that arise from: (i) the contents of your bins; (ii) your

violation of this Agreement or any representation, warranty, or agreements referenced herein, or

any applicable law or regulation; (iii) your access to, use of or alleged use of the Service; (iv) your

violation of any third party right, including, without limitation, any intellectual property right,

publicity, confidentiality, property or privacy right; or (v) any disputes or issues between you and

any third party. The Company shall provide notice to you promptly of any such claim, suit or

proceeding and shall have the right to control the defense of such action, at your expense, in

defending any such claim, suit or proceeding. The Company reserves the right, at your sole

expense, to assume the exclusive defense and control of any matter otherwise subject to

indemnification by you, and in such case, you agree to cooperate with the Company’s defense of

such claim and to reimburse the Indemnified Party for all such costs and expenses.</p>

			<h4>Disclaimer</h4>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;USE OF THE SERVICE AND SOFTWARE IS ENTIRELY AT YOUR OWN RISK.

CHANGES ARE PERIODICALLY MADE TO THE SERVICE AND SOFTWARE AND MAY BE

MADE AT ANY TIME WITHOUT NOTICE TO YOU. EXCEPT AS OTHERWISE EXPRESSLY

STATED IN THIS AGREEMENT, THE SERVICES AND THE SOFTWARE ARE PROVIDED ON

AN “AS IS” BASIS WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,

INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY, FITNESS FOR

A PARTICULAR PURPOSE AND NON-INFRINGEMENT. THE COMPANY MAKES NO

WARRANTIES OR REPRESENTATIONS ABOUT THE ACCURACY OR COMPLETENESS OF

THE CONTENT PROVIDED THROUGH THE SOFTWARE OR THE CONTENT OF ANY

WEBSITES LINKED TO THE SERVICE. THE COMPANY ASSUMES NO LIABILITY OR

RESPONSIBILITY FOR: (i) YOUR FAILURE TO PROPERLY PACK YOUR ITEMS IN OUR BINS,

(ii) FRAGILE, PERISHABLE OR OTHER BREAKABLE ITEMS YOU STORE, (iii) ERRORS,

MISTAKES, OR INACCURACIES OF THE CONTENT OF THIS WEBSITE; (iv) PERSONAL

INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF THE SERVICE OR THE SOFTWARE; OR (v) ANY

UNAUTHORIZED ACCESS TO OR USE OF THE COMPANY’S SECURE SERVERS.</p>

			<h4>Limitation of Liability</h4>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IN NO EVENT WILL THE COMPANY’S AGGREGATE LIABILITY TO YOU FOR ANY

LOSS, LIABILITY, CLAIM, EXPENSE, DAMAGE OR OTHER MATTER EXCEED $250.00 PER

BIN STORED, REGARDLESS OF WHETHER SUCH LIABILITY ARISES UNDER CONTRACT,

TORT, STRICT LIABILITY, NEGLIGENCE OR OTHER LEGAL THEORY. IN NO EVENT WILL

THE COMPANY BE LIABLE TO YOU FOR ANY INDIRECT, PUNITIVE, SPECIAL,

EXEMPLARY, INCIDENTAL, CONSEQUENTIAL OR OTHER DAMAGES OF ANY TYPE OR

KIND (INCLUDING PERSONAL INJURY, LOSS OF DATA, REVENUE, PROFITS, USE OR

OTHER ECONOMIC ADVANTAGE). THE COMPANY SHALL NOT BE LIABLE FOR ANY LOSS,

DAMAGE OR INJURY WHICH MAY BE INCURRED BY YOU, INCLUDING BUT NOT LIMITED

TO LOSS, DAMAGE OR INJURY ARISING OUT OF, OR IN ANY WAY CONNECTED WITH THE

SERVICE OR SOFTWARE, INCLUDING BUT NOT LIMITED TO: (I) THE USE OR INABILITY

TO USE OUR WEBSITE TO ORDER SERVICES, (II) ANY RELIANCE PLACED BY YOU ON

THE COMPLETENESS, ACCURACY OR EXISTENCE OF ANY ADVERTISING, OR (III) AS A

RESULT OF ANY RELATIONSHIP OR TRANSACTION BETWEEN YOU AND ANY THIRD

PARTY SERVICE PROVIDER, ADVERTISER OR SPONSOR WHOSE ADVERTISING,

PRODUCTS OR SERVICES APPEAR ON OUR WEBSITE OR IS REFERRED BY THE

SERVICE OR SOFTWARE, EVEN IF THE COMPANY HAS BEEN PREVIOUSLY ADVISED OF

THE POSSIBILITY OF SUCH DAMAGES. ANY CLAIM YOU BRING AGAINST THE COMPANY

ARISING OUT OF THIS AGREEMENT OR THE SERVICES, FOR ANY REASON, MUST BE

COMMENCED WITHIN <u><strong>ONE</strong></u> YEAR AFTER SUCH CAUSE OF ACTION ACCRUES.

OTHERWISE, SUCH CLAIM IS PERMANENTLY BARRED.</p>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;THE COMPANY MAY INTRODUCE YOU TO THIRD PARTY SERVICE PROVIDERS,

INCLUDING STORAGE PROVIDERS, DELIVERY SERVICES, AND INSURANCE CARRIERS

FOR THE PURPOSES OF PROVIDING THE SERVICE OR OTHER ANCILLARY SERVICES.

THE COMPANY WILL NOT ASSESS THE SUITABILITY, LEGALITY OR ABILITY OF ANY

THIRD PARTY SERVICE OR PRODUCT PROVIDERS AND YOU EXPRESSLY WAIVE AND

RELEASE THE COMPANY FROM ANY AND ALL LIABILITY, CLAIMS OR DAMAGES ARISING

FROM OR IN ANY WAY RELATED TO THE THIRD PARTY SERVICE OR PRODUCT

PROVIDERS. THE COMPANY WILL NOT BE A PARTY TO DISPUTES, NEGOTIATIONS OF

DISPUTES OR SETTLEMENTS BETWEEN YOU AND SUCH THIRD PARTY PROVIDERS.

THE COMPANY CANNOT AND WILL NOT PLAY ANY ROLE IN MANAGING PAYMENTS

BETWEEN YOU AND THE THIRD PARTY PROVIDERS. RESPONSIBILITY FOR THE

DECISIONS YOU MAKE REGARDING SERVICES OFFERED THROUGH THE SERVICE OR

SOFTWARE (WITH ALL ITS IMPLICATIONS) RESTS SOLELY WITH YOU. THE COMPANY

WILL NOT ASSESS THE SUITABILITY, LEGALITY OR ABILITY OF ANY SUCH THIRD

PARTIES AND YOU EXPRESSLY WAIVE AND RELEASE THE COMPANY FROM ANY AND

ALL LIABILITY, CLAIMS, CAUSES OF ACTION, OR DAMAGES ARISING FROM YOUR USE

OF THE SERVICE OR SOFTWARE, OR IN ANY WAY RELATED TO THE THIRD PARTIES

INTRODUCED TO YOU BY THE SOFTWARE OR SERVICE.</p>

			<h4>Dispute Resolution</h4>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You agree that in the event of any dispute regarding the Service or Software, this

Agreement or any relationship with the Company, you will first attempt, in good faith, to resolve

such dispute in an amicable manner by mediation with a mutually acceptable mediator. If unable

to agree upon an acceptable mediator, either you or the Company may request that a mutually

agreed upon mediation service appoint a neutral mediator, and the mediation shall be conducted

under the Commercial Mediation Rules of the mutually acceptable mediation service. Any

disputes remaining unresolved after mediation shall be settled by binding arbitration conducted in

Dallas County, Texas utilizing a mutually agreed arbitrator or arbitration service. The arbitration

shall be conducted under the Commercial Arbitration Rules of the mutually agreed arbitrator or

arbitration service. Both parties shall be entitled in any arbitration to conduct reasonable

discovery, including document production and a reasonable number of depositions not to exceed

five per party. The prevailing party shall be entitled to recover its costs and reasonable attorney’s

fees, as determined by the arbitrator.</p>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You and the Company agree that any claim, action or proceeding arising out of or related

to the Agreement, this Service or the Software must be brought in your individual capacity, and

not as a plaintiff or class member in any purported class, collective, or representative proceeding.

The arbitrator may not consolidate more than one person’s claims, and may not otherwise preside

over any form of a representative, collective, or class proceeding. YOU ACKNOWLEDGE AND

AGREE THAT YOU AND THE COMPANY ARE EACH WAIVING THE RIGHT TO A TRIAL BY

JURY OR TO PARTICIPATE AS A PLAINTIFF OR CLASS MEMBER IN ANY PURPORTED

CLASS ACTION OR REPRESENTATIVE PROCEEDING.</p>

			<h4>Force Majeure</h4>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In no event will the Company or its agents be liable for any failure to comply with this

Agreement, or to provide the Service as set forth in the Company’s website, to the extent that

such failure arises from factors outside the Company’s reasonable control, including due to severe

weather, Internet or other utility failure, act of war, or other unexpected intervening event.</p>

			<h4>Termination</h4>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;At its sole discretion, the Company may modify, discontinue or terminate the Service or

Software, or may modify, suspend or terminate your access to the Software or the Service, for

any reason, or no reason, with or without notice to you and without liability to you or any third

party. In addition to suspending or terminating your access to the Software or the Service, the

Company reserves the right to take appropriate legal action, including without limitation pursuing

civil, criminal or injunctive redress. Even after your right to use the Software is terminated, this

Agreement will remain enforceable against you. You may terminate this Agreement at any time

by ceasing all use of the Service and Software; provided, that you will remain responsible for all

charges, costs, liabilities and other obligations which accrued prior to such termination. All

provisions which by their nature should survive to give effect to those provisions shall survive the

termination of this Agreement.</p>

			<h4>General</h4>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No joint venture, partnership, employment, or agency relationship exists between you, the

Company or any third party provider as a result of this Agreement or use of the Service or

Software. This Agreement is governed by the laws of the State of Texas, without regards to its

conflict of laws principles. The failure of either party to exercise in any respect any right provided

for herein shall not be deemed a waiver of any further rights hereunder. If any provision of this

Agreement is found to be invalid in any court having competent jurisdiction, the invalidity of such

provision shall not affect the validity of the remaining provisions of this Agreement, which shall

remain in full force and effect. Any offer for any product, feature, service or software made on

this Website is void where prohibited. This Agreement is not assignable, transferable or sub-
licensable by you except with the Company’s prior written consent. The Company may transfer,

assign or delegate this Agreement and its rights and obligations without your consent. Both

parties agree that this Agreement is the complete and exclusive statement of the mutual

understanding of the parties and supersedes and cancels all previous written and oral

agreements, communications and other understandings relating to the subject matter of this

Agreement and, except as otherwise provided herein, that all modifications must be in a writing

signed by both parties.</p>

			<h4>Contact Information</h4>

			<p>Box Gofer welcomes your questions or comments regarding the Services, this website or this

Agreement:</p>

			<p>
				Email Address: <a class="text-primary" href="mailto:support@boxgofer.com">support@boxgofer.com</a><br><br>
				Effective Date of Agreement: March 22, 2016
			</p>

		</div>
	</section>
</div>

@stop