@extends('layouts.web') @section('title') FAQ @stop

@section('content')
<div class="container faq-page">
	<section class="page static-page">
		<div class="text-justify">
			<div class="text-center" style="margin: 40px 0px;"><img class="width-100" src="{{ asset('public/web/images/logo-faq.jpg') }}"></div>
			<a data-toggle="collapse" href="#collapse-1" aria-expanded="false" aria-controls="collapse-1">What is Box Gofer?</a><br />
			<p class="collapse mar-bot-10" id="collapse-1">We're a full-service storage company started by some enterprising Texans who wanted to de-clutter our homes and offices without the hassle and headache of traditional self-storage.  Now, you can move and store all of your stuff with a few clicks online - no more rented trailers, dusty storage facilities and digging through boxes to find what you've stored.  We provide a solution so that you can focus your free time on family, friends and fun - i.e., the important things in life.</p><br />

			<a data-toggle="collapse" href="#collapse-2" aria-expanded="false" aria-controls="collapse-2">Why use Box Gofer?</a><br />
			<p class="collapse mar-bot-10" id="collapse-2">We take the stress out of getting the clutter out of your space.  We simply deliver empty bins at your doorstep, you pack them up, we retrieve them and store at our secure, climate-controlled storage facility.  We'll even take your other things that won't fit into one of our bins!</p><br />

			<a data-toggle="collapse" href="#collapse-3" aria-expanded="false" aria-controls="collapse-3">How do I use Box Gofer?</a><br />
			<p class="collapse mar-bot-10" id="collapse-3">It's easy.  Simply go to <a href="http://boxgofer.com" target="_blank">www.boxgofer.com</a> to schedule <i>your</i> storage needs on <i>your</i> schedule.  We'll come to your place with clean, eco-friendly bins with unique barcodes for inventory tracking, allow you to photograph your bins when full and we take them back to our secure, climate controlled facility.  When you're ready for your stuff, go to our website and select the bins you want to have returned to you and the specific date and time that is most convenient for you.  No sweat!</p><br />

			<a data-toggle="collapse" href="#collapse-4" aria-expanded="false" aria-controls="collapse-4">Where is Box Gofer available?</a><br />
			<p class="collapse mar-bot-10" id="collapse-4">Our services are currently available in most parts of North Dallas, Richardson, the Park Cities, Lake Highlands, Garland, Plano and Addison.  We are quickly expanding our service territory and will serve the rest of the Metroplex in the near future, so please sign up and we will let you know when our Gofers are coming to your area!</p><br />

			<a data-toggle="collapse" href="#collapse-5" aria-expanded="false" aria-controls="collapse-5">How does the pricing work?</a><br />
			<div class="collapse mar-bot-10" id="collapse-5" style="padding-left: 45px;">
				<p>$4/mo for a Large bin that measures 25" x15.5" x 11" and holds 2.5 cubic feet of stuff.</p>
				<p>$6/mo for an Extra Large bin that measures 26" x19" x 14" and holds 4.0 cubic feet of stuff.</p>
				<p>$6/mo for Other Stuff - anything that won't fit in a bin and weighs less than 35 lbs. that one Gofer can carry.</p>
				<p>There is a $14.99 fee each time we bring your bins back to you, but no fees to bring you more bins or to pick up the empty bins after you've gotten your stuff back.</p>
			</div><br />

			<a data-toggle="collapse" href="#collapse-6" aria-expanded="false" aria-controls="collapse-6">What can I store?</a><br />
			<div class="collapse mar-bot-10" id="collapse-6">
				<p >Boxes, furniture, bicycles, golf bags, holiday decor, baby clothes, books, appliances, vinyl records and camping gear; pretty much anything except the following:</p>
				<ul>
					<li>Liquids</li>
					<li>Illegal substances</li>
					<li>Perishables</li>
					<li>Hazardous materials</li>
				</ul>
				<p>Basically, if you use common sense you should be fine.  If you're not very good with common sense, please take a look at our Terms and Conditions for a complete list of prohibited items.</p>
			</div><br />

			<a data-toggle="collapse" href="#collapse-7" aria-expanded="false" aria-controls="collapse-7">Do I need to have my items packed before Box Gofer drops off the empty bins?</a><br />
			<div class="collapse mar-bot-10" id="collapse-7">
				<p>Once we drop off the empty bins, you will have up to 2 weeks to pack your items. During this time, you can label and take photos of your packed bins so you know what you've stored in your bins!  After two weeks, we'll send you a reminder to schedule the pickup of your bins and begin billing you the monthly rate for storage.</p>
				<p>To save you time, our moving team can wait up to 20 minutes when dropping off the empty bins and pick up the packed bins before we leave.  If that sounds like the best plan for you, we recommend having your storage items organized so you can easily load them into the bins.</p>
			</div><br />

			<a data-toggle="collapse" href="#collapse-8" aria-expanded="false" aria-controls="collapse-8">Will you wait while I pack/unpack my bins?</a><br />
			<p class="collapse mar-bot-10" id="collapse-8">Yes! Our moving team will wait up to 20 minutes for you to pack and/or unpack your bins. However, due to heavy demand for our Gofers around the Metroplex, we are unable to wait longer than the allotted 20 minutes.</p><br />

			<a data-toggle="collapse" href="#collapse-9" aria-expanded="false" aria-controls="collapse-9">How do I remember what I stored?</a><br />
			<p class="collapse mar-bot-10" id="collapse-9">We provide a platform to catalog photos and descriptions of each bin, all online.  You can refer to them anytime, anywhere as long as you can get access to the website.</p><br />

			<a data-toggle="collapse" href="#collapse-10" aria-expanded="false" aria-controls="collapse-10">How does the online access of stored items work?</a><br />
			<p class="collapse mar-bot-10" id="collapse-10">Once our Gofers have picked up your items and brought them back to our facility, you'll be able to see all of the items that you stored.  Anytime you need something returned, simply log in to your account, click on the item you want returned, and schedule a return delivery date and time.</p><br />

			<a data-toggle="collapse" href="#collapse-11" aria-expanded="false" aria-controls="collapse-11">When will I be billed the first time?</a><br />
			<p class="collapse mar-bot-10" id="collapse-11">We allow you two weeks after we drop off the bins to pack them and schedule your pick up.  Billing begins at the earlier of (i) 14 days after drop-off, or (ii) when the bins are picked up by one of our Gofers.</p><br />

			<a data-toggle="collapse" href="#collapse-12" aria-expanded="false" aria-controls="collapse-12">Do you offer pro-rated billing?</a><br />
			<p class="collapse mar-bot-10" id="collapse-12">No, we will bill for the full month of storage on each monthly anniversary date of your first invoice.</p><br />

			<a data-toggle="collapse" href="#collapse-13" aria-expanded="false" aria-controls="collapse-13">How do I update my billing information?</a><br />
			<p class="collapse mar-bot-10" id="collapse-13">You can view your billing history, update your payment info, and edit your account details by logging in to your account on www.boxgofer.com.</p><br />

			<a data-toggle="collapse" href="#collapse-14" aria-expanded="false" aria-controls="collapse-14">What if I only want a few things delivered out of all the stuff I stored?</a><br />
			<p class="collapse mar-bot-10" id="collapse-14">That's easy - simply pick and choose the bins or Other Stuff you want delivered back to your doorstep and that's what we'll deliver.  You are in complete control of which bins or Other Items you want back and when you want them.  In order to maintain our commitment to your privacy and the security of your stuff, we are unable to open your bins to search for a specific item contained therein.</p><br />

			<a data-toggle="collapse" href="#collapse-15" aria-expanded="false" aria-controls="collapse-15">How long will it take you to bring me bins so that I can get started, or to bring back my stuff from storage when I want it returned?</a><br />
			<p class="collapse mar-bot-10" id="collapse-15">We can usually get there within 24 hours, depending on our availability during your desired delivery windows.  In no event will it take longer than 48 hours to get your items back to you.</p><br />

			<a data-toggle="collapse" href="#collapse-16" aria-expanded="false" aria-controls="collapse-16">Can I come get my stuff?</a><br />
			<p class="collapse mar-bot-10" id="collapse-16">In order to maximize the security of your possessions, we utilize a strict pick-up and drop-off policy to your doorstep by our Gofers.  Rest assured that your stuff is safe in our climate controlled storage and that no other customers have access to your items.</p><br />

			<a data-toggle="collapse" href="#collapse-17" aria-expanded="false" aria-controls="collapse-17">Is my stuff safe?</a><br />
			<p class="collapse mar-bot-10" id="collapse-17">Of course!  Your stuff is our business and we take pride in doing business the right way.  We store all of your items in a secure, monitored facility.  Our facilities are also climate controlled so that your items are not exposed to weather or extreme conditions.  Coverage of $250/bin is included as part of your monthly fee.  Make sure that your stuff qualifies for coverage under our Terms & Conditions (<a href="http://boxgofer.com/terms" target="_blank">www.boxgofer.com/terms</a>).  Follow the rules and we'll take care of your stuff.</p><br />

			<a data-toggle="collapse" href="#collapse-18" aria-expanded="false" aria-controls="collapse-18">Is there a minimum storage term?</a>
			<p class="collapse mar-bot-10" id="collapse-18">Absolutely not.  You can ask for your items to be returned to you at any time.  You will only be charged for the remainder of the monthly billing cycle from the day we come back to retrieve your empty bins.</p><br />
		</div>
	</section>
</div>

@stop