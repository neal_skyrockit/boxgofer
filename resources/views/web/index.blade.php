@extends('layouts.comingsoon')

@section('title') Home @stop

@section('content')
	<div style="text-align: center;">
		<img class="coming-logo" src="{{ asset('web/images/logo.svg') }}" />
		<div class="comming-text">coming soon</div>
	</div>
@stop