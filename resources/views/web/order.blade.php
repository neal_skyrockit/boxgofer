@extends('layouts.web') @section('title') order @stop @section('content')

<!--order page-->
<section class="page" id="page-section" ng-controller="OrderController">
    <div class="order-line-section">
        <div class="container">
            <p class="order-line">
                <label class="step schedule-step" ng-class=" partial == 1 ? 'active' : ''  ">1</label>
                <label class="step review-step" ng-class=" partial == 2 ? 'active' : '' ">2</label>
                <label class="step billing-step" ng-class=" partial == 3 ? 'active' : '' ">3</label>
                <label class="step confirm-step" ng-class=" partial == 5 ? 'active' : '' ">4</label>
            </p>
        </div>
    </div>
    <div class="container">
        <div class="order-view" ng-hide="init==false">
            <div class="partial" ng-class="partial == 1 ? 'current' : ''">
                @include('modals.order1')
            </div>
            <div class="partial" ng-class="partial == 2 ? 'current' : ''">
                @include('modals.order2')
            </div>
            <div class="partial" ng-class="partial == 3 ? 'current' : ''">
                @include('modals.order3')
            </div>
            <div class="partial" ng-class="partial == 5 ? 'current' : ''">
                @include('modals.order4')
            </div>
            <div class="partial" ng-class="init != false ? 'current' : ''">
                <div class="row">
                    <div class="col-xs-12">
                        <p ng-repeat="error in errors" ng-bind="error" class="error"></p>
                    </div>
                </div>
                <!--row 4-->
                <div class="row">
                    <div class="col-xs-12 unprocessing">
                    	<div ng-show="partial == 3" class="checkbox chb-tc">
							<label><input type="checkbox" name="rgaccept" id="orderaccept" class="large-checkbox-size"> Check this box to indicate your agreement to our <a href="/terms" target="_blank" class="color">Terms & Conditions.</a></label>
						</div>
                        <button class="button-next pull-right" ng-click="next()" ng-bind="orderbutton">&nbsp;</button>
                        <button class="button-back pull-right" ng-click="back()" ng-hide="partial == 5 || partial == 1">Back</button>
                        <!-- <a href="/refer" class="pull-right color refer-text" ng-hide="partial != 4">Refer a Friend and Receive a Month Free</a> -->
                    </div>
                    <!-- <div class="col-xs-12 ta-r isprocessing">
                        <img src="{{asset('web/images/Processing1.gif')}}" height="60" />
                    </div> -->
                </div>
                <!-- end row 4 -->
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript" src="https://checkout.stripe.com/checkout.js"></script>
<script type="text/javascript">

	// Client key test
	Stripe.setPublishableKey('pk_test_4OWhoXR8ddGoJtTBen4khKkB');
    //Stripe.setPublishableKey('pk_test_wfI6Z7YHKUVxtYIlA6UJLvLV');
    //Stripe.setPublishableKey('pk_live_SLs496thS6hqT23llFfB2tiz');

    var disableDate = {!! json_encode($disable_date) !!};
    $(function() {
        $('#datepicker').datetimepicker({
        	useCurrent: false,
            minDate: moment().add(1, 'd').format('MM/DD/YYYY'),
            //defaultDate: moment().add(1, 'd').format('MM/DD/YYYY'),
            format: 'MM/DD/YYYY',
            daysOfWeekDisabled: [0],
            disabledDates: disableDate
        });
    });
</script>
<!--end order page-->
@stop