@extends('layouts.web') @section('title') Privacy Policy @stop

@section('content')

<div class="container">
	<section class="page static-page">
		<div class="text-center"><img src="{{ asset('public/web/images/logo.png') }}"></div>
		<h2>Privacy Policy</h2>
		<div class="text-justify">

			<h4>Introduction</h4>

			<p>This Privacy Policy addresses the collection, storage and use of information that you provide to

Box Gofer, LLC (the “<u><em>Company</em></u>”), in connection with your use of its storage services (the

“<u><em>Service</em></u>”). The Company is committed to protecting the privacy of its customers and is

dedicated to ensuring that the information you submit to the Company through this website

(<a class="text-primary" href="/">www.boxgofer.com</a>), remains private and

to you in a safe and user-friendly manner.
			</p>

			<h4>What this Privacy Policy Covers</h4>

			<p>This policy covers how the Company treats personal information that it receives, collects and

stores, including information related to your use of the Service. Personal information is

information about you that is personally identifiable like your name, address, email address,

phone number, driver's license number, certain credit card information, contents of your stored

bins, photographs you provide, and other personal information provided to us in connection with

your use of this website and the Service, and that is not otherwise publicly available.
			</p>

			<p>This policy applies to the Company, but does not apply to any third parties who you elect to

receive services from in connection with the Service, such as those third party advertisers on

the Company’s website. Such other companies or persons may have their own privacy policy,

which the Company encourage you to read and understand.
			</p>

			<h4>Information Collection and Use</h4>

			<p>The Company collects personal information when you set up an account and use this website,

including your name, email address, birth date, physical addresses (including pick-up and

delivery addresses), driver's license, and certain other information necessary for the Company

to provide the Service to you. The information will be used to drop-off and pick-up and storage

bins, as well as for billing and to notify you of certain other events, dates, promotions and

information about the Service and your stored bins.
			</p>

			<h4>Information Sharing and Disclosure</h4>

			<p>The Company does not sell, share, license or provide personal information about you to any

other people or nonaffiliated companies other than on an anonymous basis, to certain services

providers of the Company who assist in optimizing the Service and the website. In connection

with the Company’s provision of your information in that case, note that:
			<ul>
				<li><p><strong>(a)</strong> The Company will provide the information to trusted partners who work on

the Company’s behalf and are under confidentiality agreements. These companies or

contractors may use your personal information to help the Company provide the Service

to you.</p>
				</li>
				<li><p><strong>(b)</strong> The Company will respond to subpoenas, court orders or legal process, or to

establish or exercise the Company’s legal rights or defend against legal claims. The

Company believes it is necessary to share information in order to investigate, prevent or

take action regarding illegal activities, suspected fraud, situations involving potential

threats to the physical safety of any person, or as otherwise required by law.</p>
				</li>
				<li><p><strong>(c)</strong> The Company will transfer information about you if it is acquired by or

merged with another company. In that event, the Company will post appropriate

notifications on this privacy policy before your information is transferred and becomes

subject to a different privacy policy.</p>
				</li>
			</ul>
			</p>

			<h4>Confidentiality and Security</h4>

			<p>The Company will maintain the confidentiality of the information that it collects during your visits

to its website. The Company does this by maintaining internal practices that help to protect the

security and confidentiality of this information and by limiting access to personal information

about you to employees who the Company reasonably believes need to come into contact with

that information to provide the Service to you or in order to do their jobs.
			</p>

			<h4>Cookies</h4>

			<p>As is common practice with websites, the Company uses cookies to enhance your use of the

Company’s website and to better provide the Service to you. A "cookie" is a small text file that a

website can place on your computer's hard drive in order, for example, to collect information

about your activities on the site or to recognize your computer in the future. The cookie

transmits this information back

only computers that can read it.
			</p>

			<p>When you log onto the Company’s website, the website assigns you a unique identification

number and records that number in the cookie file for your computer. Then, when you visit the

website again, the Company is able to read this number to help gain prompt access to your

information. The cookie can help ensure that your information is kept accurate and also, as a

matter of convenience, keeps you from having to re-enter certain personal and identifying

information. Despite its existence, the cookie will not be associated with any personal

information about you and the Company will not be able to identify you personally online. The

information stored

encryption key.
			</p>

			<p>While the Company believes that certain cookies enhance your experience with its website, you

have the option of setting your web browser to reject cookies. However, doing so may

negatively impact the performance of this website.
			</p>

			<h4>Changes to this Privacy Policy</h4>

			<p>The Company may, from time to time, update this policy. The Company will post on this policy

page, the date and content of any significant changes in the way the Company treat personal

information.
			</p>

			<h4>Disclaimer</h4>

			<p>The security of all information associated with the Company’s customers is an important

concern to it. As stated above, the Company exercises due care in providing secure

transmission of your information from your computer to the Company’s systems, and its

affiliates', servers, if applicable. Unfortunately, no data transmission over the Internet can be

guaranteed to be 100% secure. As a result, while the Company strives to protect your personal

information, the Company cannot ensure or warrant the security of any personal information you

transmit to it or that it transfers to you, and you do so at your own risk. Once the Company

receives your transmission, the Company uses industry standard efforts to safeguard the

confidentiality of your personal information, such as firewalls and secure socket layers.

However, "perfect security" does not exist on the Internet.
			</p>

			<h4>Acceptance of Privacy Policy Terms</h4>

			<p>By using this website, you acknowledge your agreement with the Company's privacy policy. IF

YOU DO NOT AGREE WITH THIS POLICY, PLEASE DO NOT USE THIS WEBSITE. Your

continued use of the Company's website and Service following the posting of changes to these

terms will mean you accept those changes.
			</p>

			<h4>General</h4>

			<p>This privacy policy is governed by the internal substantive laws of the State of Texas, without

regard to its conflicts of law principles. Jurisdiction for any claims arising under the terms and

conditions of this privacy policy shall lie exclusively with the state and federal courts of the State

of Texas, in the County of Dallas. If any provision of this privacy policy is found to be invalid by

a court having competent jurisdiction, the invalidity of such provision shall not affect the validity

of the remaining provisions of this privacy policy, which shall remain in full force and effect.
			</p>

		</div>
	</section>
</div>

@stop