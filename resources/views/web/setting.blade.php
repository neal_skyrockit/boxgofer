@extends('layouts.web') @section('title') setting @stop

@section('content')

<div class="container" ng-controller="SettingController">
    <!--page personal information-->
    <section class="page page-account" id="page-per-info">
        <h2>Account Settings</h2>
        <div class="route-acc">
            <a href="#" class="a-perInfo active">Personal Information</a>
            <a href="/history" class="a-history">History</a>
        </div>

        @if ($errors->has())
	        @foreach ($errors->all() as $error)
	        <div class='bg-danger alert'>{{ $error }}</div>
	        @endforeach
        @endif

        <form class="frm-eidtprofile" method="post" action="/settings">
            {{ csrf_field() }}
            <div class="form-group">
                <input type="text" name="name" class="form-control" id="name" value="{{ $user->name }}" placeholder="Name" />
            </div>
            <div class="form-group">
                <input type="email" name="email" class="form-control" id="email" value="{{ $user->email }}" placeholder="Email">
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" id="password" value="" placeholder="Password">
            </div>
            <div class="form-group">
                <input type="text" name="phone" class="form-control" id="setting-phone" value="{{ $user->phone }}" placeholder="Phone (123) 456-7890" maxlength="14">
            </div>
            <div class="form-group">
                <input type="text" name="address" class="form-control" id="address" value="{{ $user->physicalAddress }}" placeholder="Street Address">
            </div>
            <div class="row">
                <div class="form-group col-xs-6" id="select-city">
                    <select class="form-control" name="city" id="city">
                        @if ($user->place_id == '')
                        <option value="">City</option>
                        @endif
                        @foreach($places as $place)
                        <option value="{{ $place->name }}">{{ $place->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-xs-6" id="select-zipcode" >
                    <select id="zipcode" class="form-control" name="zipcode" data-init="{{$user->zipcode}}"></select>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-save">Save change</button>
            </div>
        </form>
    </section>
    <!--end page-->
</div>
<script>
    var listCityXZipcodes = {};
    var listZipcodeXCity = {};
    var initZipcode = {{$user->zipcode}};
    city.onchange = function(){
        var curCity = city.value;
        if(curCity === ""){
            zipcode.innerHTML = "";
            return;
        }
        var options = function(){
            var retVal = "";
            listCityXZipcodes[curCity].forEach(function(obj){
                if(obj==initZipcode)
                    retVal += "<option value="+obj+" selected>"+obj+"</option>";
                else
                    retVal += "<option value="+obj+" >"+obj+"</option>";


            });
            return retVal;
        }

        zipcode.innerHTML = options();
    }

    function setZipcodeXCity(c,zipcodes){
        zipcodes.forEach(function(obj){
            listZipcodeXCity['z'+obj] = c;
        });
    }

    function init(){
        var refCity = listZipcodeXCity['z'+initZipcode];
        if(refCity === undefined
        || refCity === null ){
            return;
        }
        city.value = refCity;
        city.onchange();
    }


     @foreach($places as $place)
        listCityXZipcodes['{{$place->name}}'] = <?php echo json_encode($place->zipcodes); ?> ;
        setZipcodeXCity('{{$place->name}}',<?php echo json_encode($place->zipcodes); ?>);
     @endforeach



    init();
</script>

@stop