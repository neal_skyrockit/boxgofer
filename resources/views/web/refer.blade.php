@extends('layouts.web') @section('title') zipcode @stop

@section('content')

<div class="container">
    <!--page personal information-->
    <section class="page" id="page-referFriend" ng-controller="ReferController">
        <h2>Refer a Friend</h2>
        <div class="referFriend-inner">
            <form class="frm-referFriend" action="">
                <div class="form-group">
                    <!-- <label class="col-md-4 col-sm-4 tx-center" for="email">Get $1 off your next month’s rent for every bin ordered by a friend</label> -->
                    <p class="col-md-10">Get $1 off your next month’s rent for every bin ordered by a friend</p>
                    <div class="col-md-7 col-sm-7">
                        <div class="form-group">
                            <input class="form-control" id="email" placeholder="Your friend email" ng-model="referEmail" />
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <img src="{{asset('web/images/Processing1.gif')}}" height="60" ng-show="processing == true" />
                        <button type="submit" class="btn btn-save" ng-click="share()" ng-show="processing != true">share</button>
                    </div>
                </div>
            </form>
            <p class="error" ng-repeat="error in errors" ng-bind="error"></p>
            <!-- <div ng-show="success == true">
                <h2>Successful Referrals</h2>
                <p class="referFriend-result">You have no successful referrals</p>
            </div> -->
        </div>
    </section>
    <!--end page-->
</div>

@stop