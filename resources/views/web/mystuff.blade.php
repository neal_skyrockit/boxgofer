@extends('layouts.web') @section('title') My Stuff @stop @section('content')

<div class="container" ng-controller="MystuffController">

    <!--page dashboard my stuff-->
    <section class="page" id="page-dashmystuff">
    @if(count($storage) > 0)
        <h2>{{ $username }}'s Items</h2>
    @else
        <h2>Welcome {{ $username }}</h2>
    @endif
        <div class="row">

        	<div class="col-xs-4 ta-c">
                <a class="icon-dash"><div class="image-helper-middle"></div><img class="stuff-car-icon" src="{{ asset('web/images/icon-transit.png') }}"> </a>
                <p class="status">{{ $transit }} Item{{ ($transit > 1) ? 's' : '' }} In Transit</p>
            </div>

            <div class="col-xs-4 ta-c">
            	<a class="icon-dash"><div class="image-helper-middle"></div><img src="{{ asset('web/images/dash-home.png') }}"> </a>
                <p class="status">{{ $doorstep }} Item{{ ($doorstep > 1) ? 's' : '' }} at Your Doorstep</p>
            </div>

            <div class="col-xs-4 ta-c">
                <a class="icon-dash"><div class="image-helper-middle"></div><img src="{{ asset('web/images/icon-storage.png') }}"> </a>
                <p class="status">{{ $instorage }} Item{{ ($instorage > 1) ? 's' : '' }} in Storage</p>
            </div>

        </div>
        <div class="wrap-dash-content">

            @if(count($storage) > 0)

            <div class="wrap-dash-top">
                <h3>Your items {{ $instorage }}</h3>
                <!-- <a href="/order" class="btn btn-order">Order Bins</a> -->
                <a href="{{ url('/zipcode') }}" class="btn btn-order">Order Bins</a>
            </div>
            <div class="wrap-dashmystuff-inner">

                @if ($errors->has())
	                @foreach ($errors->all() as $error)
	                <div class='bg-danger alert'>{{ $error }}</div>
	                @endforeach
                @endif

                @if (session('success'))
                <div class="alert bg-success">{{ session('success') }}</div>
                @endif

                <!-- Start -->
                @foreach($storage as $foo)
                <div class="item-mystuff row">
                    <div class="col-xs-4 col-sm-3 col-md-5 item-mystuff-inner">
                        <div class="item-mystuff-desc-group">
                            <!-- <input type="checkbox" id="item-mystuff-desc-editor-trigger-{{$foo->_id}}" class="item-mystuff-desc-editor-trigger hidden" /> -->

                            <div class="item-mystuff-desc-text desc-text-{{ $foo->_id }}">Enter bin contents text here</div>
                            <div class="item-mystuff-desc-content desc-content-{{ $foo->_id }}">{{ $foo->content }}</div>
                            <div class="item-mystuff-desc-editor desc-editor-{{ $foo->_id }}">
	                           	<form method="post" ng-submit="changeContent('{{ $foo->_id }}')">
		                           <textarea style="width: 100%; margin-bottom: 5px;" rows="2" cols="10" class="form-control desc-input" id="pwd_{{ $foo->_id }}" placeholder="Enter bin contents text here">{{ $foo->content }}</textarea>
	                               <button class="btn bg-color button-save">Save</button>
	                               <span class="btn bg-color-grey label-cancel" data-id="{{ $foo->_id }}">Cancel</span>
		                        </form>
	                        </div>

                            <div class="col-md-1 item-mystuff-show-desc show-desc-{{ $foo->_id }}" data-id="{{ $foo->_id }}"><img src="{{asset('public/web/images/icon-edit.png')}}" /></div>
                            <div class="col-md-10 item-mystuff-show-content show-content-{{ $foo->_id }}" style="width: 150px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis">{{ $foo->content }}</div>
                                <!-- <label for="item-mystuff-desc-editor-trigger-{{$foo->_id}}"><img src="{{asset('public/web/images/icon-edit.png')}}" /></label>
                                <span title="{{ $foo->content }}" class="content_{{ $foo->_id }}">{{ $foo->content }}</span> -->

                        </div>
                        <div class="item-mystuff-barcode-group">
                            <div class="item-his-barcode">
                                <img class="barcode-image" src="{{ CommonHelper::generateBarcode($foo->barcode) }}" />
                                <!-- <span class="">{{$foo->barcode}}</span> -->
                            </div>
							<!-- <span>{{$foo->transactions->transcode}}</span> -->

                            <div class="clearfix"></div>
                        </div>
                        <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						    <div class="modal-dialog">
							    <div class="modal-content">
							        <div class="modal-body">
							      	  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							          <img src="" class="imagepreview" style="width: 100%;" >
							        </div>
							    </div>
							</div>
						</div>

                    </div>
                    <div class="col-xs-8 col-md-7 col-sm-8 item-mystuff-deliver">
                    		<div class="row">
                    			<div class="col-xs-6 col-md-7">
		                            <form id="frm-uploadimg" class="add-img " method="post" action="/uploadImages" enctype="multipart/form-data">
	                                    <div class="item-his-group">
			                                <div class="item-his-popup-container">
			                                    <div class="item-his-popup">
			                                        @foreach($foo->images as $img)
			                                            <div>
			                                                <img class="upimage" src="{{ asset('public/uploads/' . $img) }}" /><img ng-click="deleteimage('{{ $foo->_id }}', '{{ $img }}');" class="delimage" width="20" height="20" src="{{ asset('public/web/images/imgpsh_fullsize.png') }}" />
			                                            </div>
			                                        @endforeach
			                                    </div>
			                                </div>
			                            </div>
	                                    <span class="">
	                                        <label for="uploadimg{{ $foo->_id }}">
	                                            <span class="btn btn-add-img"></span>
	                                            </label>
	                                        <input id="uploadimg{{ $foo->_id }}" data-id="{{ $foo->_id }}" class="hidden myInput myuploadimg" type="file" name="image">
	                                        <input type="text" class="hidden" name="storage" value="{{ $foo->_id }}" />
	                                    </span>
	                                    <span class="save-img save_{{ $foo->_id }}">
	                                        <input type="submit" class="btn btn-save" id="btn-save-img" value="Upload" name="submit" />
	                                    </span>
	                                    <div class="word-wrap">
	                                        <p class="name-img name_{{ $foo->_id }}"></p>
	                                    </div>
			                        </form>
                    			</div>
                    			<div class="col-xs-6 col-md-5 pad-l-r-5">
                    				<p>
			                            <button class="btn btn-deliver act-deliver" data-id="{{ $foo->_id }}">Deliver <span class="hidden-991">to My Doorstep</span></button>
			                        </p>


			                        <!-- chuong: Comment for now because it access by scan IT4
			                        <p>
			                            <button class="btn btn-deliver act-cancel" data-id="{{ $foo->_id }}">Cancel <span  class="hidden-991">this item</span></button>
			                        </p>
			                        -->
                    			</div>
                    		</div>
                    </div>
                </div>
                @endforeach
                <!-- End -->

            </div>
            @if(count($storage) > 1)
            <div class="deliver-all">
                <p>
                    <button class="btn btn-deliver act-deliver" data-id="All" data-address="No Address">Deliver All Items to My Doorstep</button>
                </p>


                <!-- Chuong: Comment for now because it access by scan IT4
                <p>
                    <button class="btn btn-deliver act-cancel" data-id="{{ $storage[0]->_id }}" data-address="No Address">Cancel All Items</button>
                </p>
                -->
            </div>
            @endif

            <div>
				{!! $storage->render() !!}
            </div>

            <input type="hidden" id="cuskey" value="{{ $cuskey }}" />

        </div>

        @else

        <div class="wrap-dash-inner">
            <img src="{{ asset('web/images/section-2-image1.jpg') }}">
            <!-- <a href="/order" class="btn btn-order">Order Bins</a> -->
            <a href="{{ url('/zipcode') }}" class="btn btn-order">Order Bins</a>
        </div>

        @endif

        @include('modals.deliver')
        @include('modals.cancelbin')
</div>

</section>
<!--end page-->
</div>

<script type="text/javascript">
    $(function() {
    	var disableDate = {!! json_encode($disable_date) !!};
    	$('#datepicker').datetimepicker({
    		useCurrent: false,
            minDate: moment().add(1, 'd').format('MM/DD/YYYY'),
            defaultDate: moment().add(1, 'd').format('MM/DD/YYYY'),
            format: 'MM/DD/YYYY',
            daysOfWeekDisabled: [0],
            disabledDates: disableDate
        });
        $('#datepicker2').datetimepicker({
        	useCurrent: false,
            minDate: moment().add(1, 'd').format('MM/DD/YYYY'),
            defaultDate: moment().add(1, 'd').format('MM/DD/YYYY'),
            format: 'MM/DD/YYYY',
            daysOfWeekDisabled: [0],
            disabledDates: disableDate
        });
		// Use for cancel
        $('#datepickerca').datetimepicker({
        	useCurrent: false,
            minDate: moment().add(1, 'd').format('MM/DD/YYYY'),
            defaultDate: moment().add(1, 'd').format('MM/DD/YYYY'),
            format: 'MM/DD/YYYY',
            daysOfWeekDisabled: [0],
            disabledDates: disableDate
        });
    });

    var listCityXZipcodes = {};
    var listZipcodeXCity = {};
    var addressFull = {};
    var initZipcode = {{$user->zipcode}};
    city.onchange = function(){
        var curCity = city.value;
        if(curCity === ""){
            zipcode.innerHTML = "";
            return;
        }
        var options = function(){
            var retVal = "";
            listCityXZipcodes[curCity].forEach(function(obj){
                if(obj==initZipcode)
                    retVal += "<option value="+obj+" selected>"+obj+"</option>";
                else
                    retVal += "<option value="+obj+" >"+obj+"</option>";
            });
            return retVal;
        }

        zipcode.innerHTML = options();
    }
	//
    cityca.onchange = function(){
        var curCity = cityca.value;
        if(curCity === ""){
            zipcode.innerHTML = "";
            return;
        }
        var options = function(){
            var retVal = "";
            listCityXZipcodes[curCity].forEach(function(obj){
                if(obj==initZipcode)
                    retVal += "<option value="+obj+" selected>"+obj+"</option>";
                else
                    retVal += "<option value="+obj+" >"+obj+"</option>";
            });
            return retVal;
        }

        zipcodeca.innerHTML = options();
    }

    function setZipcodeXCity(c,zipcodes){
        zipcodes.forEach(function(obj){
            listZipcodeXCity['z'+obj] = c;
        });
    }

    function setAddressFull(foo) {
        Object.keys(foo).forEach(function(key) {
        	addressFull[key] = foo[key];
        });
    }

    function init(){
        var refCity = listZipcodeXCity['z'+initZipcode];
        if(refCity === undefined
        || refCity === null ){
            return;
        }
        city.value = refCity;
        city.onchange();
        cityca.value = refCity;
        cityca.onchange();
    }

     @foreach($places as $place)
        listCityXZipcodes['{{$place->name}}'] = <?php echo json_encode($place->zipcodes); ?> ;
        setZipcodeXCity('{{$place->name}}',<?php echo json_encode($place->zipcodes); ?>);
        setAddressFull(<?php echo json_encode($place->full); ?>);
     @endforeach

    init();
</script>

@stop