@extends('layouts.web') @section('title') Login @stop

@section('content')

<div class="container" ng-controller="LoginController">
	<!--login page-->
	<section id="page-login">

		<!--create account section-->
		<div class="section col-md-6">

			@if ($errors->has())
		        @foreach ($errors->all() as $error)
		            <div class='bg-danger alert'>{{ $error }}</div>
		        @endforeach
		    @endif

			<form class="frm-login" method="post" action="/login">
				<h3 class="">LOGIN</h3>
				<div class="form-group">
					<input type="email" name="email" class="form-control" id="email" placeholder="Email">
				</div>
				<div class="form-group">
					<input type="@{{ isshowpass ? 'text' : 'password' }}" name="password" class="form-control" id="password" placeholder="Password">
				</div>
				<div class="checkbox">
					<label><input type="checkbox" ng-model="isshowpass"> Show Password</label>
					<a data-toggle="modal" data-target="#myModalForgotpw" class="pull-right">Forgot Password?</a>
				</div>
				<div class="form-group">
					<button type="submit" class="btn button-signup">Login</button>
				</div>
				<div class="form-group">
					<p class="txt1">
						Register a new account with BoxGofer? <a href="/zipcode" class="color">Register</a>
					</p>
				</div>
				<div class="form-group">
					<a href="/fb/login" class="btn button-singup-facebook">Login with Facebook</a>
				</div>
			</form>

		</div>
		<!--description section-->
		<div class="section col-md-6">
			<div class="page-login-description">
				<div class="row row-cus">
					<div class="content content-1">
						<h3>Storage at Your Doorstep</h3>
						<p>We’ll deliver eco-friendly bins to your doorstep. Simply fill them up and schedule a pick-up. When you’re ready to get them back, let us know which ones you want returned to you and they’ll be back at your doorstep within 48 hours.</p>
					</div>
				</div>
				<div class="row row-cus">
					<div class="content content-2">
						<h3>Pick-up and Delivery</h3>
						<p>When you schedule service, we’ll bring you empty bins for you to fill up, we’ll come back to pick them up and take them to our secure warehouse at no additional cost to you. We’ll charge a flat fee of $14.99 when you schedule the full bins to be returned to your doorstep – for as many bins as you want to have delivered for that one low price.</p>
					</div>
				</div>
				<div class="row row-cus">
					<div class="content content-3">
						<h3>Secure, Climate-Controlled Storage</h3>
						<p>We’ll store all of your items in our secured climate-controlled facility which is monitored 24/7. We protect your stuff as we would our own belongings, so that it is returned to you exactly as it was when our Gofers picked it up.</p>
					</div>
				</div>
			</div>
		</div>

		@include('modals.forgotpw')

	</section>
	<!--end login page-->
</div>

@stop