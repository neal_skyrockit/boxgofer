@extends('layouts.web') @section('title') zipcode @stop

@section('content')

<div class="container">
	<section id="page-yourzipcode" ng-controller="ZipcodeController">
		@if ($errors->has())
	        @foreach ($errors->all() as $error)
	            <div class='bg-danger alert text-center'>{{ $error }}</div>
	        @endforeach
	    @elseif($message != '')
	    	<div class='bg-success alert text-center'>{{ $message }}</div>
	    	<div class="zipcodesection text-center"><a href="/login">Go to login</a></div>
	    @endif

	    @if($message == '')
			<form class="frm-yourzipcode" action="/resetpassword?token={{ $token }}" method="post">
				<h3>ENTER YOUR NEW PASSWORD</h3>
				<div class="zipcodesection">
					<div class="form-group">
						<div class="part col-xs-12">
							<div class="form-group">
							<input type="password" name="newpass" class="form-control bor-color" placeholder="New password" />
							</div>
						</div>
						<div class="part col-xs-12">
						<div class="form-group">
							<input type="password" name="confpass" class="form-control bor-color" placeholder="Confirm new password" />
							</div>
						</div>
						<div class="part col-xs-12">
						<div class="form-group">
							<input type="submit" class="btn bg-color" value="Submit" name="resetpw" />
							</div>
						</div>
					</div>
				</div>
			</form>
		@endif

	</section>
</div>

@stop