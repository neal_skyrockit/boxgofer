@extends('layouts.web') @section('title') zipcode @stop

@section('content')

<div class="container">
	<section id="page-yourzipcode" ng-controller="ZipcodeController">
		<form class="frm-yourzipcode">
			<h3>ENTER YOUR ZIPCODE</h3>
			<div class="zipcodesection">
				<div class="row">
					<div class="part col-xs-3">
						<label>Zip Code</label>
					</div>
					<div class="part col-xs-5">
						<input type="text" class="form-control bor-color" ng-model="zipcode" placeholder="Zip code" />
					</div>
					<div class="part col-xs-4">
						<button class="btn bg-color" ng-click="checkZipCode('{{ $token }}')">Submit</button>
					</div>
				</div>
			</div>
			<div class="supportsection" ng-hide="!placesuggest">
				<p>Our Gofer's aren't serving your home yet, but please enter your
					e-mail below and we will notify you when our Gofer's are heading
					your way.</p>
				<div class="row">
					<div class="part col-xs-8">
						<input ng-model="suggestemail" class="form-control bor-color" placeholder="Email" />
					</div>
					<div class="part col-xs-4">
						<button class="btn bg-color" ng-click="sendSuggestion()">Send</button>
					</div>
				</div>
			</div>
		</form>

	</section>
</div>

@stop