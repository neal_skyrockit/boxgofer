@extends('layouts.web') @section('title') history @stop @section('content')

<div class="container" ng-controller="HistoryController">
    <!--page personal information-->
    <section class="page page-account" id="page-acc-history">
        <h2>Account Settings</h2>
        <div class="route-acc">
            <a href="/settings" class="a-perInfo">Personal Information</a>
            <a href="#" class="a-history active">History</a>
        </div>
        <div class="wrap-his">
        @if(count($liststore) == 0)
        	<p class="text-center">You currently do not have anything stored with us.  Let us store your stuff and we'll show you some history!</p>
        @else
            <div class="wrap-his-head hidden-xs">
                <p class="col-sm-4"></p>
                <div class="col-sm-8">
                    <div class="row">
                        <p class="col-sm-4">Drop-off</p>
                        <p class="col-sm-4">Pick-up</p>
                        <p class="col-sm-4">Cancel</p>
                    </div>
                </div>
            </div>
            @foreach($liststore as $data)
            <div class="item-his">
                <div class="row">
                    <div class="col-sm-4 item-his-inner item-his-infor">
                        <p class="item-his-desc">{{$data->content == "" ? "Empty" : $data->content}}</p>
                        <div>
                            <div class="item-his-barcode">
                                <img class="barcode-image" src="{{ CommonHelper::generateBarcode($data->barcode) }}" />
                                <!-- <span class="">{{$data->barcode}}</span> -->
                            </div>
                            <div class="item-his-group">
                                <div class="item-his-popup-container">
                                    <div class="item-his-popup">
                                        <div>
                                            @foreach($data->images as $img)
                                                <img src="{{ asset('public/uploads/' . $img) }}" />
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="wrap-his-head row visible-xs">
                            <p class="col-xs-4 ta-c">
                                Drop-off
                            </p>
                            <p class="col-xs-4 ta-c">
                                Pick-up
                            </p>
                            <p class="col-xs-4 ta-c">
                                Cancel
                            </p>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 item-his-inner">
                                @foreach($data->dropofftime as $dot)
                                <p class="item-his-date">{{$dot}}</p>
                                @endforeach
                            </div>
                            <div class="col-xs-4 item-his-inner">
                                @foreach($data->pickuptime as $put)
                                <p class="item-his-date">{{$put}}</p>
                                @endforeach
                            </div>
                            <div class="col-xs-4 item-his-inner">
                                <p class="item-his-date">{{ $data->canceltime }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        @endif
        </div>

        <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
			    <div class="modal-content">
			        <div class="modal-body">
			      	  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			          <img src="" class="imagepreview" style="width: 100%;" >
			        </div>
			    </div>
			</div>
		</div>

    </section>
    <!--end page-->
</div>

@stop