@extends('layouts.web') @section('title') register @stop

@section('content')

<div class="container">
	<!--login page-->
	<section id="page-login">

		<!--create account section-->
		<div class="section col-md-6">

			@if ($errors->has())
		        @foreach ($errors->all() as $error)
		            <div class='bg-danger alert'>{{ $error }}</div>
		        @endforeach
		    @endif

			<form class="frm-login" method="post" action="/register/{{ $zipcode }}{{ ($token != '') ? '/' . $token : ''  }}">
				<h3 class="">CREATE AN ACCOUNT</h3>
				<div class="form-group">
					<input name="name" class="form-control" id="name" placeholder="Name">
				</div>
				<div class="form-group">
					<input type="email" name="email" class="form-control" id="email" placeholder="Email">
				</div>
				<div class="form-group">
					<input type="email" name="confirmemail" class="form-control" id="confirmemail" placeholder="Confirm Email">
				</div>
				<div class="form-group">
					<input name="password" type="password" class="form-control" id="password" placeholder="Password">
				</div>
				<div class="checkbox pad-l-4">
					<label><input type="checkbox" name="rgaccept" id="rgaccept" class="large-checkbox-size">  Check this box to indicate your agreement to our <a href="/terms" target="_blank" class="color">Terms & Conditions.</a></label>
				</div>
				<div class="form-group">
					<button type="submit" class="btn button-signup">Sign up</button>
				</div>
				<div class="form-group">
					<p class="txt1">
						Already registered with BoxGofer? <a href="/login" class="color">Log in</a>
					</p>
				</div>
				<div class="form-group">
					<a href="/fb/login" class="btn button-singup-facebook">Sign up with Facebook</a>
				</div>
			</form>

		</div>
		<!--description section-->
		<div class="section col-md-6">
			<div class="page-login-description">
				<div class="row row-cus">
					<div class="content content-1">
						<h3>Storage at Your Doorstep</h3>
						<p>We’ll deliver eco-friendly bins to your doorstep. Simply fill them up and schedule a pick-up. When you’re ready to get them back, let us know which ones you want returned to you and they’ll be back at your doorstep within 48 hours.</p>
					</div>
				</div>
				<div class="row row-cus">
					<div class="content content-2">
						<h3>Pick-up and Delivery</h3>
						<p>When you schedule service, we’ll bring you empty bins for you to fill up, we’ll come back to pick them up and take them to our secure warehouse at no additional cost to you. We’ll charge a flat fee of $14.99 when you schedule the full bins to be returned to your doorstep – for as many bins as you want to have delivered for that one low price.</p>
					</div>
				</div>
				<div class="row row-cus">
					<div class="content content-3">
						<h3>Secure, Climate-Controlled Storage</h3>
						<p>We’ll store all of your items in our secured climate-controlled facility which is monitored 24/7. We protect your stuff as we would our own belongings, so that it is returned to you exactly as it was when our Gofers picked it up.</p>
					</div>
				</div>
			</div>
		</div>

	</section>
	<!--end login page-->
</div>
<script type="text/javascript">
$(function(){
	$( ".frm-login" ).submit(function( event ) {

		if ($('#name').val() == '') {
			$('#name').css('border-color', 'red');
			alert('The name field is required.');
			return false;
		} else {
			$('#name').css('border-color', '#6D6E71');
		}

		if ($('#email').val() == '') {
			$('#email').css('border-color', 'red');
			alert('The email field is required.');
			return false;
		} else {
			$('#email').css('border-color', '#6D6E71');
		}

		if ($('#confirmemail').val() == '') {
			$('#confirmemail').css('border-color', 'red');
			alert('The confirm email field is required.');
			return false;
		} else {
			$('#confirmemail').css('border-color', '#6D6E71');
		}

		if ($('#email').val() != $('#confirmemail').val()) {
			alert('The confirm email and email must match.');
			return false;
		}

		if ($('#password').val() == '') {
			$('#password').css('border-color', 'red');
			alert('The password field is required.');
			return false;
		} else {
			$('#password').css('border-color', '#6D6E71');
		}

		if (! $('#rgaccept').prop('checked')) {
			$('.checkbox').css('color', 'red');
			alert('You must check the box indicating your agreement with the Terms and Conditions.');
			return false;
		}
		//event.preventDefault();
	});
});
</script>

@stop