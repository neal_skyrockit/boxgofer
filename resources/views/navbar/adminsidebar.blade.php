
<nav id="leftpart">
@if(Auth::guard('managers')->user()->role != 'operator')
    <a class="nav-item {{ Request::is('admin') ? 'current' : '' }}" href="/admin/">
        <i class="icon icon-dashboard"></i>
        <span>Dashboard</span>
    </a>
@endif
    <a class="nav-item {{ Request::is('admin/customers') ? 'current' : '' }}" href="/admin/customers">
        <i class="icon icon-clients"></i>
        <span>Customers</span>
    </a>

@if(Auth::guard('managers')->user()->role != 'driver')
    <a class="nav-item {{ Request::is('admin/invoices') || Request::is('admin/invoices/*') ? 'current' : '' }}" href="/admin/invoices" >
        <i class="icon icon-invoice"></i>
        <span>Invoices</span>
    </a>
@endif

    <a class="nav-item {{ Request::is('admin/orders') || Request::is('admin/orders/*') ? 'current' : '' }}" href="/admin/orders">
        <i class="icon icon-quickbook"></i>
        <span>Orders</span>
    </a>

	<a class="nav-item {{ Request::is('admin/bintracking') ? 'current' : '' }}" href="/admin/bintracking">
        <i class="icon icon-bin-tracking"></i>
        <span>Bin Tracking</span>
    </a>

@if(Auth::guard('managers')->user()->role != 'operator' && Auth::guard('managers')->user()->role != 'driver' && Auth::guard('managers')->user()->role != 'admin')
    <a class="nav-item {{ Request::is('admin/staff') ? 'current' : '' }}"  href="/admin/staff">
        <i class="icon icon-staff"></i>
        <span>Staff</span>
    </a>
@endif

@if(Auth::guard('managers')->user()->role != 'operator' && Auth::guard('managers')->user()->role != 'driver')
     <a class="nav-item {{ Request::is('admin/bin') ? 'current' : '' }}"  href="/admin/bin">
        <i class="icon icon-bin"></i>
        <span>Bin</span>
    </a>
@endif

@if(Auth::guard('managers')->user()->role == 'owner' || Auth::guard('managers')->user()->role == 'manager' || Auth::guard('managers')->user()->role == 'driver')
	<a class="nav-item {{ Request::is('admin/warehouse') ? 'current' : '' }}"  href="/admin/warehouse">
		<i class="icon icon-warehouse"></i>
		<span>Warehouse</span>
	</a>
@endif

    <a class="nav-item {{ Request::is('admin/calendar') ? 'current' : '' }}"  href="/admin/calendar">
        <i class="icon icon-calendar-admin"></i>
        <span>Calendar</span>
    </a>

@if(Auth::guard('managers')->user()->role == 'owner' || Auth::guard('managers')->user()->role == 'manager')
    <a class="nav-item {{ Request::is('admin/promotion') ? 'current' : '' }}"  href="/admin/promotion">
        <i class="icon icon-Promotion"></i>
        <span>Promotion</span>
    </a>
@endif

</nav>