<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="/admin">Boxgofer Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Auth::guard('managers')->user()->name }} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <!-- <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li> -->
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="/auth/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="{{ Request::is('admin') ? 'active' : '' }}">
                        <a href="/admin"><i class="fa fa-fw fa-dashboard"></i>Dashboard</a>
                    </li>
                    <li class="{{ Request::is('admin/userlist') ? 'active' : '' }}">
                        <a href="/admin/userlist"><i class="fa fa-fw fa-table"></i>Users</a>
                    </li>
                    <li class="{{ Request::is('admin/translist') ? 'active' : '' }}">
                        <a href="/admin/translist"><i class="fa fa-fw fa-table"></i>Transactions</a>
                    </li>
                    <li class="{{ Request::is('admin/storagelist') ? 'active' : '' }}">
                        <a href="/admin/storagelist"><i class="fa fa-fw fa-table"></i>Storage</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>