<!--footer-->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h3>Boxgofer</h3>
				<nav>
					<ul>
						<li><a href="/faq">FAQ</a></li>
						<li><a href="/aboutus">About Us</a></li>
						<li><a href="/privacy">Privacy</a></li>
						<li><a href="/terms">Terms & Conditions</a></li>
					</ul>
				</nav>
			</div>
			<div class="col-md-4">
				<h3>Contact</h3>
				<ul>
					<li><a href="mailto:support@boxgofer.com">support@boxgofer.com</a>
					</li>
				</ul>
			</div>
			<div class="col-md-4">
				<h3>Hours</h3>
				<ul>
					<li>Monday to Friday: 9 am to 6 pm</li>
					<li>Saturday: 10 am to 5 pm</li>
				</ul>
			</div>
		</div>
		<h3>
			Connect with us: <a class="icon icon-facebook"></a> <a class="icon icon-twitter"></a>
		</h3>
		<p class="ta-center">2016 Box Gofer all rights reserved</p>
	</div>
</footer>
<!--end footer-->
