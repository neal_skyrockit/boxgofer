<!--header-->
<header>
    <nav class="clearfix container">
        <a href="/home" id="logo"><img src="{{ asset('web/images/logo.svg') }}" /></a>
        <label for="navigation-trigger" class="navigation-trigger-label"></label>
        <input id="navigation-trigger" class="navigation-trigger hidden" type="checkbox" />
        <div class="navigation-group">
            <ul class="quickaction">
            	<!-- <li><a href="/order" class="order-now">Order Now</a></li> -->
                <li><a href="{{ url('/zipcode') }}" class="order-now">Order Now</a></li>
                @if (Auth::guard('user')->user())
                <!-- <li>
                    <a href="/refer" class="referral"></a>
                </li> -->
                <li class="user-actions-group">
                    <input id="trigger-user-actions" type="checkbox" class="hidden trigger-user-actions">
                    <label for="trigger-user-actions" class="user-actions">{{ Auth::guard('user')->user()->name }}</label>
                    <ul class="sub-menu">
                        <li><a href="/mystuff">My Stuff</a></li>
                        <li><a href="/settings">Account Settings</a></li>
                        <!-- <li><a href="/refer">Referrals</a></li> -->
                        <li><a href="/logout">Logout</a></li>
                    </ul>
                </li>
                @else
                <li><a href="/login" class="login">Login</a></li>
                @endif
            </ul>
            <ul class="clearfix">
                <li><a href="/home#how-it-work">How it Works</a></li>
                <li><a href="/faq">FAQ</a></li>
                <li><a href="/aboutus">About Us</a></li>
            </ul>
        </div>
    </nav>
</header>
<!--end header-->