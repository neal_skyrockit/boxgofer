<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Boxgofer - Bins in Storage Email</title>
</head>
<body>
	<div>
		<img src="http://boxgofer.com/web/images/logo.png" />
	</div>
	<p>{{ $storage->relationUser->name }},</p>
	<p>Great news!  We have received your items in our secure storage facility and are preparing a nice warm meal for them as we speak - - - not really, but we are treating them like we do our own stuff.</p>
	<p>Remember, if you took pictures or have a written inventory for items in each bin, please upload them to your account's My Stuff page as soon as possible lest you forget.  When you're ready to have them returned to you, well, you know the drill by now.</p>
	<p>Sincerely,</p>
	<p>The Box Gofer Team</p>

</body>
</html>
