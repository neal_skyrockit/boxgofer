<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Boxgofer - Forgot Password</title>
</head>
<body>

	<div style="text-align: center; line-height: 50px;">
		<h2 style="font-family: 'Open Sans', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif !important;
				font-size: 24px !important; color: #343d4d !important;
				font-weight: 600 !important; margin: 0; padding: 0;">
			{{ $title }}
		</h2>
	</div>
	<div style="text-align: center; line-height: 50px;">{{ $content }}</div>
	<div>&nbsp;</div>
	<div style="text-align: center; line-height: 50px;">
		<table border="0" cellspacing="0" cellpadding="0" width="250" style="margin: 0 auto;">
			<tr bgcolor="#529fd2">
				<td style="font-weight: bold;text-align: center; padding: 12px 2px 12px 0px;">
					<table style="width:100%; border:0; text-align:center;" cellpadding="0" cellspacing="0">               
			            <tr>
			            <td>
			            	<a target="_blank" href="{{ $url }}" style="font-size: 18px !important; text-decoration:none;">
							  <span style="color:#fefefe; font-weight: bold; font-family: 'Open Sans', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif !important; font-size: 18px !important;">
							    <font color="#fefefe">Reset Password</font>
							  </span>
							</a>
			            </td>
			            </tr>                    
			        </table>    
				</td>
			</tr>
		</table>
	</div>
</body>
</html>
