<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
      <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
      <title>Boxgofer</title>
      <style type="text/css">
      	.container {
      		width: 500px;
      	}
      	.table > tbody > tr > td:nth-child(2) {
		   text-align: right;
		}
      </style>
    </head>
    <body>
	<div class="container">
		<h2>Invoice status: {{ $status }}</h2>
		<div><strong>Pay to:</strong> {{ $paytoEmail }}</div>
		<div><strong>Currency:</strong> {{ $currency | 'USD' }}</div>
		<div><strong>Amount:</strong> {{ $totalAmount }} {{ $currency | 'USD' }}</div>

	     <table class="table" border="1">
			<thead>
				<tr bgcolor="#eee">
					<td colspan="4"><h4><strong>Invoice</strong></h4></td>
				</tr>
				<tr>
					<td colspan="4">
						<p>{{ $fullName }}</p>
						<p>{{ $addressLine1 }}</p>
						<p>{{ $addressCity }}, {{ $addressState }} {{ $addressCode }} {{ $addressCountry }}</p>
						<p>{{ $phone }}</p>
						<p>{{ $email }}</p>
					</td>
				</tr>
				<tr bgcolor="#eee">
					<td><strong>Name</strong></td>
					<td><strong>Quantity</strong></td>
					<td><strong>Unit Price</strong></td>
					<td><strong>Amount</strong></td>
				</tr>
			</thead>
		    <tbody border="1">
				@foreach($items as $item)
		      	<tr>
					<td>{{ $item->name }}</td>
					<td>{{ $item->quantity }}</td>
					<td>{{ $item->unit_price->value }}</td>
					<td>{{ sprintf('$%0.2f', ($item->unit_price->value * $item->quantity) / 100.0) }} {{ $currency }}</td>
				</tr>
				@endforeach

				<tr>
					<td colspan="3"><strong>Total:</strong></td>
					<td><strong>{{ $totalAmount }} {{ $currency | 'USD' }}</strong></td>
				</tr>
		    </tbody>
		  </table>
	  </div>
    </body>
</html>