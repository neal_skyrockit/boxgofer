<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Boxgofer</title>
    </head>
    <body>
    	<div style="text-align: center; line-height: 50px;"><strong>{{ $username }}</strong> referred you to regist an account on Boxgofer (<a href="http://boxgofer.com" target="_blank">Boxgofer.com</a>)</div>
		<div style="text-align: center; line-height: 50px;">
			<a href="http://{{ $email }}" target="_blank">Click here to register</a>
		</div>
    </body>
</html>