<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Boxgofer - Bins at Doorstep Email</title>
</head>
<body>
	<div>
		<img src="http://boxgofer.com/web/images/logo.png" />
	</div>
	<p>{{ $storage->relationUser->name }},</p>
	<p>You've received our eco-friendly bins at your location, so now it's time to fill them up.  Once they're ready to be picked up, just go to <a href="http://www.boxgofer.com" target="_blank">www.BoxGofer.com</a> and log-in to your account to schedule a pick-up.  You've got all the time you need and we won't even bill you until you've had them for two weeks (unless you're quick and we pick them up before then).  We'll send you a reminder in a couple of weeks if you haven't already scheduled a pick-up.</p>
	<p>Don't forget to take pictures of the contents in your bins and the corresponding barcodes once they are packed.  After they are securely in storage, you can upload your photos to our website next to the appropriate bins so that you know what is in each one - you'll want to know down the road if you only want certain bins returned.  You will also be able to log your index for each bin online if that is preferable to taking pictures.</p>
	<p>Sincerely,</p>
	<p>The Box Gofer Team</p>

</body>
</html>
