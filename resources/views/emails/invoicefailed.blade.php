<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Boxgofer</title>
    </head>
    <body>
		<div>
			Dear {{ $username }},<br><br>
			Unfortunately your most recent invoice payment for {{ $amount }} was declined.
		    This could be due to a change in your card number or your card expiring, cancelation of your credit card,
		    or the bank not recognizing the payment and taking action to prevent it.<br><br>
		    Thanks and Best regards.
		</div>
    </body>
</html>