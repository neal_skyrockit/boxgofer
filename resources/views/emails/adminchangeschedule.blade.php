<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Boxgofer - Change schedule</title>
</head>
<body>
	<div>
		<img src="http://boxgofer.com/web/images/logo.png" />
	</div>
	<p>{{ $cusname }},</p>
	<p>We have received your request to change a scheduled {{ $status }} of your bins or other items. Please see the revised address, date and time below.</p>
	<p><b>{{ $status }} Address and Contact Phone Number:</b></p>
	<p>{{ $address }}</p>
	<p>{{ $phone }}</p>
	<p><b>Date &amp; Time:</b></p>
	<p>{{ $formatDateEmail['date'] }}</p>
	<p>{{ $formatDateEmail['time'] }}</p>
	
	<p>If this accurately reflects your request, we�re all set and will see you then!  If you didn�t request this change, please let us know so we can change it back.  As always, you can reach us at <a href="mailto:info@boxgofer.com">info@boxgofer.com</a>.  If you would prefer to talk to one of our Gofers, just include your phone number and the best time to reach you and we�ll give you a call.</p>
	
	<p>Sincerely,</p>
	<p>The Box Gofer Team</p>
</body>
</html>
