<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Boxgofer - Empty Bins Pick Up Email</title>
</head>
<body>
	<div>
		<img src="http://boxgofer.com/web/images/logo.png" />
	</div>
	<p>{{ $storage->relationUser->name }},</p>
	<p>Congratulations!  We have picked up your items and closed out the billing on the items returned.  If you have any questions or comments, we'd love to hear from you at <a href="mailto:info@boxgofer.com">info@boxgofer.com.</a></p>
	<p>Thank you for being such an important part of our success here at Box Gofer.  If you have friends or family that would benefit from our services, please visit our website and click on the Refer a Friend link.  They'll thank you and we'll thank you!</p>
	<p>Sincerely,</p>
	<p>The Box Gofer Team</p>

</body>
</html>
