<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
      	<title>Boxgofer</title>
    </head>
    <body>
		<div id="invoice-container" class="abc" style="padding: 0 15px;">
			<div style="text-align: center;">
				<img height="100" src="http://boxgofer.com/web/images/logo.png" />
			</div>
			<h1 style="text-align: center;">INVOICE</h1>
			<table width="100%">
				<tr>
					<td width="70%"></td>
					<td>
						<div style="font-weight: bold;">Date: <span>{{ $dayStart }}</span></div>
						<div style="font-weight: bold;">Invoice Number: <span>{{ $invoice_num }}</span></div>
					</td>
				</tr>
			</table><br />

			<table width="100%">
				<tr>
					<td width="10%" style="vertical-align: top; font-weight: bold;">Bill to:</td>
					<td width="30%">
						<div>{{ $customer }}</div>
						<div>{{ $addressstreet }} </div>
						@if($aptsuite)<div>{{ $aptsuite }}</div>@endif
						<div>{{ $cityzipcode }}</div>
					</td>
					<td>
						<div><span style="font-weight: bold;">Phone:</span> {{ $phone }}</div>
						<div><span style="font-weight: bold;">Customer Number:</span> {{ $userid }}</div>
					</td>
				</tr>
			</table><br />

			<table style="border: 1px solid #ddd; border-collapse: collapse; width: 100%; max-width: 100%; margin-bottom: 20px;">
				<tr bgcolor="#5A5B5D" style="background-color: #5A5B5D !important; color: #ffffff; font-size: 16px;">
					<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">Item</th>
					<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">Description</th>
					<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">Quantity</th>
					<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">Unit Cost</th>
					<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">Subtotal</th>
				</tr>

				@if($itemType == 'deliver' && $quantity > 0)
				<tr style="text-align: center;">
					<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">Other</th>
					<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top; text-align: justify;">Bin Fee - {{ $description }}</td>
					<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">{{ $quantity }}</td>
					<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">{{ $amount }}</td>
					<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">{{ $amount }}</td>
				</tr>
				@endif
				@if($itemType == 'purchase' && $quantity > 0)
				<tr style="background-color: #f9f9f9; text-align: center;">
					<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">Other</th>
					<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top; text-align: justify;">{{ $description }}</td>
					<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">{{ $quantity }}</td>
					<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">{{ $amountNoTax }}</td>
					<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">{{ $amountNoTax }}</td>
				</tr>
				<tr style="background-color: #f9f9f9; text-align: center;">
					<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">Tax</th>
					<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top; text-align: justify;">Tax on bin purchase (8.25%)</td>
					<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">{{ $quantity }}</td>
					<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">{{ $tax }}</td>
					<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">{{ $tax }}</td>
				</tr>
				@endif

				<tr style="text-align: center;">
					<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;"></th>
					<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;"></td>
					<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;"></td>
					<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;"><span style="font-weight: bold;">Total</span></td>
					<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;"><span style="font-weight: bold;">{{ $totalAmount }}</span></td>
				</tr>
			</table>

			<h4>
				Paid by: <span style="margin-left: 20px;">Visa Credit Card</span> <span style="margin-left: 20px;">{{ $card_number }}</span> <span style="margin-left: 20px;"><span>{{ $totalAmount }}</span></span>
			</h4>

			<h4 style="margin-top: 20px;">Thank you for using our services. If you have any questions, please don't hesitate to e-mail us at support@boxgofer.com.</h4>
			<div>Sincerely,</div>
			<div style="font-weight: bold;">Box Gofer</div>
		</div>
    </body>
</html>