<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Boxgofer - Return Bins Request Email</title>
</head>
<body>
	<div>
		<img src="http://boxgofer.com/web/images/logo.png" />
	</div>
	<p>{{ $storage->relationUser->name }},</p>
	<p>We have received your request to have your items returned.  This is where Storage at Your Doorstep&#8482; is the best storage option out there!.  One of our Gofers will deliver the bins to your doorstep at the time specified below.</p>
	<p><b>Drop-off Address & Contact Phone Number:</b></p>
	<p>{{ $storage->address }}</p>
	<p>{{ $storage->phone }}</p>
	<p><b>Date & Time:</b></p>
	<p>{{ $formatDateEmail['date'] }}</p>
	<p>{{ $formatDateEmail['time'] }}</p>
	<p>Once our Gofers deliver your bins back to you, please sort through your items and schedule a time for us to come pick up the bins - either still full or empty.  We'll take them either way.</p>
	<p>We would like to pick them up as soon as possible and you will continue to pay the normal monthly rental rate on the bins until they are picked up by one of our Gofers, so don't forget to schedule the pick-up of the empty bins.  We will send you a reminder in a couple of weeks to schedule your pickup.  In the event you want to keep the bins, they become damaged, lost or stolen while in your possession, we will bill your account $65 per bin that is not returned or that is not able to be used by future customers. </p>
	<p>If you have any questions, please e-mail us at <a href="mailto:info@boxgofer.com">info@boxgofer.com</a>.  If you would prefer to talk to one of our Gofers, just include your phone number and the best time to reach you and we'll give you a call.</p>
	<p>Sincerely,</p>
	<p>The Box Gofer Team</p>

</body>
</html>
