<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
      <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
      <title>Boxgofer</title>
      <style type="text/css">
      	#invoice-container {
      		width: 450px;
      		padding: 0px 10px;
      		margin-bottom: 20px;
      	}
      	.table > tbody > tr > td:nth-child(2) {
		   text-align: right;
		}
		.btn-print {
			height: 30px;
		    width: 70px;
		    background: url('http://boxgofer.com/bk/images/print.png') no-repeat;
		    background-position: left 5px center;
		    background-size: auto 70%;
		    border: 2px solid #70b447;
		    border-radius: 4px;
		    cursor: pointer;
		    position: relative;
		    float: right;
    		margin-top: 30px;
		}
		.btn-print:before {
		    content: "Print";
		    font-weight: bold;
		    font-size: 14px;
		    color: #70b447;
		    position: absolute;
		    height: 26px;
		    line-height: 26px;
		    right: 5px;
		}
		.title {
			font-size: 18px;
			font-weight: bold;
		}
		.color-green {
			color: #71b649;
		}
		.btn-invoice {
			background: #71b649;
			color: white;
			-moz-border-radius: 4px;
		    -webkit-border-radius: 4px;
		    border-radius: 4px;
	        font-weight: bold;
		    margin: 25px 0px;
		    padding: 5px 0px;
		    text-align: center;
		}
		.btn-invoice-2 {
		    padding: 10px 40px;
		    color: #71b649;
		    font-weight: bold;
		    border: 2px solid #71b649;
		    -moz-border-radius: 4px;
		    -webkit-border-radius: 4px;
		    border-radius: 4px;
		}
		table td {
			border-top: none !important;

		}
		.tr-gray td {
			background: #f3f3f5;
		}
		.mailto {
			color: #71b649;
			text-decoration: none;
		}
      </style>
    </head>
    <body>
	<div id="invoice-container">
		<div class="row">
			<div class="col-xs-6">
				<a href="http://boxgofer.com/home" id="logo"><img width="130" src="http://boxgofer.com/web/images/logo.svg"></a>
			</div>
			<div class="col-xs-6" onclick="window.print();">
				<div id="printer" class="btn-print"></div>
			</div>
		</div>
		<h5 class="title color-green">Your Payment Infomation</h5>
		<div class="btn-invoice">INVOICES No.</div>
		<div style="text-align: center; margin-bottom: 30px;"><span class="btn-invoice-2">{{ $invoiceId }}</span></div>
	     <table class="table">
		    <tbody>
		      	<tr class="tr-gray">
					<td>Day Start:</td>
					<td>{{ $dayStart }}</td>
				</tr>
				<tr>
					<td class="color-green">Period:</td>
					<td class="color-green">{{ $period }}</td>
				</tr>
				<tr>
					<td>Customer:</td>
					<td>{{ $customer }}</td>
				</tr>
				<tr class="tr-gray">
					<td>Pay By:</td>
					<td>{{ $payBy }}</td>
				</tr>
				<tr>
					<td>Items:</td>
					<td>{{ $items }}</td>
				</tr>
				<tr class="tr-gray">
					<td>Billing Add:</td>
					<td>{{ $billingAdd }}</td>
				</tr>
				<tr>
					<td class="color-green">Amount:</td>
					<td class="color-green">{{ $amount }}</td>
				</tr>
		    </tbody>
		  </table>

		  <p>Thank you for using our services. If you have any remaining questions, please do not hesitate to call us. You will find the Boxgofer contact information <a class="mailto" href="mailto:support@boxgofer.com">here</a></p>

		  <div>Sincerely</div>
		  <b>Boxgofer.</b>
	  </div>

	<script type="text/javascript">
		var hd = '<meta charset="utf-8">';
		hd += '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
		hd += '<meta name="viewport" content="width=device-width, initial-scale=1">';
		hd += '<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">';
		hd += '<title>Boxgofer</title>';
		hd += '<style type="text/css">';
		hd += '	#invoice-container {';
		hd += '		width: 450px;';
		hd += '		padding: 0px 10px;';
		hd += '		margin-bottom: 20px;';
		hd += '	}';
		hd += '	.table > tbody > tr > td:nth-child(2) {';
		hd += '	   text-align: right;';
		hd += '	}';
		hd += '	.title {';
		hd += '		font-size: 18px;';
		hd += '		font-weight: bold;';
		hd += '	}';
		hd += '	.color-green {';
		hd += '		color: #71b649;';
		hd += '	}';
		hd += '	.btn-invoice {';
		hd += '		background: #71b649;';
		hd += '		color: white;';
		hd += '		-moz-border-radius: 4px;';
		hd += '		-webkit-border-radius: 4px;';
		hd += '		border-radius: 4px;';
		hd += '		font-weight: bold;';
		hd += '		margin: 25px 0px;';
		hd += '		padding: 5px 0px;';
		hd += '		text-align: center;';
		hd += '	}';
		hd += '	.btn-invoice-2 {';
		hd += '		padding: 10px 40px;';
		hd += '		color: #71b649;';
		hd += '		font-weight: bold;';
		hd += '		border: 2px solid #71b649;';
		hd += '		-moz-border-radius: 4px;';
		hd += '		-webkit-border-radius: 4px;';
		hd += '		border-radius: 4px;';
		hd += '	}';
		hd += '	table td {';
		hd += '		border-top: none !important;';
		hd += '	}';
		hd += '	.tr-gray td {';
		hd += '		background: #f3f3f5;';
		hd += '	}';
		hd += '	.mailto {';
		hd += '		color: #71b649;';
		hd += '		text-decoration: none;';
		hd += '	}';
		hd += '  </style>';

		var body = document.getElementById('invoice-container').innerHTML;

		function printElem()
		{
			var mywindow = window.open('', 'my div', 'height=650,width=450');

			mywindow.document.write('<html><head>'+ hd);
			mywindow.document.write('</head><body onload="document.getElementById(\'printer\').style.display = \'none\';"><div id="invoice-container">'+ body +'</div></body></html>');

			mywindow.document.close(); // necessary for IE >= 10
			mywindow.focus(); // necessary for IE >= 10

			mywindow.print();
			mywindow.close();

			return true;
		}
	</script>
    </body>
</html>