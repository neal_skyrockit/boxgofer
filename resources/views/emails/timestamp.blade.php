<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Boxgofer</title>
    </head>
    <body>
		<div>
			<img src="http://boxgofer.com/web/images/logo.png" />
		</div>
		<p>{{ $name }},</p>
		<p>Thanks for ordering Storage at Your Doorstep<span style="font-size: 11px;">&#8482;</span> from Box Gofer.  One of our Gofers will deliver the bins to your doorstep at the time specified below.</p>
		<p><b>Drop-off Address & Contact Phone Number:</b></p>
		<p>{{ $address }}</p>
		<p>{{ $phone }}</p>
		<p><b>Date & Time:</b></p>
		<p>{{ $formatDateEmail['date'] }}</p>
		<p>{{ $formatDateEmail['time'] }}</p>
		<p>Once our Gofers deliver the bins, all you have to do is to fill them up and schedule a pick-up at <a href="http://www.boxgofer.com" target="_blank">www.BoxGofer.com</a>.  We'll come back to get them and take them to one of our secure, climate controlled storage facilities for safe keeping until you need them back.  We'll start billing you monthly at the earlier of: 14 days after we drop them off; or when we pick them up.  We'll send you an e-mail reminder the day before your account will be billed.</p>
		<p>Be sure to take pictures of your packed bins before you close the lid!  Once the bins are in storage, you can upload pictures of your items in each bin so that you can determine which bins or Other Stuff you want returned at the appropriate time.</p>
		<p>If you have any questions, please e-mail us at <a href="mailto:info@boxgofer.com">info@boxgofer.com</a>.  If you would prefer to talk to one of our Gofers, just include your phone number and the best time to reach you and we'll give you a call.</p>
		<p>Sincerely,</p>
		<p>The Box Gofer Team</p>
    </body>
</html>