<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Box Gofer - User Registration</title>
</head>
<body style="margin: 0; padding: 0;background-color: #f3f3f3;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td style="padding: 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="585" style="margin: 20px auto 0 auto; border: 0px solid #dbdbdb; border-collapse: collapse; box-shadow: 0px 0px 15px #888888; border-radius: 7px 7px 7px 7px; -webkit-border: 7px 7px 7px 7px; -moz-border: 7px 7px 7px 7px;">
					
					<tr>
						<td align="center" bgcolor="white" style="border-radius: 7px 7px 0 0; -webkit-border: 7px 7px 0 0; -moz-border: 7px 7px 0 0; background-color: #fff; padding: 26px 0;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td align="center">
										<img src="{{ asset('web/images/logo.svg') }}" title="Box Gofer" alt="Box Gofer" style="margin: 0 auto;" width="167">
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" bgcolor="white" style="background-color: #71B548;">&nbsp;</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 37px 42px 65px 42px; border-radius: 0 0 7px 7px; -webkit-border: 0 0 7px 7px; -moz-border: 0 0 7px 7px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
							
								<tr>
									<td style="padding: 0; color: #153643; line-height: 20px; text-align: left;">

										<p style="font-family:'Verdana', sans-serif; font-size:16px; color:#4b4b4b; padding:0; margin: 0 0 30px 0;">
											Hello <strong>{{ $user->name }}</strong>,
										</p>

										<p style="font-family:'Verdana', sans-serif; font-size:16px; color:#4b4b4b; padding:0; margin: 0 0 30px 0;">
											Please find your login information below:
										</p>

										<p style="font-family:'Verdana', sans-serif; font-size:16px; color:#4b4b4b; padding:0; margin: 0 0 30px 0;">
											Email: <strong>{{ $user->email }}</strong><br/>
											Password: <strong>{{ $user->password }}</strong><br/>
										</p>

										<br/><br/>

										<p style="font-family:'Verdana', sans-serif; font-size:16px; color:#4b4b4b; padding:0; margin: 0 0 30px 0;">
											To sign into this account, please use the web address below. <a href="{{ url('/login') }}" title="Login" style='color: #71B548;'>{{ url('/login') }}</a>
										</p>

										<br/><br/>

										<p style="font-family:'Verdana', sans-serif; font-size:16px; color:#4b4b4b; padding:0; margin: 0 0 30px 0;">
											For more information, please go to <a href="{{ url('/home') }}" style="color: #71B548;" title="Box Gofer">{{ url('/home') }}</a>
										</p>

									</td>
								</tr>
								
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" bgcolor="white" style="background-color: #71B548;">&nbsp;</td>
					</tr>
					<tr>
						<td align="center" align="center" border="0" cellpadding="0" cellspacing="0" width="585">
							<table>
								<tr>
									<td align="center">
										<p style="font-family:'Verdana', sans-serif; font-size:14px; color:#4b4b4b; padding:0;  margin: 20px 0 0px 0;">
											Copyright &copy; <?php echo date('Y'); ?> • <span style="color: #71B548; text-decoration: none;">Box Gofer</span> • Allrights Reserved
										</p>
										<br/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>