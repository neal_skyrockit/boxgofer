<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Boxgofer</title>
    </head>
    <body>
		<div style="line-height: 50px;">Click link below to dowload Delivery file for {{ $date }}: </div>
		<a href="{{ $link }}">{{ $link }}</a>
    </body>
</html>