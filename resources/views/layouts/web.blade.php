<!DOCTYPE html>
<html lang='en' ng-app="Boxgofer">
    <head>
        <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>@yield('title') | Boxgofer</title>

	    <!--css-->
	    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
	    <link href='{{ asset("web/css/bootstrap.min.css") }}' rel='stylesheet' type='text/css'>
	    <link href='{{ asset("web/css/toastr.css") }}' rel='stylesheet' type='text/css'>
	    <link href='{{ asset("web/css/main.css") }}' rel='stylesheet' type='text/css'>
        <!--main-css-->
		<!-- <style>
				@media all and (-ms-high-contrast:none){*::-ms-backdrop, header #logo img{height:100%;}
				}
				*{box-sizing:border-box;}
				.clearfix:after{content:"";display:table;clear:both;}
				.word-wrap{word-wrap:break-word;}
				body{color:#58595B;font-size:16px;font-family:'Roboto', sans-serif;font-weight:500;padding:0;margin:0;}
				body.single-page
				.page *{font-size:18px;}
				input[data-error]{border-color:red;}
				.error{color:red;}
				.currency:before{content:"$";}
				h3{font-size:18px;font-weight:500;}
				a{color:inherit;cursor:pointer;text-decoration:none;}
				a:hover{color:green;}
				.nowrap{white-space:nowrap;}
				.bg-color{background-color:#71B548;color:white;}
				.bg-color-grey{background-color:#8C8E90;color:white;}
				.color{color:#71B548;}
				.color:hover{color:#71B548;}
				.ta-center{text-align:center;}
				.bor-round{border-radius:4px;}
				.bor-color{border-width:1px;border-style:solid;border-color:#71B548;}
				.style-button{border:1px solid #71B548;color:#71B548;background-color:white;border-radius:4px;}
				.style-button:hover{border:1px solid #71B548;color:white;background-color:#71B548;}
				.padding0004{padding:0 4px;}
				.margin1400{margin:14px 0;}
				.margin0600{margin:6px 0;}
				.mar-top-8{margin-top:8px;}
				.nomarginbot{margin-bottom:0 !important;}
				.nomargin{margin:0 !important;}
				.fw-bold{font-weight:500;}
				.fs24{font-size:24px !important;}
				.fs18{font-size:18px !important;}
				.ta-c{text-align:center;}
				.ta-l{text-align:left;}
				.ta-r{text-align:right;}
				.padding1215{padding:12px 15px;}
				.h46{height:46px !important;font-size:16px !important;}
				.form-group{margin-bottom:8px;}
				.bg-loading{background:url('web/images/Processing1.gif') no-repeat center center;background-size:contain;width:50px;height:50px;}
				.width-100{width:100%;}
				.border-none{border:none !important;}
				header{background:rgba(255, 255, 255, 1);position:fixed;width:100%;top:0;z-index:1000;-moz-box-shadow:1px 1px rgba(0,0,0,0.2);-webkit-box-shadow:1px 1px rgba(0,0,0,0.2);box-shadow:1px 1px rgba(0,0,0,0.2);}
				header #logo{display:inline-block;float:left;width:167px;margin-bottom:14px;margin-top:10px;}
				header #logo img{max-width:100%;}
				header nav ul{list-style-type:none;padding:0;margin:30px auto 0;display:inline-block;float:right;}
				.navigation-trigger-label{width:30px;height:30px;position:absolute;right:14px;top:20px;display:none;cursor:pointer;}
				.navigation-trigger-label:before{content:"";position:absolute;top:0;left:0;width:100%;height:100%;background:url('web/images/hambuger.png') no-repeat;background-size:contain;background-position:center center;}
				header nav ul.quickaction a{color:#71B548;}
				header nav ul .order-now{border:solid 1px #71B548;border-radius:4px;}
				header nav ul .login{padding-right:0;text-align:right;}
				header nav ul .order-now:hover{color:white;background-color:#71B548;}
				header nav ul.quickaction .user-actions-group{margin-left:6px;}
				header nav ul.quickaction .user-actions{position:relative;color:#71B548;font-size:16px;display:block;padding:10px 34px 9px 12px;cursor:pointer;border-radius:4px;font-weight:500;border:2px solid transparent;margin:0;width:160px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;text-align:left;}
				header nav ul.quickaction .user-actions:before{content:'';position:absolute;top:calc(50% - 5px);right:5px;border-top:10px solid #71B548;border-bottom:10px solid transparent;border-left:10px solid transparent;border-right:10px solid transparent;}
				header nav ul.quickaction .user-actions:hover{border-color:#E9ECE9;}
				header nav ul li{position:relative;display:inline-block;float:left;}
				header nav ul li .sub-menu{display:none;list-style-type:none;position:absolute;padding:0;margin:-2px 0 0 0;width:100%;background-color:white;z-index:10;border:2px solid #E9ECE9;border-top:none;border-bottom-left-radius:6px;border-bottom-right-radius:6px;}
				header nav ul li > .trigger-user-actions:checked ~ .sub-menu{display:block;}
				header nav ul li > .trigger-user-actions:checked ~ .user-actions{border-color:#E9ECE9;border-bottom-left-radius:0;border-bottom-right-radius:0;border-bottom-color:transparent;}
				header nav ul li .sub-menu li{width:100%;}
				header nav ul li .sub-menu li a{border-radius:0;padding:12px;color:#58595B;font-size:14px;}
				header nav ul li a{display:block;padding:10px 18px;font-weight:500;}
				header .referral{background:url('web/images/referral.png') no-repeat center center;background-size:contain;vertical-align:middle;display:inline-block;height:18px;width:18px;padding:0;margin:12px 0 0 8px;}
				footer{background-color:#71B548;padding-top:25px;padding-bottom:15px;}
				.single-page footer{background-color:#FAFAFD;}
				footer ul{list-style-type:none;padding:0;margin:0;font-weight:400;}
				footer h3{font-size:18px;}
				footer .icon{width:30px;height:30px;vertical-align:middle;}
				footer .icon-facebook{margin-left:24px;}
				footer .icon-twitter{margin-left:2px;}
				.form-control{height:38px;box-shadow:none;color:#6D6E71;font-size:16px;}
				.btn{width:100%;height:38px;}
				.cus-deliverform .form-control{padding:5px;}
				.icon{display:inline-block;width:24px;height:24px;background-repeat:no-repeat;background-position:center center;background-size:contain;margin:0;padding:0;}
				.icon:before{content:"\00a0";}
				.icon.green{border:1px solid #71B548 !important;}
				.icon.yellow{border:1px solid #EBD836 !important;}
				.order-view .icon{width:50px;height:50px;border-radius:4px;border:1px solid #EBD836;}
				.order-view .input-table .icon{width:36px;height:36px;margin-left:6px;}
				footer .icon{width:30px;height:30px;vertical-align:middle;}
				footer .icon-facebook{margin-left:24px;}
				footer .icon-twitter{margin-left:2px;}
				.icon-facebook{background-image:url('web/images/icon-facebook.png');}
				.icon-twitter{background-image:url('web/images/icon-twitter-f.png');}
				.icon-calendar{background-image:url('web/images/icon-calendar.png');}
				.icon-clock{background-image:url('web/images/icon-clock.png');}
				.icon-calendarV2{background-image:url('web/images/icon-calendarV2.png');}
				.icon-clockV2{background-image:url('web/images/icon-clockV2.png');}
				.icon-bicycle{background-image:url('web/images/icon-bicycle.png');}
				.icon-smallpackage{background-image:url('web/images/icon-smallpackage.png');}
				.icon-bigpackage{background-image:url('web/images/icon-bigpackage.png');}
				.icon-contact-phone{background-image:url('web/images/icon-contact-phone.png');}
				.icon-home{background-image:url('web/images/icon-home.png');}
				main{min-height:200px;margin-top:100px;}
				#page-login{padding:50px 0 110px;overflow:hidden;}
				#page-login .section{padding:30px 0;}
				#page-login h3{font-size:18px;margin-bottom:20px;}
				.frm-login{max-width:345px;}
				.frm-login .btn{font-size:24px;height:46px;}
				.frm-login .form-control{height:46px;}
				.frm-login .button-signup{background-color:#71B548;color:white;}
				.frm-login .button-singup-facebook{background-color:#465D9B;color:white;}
				.frm-login .txt1{text-align:center;font-size:14px;margin-bottom:50px;margin-top:18px;color:#A7A9AC;}
				.frm-login h3{margin-bottom:35px;text-align:center;}
				.page-login-description{max-width:550px;margin:auto;}
				.page-login-description .row-cus{margin-right:0px;}
				.page-login-description .row .content{padding-left:210px;background-repeat:no-repeat;background-position:center left;background-size:150px auto;}
				.page-login-description .row .content h3{margin-top:0;text-align:left;}
				.page-login-description .row .content p{font-size:13px;}
				.page-login-description .row .content-1{background-image:url('web/images/page-login-img1.jpg');min-height:140px;}
				.page-login-description .row .content-2{background-image:url('web/images/section-2-image3.jpg');min-height:120px;}
				.page-login-description .row .content-3{background-image:url('web/images/page-login-img3.jpg');min-height:170px;}
				#page-yourzipcode{margin-top:100px;margin-bottom:200px;}
				.frm-yourzipcode{max-width:490px;margin:auto;}
				.frm-yourzipcode .zipcodesection{max-width:395px;margin:auto;}
				.frm-yourzipcode .supportsection{margin-top:40px;}
				.frm-yourzipcode .supportsection p{margin-top:0;margin-bottom:40px;}
				.frm-yourzipcode .part{padding-left:8px;padding-right:8px;}
				.frm-yourzipcode h3{margin-bottom:40px;text-align:center;}
				.frm-yourzipcode label{line-height:46px;font-weight:400;}
				.frm-yourzipcode button{font-size:16px;height:38px;}
				.section{padding:80px 0;position:relative;}
				.section h3.title-section{font-size:48px;text-align:center;margin-top:0;font-weight:bold;color:#71B548;}
				.section h4{font-size:42px;}
				.section h5{font-size:38px;text-align:center;}
				.in-section{max-width:1400px;padding:0 15px;margin:auto;}
				#section-1{height:calc(100vh - -79px);background-image:url('web/images/section-1-image1.jpg');background-repeat:no-repeat;background-size:cover;position:relative;}
				#section-1 .content-container{width:590px;height:590px;border-radius:50%;background-color:rgba(255,255,255,0.6);position:absolute;top:calc(50% - 295px);left:calc(50% - 295px);text-align:center;}
				#section-1 .content{position:absolute;width:80%;height:60%;top:20%;left:10%;text-align:center;}
				#section-1 h4{color:#71B548;margin-top:4px;}
				#section-1 .content p{color:inherit;font-size:36px;margin-top:12px;}
				#section-1 .content button{background-color:#71B548;font-size:30px;padding:15px 40px;color:white;border-radius:4px;display:inline-block;margin-top:24px;width:auto;height:auto;}
				.in-container{width:1250px;margin:0 auto;}
				#section-2 .in-section{margin-top:70px;}
				#section-2 .text{display:block;width:75%;margin:30px auto 0;text-align:center;font-size:24px;}
				#section-2 .image-circle{width:75%;height:0;padding-bottom:75%;margin:auto;border-radius:50%;border:1px solid #71B548;background-size:contain;position:relative;}
				#section-2 .image-circle:before{content:"";width:100%;height:100%;position:absolute;top:0;left:0;-moz-transform:scale(0.7);-ms-transform:scale(0.7);-o-transform:scale(0.7);-webkit-transform:scale(0.7);transform:scale(0.7);background-size:contain;background-repeat:no-repeat;background-position:center center;}
				#section-2 .image-circle-1:before{-moz-transform:scale(0.7);-ms-transform:scale(0.7);-o-transform:scale(0.7);-webkit-transform:scale(0.7);transform:scale(0.7);background-image:url('web/images/section-2-image1.jpg');}
				#section-2 .image-circle-2:before{background-image:url('web/images/section-2-image2.jpg');}
				#section-2 .image-circle-3:before{background-image:url('web/images/section-2-image3.jpg');}
				#section-3{background-color:#EFEAE8;text-align:center;}
				#section-3 .in-section{}
				#section-3 .image{height:350px;width:100%;margin:auto;background-repeat:no-repeat;background-position:center bottom;position:relative;}
				#section-3 .image-1{background-image:url('web/images/section-3-image11.png');}
				#section-3 .image-2{background-image:url('web/images/section-3-image2.png');}
				#section-3 .image-3{background-image:url('web/images/section-3-image3.png');background-size:100% auto;}
				#section-3 .text{font-size:24px;margin-top:60px;}
				#section-3 h5{margin-top:60px;font-size:17px;line-height:44px;color:#333;text-align:left;}
				#section-4{padding-top:50px;text-align:center;}
				#section-4 span{display:inline-block;text-align:center;background-repeat:no-repeat;background-position:top center;background-size:auto 80px;padding:110px 50px 0;font-size:21px;margin-top:30px;}
				#section-4 .image1{background-image:url('web/images/section-4-image1.png');}
				#section-4 .image2{background-image:url('web/images/section-4-image2.png');}
				#section-4 .image3{background-image:url('web/images/section-4-image3.png');}
				#section-4 .image4{background-image:url('web/images/section-4-image4.png');}
				#section-4 .image5{background-image:url('web/images/section-4-image5.png');}
				#section-5{background-image:url('web/images/section-5-image1.jpg');background-repeat:no-repeat;background-size:100% 100%;min-height:400px;}
				#section-5 .content{position:relative;display:block;margin:auto;}
				#section-5 h3{color:#71B548;margin-bottom:80px;}
				#section-5 p{text-align:center;color:white;font-size:24px;margin-bottom:20px;}
				#section-6{background-color:#71B548;}
				#section-6 .content{text-align:center;}
				#section-6 h5{color:white;}
				#section-6 a{background-color:white;color:#71B548;display:inline-block;font-size:24px;padding:14px 35px;border-radius:4px;margin-top:26px;}
				.order-line-section{height:168px;background-color:#FAFAFA;overflow:hidden;}
				.order-line-section .order-line{height:2px;width:80%;display:block;background-color:#DADBDD;margin:84px auto 0;position:relative;white-space:nowrap;}
				.order-line-section .step{position:absolute;background-color:#DADBDD;width:38px;height:38px;text-align:center;line-height:38px;border-radius:4px;vertical-align:middle;display:inline-block;top:-19px;font-size:18px;font-weight:500;color:white;}
				.order-line-section .step:before{position:absolute;text-align:center;width:150px;top:-40px;left:-56px;color:#DADBDD;}
				.order-line-section .step.active{background-color:#71B548;}
				.order-line-section .step.active:before{color:black;}
				.order-line-section .schedule-step{left:-19px;}
				.order-line-section .schedule-step:before{content:"Schedule";}
				.order-line-section .review-step{left:calc(33.3333% - 19px);}
				.order-line-section .review-step:before{content:"Review";}
				.order-line-section .billing-step{right:calc(33.3333% - 19px);}
				.order-line-section .billing-step:before{content:"Billing";}
				.order-line-section .confirm-step{right:-19px;}
				.order-line-section .confirm-step:before{content:"Confirmation";}
				.order-view{margin:50px 0;}
				.order-view > .partial{display:none;}
				.order-view > .partial.current{display:block;}
				.order-view > .partial > .row{margin-bottom:10px;}
				.order-view > .partial span{position:relative;}
				.order-view > .partial span > .description{position:absolute;left:0;top:24px;font-size:16px;color:#71b548;}
				.order-view > .partial span > .description:before{content:"$";}
				.order-view > .partial span > .description:after{content:"/mo";}
				.order-view .refer-text{padding:6px 20px;}
				.grey-section{background-color:#FAFAFA;border-radius:4px;padding:18px;}
				.payment{display:inline-block;height:35px;width:100%;background-position:center center;background-size:contain;background-repeat:no-repeat;}
				.payment-credit{background-image:url('web/images/payment-credit.png');background-position:top left;}
				.payment-paypal{text-align:right;}
				.payment-paypal input{height:40px;}
				.grey-section.nopadding{padding:0 !important;margin-top:6px !important;}
				.model-deliver{padding:10px 30px !important;}
				.padding5{padding:0 5px;}
				.deliverform .grey-section{padding:6px;margin:6px 0;}
				.deliverform .input-table:first-child{margin-bottom:6px;}
				.deliverform .row{margin-right:-5px;margin-left:-5px;}
				.deliverform .grey-section.pickuptime{display:none;}
				.grey-section.billing-step{padding:14px 50px 30px;}
				.grey-section.billing-step > .row{margin-top:16px;}
				.grey-section.billing-step > .row span{line-height:40px;}
				.order-view .grey-section.nopadding span{vertical-align:middle;}
				.order-view .grey-section.nopadding .text{margin-left:14px;}
				.message-group{display:none;position:fixed;width:100%;height:100%;z-index:10000;top:0;left:0;}
				.message-group .blur-layer{position:absolute;width:100%;height:100%;top:0;left:0;background-color:black;opacity:0.6;}
				.message-group .message-body{display:block;position:absolute;min-height:100px;width:400px;top:calc(50% - 100px);left:calc(50% - 200px);text-align:center;padding:50px 100px;background-image:url('web/images/bg-congratulation.png');background-repeat:no-repeat;background-position:center center;}
				.message-group .message-body h3{font-size:32px;color:#58595B;font-weight:400;margin:16px 0;}
				.message-group .message-body p{font-size:16px;color:white;text-align:left;}
				.order-view .message-group .message-body{background-color:#71B548;width:560px;top:calc(50% - 150px);left:calc(50% - 280px);}
				.number-group{white-space:nowrap;display:inline-block;margin:6px 10px;position:relative;}
				.number-group button{display:inline-block;width:36px;height:36px;border-radius:4px;border:1px solid #71B548;background:transparent;vertical-align:middle;position:relative;}
				.number-group .decrease::before,
				.number-group .increase::before{content:"";position:absolute;width:50%;height:2px;top:calc(50% - 1px);left:25%;background-color:#71B548;}
				.number-group .increase::after{content:"";position:absolute;height:50%;width:2px;left:calc(50% - 1px);top:25%;background-color:#71B548;}
				.number-group input{height:38px;width:80px;font-size:14px;border:1px solid #BBBDBF;border-radius:4px;box-shadow:none;background:transparent;vertical-align:middle;margin:0 6px;padding:0 20px;}
				.number-group .message p{margin:0;color:red;}
				.number-group .message{width:100%;position:absolute;top:calc(100% + 10px);text-align:center;background-color:white;padding:6px;z-index:1000;box-shadow:2px 2px 3px grey;border-radius:4px;}
				.number-group .message:before{content:'';position:absolute;width:0;height:0;top:-20px;left:calc(50% - 10%);border-top:10px solid transparent;border-bottom:10px solid white;border-left:10px solid transparent;border-right:10px solid transparent;}
				.tooltip-group{display:inline-block;}
				.tooltip-group .guide{position:relative;display:inline-block;width:20px;height:20px;background-image:url('web/images/icon-guide.png');background-position:center center;background-size:contain;cursor:pointer;margin:0;white-space:normal;}
				.tooltip-group .guide:before,
				.tooltip-group .guide:after{display:none;}
				.tooltip-group .trigger-tooltip:checked + .guide:before,
				.tooltip-group .trigger-tooltip:checked + .guide:after{display:block;}
				.tooltip-group .guide:before{content:'';position:absolute;top:-10px;left:0;border-top:20px solid transparent;border-bottom:20px solid transparent;border-left:20px solid transparent;border-right:20px solid #fff;z-index:1001;}
				.tooltip-group .guide:after{content:attr(data-tooltip);border-radius:4px;position:absolute;left:38px;top:-20px;padding:20px;min-height:60px;width:230px;background-color:white;-webkit-box-shadow:3px 3px 7px 0 rgba(0,0,0,0.35);-moz-box-shadow:3px 3px 7px 0 rgba(0,0,0,0.35);box-shadow:3px 3px 7px 0 rgba(0,0,0,0.35);font-size:14px;font-weight:400;z-index:1000;}
				.order-view .form-control{background-color:transparent;}
				.order-view .form-control.w120{width:120px !important;}
				.order-view .form-control.w165{width:165px !important;}
				.order-view .button-next{font-size:16px;padding:8px 30px;color:white;background-color:#71B548;font-weight:500;border:none;border-radius:4px;}
				.order-view .button-back{font-size:16px;padding:8px 30px;color:white;background-color:#8C8E90;font-weight:500;border:none;border-radius:4px;margin-right:10px;}
				.order-view p.color{font-size:18px;}
				.input-table{position:relative;display:table;border-collapse:separate;}
				.input-table input,
				.input-table label{vertical-align:middle;display:inline-block;}
				.order-view .input-table{margin-top:6px;}
				.order-view .input-table .cell .form-control{width:100%;font-size:14px;color:#6D6E71;}
				.label-price{height:38px;line-height:38px;padding-left:20px;width:120px;background-color:transparent;box-shadow:none;font-size:16px;border:1px solid #BBBDBF;border-radius:6px;color:#71B548;margin:0;}
				.order-view .chb-tc{display:block;margin-top:-10px;margin-bottom:20px;margin-left:295px;}
				.large-checkbox-size{-ms-transform:scale(1.5);-moz-transform:scale(1.5);-webkit-transform:scale(1.5);-o-transform:scale(1.5);transform:scale(1.5);}
				.page h2{font-size:24px;color:#71B548;text-align:center;}
				.route-acc{max-width:345px;margin:auto;margin-top:30px;margin-bottom:20px;overflow:hidden;padding:0 15px;}
				.route-acc a{display:inline-block;font-size:18px;}
				.a-perInfo{text-align:left;float:left;}
				.a-history{text-align:right;float:right;}
				.route-acc a:hover{color:#71B548;}
				.active{color:#71B548;border-bottom:solid 1px #71B548;}
				.btn-save{font-size:16px;background-color:#71B548;color:white;}
				.frm-eidtprofile{max-width:345px;margin:0 auto;}
				#select-city,
				#select-zipcode{background:url('web/images/arrow-bot.png') no-repeat right;background-position:right 22px center;overflow:hidden;cursor:pointer;margin-bottom:20px;}
				#select-city select,
				#select-zipcode select{background:transparent;appearance:none;-moz-appearance:none;-webkit-appearance:none;cursor:pointer;}
				.wrap-his{margin-top:50px;}
				.wrap-his-head{overflow:hidden;color:#71B548;font-size:18px;}
				.wrap-his-head p{padding:0px 30px;}
				.item-his{border:solid 1px #71B548;border-radius:6px;padding:15px 0;min-height:90px;font-size:16px;margin-bottom:20px;}
				.item-his .item-his-inner{border-right:solid 1px #71B548;margin:0;min-height:116px;}
				.item-his .item-his-infor{padding-left:115px !important;background:url('web/images/img_ex.png') no-repeat;background-size:84px auto;background-position:left 18px center;}
				.item-his .item-his-inner:last-child{border-right:none;}
				.item-his .item-his-img{text-align:center;}
				.item-his .item-his-img img{max-width:100%;}
				.item-his .item-his-date,
				.item-his .item-his-hours{margin:0;font-size:14px;text-align:center;}
				.item-his-barcode{margin-right:10px;width:200px;text-align:center;float:left;}
				.item-mystuff .item-his-barcode{margin-right:20px;}
				.item-his-barcode img{max-width:100%;}
				.item-his .item-his-desc{margin-bottom:6px;}
				.item-his .item-his-desc img{cursor:pointer;margin:-10px 10px 0 0;}
				.item-his-group{float:left;position:relative;width:30px;height:28px;background:url("web/images/img_default.png") left no-repeat;cursor:pointer;vertical-align:middle;}
				.item-his-group:hover .item-his-popup-container{display:block;}
				.item-his-popup-container{display:none;position:absolute;left:40px;top:-12px;z-index:100;border-radius:6px;background-color:white;-webkit-box-shadow:5px 5px 7px 0 rgba(0,0,0,0.35);-moz-box-shadow:5px 5px 7px 0 rgba(0,0,0,0.35);box-shadow:5px 5px 7px 0 rgba(0,0,0,0.35);border:solid 1px #e9ece9;}
				.item-his-popup-container:before{content:'';background:url("web/images/arrow-popup.png") left no-repeat;background-size:contain;position:absolute;width:22px;height:29px;top:10px;left:-21px;}
				.item-his-popup{position:relative;padding:20px;min-width:260px;min-height:200px;max-height:300px;overflow-y:auto;}
				.item-his-popup .upimage{width:88%;margin-top:6px;}
				.item-his-popup .delimage{vertical-align:top;margin:10px 0 0 2px;}
				#page-referFriend{padding:40px 0;margin:0 auto;}
				.referFriend-inner{max-width:580px;margin:0 auto;padding:30px 0;}
				.referFriend-inner h2{text-align:left;}
				.frm-referFriend{margin:20px 0 50px 0;}
				.frm-referFriend .form-group{overflow:hidden;}
				.frm-referFriend label{font-weight:normal;font-size:18px;line-height:38px;color:#000;}
				.referFriend-result{margin:40px 0px 40px 40px;font-style:italic;}
				.wrap-dash{text-align:center;margin:40px 0 0 0;overflow:hidden;}
				.wrap-dash-left{display:inline-block;float:left;}
				.wrap-dash-right{display:inline-block;float:right;}
				.wrap-dash-mid{margin:0 auto;display:inline-block;}
				.image-helper-middle{display:inline-block;height:100%;vertical-align:middle;}
				.icon-dash{display:block;height:60px;margin-bottom:10px;}
				.icon-dash img{vertical-align:bottom;}
				.wrap-dash .status{font-size:18px;}
				.wrap-dash-content{padding:20px 0;}
				.wrap-dash-top h3{display:inline-block;font-size:24px;margin-right:40px;}
				.wrap-dash-inner{width:320px;margin:0 auto;text-align:center;}
				.wrap-dash-inner img{max-width:216px;max-height:216px;}
				.btn-order::before{content:"";position:absolute;width:18px;height:2px;top:18px;left:13px;background-color:#71B548;}
				.btn-order::after{content:"";position:absolute;height:18px;width:2px;left:21px;top:10px;background-color:#71B548;}
				.btn-order{border:solid 1px #71B548;color:#71B548;font-size:16px;background:transparent;vertical-align:middle;position:relative;width:125px;margin:10px;padding-left:35px;}
				.wrap-dashmystuff-inner{padding:40px 0;}
				.item-mystuff{padding:15px 0;font-size:18px;margin-bottom:20px;margin:0 -15px;}
				.item-mystuff-img{text-align:center;}
				.item-mystuff-img img{width:185px;height:185px;}
				.item-mystuff-desc-group{position:relative;}
				.item-mystuff-desc-editor{display:none;position:absolute;bottom:calc(100% + 10px);padding:6px;border-radius:4px;width:350px;background-color:#F2F5F1;}
				.item-mystuff-desc-editor-trigger:checked + .item-mystuff-desc-editor{display:block;}
				.item-mystuff-desc-editor:before{content:"";width:0;height:0;border-top:10px solid #F2F5F1;border-bottom:10px solid transparent;border-left:10px solid transparent;border-right:10px solid transparent;position:absolute;bottom:-20px;left:6px;}
				.item-mystuff .item-mystuff-inner{background:url('web/images/img_ex.png') no-repeat;background-size:160px auto;background-position:top left;padding-left:180px;min-height:180px;}
				.item-mystuff .item-mystuff-barcode{display:inline-block;margin:0 50px 0 10px;}
				.item-mystuff .item-mystuff-desc{margin-top:15px;position:relative;height:24px;}
				.item-mystuff .item-mystuff-desc > label{margin:0;position:absolute;top:0;z-index:1;}
				.item-mystuff .item-mystuff-desc > span
				,.item-his .item-his-desc{background-color:#FFF;display:block;overflow:hidden;position:relative;text-overflow:ellipsis;white-space:normal;word-wrap:break-word;max-height:24px;padding-right:14px;}
				.item-mystuff .item-mystuff-desc > span{padding-left:30px;}
				.item-mystuff .item-mystuff-desc > span:before
				,.item-his .item-his-desc:before{content:"...";position:absolute;top:0;right:0;}
				.item-mystuff .item-mystuff-desc > span:after
				,.item-his .item-his-desc:after{content:'';height:100%;width:100%;background-color:inherit;position:absolute;white-space:normal;word-wrap:break-word;}
				.item-mystuff .item-mystuff-desc{margin-top:15px;}
				.item-mystuff .item-mystuff-group{position:relative;width:26px;height:23px;display:inline-block;background:url("web/images/img_default.png") left no-repeat;cursor:pointer;}
				.item-mystuff-barcode-group{margin-right:20px;float:left;min-height:56px;}
				.add-img{display:inline-block;position:relative;max-width:250px;}
				.save-img{display:none;}
				.btn-add-img{border:solid 1px #71B548;color:#71B548;background:transparent;width:100px;position:relative;background:url("web/images/img_default.png") 54px center no-repeat;}
				.btn-add-img::before{content:"";position:absolute;width:18px;height:2px;top:17px;left:16px;background-color:#71B548;}
				.btn-add-img::after{content:"";position:absolute;height:18px;width:2px;left:24px;top:9px;background-color:#71B548;}
				.item-mystuff .item-mystuff-group:hover .item-mystuff-popup{display:block;}
				.item-mystuff-popup{border-radius:6px;background-color:white;-webkit-box-shadow:5px 5px 7px 0 rgba(0,0,0,0.35);-moz-box-shadow:5px 5px 7px 0 rgba(0,0,0,0.35);box-shadow:5px 5px 7px 0 rgba(0,0,0,0.35);min-width:170px;min-height:200px;left:50px;top:-5px;position:absolute;z-index:100;padding:20px;border:solid 1px #e9ece9;display:none;}
				.item-mystuff-popup img{margin-bottom:10px;}
				.item-mystuff-popup:before{content:'';background:url("web/images/arrow-popup.png") left no-repeat;background-size:contain;position:absolute;width:22px;height:29px;top:10px;left:-20px;}
				.item-mystuff-deliver{padding-top:16px;}
				.btn-deliver{border:solid 1px #71B548;color:#71B548;background:transparent;background:url("web/images/icon-deliver.png") right 8px top 8px no-repeat;font-size:16px;padding:6px 40px 6px 22px;margin-bottom:6px;text-align:center;}
				.deliver-all{width:282px;margin:0 auto;}
				.drop-pick-option-trigger{font-size:16px;font-weight:400;padding-left:30px;display:block;position:relative;}
				.drop-pick-option-trigger:before{content:"";width:20px;height:20px;border-radius:50%;border:1px solid #71B548;position:absolute;left:0;top:2px;}
				.drop-pick-option:checked + .drop-pick-option-trigger:after{content:"";width:12px;height:12px;border-radius:50%;background-color:#71B548;position:absolute;left:4px;top:6px;}
				.themodel{padding-top:30px;font-size:16px;}
				.themodel .modal-header{border:none;}
				.themodel .modal-body{padding:10px 60px 10px 80px;}
				.themodel .modal-body h2{font-size:24px;color:#71B548;}
				.themodel .modal-body h3{font-size:20px;color:#71B548;}
				.themodel .modal-body .btn-save{width:120px;}
				.themodel .modal-body .btn-save:last-child{margin:6px;}
				.themodel .modal-footer{border:none;}
				.themodel input[name="password"]{}
				.current-address{background:url("web/images/home-address.png") left no-repeat;height:50px;line-height:50px;padding-left:80px;font-size:16px;margin:10px 0;}
				.themodel .form-control.w120{width:78%;}
				.themodel .input-table .icon{width:36px;height:36px;border-radius:4px;float:right;}
				.after-submit{padding:100px 0;}
				.after-submit img{margin-right:20px;width:72px;height:auto;}
				.after-submit span{font-size:28px;color:#71B548;}
				.tx-center{text-align:center;}
				.padd-0{padding:0;}
				.page{padding:40px 0;margin:0 auto;}
				.marTop-20{margin-top:20px;}
				#overlay{display:none;height:100%;position:fixed;left:0;top:0;bottom:0;right:0;background:#000;opacity:0.5;filter:alpha(opacity=50);z-index:9999;}
				#loading{width:60px;height:60px;position:absolute;top:50%;left:50%;margin:-28px 0 0 -25px;}
				.static-page p{font-weight:normal;font-size:16px;}
				.static-page ul{list-style-type:none;}
				.static-page-term ul{list-style-type:disc;}
				.input-table .dropdown-menu{min-width:120px;display:block;top:36px;bottom:auto;left:0px;right:auto;text-align:center;padding:10px;}
				.input-table .order-time div{line-height:30px;cursor:pointer;}
				.input-table .order-time div:hover{color:#71B548;}
				.text-inactive{color:#D4D6D6;}
				.input-table .order-time div.text-inactive:hover{color:#D4D6D6;}
				.order-time:before{border-left:7px solid transparent;border-right:7px solid transparent;border-bottom:7px solid #cccccc;border-bottom-color:rgba(0, 0, 0, 0.2);top:-7px;left:7px;}
				.order-time:after{border-left:6px solid transparent;border-right:6px solid transparent;border-bottom:6px solid white;top:-6px;left:8px;}
				table.aboutus-table{border:1px solid #d9dbda;border-collapse:separate;width:100%;-moz-border-radius:10px;-webkit-border-radius:10px;border-radius:10px;}
				.aboutus-table a{color:#72b44a;font-style:italic;}
				.aboutus-table th, td{border-top:none!important;vertical-align:middle !important;}
				.aboutus-table th{border-bottom:1px solid #d9dbda;text-align:center;color:#72b44a;}
				.aboutus-table td{font-size:16px;font-weight:normal;}
				.aboutus-table td:nth-child(1){padding-left:25px;text-align:left;}
				.aboutus-table td:nth-child(4){padding-right:22px;}
				.aboutus-table td:nth-child(2), .aboutus-table td:nth-child(3){text-align:center;}
				.aboutus-table .about-label, .aboutus-table .about-label-2, .aboutus-table .about-label-3{display:inline-block;width:145px;background:#F5F5F5;padding:10px 30px;font-weight:bold;color:black;-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px;}
				.aboutus-table .about-label-2{background:#fff;border:1px solid #72b44a;color:#72b44a;}
				.aboutus-table .about-label-3{background:#72b44a;color:#fff;}
				.aboutus-table .color-green{color:#72b44a;}
				.aboutus-table .tr-bold td{color:#72b44a;}
				.border-right-black{border-right:1px dotted #d9dbda;}
				.aboutus-title{margin:40px 0px 35px 0px;font-weight:bold;font-size:20px;}
				.faq-page ul{list-style-type:disc;font-size:14px;font-weight:normal;}
				.faq-page a[data-toggle]{text-decoration:none;color:#72b647;}
				.faq-page .mar-bot-10{margin-top:10px;}
				.faq-page a{color:#4997E6;text-decoration:underline;}
				.barcode-image{cursor:pointer;}
				#page-dashmystuff .item-his-group{display:inline-block;float:initial;}
				#page-dashmystuff #frm-uploadimg{vertical-align:top;width:100%;}
				#page-dashmystuff #frm-uploadimg .btn-save{width:auto;}
				#page-dashmystuff #frm-uploadimg .name-img{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;}
				.pad-l-r-5{padding-left:5px;padding-right:5px;}
				.pad-l-4{padding-left:4px;}
				.item-mystuff-desc-editor .desc-input{width:195px;float:left;}
				.item-mystuff-desc-editor .button-save{width:60px;margin-left:5px;}
				.item-mystuff-desc-editor .label-cancel{width:70px;}
				.item-mystuff-desc{width:200px;}
				#how-it-work{position:absolute;top:-30px;}
				.coming-logo{width:260px;margin-top:160px;}
				.comming-text{color:#fff;font-size:125px;margin-top:20px;}


		</style>-->
		
		<!--end-main-css-->
		<!--responsive-css-->
		<style>
			@media screen and (min-width:1200px){.container{width:990px;}
			}
			@media screen and (max-width:1601px){#section-1 .content-container{width:500px;height:500px;border-radius:50%;background-color:rgba(255,255,255,0.6);position:absolute;top:calc(50% - 250px);left:calc(50% - 250px);text-align:center;}
			}
			@media screen and (max-width:1367px){body{font-size:15px;}
			header #logo{width:130px;}
			header nav ul{margin:23px auto 0;}
			header nav ul li a{padding:8px 22px;}
			header nav ul.quickaction .user-actions:before{top:calc(50% - 4px);border-top:8px solid #71B548;border-bottom:8px solid transparent;border-left:8px solid transparent;border-right:8px solid transparent;}
			header nav ul.quickaction .user-actions{font-size:15px;padding:7px 28px 7px 15px;}
			#section-1{height:calc(100vh - -14px);}
			main{margin-top:84px;}
			#section-1 .content-container{width:430px;height:430px;border-radius:50%;background-color:rgba(255,255,255,0.6);position:absolute;top:calc(50% - 215px);left:calc(50% - 215px);text-align:center;}
			#section-1 h4{color:#71B548;margin-top:4px;font-size:36px;}
			#section-1 .content p{font-size:28px;margin-top:10px;}
			#section-1 .content button{font-size:24px;padding:8px 30px;border-radius:4px;margin-top:20px;}
			.section h5{font-size:34px;}
			.section{padding:50px 0;}
			.section h3.title-section{font-size:40px;}
			#section-2 .in-section2{width:85%;margin-top:50px;}
			#section-2 .text{margin:20px auto 0;font-size:18px;}
			#section-3{padding:60px 0;}
			#section-3 .in-section3{margin-top:110px;}
			#section-3 h5{margin-top:12px;font-size:15px;line-height:42px;}
			#section-4 .in-container span{background-size:auto 65px;padding:110px 38px 0;font-size:18px;}
			#section-5 p{font-size:20px;margin-bottom:15px;}
			#section-5 h3{color:#71B548;margin-bottom:50px;}
			#section-6 a{font-size:20px;padding:12px 30px;border-radius:4px;margin-top:20px;}
			footer h3{font-size:16px;}
			#page-login{padding:35px 0 35px;}
			.frm-login .form-control{height:40px;font-size:15px;}
			#page-login h3{font-size:16px;margin-bottom:15px;}
			.frm-login .btn{font-size:20px;height:40px;line-height:25px;}
			.page-login-description .row .content-1{min-height:130px;}
			.page-login-description .row .content{padding-left:190px;background-repeat:no-repeat;background-position:top left;background-size:140px auto;}
			#page-login .section{padding:20px 0;}
			.page h2{font-size:22px;}
			.page{padding:30px 0;}
			.item-his{font-size:16px;padding:12px 0;}
			.item-his .item-his-inner{margin:12px 0;}
			.item-his .item-his-date,
			.item-his .item-his-hours{margin:0 15px 10px 15px;}
			.wrap-his{margin-top:35px;}
			.wrap-his-head{font-size:16px;}
			.route-acc{margin-top:25px;margin-bottom:15px;}
			.route-acc a{font-size:16px;}
			.form-control{height:36px;font-size:15px;}
			.btn{height:36px;font-size:15px;}
			.wrap-dash .status{font-size:16px;}
			.wrap-dash .icon-dash{height:50px;margin-bottom:5px;}
			.wrap-dash .icon-dash img{width:80%;}
			.wrap-dash-top h3{font-size:20px;}
			.item-mystuff{font-size:16px;}
			.item-mystuff-img img{width:160px;height:160px;}
			.item-mystuff .item-mystuff-desc{margin-top:10px;}
			.in-container{width:85%;}
			}
			@media screen and (max-width:1281px){}
			@media screen and (max-width:1024px){header nav ul li a{padding:6px 12px;}
			header #logo{width:100px;margin-bottom:13px;}
			header nav ul{margin:20px auto 0;}
			header nav ul.quickaction .user-actions{padding:5px 28px 5px 15px;}
			main{margin-top:70px;}
			#section-1{height:calc(100vh - 78px);}
			#section-1 .content-container{width:400px;height:400px;top:calc(50% - 200px);left:calc(50% - 200px);}
			#section-1 h4{font-size:32px;}
			#section-1 .content p{font-size:24px;}
			#section-1 .content button{font-size:20px;padding:7px 25px;}
			.section h3.title-section{font-size:36px;}
			.section{padding:40px 0;}
			#section-2 .text{margin:15px auto 0;font-size:17px;}
			#section-2 .in-section2{margin-top:40px;}
			#section-3 .in-section3{margin-top:70px;width:85%;}
			#section-3 h5{margin-top:15px;line-height:36px;}
			#section-4 .in-container span{background-size:auto 55px;padding:70px 30px 0;font-size:16px;}
			#section-5{background-size:cover;}
			.section h5{font-size:30px;}
			#section-6 a{font-size:18px;padding:10px 25px;margin-top:15px;}
			.in-container{width:90%;}
			}
			@media screen and (max-width:991px){.hidden-991{display:none !important;}
			header nav ul li a{padding:10px 12px;}
			header nav ul.quickaction .user-actions{padding:9px 28px 9px 15px;width:120px;}
			header nav ul li .sub-menu li a{font-size:12px;padding:8px 6px;}
			#page-dashmystuff .item-mystuff-deliver{clear:left;width:100%;}
			}
			@media screen and (max-width:767px){.navigation-trigger-label{display:block;}
			header nav ul{float:left;}
			header nav ul{width:100%;margin:0;}
			header nav ul li{display:block;width:100%;}
			header nav ul .order-now{border:none;}
			header nav ul.quickaction .user-actions{width:100%;text-align:left;padding:10px;}
			header nav ul li .sub-menu{position:relative;}
			header nav ul li .sub-menu li a{font-size:14px;padding:12px 20px;}
			header nav ul.quickaction .user-actions-group{margin:0;}
			.navigation-group{display:none;}
			.navigation-trigger:checked + .navigation-group{display:block;}
			main{margin-top:68px;}
			#section-1{height:calc(100vh - 68px);}
			#section-2 .image-circle{width:60%;padding-bottom:60%;}
			#section-2 .text{font-size:24px;margin-bottom:60px;}
			#section-3 .image-1{background-size:contain;}
			#section-3 .image-2{background-size:contain;}
			#section-3 .image-3{background-size:contain;}
			#section-3 .text{font-size:28px;margin-top:10px;margin-bottom:40px;}
			#section-4 span{display:block;margin-top:40px;padding:90px 50px 0;}
			.item-mystuff .item-mystuff-inner{background-size:120px auto;background-position:top left;padding-left:140px;min-height:120px;}
			.comming-text{font-size:100px;margin-top:60px;}
			}
			@media screen and (max-width:500px){.bin-type-container .type-title{display:block;}
			.bin-type-container .type-title .description{position:inherit !important;top:0 !important;left:0 !important;margin-left:10px;}
			.bin-type-container #Item-Large{margin-left:0px !important;}
			.current-address{height:auto !important;line-height:30px;font-size:15px;}
			.order-line .schedule-step, .order-line .review-step, .order-line .billing-step, .order-line .confirm-step{font-size:16px;}
			.order-line-section{height:145px !important;}
			.order-view .chb-tc{margin-left:0px;}
			.grey-section.billing-step{padding-left:10px;padding-right:10px;}
			.item-mystuff-desc-editor-trigger:checked + .item-mystuff-desc-editor{left:-125px;}
			.item-mystuff-desc-editor:before{left:130px;}
			.item-mystuff-desc-editor{width:325px;}
			.item-mystuff-desc-editor .desc-input{width:165px;float:left;}
			.comming-text{font-size:50px;margin-top:20px;}
			}
			@media screen and (max-width:410px){#page-dashmystuff .stuff-car-icon{width:95%;}
			.item-mystuff-desc, .item-mystuff .item-his-barcode{width:170px;}
			}
			@media screen and (max-width:320px){.coming-logo{width:185px;margin-top:100px;}
			.comming-text{font-size:40px;margin-top:20px;}
			}
			@media only screen
			and (min-device-width:768px)
			and (max-device-width:1024px)
			and (orientation:portrait){.comming-text{font-size:85px;}
			}
		</style>
		<!--end-responsive-css-->
        <!--datetimepicker-->
		<style>
			.bootstrap-datetimepicker-widget{list-style:none}.bootstrap-datetimepicker-widget.dropdown-menu{margin:2px 0;padding:4px;width:19em}@media (min-width: 768px){.bootstrap-datetimepicker-widget.dropdown-menu.timepicker-sbs{width:38em}}@media (min-width: 992px){.bootstrap-datetimepicker-widget.dropdown-menu.timepicker-sbs{width:38em}}@media (min-width: 1200px){.bootstrap-datetimepicker-widget.dropdown-menu.timepicker-sbs{width:38em}}.bootstrap-datetimepicker-widget.dropdown-menu:before,.bootstrap-datetimepicker-widget.dropdown-menu:after{content:'';display:inline-block;position:absolute}.bootstrap-datetimepicker-widget.dropdown-menu.bottom:before{border-left:7px solid transparent;border-right:7px solid transparent;border-bottom:7px solid #ccc;border-bottom-color:rgba(0,0,0,0.2);top:-7px;left:7px}.bootstrap-datetimepicker-widget.dropdown-menu.bottom:after{border-left:6px solid transparent;border-right:6px solid transparent;border-bottom:6px solid #fff;top:-6px;left:8px}.bootstrap-datetimepicker-widget.dropdown-menu.top:before{border-left:7px solid transparent;border-right:7px solid transparent;border-top:7px solid #ccc;border-top-color:rgba(0,0,0,0.2);bottom:-7px;left:6px}.bootstrap-datetimepicker-widget.dropdown-menu.top:after{border-left:6px solid transparent;border-right:6px solid transparent;border-top:6px solid #fff;bottom:-6px;left:7px}.bootstrap-datetimepicker-widget.dropdown-menu.pull-right:before{left:auto;right:6px}.bootstrap-datetimepicker-widget.dropdown-menu.pull-right:after{left:auto;right:7px}.bootstrap-datetimepicker-widget .list-unstyled{margin:0}.bootstrap-datetimepicker-widget a[data-action]{padding:6px 0}.bootstrap-datetimepicker-widget a[data-action]:active{box-shadow:none}.bootstrap-datetimepicker-widget .timepicker-hour,.bootstrap-datetimepicker-widget .timepicker-minute,.bootstrap-datetimepicker-widget .timepicker-second{width:54px;font-weight:700;font-size:1.2em;margin:0}.bootstrap-datetimepicker-widget button[data-action]{padding:6px}.bootstrap-datetimepicker-widget .btn[data-action="incrementHours"]::after{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0;content:"Increment Hours"}.bootstrap-datetimepicker-widget .btn[data-action="incrementMinutes"]::after{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0;content:"Increment Minutes"}.bootstrap-datetimepicker-widget .btn[data-action="decrementHours"]::after{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0;content:"Decrement Hours"}.bootstrap-datetimepicker-widget .btn[data-action="decrementMinutes"]::after{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0;content:"Decrement Minutes"}.bootstrap-datetimepicker-widget .btn[data-action="showHours"]::after{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0;content:"Show Hours"}.bootstrap-datetimepicker-widget .btn[data-action="showMinutes"]::after{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0;content:"Show Minutes"}.bootstrap-datetimepicker-widget .btn[data-action="togglePeriod"]::after{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0;content:"Toggle AM/PM"}.bootstrap-datetimepicker-widget .btn[data-action="clear"]::after{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0;content:"Clear the picker"}.bootstrap-datetimepicker-widget .btn[data-action="today"]::after{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0;content:"Set the date to today"}.bootstrap-datetimepicker-widget .picker-switch{text-align:center}.bootstrap-datetimepicker-widget .picker-switch::after{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0;content:"Toggle Date and Time Screens"}.bootstrap-datetimepicker-widget .picker-switch td{padding:0;margin:0;height:auto;width:auto;line-height:inherit}.bootstrap-datetimepicker-widget .picker-switch td span{line-height:2.5;height:2.5em;width:100%}.bootstrap-datetimepicker-widget table{width:100%;margin:0}.bootstrap-datetimepicker-widget table td,.bootstrap-datetimepicker-widget table th{text-align:center;border-radius:4px}.bootstrap-datetimepicker-widget table th{height:20px;line-height:20px;width:20px}.bootstrap-datetimepicker-widget table th.picker-switch{width:145px}.bootstrap-datetimepicker-widget table th.disabled,.bootstrap-datetimepicker-widget table th.disabled:hover{background:none;color:#777;cursor:not-allowed}.bootstrap-datetimepicker-widget table th.prev::after{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0;content:"Previous Month"}.bootstrap-datetimepicker-widget table th.next::after{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0;content:"Next Month"}.bootstrap-datetimepicker-widget table thead tr:first-child th{cursor:pointer}.bootstrap-datetimepicker-widget table thead tr:first-child th:hover{background:#eee}.bootstrap-datetimepicker-widget table td{height:54px;line-height:54px;width:54px}.bootstrap-datetimepicker-widget table td.cw{font-size:.8em;height:20px;line-height:20px;color:#777}.bootstrap-datetimepicker-widget table td.day{height:20px;line-height:20px;width:20px}.bootstrap-datetimepicker-widget table td.day:hover,.bootstrap-datetimepicker-widget table td.hour:hover,.bootstrap-datetimepicker-widget table td.minute:hover,.bootstrap-datetimepicker-widget table td.second:hover{background:#eee;cursor:pointer}.bootstrap-datetimepicker-widget table td.old,.bootstrap-datetimepicker-widget table td.new{color:#777}.bootstrap-datetimepicker-widget table td.today{position:relative}.bootstrap-datetimepicker-widget table td.today:before{content:'';display:inline-block;border:solid transparent;border-width:0 0 7px 7px;border-bottom-color:#337ab7;border-top-color:rgba(0,0,0,0.2);position:absolute;bottom:4px;right:4px}.bootstrap-datetimepicker-widget table td.active,.bootstrap-datetimepicker-widget table td.active:hover{background-color:#337ab7;color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,0.25)}.bootstrap-datetimepicker-widget table td.active.today:before{border-bottom-color:#fff}.bootstrap-datetimepicker-widget table td.disabled,.bootstrap-datetimepicker-widget table td.disabled:hover{background:none;color:#777;cursor:not-allowed}.bootstrap-datetimepicker-widget table td span{display:inline-block;width:54px;height:54px;line-height:54px;margin:2px 1.5px;cursor:pointer;border-radius:4px}.bootstrap-datetimepicker-widget table td span:hover{background:#eee}.bootstrap-datetimepicker-widget table td span.active{background-color:#337ab7;color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,0.25)}.bootstrap-datetimepicker-widget table td span.old{color:#777}.bootstrap-datetimepicker-widget table td span.disabled,.bootstrap-datetimepicker-widget table td span.disabled:hover{background:none;color:#777;cursor:not-allowed}.bootstrap-datetimepicker-widget.usetwentyfour td.hour{height:27px;line-height:27px}.bootstrap-datetimepicker-widget.wider{width:21em}.bootstrap-datetimepicker-widget .datepicker-decades .decade{line-height:1.8em!important}.input-group.date .input-group-addon{cursor:pointer}.sr-only{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0}
		</style>
		<!--end-datetimepicker-->
		<script src="{{ asset('web/js/jquery.min.js') }}"></script>
		<script src="{{ asset('web/js/toastr.min.js') }}"></script>
		<!--main-js-->
		<script src="{{ asset('web/js/main.js') }}"></script>
		<!-- 
		<script>
			function NunmberGroup(e){function u(){return t.test(r.value)?!0:!1}var n=e,r=n.querySelector("input.amount"),a=n.querySelector(".decrease"),l=n.querySelector(".increase"),t=/^\d+$/;a.onclick=function(){if(u()){var e=parseInt(r.value);0>=e?r.value=0:r.value=e-1}else r.value=0},l.onclick=function(){if(u()){var e=parseInt(r.value);r.value=e+1}else r.value=0}}
		</script>
		-->
		<!--end-main-js-->

		<script type="text/javascript">
		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		</script>

    </head>
    <body class="{{ Request::is('home') ? 'single-page' : '' }}">

    	<div id="wrapper">

    		@include('navbar.webheader')

	        <main>
                @yield('content')
	        </main>

			@include('navbar.webfooter')

			<div id="overlay">
            	<img id="loading" src="{{ asset('web/images/loading3.gif')}}" />
            </div>

        </div>

        <script src="{{ asset('web/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('web/js/angular.min.js') }}"></script>
        <script type="text/javascript" src="{{asset('web/js/moment.js')}}"></script>
		<script type="text/javascript" src="{{ asset('web/js/moment-timezone-with-data.js') }}"></script>
		<script type="text/javascript" src="{{asset('web/js/bootstrap-datetimepicker.js')}}"></script>
        <script src="{{ asset('web/js/app.js') }}"></script>
        <script>moment.tz.setDefault("America/Chicago");</script>
        <!--
        <script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-74642451-1', 'auto');
			ga('send', 'pageview');
		</script>
		 -->

    </body>
</html>