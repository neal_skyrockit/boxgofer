<!DOCTYPE html>
<html lang='en'>
    <head>
        <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title') | Boxgofer</title>
		
		<link href='https://fonts.googleapis.com/css?family=Roboto:100' rel='stylesheet' type='text/css'>
		<link href='{{ asset("web/css/main.css") }}' rel='stylesheet' type='text/css'>
		<link href='{{ asset("web/css/responsive.css") }}' rel='stylesheet' type='text/css'>

<style>
	* { margin: 0; padding: 0; }

	html {
		background: url({{ asset('web/images/comingsoon.jpg') }}) no-repeat center center fixed;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
	}
	
	body {
      font-family: 'Roboto', sans-serif;
    }
</style>

    </head>
    <body>

    	<div id="wrapper">
	        <div id="page-wrapper">
                @yield('content')
	        </div>
        </div>
    </body>
</html>