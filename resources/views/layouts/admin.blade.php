<!DOCTYPE html>
<html lang="en" ng-app="BoxgoferAdmin">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') | Boxgofer Management</title>
    <link href='{{ asset("bk/css/bootstrap.min.css") }}' rel="stylesheet">
    <link href='{{ asset("bk/css/main.css") }}' rel="stylesheet">
    <link href='{{ asset("bk/css/responsive.css") }}' rel="stylesheet">
    <link href='{{ asset("bk/css/bootstrap-datetimepicker.css") }}' rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    <link href='{{ asset("bk/css/fullcalendar.css") }}' rel="stylesheet">

</head>

<body onload="startTime()">
    <header>
        <a href="/" class="logo"></a>
        <!-- <span id="currentDate">{{ date("l, F j, Y") }} - </span><span id="bg-timer"></span> -->
        <span id="currentDate"></span>
        <a href="/auth/logout" class="cus-btn pull-right" style="margin:30px 30px 0 0">Signout</a>
        <button type="button" onclick="window.history.back();" class="btn btn-default btn-sm pull-right" style="margin:30px 30px 0 0; height: 36px;">
          <span class="glyphicon glyphicon-step-backward"></span> Back
        </button>
    </header>
    <main>
        @include('navbar.adminsidebar')
        <div class="main-content">
            @yield('content')
        </div>
    </main>
    <div id="overlay-admin">
		<img id="loading" src="{{ asset('web/images/loading3.gif')}}" />
	</div>
    <!-- require js -->
    <script type="text/javascript" src='{{ asset("bk/js/jquery.min.js") }}'></script>
    <script type="text/javascript" src='{{ asset("bk/js/angular.min.js") }}'></script>
    <script type="text/javascript" src='{{ asset("bk/js/app.js") }}'></script>
    <script type="text/javascript" src='{{ asset("bk/js/moment.js") }}'></script>
	<script type="text/javascript" src='{{ asset("bk/js/moment-timezone-with-data.js") }}'></script>
    @yield('scripts')
    <script>
    	moment.tz.setDefault("America/Chicago");
	    function startTime() {
	    	var today = moment().format("dddd, MMMM D, YYYY hh:mm:ss");
	        document.getElementById('currentDate').innerHTML = today;
	        var t = setTimeout(startTime, 500);
	    }
    </script>
</body>

</html>