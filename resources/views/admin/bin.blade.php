@extends('layouts.admin') @section('title') Bin List @stop @section('content')

<!-- page content -->
<section class="main-container" id="client-page" ng-controller="BinController">
    <!-- main part -->
    <div class="col-xs-12 col-md-8 main-part">
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">
                    <!-- header bar -->
                    <div class="row header-bar">
                        <div class="col-md-12 col-lg-3">
                            <h3>Bins</h3>
                        </div>
                        <div class="col-md-12 col-lg-6 actions ta-l">

                        </div>
                        <div class="col-md-12 col-lg-3">

                        </div>
                    </div>
                    <!-- end header bar -->
                    <!-- data container -->
                    <div class="data-container">
                        <table class="custom-table v2 editfields ct-editfields clean">
                            <thead>
                                <tr>
                                    <th class="ta-c">
                                        <p></p>
                                    </th>
                                    <th class="ta-c">
                                        <p>Bin Counts.<br>Total Inventory</p>
                                    </th>
                                    <th class="ta-c">
                                        <p>Monthly Price<br>per Bin</p>
                                    </th>
                                    <th class="ta-c">
                                        <p>Description</p>
                                    </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($bins as $idx => $bin)
                                <tr id="bin_{{$bin->_id}}"
                                    ng-init="bins[{{$idx}}] = mappingBin('{{$bin->_id}}','{{$bin->name}}',{{$bin->total}},{{$bin->total_rented}},'{{$bin->description}}');">
                                    <td style="width: 5%;">{{$bin->name}}</td>
                                    <td style="width: 15%;">
                                        <div class="field-group" ng-class="{'error-field': bins[{{$idx}}].err_total !== undefined }">
                                            <input type="number" name="total_remain" @if($idx != 2)disabled="disabled"@endif value="{{ $bin->total - $bin->total_rented }}" min="0" />
                                            <span class="error-message" ng-bind="bins[{{$idx}}].err_total"><span>
                                        </div>
                                    </td>
                                    <td style="width: 15%;">
                                        <div class="field-group" ng-class="{'error-field': bins[{{$idx}}].err_price !== undefined }">
                                            <input name="price" value="${{ number_format($bin->price, 2) }}" />
                                            <span class="error-message" ng-bind="bins[{{$idx}}].err_price"><span>
                                        </div>
                                    </td>
                                    <td style="width: 55%;">
                                        <div class="field-group" ng-class="{'error-field': bins[{{$idx}}].err_description !== undefined }">
                                            <textarea name="description" value="{{ stripslashes($bin->description) }}" >{{ stripslashes($bin->description) }}</textarea>
                                            <span class="error-message" ng-bind="bins[{{$idx}}].err_description"><span>
                                        </div>
                                    </td>
                                    <td style="width: 10%;">
                                        <div class="field-group">
                                            <button class="cus-btn process" ng-click="editBin('{{$bin->_id}}',{{$idx}},$event)">Save</button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div>

                        </div>
                    </div>
                    <!-- end data container -->
                </div>
            </div>
        </div>
    </div>
    <!-- end main part -->
    <!-- side part -->
    <input type="checkbox" id="triggerHideInformationPart" class="hidden" />
    <div class="col-md-4 information-part hidden">
        <label for="triggerHideInformationPart" class="triggerHideInformationPart hidden-md hidden-lg"></label>
        <article class="" id="view-staff-section">
            <div class="client-information">
            	<h4>Bin Inventory</h4>
                <table class="custom-table bin-table">
                    <thead>
                        <tr>
                            <th class="text-center">
                                <p>Bin Size</p>
                            </th>
                            <th class="text-center">
                                <p>Rented</p>
                            </th>
                            <th class="text-center">
                                <p>Inventory</p>
                            </th>
                            <th class="text-center">
                                <p>Total</p>
                            </th>
                            <th>
                                <p class="text-center">Inventory<br>Months<br>Remaining</p>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                            <tr ng-repeat="bin in bins">
                                <td>@{{bin.name}}</td>
                                <td><span>@{{bin.total_rented}}<span></td>
                                <td><span>@{{bin.total - bin.total_rented}}<span></td>
                                <td><span>@{{bin.total}}</span></td>
                                <td></td>
                            </tr>
                    </tbody>
                </table>
            </div>

            <div class="bin-trans">
            	<h4>Bin Transactions</h4>
				<table class="bs-table bin-table">
                    <thead class="bs-th">
                        <tr>
                            <th>Date</th>
                            <th>Bin Size</th>
                            <th>Bins<br>Ordered</th>
                            <th>Bins<br>Removed</th>
                            <th>Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                    		<tr ng-repeat="change in changes">
                                <td ng-if="change.binDate">@{{change.binDate}}</td>
                                <td ng-if="change.binDate"><span>@{{ change.binType == 1 ? 'Large' : 'Extra Large' }}</span></td>
                                <td ng-if="change.binDate"><span>@{{change.binAdd}}<span></td>
                                <td ng-if="change.binDate"><span>@{{change.binRemove}}<span></td>
                                <td ng-if="change.binDate"><span>@{{change.binDesc}}<span></td>
                            </tr>

						<tr>
							<form name="bsform" ng-submit="bs_submit()">
								<td style="width: 23%;">
									<div class="cell pos-relative">
										<input ng-model="bs.date" name="date" id="bs-datepicker_date" placeholder="Date" class="form-control calendar text-small-size text-center">
									</div>
								</td>
								<td style="width: 15%;"><select ng-model="bs.type" class="form-control text-small-size" ng-options="option.name for option in bin_type"></select></td>
								<td style="width: 13%;">
									<input ng-model="bs.add_num" id="bs-add-num" class="form-control" name="bs_add_num" />
								</td>
								<td style="width: 13%;">
									<input ng-model="bs.remove_num" id="bs-remove-num" class="form-control" name="bs_remove_num" />
								</td>
								<td >
									<textarea ng-model="bs.desc" class="form-control" name="bs_desc" ></textarea>
								</td>
							</form>
						</tr>
                    </tbody>
                </table>
                <button class="cus-btn process pull-right" ng-click="submitted = true; bs_submit();">Submit</button>
            </div>
        </article>
    </div>
    <!-- end side part -->
</section>
<!-- end page content -->

@stop @section('scripts')
<script type="text/javascript" src='{{ asset("bk/js/bootstrap.min.js") }}'></script>
<script type="text/javascript" src='{{ asset("bk/js/bootstrap-datetimepicker.js") }}'></script>
<script type="text/javascript">
    $(function() {
        $('#datepicker_birthday').datetimepicker({
            format: 'YYYY/MM/DD'
        });
        $('#datepicker_datehire').datetimepicker({
            format: 'YYYY/MM/DD'
        });
        $('#bs-datepicker_date').datetimepicker({
        	format: 'MMM D.YYYY',
        	defaultDate: moment().tz("America/Chicago"),
        });
    });
</script>
<script>
    function triggerCreateStaff(status){
        document.getElementById('triggerCreateStaff').checked = status;
    }
</script>
@stop