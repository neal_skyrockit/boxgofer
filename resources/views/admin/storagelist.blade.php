@extends('layouts.admin')

@section('title') Dashboard @stop

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="panel-body">

			<h2>Storage</h2>

		    @if ($errors->has())
		        @foreach ($errors->all() as $error)
		            <div class='bg-danger alert'>{{ $error }}</div>
		        @endforeach
		    @endif

		    <nav class="navbar navbar-default navbar-custom" role="navigation">
			    <form class="navbar-form navbar-left" role="search" method="get">
			    	<div class="form-group">
						<span>From</span><input name="from" id="datepicker-from" type="text" id="datepicker-from" class="form-control input-sm" placeholder="Datetime From" value="{{ isset($search['from']) ? $search['from'] : '' }}">
					</div>
					<div class="form-group">
						<span>To</span><input name="to" id="datepicker-to" type="text" id="datepicker-to" class="form-control input-sm" placeholder="Datetime To" value="{{ isset($search['to']) ? $search['to'] : '' }}">
					</div>

					<div class="form-group">
						<span>Status</span>
						<select class="form-control input-sm" name="status">
							<option {{ (!isset($search['status']) || ($search['status'] == "")) ? 'selected="selected"' : '' }} value="">All</option>
							<option {{ (isset($search['status']) && $search['status'] == 3) ? 'selected="selected"' : '' }} value="3">Done</option>
							<option {{ (isset($search['status']) && $search['status'] == 2) ? 'selected="selected"' : '' }} value="2">Stored</option>
							<option {{ (isset($search['status']) && $search['status'] == 1) ? 'selected="selected"' : '' }} value="1">Transiting</option>
							<option {{ (isset($search['status']) && $search['status'] == 0) && $search['status'] != "" ? 'selected="selected"' : '' }} value="0">Home</option>
						</select>
					</div>

					<div class="form-group">
						<span>Search by</span><input name="search" type="text" class="form-control input-sm" placeholder="Id or Barcode or Phone or Address" value="{{ isset($search['search']) ? $search['search'] : '' }}" />
					</div>
					<button type="submit" class="btn btn-primary btn-sm">Search</button>
					<a href="/admin/storagelist" class="btn btn-default btn-sm">Reset</a>
				</form>
			</nav>

			<table class="table table-hover table-responsive">
				<thead>
					<tr>
						<th>Id</th>
						<th>Barcode</th>
						<th>Phone</th>
						<th>Address</th>
						<th>Status</th>
						<th>Datetime</th>
					</tr>
				</thead>
				<tbody>
					@foreach($storage as $foo)
					<tr>
						<td>{{ $foo->_id }}</td>
						<td>{{ $foo->barcode }}</td>
						<td>{{ $foo->phone }}</td>
						<td>{{ $foo->address }}</td>
						<td>{{ ($foo->status == 0) ? 'Home' : ($foo->status == 1 ? 'Transiting' : ($foo->status == 2 ? 'Stored': 'Done')) }}</td>
						<td>{{ $foo->datetime }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			<div>
				{!! $storage->render() !!}
			</div>

		</div>
	</div>
</div>
<script>
jQuery(document).ready(function($){
	$('#datepicker-from').datepicker({
	    format: 'yyyy-mm-dd'
	});
	$('#datepicker-to').datepicker({
	    format: 'yyyy-mm-dd'
	});
});
</script>

@stop