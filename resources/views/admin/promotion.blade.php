@extends('layouts.admin') @section('title') Bin List @stop
@section('content')

<!-- page content -->
<section
	class="main-container" id="client-page" ng-controller="PromoController">
	<!-- main part -->
	<div class="col-xs-12 col-md-8 main-part">
		<div class="row">
			<div class="col-md-12 part">
				<div class="content">
					<!-- header bar -->
					<div class="row header-bar">
						<div class="col-md-12 col-lg-3">
							<h3>Promotions</h3>
						</div>
						<div class="col-md-12 col-lg-6 actions ta-l"></div>
						<div class="col-md-12 col-lg-3"></div>
					</div>
					<!-- end header bar -->
					<!-- data container -->
					<div class="data-container">
						<table class="custom-table pro-custom-table">
							<thead>
								<tr>
									<form id="promo-form" name="promoform" ng-submit="promo_submit()">

										<td>
											<div class="form-group promo-form-code">
												<input type="text" class="form-control" ng-model="add.code" placeholder="Code (only letters and numbers)" />
											</div>
										</td>
										<td>
											<div class="form-group promo-form-percent">
												<input class="form-control" type="number" ng-model="add.percent" min="0" max="100"
													step="1" placeholder="%" />
											</div>
										</td>
										<td style="width: 10%;">
											<div class="form-group">
												<button class="cus-btn process">Add</button>
											</div>
										</td>

									</form>

								</tr>

								<tr>
									<td class="ta-c">Code</td>
									<td class="ta-c">Percent Off</td>
									<td class="ta-c"></td>
								</tr>

							</thead>
							<tbody>

								@foreach($promos as $foo)
								<tr id="promo_{{ $foo->id }}" class="promo-class">
									<td class="ta-c">
										<p>{{ $foo->id }}</p>
									</td>
									<td class="ta-c">
										<p>{{ $foo->percent_off }}%</p>
									</td>
									<td class="ta-c">
										<p>
											<button class="btn btn-danger" ng-click="promo_deletesubmit('{{ $foo->id }}')">Delete</button>
										</p>
									</td>
								</tr>
								@endforeach

							</tbody>
						</table>

					</div>
					<!-- end data container -->
				</div>
			</div>
		</div>
	</div>
	<!-- end main part -->

	<!-- side part -->
	<!-- <div class="col-md-4 information-part" ng-init="getPromoInfo('{{ count($promos) > 0 ? $promos[0]['_id'] : '' }}')" ng-show="promo._id != ''">
		<article class="" id="view-staff-section">
			<div class="client-information">
				<h3>Edit</h3>

				<form id="promo-editform" name="promoeditform">

					<div class="form-group promo-eform-code">
						<label for="promo-code">Code:</label>
						<input id="promo-code" type="text" class="form-control" name="code" ng-model="promo.code" placeholder="Code (only letters and numbers)" />
					</div>

					<div class="form-group promo-eform-percent">
						<label for="promo-percent">Percent:</label>
						<input id="promo-percent" class="form-control" type="number" ng-model="promo.percent" name="percent" min="0" max="100" step="1" placeholder="%" />
					</div>

					<div class="form-group promo-eform-status">
						<label for="promo-status">Status:</label>
						<select id="promo-status" class="form-control" name="status"
								ng-options="option.value for option in statusDefault track by option.key"
      							ng-model="promo.status">
						</select>
					</div>

					<div class="form-group">
						<button class="cus-btn" ng-click="promo_editordeletesubmit('edit')">Save</button>
						<button class="btn btn-danger" ng-click="promo_editordeletesubmit('delete')">Delete</button>
					</div>

				</form>

			</div>
		</article>
	</div> -->
	<!-- end side part -->

</section>
<!-- end page content -->

@stop @section('scripts')
<script
	type="text/javascript" src='{{ asset("bk/js/bootstrap.min.js") }}'></script>
@stop
