@extends('layouts.admin') @section('title') Dashboard @stop @section('content')
<section class="main-container" id="dashboard-page" ng-controller="ChartController">
    <!-- main part -->
    <div class="col-xs-12 main-part">
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">
                    <h3 class="title">Bins in Storage</h3>
                    <div id="BinChart" style="height:300px">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">
                    <h3 class="title">Cash on Hand</h3>
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">
                    <h3 class="title">Daily Schedule Bar Chart</h3>
                    <div id="DailyChart" style="height:300px">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">
                    <h3 class="title">Ordered Bins by Zip Code</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">
                    <h3 class="title">Web Traffic</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- end main part -->
</section>
@stop @section('scripts')
<script type="text/javascript" src='{{ asset("bk/js/canvasjs.js") }}'></script>
<script>
    
    function dataPointObject(value,name){
        return {
            y: value,
            label: name
        }
    }
    
    function dataObject(color,listDataPoint,legendText){
        return {
            type: "stackedColumn",
                        
            showInLegend: "true",
            color:color,
            legendText: legendText || "",
			name:legendText || "",
            dataPoints:listDataPoint || []
        };
    }
 
    function BinChart(){
        var _ = this;
        
        _.axisY = ['orderedL','orderedXL','orderedOther','unusedL','unusedXL'];
        _.legend = ['Ordered - L','Ordered - XL','Ordered - Other','Unused - L','Unused - XL'];
        _.color = ['#4e81bd','#c1504c','#9bbb5a','#8064a1','#4aadc7'];
        _.listData = [];          
    }
    
    BinChart.prototype.render = function(id){
        var _ = this;    
        _.chart = new CanvasJS.Chart(id,
                {
                    data : _.listData ,
                    toolTip:{   
                        content: "{name}: {y}"      
                    }
                }); 
        _.chart.render();
    }
    
    BinChart.prototype.mappingData = function(datas){
        var _ = this;
        _.listData = [];

        for(var i = 0; i < 5;i++){
            var obj = dataObject(_.color[i],datas[_.axisY[i]],_.legend[i]) ;
            _.listData.push(obj);
        }
        console.log(_.listData);
    }
    
    function DailyChart(){
        var _ = this;
        _.axisY = ['L','XL','Other'];
        _.legend = ['Large','XL','Other items'];
        _.color = ['#4e81bd','#c1504c','#9bbb5a'];
        _.listData = [];
    }
    
    DailyChart.prototype = BinChart.prototype;
    
    function ZipcodeChart(){
        var _ = this;
        _.axisY = ['L','XL','Other'];
        _.legend = ['Large','XL','Other items'];
        _.color = ['#4e81bd','#c1504c','#9bbb5a'];
        _.listData = [];
    }
    
    DailyChart.prototype = BinChart.prototype;
            
</script>
@stop