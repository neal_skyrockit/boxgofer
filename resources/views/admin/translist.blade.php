@extends('layouts.admin')

@section('title') Orders @stop

@section('content')
<!-- page content -->
<section class="main-container" id="transaction-page" ng-controller="TransactionController">
    <!-- main part -->
    <div class="col-xs-8 main-part">
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">
                    <!-- header bar -->
                    <div class="row header-bar">
                        <div class="col-md-12 col-lg-2">
                            <h3>Orders</h3>
                        </div>
                        <div class="col-md-12 col-lg-10">
                            <form action="/admin/orders" method="get" class="search-group">
                                <div class="cell">
                                    <div class="display-table col-md-6 col-xs-12">
                                        <div class="cell labelContainer">Date</div>
                                        <div class="cell">
                                            <input name="from" id="datepicker_from" value="{{ isset($search['from']) ? $search['from'] : '' }}"  class="form-control calendar">
                                        </div>
                                    </div>
                                    <p class="visible-xs"></p>
                                    <div class="display-table col-md-6 col-xs-12">
                                        <div class="cell labelContainer">To</div>
                                        <div class="cell">
                                            <input name="to" id="datepicker_to" value="{{ isset($search['to']) ? $search['to'] : '' }}" class="form-control calendar">
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="cell btnContainer">
                                    <button type="submit" class="btnSearch">
                                        <i></i>
                                    </button>
                                </div> -->
                                <div class="cell">
                                    <input name="search" type="text" class="form-control nobor-r" value="{{ isset($search['search']) ? $search['search'] : '' }}" placeholder="Search Order">
                                </div>
                                <div class="cell btnContainer">
                                    <button type="submit" class="btnSearch nobor-l">
                                        <i></i>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end header bar -->
                    <!-- data container -->
                    <div class="data-container">
                        <table class="data-table">
                            <thead>
                                <tr>
                                    <th class="hidden-xs">UNIT</th>
                                    <th>Order</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>CustomerId</th>
                                    <th>Items</th>
                                    @if(Auth::guard('managers')->user()->role != 'driver')
                                    <th class="hidden-xs">Day Started</th>
                                    @endif
                                    <th>Order Payment</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($trans as $unit => $t)
                                <tr id="tras_{{$t->_id}}"
                                    ng-click="getTransInfo('{{$t->_id}}')">

                                    <td class="ta-c hidden-xs">{{ ($unit + 1) }}</td>
                                    <td class="wb-b cl-0"><a href="/admin/bintracking?search={{$t->transcode}}">Order {{$t->transcode}}</a></td>

                                    <td class="cl-0"><span>{{$UserInfo[$t->user_id]['name']}}</span></td>
                                    <td class="wb-b crop"><span>{{$UserInfo[$t->user_id]['email']}}</span></td>
                                    <th class="crop">{{$UserInfo[$t->user_id]['customerId']}}</th>
                                    <td>
                                        {{$StorageInfo[$t->_id]['l']}}L - {{$StorageInfo[$t->_id]['e']}}X - {{$StorageInfo[$t->_id]['o']}}O
                                    </td>
                                    @if(Auth::guard('managers')->user()->role != 'driver')
                                    <td class="hidden-xs"><span>{{ date("M d.Y", strtotime($t->created_at))  }}</span></td>
                                    @endif
                                    <td class="cl-0 ta-c"><span class="currency">{{isset($t->paymentInfo['total']) ? $t->paymentInfo['total'] : 0}}</span></td>
                                    <td class="action-group bin-track-btn-2">
                                    @if ($t->status != 9)
                                    	<button class="btn cancel-btn act-cancel" ng-click="cancelOrder('{{ $t->_id }}')">Cancel</button>
                                    @else
                                    	<div class="text-danger">Done</div>
                                    @endif
                                    </td>
                                    <!-- <td class="action-group">
                                        <span class="action" title="{{ $t->status == 0 ? 'This is pending' : ($t->status == 1 ? 'This is working' : 'This is finish') }}" data-status="{{ $t->status == 0 ? 'pending' : ($t->status == 1 ? 'ok' : 'done') }}"></span>
                                    </td> -->
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div>
                            {!! $trans->render() !!}
                        </div>
                    </div>
                    <!-- end data container -->
                </div>
            </div>
        </div>
    </div>
    <!-- end main part -->
    <!-- side part -->
    <div ng-init="getTransInfo('{{ count($trans) != 0 ? $trans[0]->_id : 0 }}')">
	    <div class="col-xs-4 information-part" ng-if="transaction.bins">
	        <div class="client-information">
	            <h3 class="header order-num font-size-18" ng-bind="transaction.info.transcode"></h3>
	            <div class="bins-amount-container">
	                <div class="row">
	                    <span class="col-xs-6 col-sm-4 col-md-3 bin-number "
	                    ng-class="{ 'current': b._id == bin._id}"
	                    ng-repeat="b in transaction.bins"
	                    ng-click="detail(b,($index+1))">@{{'Bin '+ ($index+1)}}</span>
	                </div>
	            </div>
	            <div class="current-bin">
	                <h3>Bin @{{bin.idx}}</h3>
	                <b><p class="ta-c" ng-bind="bin.barcode"></p></b>
	            </div>
	            <div class="image-section">
	                <input id="upload-image-trigger" name="image-trigger" type="radio" class="hidden" />
	                <input id="list-image-trigger" name="image-trigger" type="radio" class="hidden" />
	                <div class="trigger-label-container">
	                <span onclick="triggerClick('upload-image-trigger')" class="bt-img-trigger"><img class="bin-track-btn bin-track-img" src="{{ asset('bk/images/add photo.png') }}" /></span>
	            	<span onclick="triggerClick('list-image-trigger')" class="bt-list-img-trigger"><img class="bin-track-btn bin-track-img" src="{{ asset('bk/images/viewphoto.png') }}" /></span>
	                    <!--
	                    <label class="upload-image-trigger-label" onclick="triggerClick('upload-image-trigger')"></label>
	                    <label class="list-image-trigger-label" onclick="triggerClick('list-image-trigger')"></label>
	                     -->
	                </div>
	                <div class="list-image-group">
	                    <div class="list-image-container">
	                        <div class="list-image">
	                        	<div ng-repeat="image in bin.images">
	                            	<img class="upimage" src="/public/uploads/@{{image}}" /><img ng-click="deleteimage(bin._id, image);" class="delimage" width="20" height="20" src="{{ asset('public/web/images/imgpsh_fullsize.png') }}" />
	                            </div>
	                        </div>
	                        <button class="btn fullbtn btn-close" onclick="closeImageTrigger('list-image-trigger')">Close</button>
	                    </div>
	                </div>
	                <div class="upload-image-group">
	                    <input id="file" type="file" class="hidden" onchange="chooseImage(this)" value="" />
	                    <div class="upload-image-container">
	                        <table id="image-information">
	                            <tr>
	                                <td style="width:50px">
	                                    <img id="bin_upload_image" class="disimg" />
	                                </td>
	                                <td style="width:50px" class="wb-b-i name"></td>
	                            </tr>
	                        </table>
	                        <button class="btn btn-success" ng-click="uploadImage(bin)">Upload</button>
	                        <button class="btn" onclick="closeImageTrigger('upload-image-trigger')">Cancel</button>
	                    </div>
	                </div>
	            </div>

	            @if(Auth::guard('managers')->user()->role != 'driver')
	            <table class="custom-table bg-grey">
	                <tr>
	                    <td>Start Date:</td>
	                    <td class="w150"><span ng-bind="convertDate(bin.created_at)"></span></td>
	                </tr>
	            </table>

	            <h3 class="fs18 cl-0 nomargin padding2020">Activity :</h3>

	            <table class="custom-table custom-table-new order-edit-table">
	                <thead>
	                    <tr class="clean bg-grey">
	                        <th class="ta-c" style="width:50%">Drop-off</th>
	                        <th class="ta-c">Pick-up</th>
	                    </tr>
	                </thead>
	                <tbody class="ct-new">
	                    <tr ng-repeat="dt in bin.dropofftime">
	                        <td ng-if="dt == editDropOffDate" class="w150 ta-c bin-last-row">
	                        	<span class="cursor-pointer" ng-click="openDateTimeModal(bin._id, 'Drop-off')">
	                        		<span ng-bind="convertDate(dt, true, true)"></span>
	                        		<img src="{{ asset('bk/images/change_activity_day.png') }}" />
	                        	</span>
	                        </td>
	                        <td ng-if="dt != editDropOffDate" class="ta-c"><span ng-bind="convertDate(dt, true, true)"></span></td>

	                        <td ng-if="bin.pickuptime.length && bin.pickuptime[$index] == editPickUpDate" class="ta-c bin-last-row">
	                        	<span class="cursor-pointer" ng-click="openDateTimeModal(bin._id, 'Pick-up')">
	                        		<span ng-bind="convertDate(bin.pickuptime[$index], true, true)"></span>
	                        		<img src="{{ asset('bk/images/change_activity_day.png') }}" />
	                        	</span>
	                        </td>
	                        <td ng-if="bin.pickuptime.length == 0 || bin.pickuptime[$index] != editPickUpDate" class="w150 ta-c"><span ng-bind="convertDate(bin.pickuptime[$index], true, true)"></span></td>
	                    </tr>
	                </tbody>
	               <tfoot ng-show="bin.canceltime">
	                    <tr class="clean bg-grey">
	                        <td>Cancel:</td>
	                        <td class="w150"><span ng-bind="bin.canceltime"></span></td>
	                    </tr>
	                </tfoot>

	            </table>
	            @endif

	            <table class="custom-table" style="margin-top:10px" ng-if="transaction.info.paymentInfo.promo_percent_off === undefined || transaction.info.paymentInfo.promo_percent_off == 0">
	                <tr class="clean bg-grey">
	                    <td>
	                        <h4 class="cl-0 nomargin">Total Payment</h4>
	                    </td>
	                    <td class="w150 cl-0">
	                        <h4 class="currency nomargin" ng-bind="transaction.info.paymentInfo.total || 0"></h4>
	                    </td>
	                </tr>

	            </table>

	            <table class="custom-table" style="margin-top:10px" ng-if="transaction.info.paymentInfo.promo_percent_off !== undefined && transaction.info.paymentInfo.promo_percent_off > 0">

	                <tr class="clean">
	                    <td>
	                        <h4 class="cl-0 nomargin">Discount</h4>
	                    </td>
	                    <td class="w150 cl-0">
	                        <h4 class="nomargin">$@{{ transaction.info.paymentInfo.promo_percent_off * transaction.info.paymentInfo.total / 100 }} (@{{ transaction.info.paymentInfo.promo_percent_off }}%)</h4>
	                    </td>
	                </tr>

	                <tr class="clean bg-grey">
	                    <td>
	                        <h4 class="cl-0 nomargin">Total Payment</h4>
	                    </td>
	                    <td class="w150 cl-0">
	                        <h4 class="currency nomargin" ng-bind="transaction.info.paymentInfo.total - (transaction.info.paymentInfo.promo_percent_off * transaction.info.paymentInfo.total / 100) || 0"></h4>
	                    </td>
	                </tr>

	            </table>

	        </div>

	    </div>
	</div>
    <!-- end side part -->

    <!-- start modal -->
	@include('modals.bindatetime')

</section>
<!-- end page content -->

@stop @section('scripts')
<script type="text/javascript" src='{{ asset("bk/js/bootstrap.min.js") }}'></script>
<script type="text/javascript" src='{{ asset("bk/js/bootstrap-datetimepicker.js") }}'></script>
<script type="text/javascript">
	var selectedFromDay = '{{ isset($search["from"]) ? $search["from"] : "" }}',
		selectedToDay = '{{ isset($search["to"]) ? $search["to"] : "" }}';
    $(function() {
        $('#datepicker_from').datetimepicker({
        	format: 'MMM D.YYYY'
        });
        $('#datepicker_to').datetimepicker({
        	format: 'MMM D.YYYY'
        });

        if (selectedFromDay != '') $('#datepicker_from').val(selectedFromDay);
        if (selectedToDay != '') $('#datepicker_to').val(selectedToDay);
    });
</script>
<script>
    function closeImageTrigger(strEle) {
        document.getElementById(strEle).checked = false;
    }

    function chooseImage(ele) {
        var file = ele.files[0];
        var ti = document.getElementById('image-information');
        var img = document.getElementById('bin_upload_image');
        if (file != null && file != undefined) {
            ti.querySelector('.name').innerHTML = file.name;

            var reader = new FileReader();
            reader.onload = (function(theFile) {
                return function(e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    img.src = e.target.result;
                };
            })(file);
            // Read in the image file as a data URL.
            reader.readAsDataURL(file);
            document.getElementById('upload-image-trigger').checked = true;
        }else{
            img.removeAttribute('src');
             ti.querySelector('.name').innerHTML = '';
        }
    }

    function triggerClick(strEle) {
        var ele = document.getElementById(strEle);

        if (strEle == 'upload-image-trigger') {
            var ele = document.getElementById('file');
            var file = ele.files[0];
            if (file != null && file != undefined)
                document.getElementById('upload-image-trigger').checked = true;

            ele.click();
        } else {
            if (ele.checked === true) {
                ele.checked = false;
            } else {
                ele.checked = true;
            }
        }
    }

    function clearFile(){
        document.getElementById('file').value = "";
         document.getElementById('file').onchange();
         var inps = document.querySelectorAll('input[name="image-trigger"]') ;
         [].forEach.call(inps,function(obj){
             obj.checked = false;
         });
    }

    $(function() {
    	$('#datepicker').datetimepicker({
    		useCurrent: false,
            minDate: moment().add(1, 'd').format('MM/DD/YYYY'),
            format: 'MM/DD/YYYY',
            daysOfWeekDisabled: [0]
        });
    });
</script>
@stop