@extends('layouts.admin')

@section('title') Bin List @stop

 @section('content')

<!-- page content -->
<section class="main-container" id="client-page" ng-controller="BinController">
    <!-- main part -->
    <div class="col-xs-12 col-md-8 main-part">
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">
                    <!-- header bar -->
                    <div class="row header-bar">
                        <div class="col-md-12 col-lg-3">
                            <h3>Calendar</h3>
                        </div>
                        <div class="col-md-12 col-lg-6 actions ta-l">

                        </div>
                        <div class="col-md-12 col-lg-3">

                        </div>
                    </div>
                    <!-- end header bar -->
                    <!-- data container -->
                    <div class="data-container">
                        <div class="fc fc-ltr fc-unthemed" id="calendar">
                        </div>
                    </div>
                    <!-- end data container -->
                </div>
            </div>
        </div>
    </div>
    <!-- end main part -->

    <!-- side part -->
    <div class="col-xs-4 information-part information-part-cus">
        <h3 class="header">Schedule</h3>
        <hr/>
        <div class="schedule-group">
            <p class="datetime"> Today, {{ $arr['today'][0] }}</p>
            <div class="tasks">
			@if(count($arr['today'][1]) > 0)
				@foreach($arr['today'][1] as $foo)
                <div class="task">
                    <p class="time">{{ $foo['time'] }}</p>
                    <p class="content">{{ $foo['storagestatus'] }} {{ ($foo['totalbin'] == 1) ? $foo['totalbin'] . ' bin' : $foo['totalbin'] . ' bins' }} ({{ CommonHelper::processSpecialStr($foo['itemtype']) }}) at {{ $foo['address'] }} </p>
                </div>
                @endforeach
			@endif
            </div>
        </div>
        <div class="schedule-group">
            <p class="datetime">{{ $arr['tomorrow'][0] }}</p>
            <div class="tasks">
			@if(count($arr['tomorrow'][1]) > 0)
                @foreach($arr['tomorrow'][1] as $foo)
                <div class="task">
                    <p class="time">{{ CommonHelper::processTime($foo['datetime']) }}</p>
                    <p class="content">{{ $foo['storagestatus'] }} {{ ($foo['totalbin'] == 1) ? $foo['totalbin'] . ' bin' : $foo['totalbin'] . ' bins' }} ({{ CommonHelper::processSpecialStr($foo['itemtype']) }}) at {{ $foo['address'] }} </p>
                </div>
                @endforeach
			@endif
            </div>
        </div>
        <div class="schedule-group">
            <p class="datetime">{{ $arr['aftertomorrow'][0] }}</p>
            <div class="tasks">
			@if(count($arr['aftertomorrow'][1]) > 0)
                @foreach($arr['aftertomorrow'][1] as $foo)
                <div class="task">
                    <p class="time">{{ CommonHelper::processTime($foo['datetime']) }}</p>
                    <p class="content">{{ $foo['storagestatus'] }} {{ ($foo['totalbin'] == 1) ? $foo['totalbin'] . ' bin' : $foo['totalbin'] . ' bins' }} ({{ CommonHelper::processSpecialStr($foo['itemtype']) }}) at {{ $foo['address'] }} </p>
                </div>
                @endforeach
			@endif
            </div>
        </div>
    </div>
    <!-- end side part -->

</section>
<!-- end page content -->

@stop
@section('scripts')
<script type="text/javascript" src='{{ asset("bk/js/fullcalendar.min.js") }}'></script>
<script>
    $(document).ready(function(){

        $('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				//right: 'month,basicWeek,basicDay'
                right:''
			},
            height: 500,
			//defaultDate: '2016-01-12',
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
				{
				    title  : 'event1',
				    start  : '2016-05-01'
				},
				{
				    title  : 'event2',
				    start  : '2016-05-05',
				    end    : '2016-05-27'
				},
				{
				    title  : 'event3',
				    start  : '2016-05-09T12:30:00',
				    allDay : false // will make the time show
				}
			],
			dayClick: function (date, jsEvent, view) {
				console.log(date);
				$(".bg-info").removeClass("bg-info");
			    $("td[data-date="+date.format('YYYY-MM-DD')+"]").addClass("bg-info");
		   }
		});

    });
</script>
@stop