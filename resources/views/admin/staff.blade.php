@extends('layouts.admin')

@section('title') Staff List

@stop @section('content')

<!-- page content -->
<section class="main-container" id="staff-page" ng-controller="StaffController">
    <!-- main part -->
    <div class="col-xs-12 col-md-8 main-part">
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">
                    <!-- header bar -->
                    <div class="row header-bar">
                        <div class="col-md-3 col-lg-3">
                            <h3>Staffs</h3>
                        </div>
                        <div class="col-md-9 col-lg-9 actions ta-l">
                        	<form action="/admin/staff" method="post" class="search-group pull-right">
                                <div class="cell">
                                    <input name="search" type="text" class="form-control nobor-r" placeholder="Search Name">
                                </div>
                                <div class="cell btnContainer">
                                    <button class="btnSearch nobor-l">
                                        <i></i>
                                    </button>
                                </div>
                            </form>
                            <a class="cus-btn pull-right" onclick="triggerCreateStaff(true)" ng-click="changeToCreate()">Create Staff</a>
                            <!--<form id="form_upload_excel" style="display:inline-block" method="post" action="/admin/upload_excel" enctype="multipart/form-data">
                                <a  class="cus-btn" href="{{asset('download/Template.csv')}}">Template</a>
                                <label for="excelFile" class="cus-btn" >Excel Upload</label>
                                <input id="excelFile" name="excelfile" type="file" class="hidden" onchange="document.getElementById('form_upload_excel').submit()"/>
                            </form>-->
                        </div>
                    </div>
                    <!-- end header bar -->
                    <!-- data container -->
                    <div class="data-container">
                        <?php if(isset($failUpload)){?>
                        @foreach($failUpload as $fail)
                        <p onclick="this.remove()" class="error">{{$fail['email'] . ' - ' . $fail['error']}}</p>
                        @endforeach
                        <?php } ?>
                        <table class="data-table">
                            <thead>
                                <tr>
                                    <th class="hidden-sm hidden-xs">UNIT</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>SS#</th>
                                    <th>Driver License</th>
                                    <th class="hidden-sm hidden-xs">Position</th>
                                    <th>Salary</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($staffs as $unit => $staff)
                                    <tr id="staff_{{$staff->_id}}"
                                        onclick="triggerCreateStaff(false)"
                                        ng-click="getStaffInfo('{{$staff->_id}}')">
                                        <td class="hidden-sm hidden-xs">{{ ($unit + 1) }}</td>
                                        <td><span class="cl-0">{{ $staff->first_name .' '. $staff->name }}</span></td>
                                        <td class="wb-b">{{$staff->phone}}</td>
                                        <td class="wb-b">{{$staff->ss}}</td>
                                        <td class="wb-b">{{$staff->driverlicense}}</td>
                                        <td class="hidden-sm hidden-xs">{{$staff->position}}</td>
                                        <td><span class="currency cl-0">{{$staff->salary}}</span></td>
                                        <td class="ta-c">
                                            <!-- <span class="action" data-status="{{$staff->status == 1 ? 'ok' : ($staff->status == 2 ? 'deny' : 'cancel')  }}"></span> -->
                                            {{ucfirst($staff->role)}}
                                        </td>
                                        <td class="text-center"><span class="btn btn-success margin-right-5" ng-click="changeToEdit()">Edit</span><span class="btn btn-danger" ng-click="deleteStaff('{{$staff->_id}}')">Delete</span></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div>
                            {!! $staffs->render() !!}
                        </div>
                    </div>
                    <!-- end data container -->
                </div>
            </div>
        </div>
    </div>
    <!-- end main part -->
    <!-- side part -->
    <input type="checkbox" id="triggerHideInformationPart" class="hidden" />
    <div class="col-md-4 information-part hidden" ng-init="getStaffInfo('{{ count($staffs) != 0 ? $staffs[0]->_id : 0 }}')">
        <label for="triggerHideInformationPart" class="triggerHideInformationPart hidden-md hidden-lg"></label>
        <input id="triggerCreateStaff" type="checkbox" class="triggerDisplay hidden"/>
        <article class="staff-information triggerDisplay-off" id="view-staff-section">
            <h3 class="header">@{{staff.name}}</h3>
            <div class="client-information">
                <table class="custom-table">
                    <tr>
                        <td>Name</td>
                        <td><span ng-bind="staff.first_name +' '+ staff.name"></span></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td class="wb-b"><span ng-bind="staff.email"></span></td>
                    </tr>
                    <tr>
                        <td>Birthday</td>
                        <td><span ng-bind="staff.birthday"></span></td>
                    </tr>
                    <tr>
                        <td>Date hired</td>
                        <td><span ng-bind="staff.datehired"></span></td>
                    </tr>
                    <tr>
                        <td>SS</td>
                        <td><span ng-bind="staff.ss"></span></td>
                    </tr>
                    <tr>
                        <td>Driver license</td>
                        <td><span ng-bind="staff.driverlicense"></span></td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td><span ng-bind="staff.phone"></span></td>
                    </tr>
                    <tr>
                        <td>Salary</td>
                        <td><span class="currency" ng-bind="staff.salary"><span></td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td><span ng-bind="staff.address"></span></td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td><span ng-bind="staff.city"></span></td>
                    </tr>
                    <tr>
                        <td>Zip Code</td>
                        <td><span ng-bind="staff.zipcode"></span></td>
                    </tr>
                    <tr>
                        <td>Position</td>
                        <td><span ng-bind="staff.position"></span></td>
                    </tr>
                    <tr>
                        <td>Comments</td>
                        <td><span ng-bind="staff.comments"></span></td>
                    </tr>
                </table>
            </div>
        </article>
        <!-- start edit staff -->
        <article class="staff-information triggerDisplay-on" id="edit-staff-section">
            <h3 class="header">@{{titleSection}}</h3>
            <div class="client-information">
                <form id="form_createstaff" method="post" action="/admin/upsertStaff">
                    <table class="custom-table clean editfields">
                    	<tr>
                            <td>First Name</td>
                            <td>
                                <input name="first_name"  ng-model="staffModel.first_name"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Last Name</td>
                            <td>
                                <input name="name"  ng-model="staffModel.name"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>
                                <input name="Email" ng-model="staffModel.email"/>
                            </td>
                        </tr>
                        <tr ng-show="titleSection == 'Create Staff'">
                            <td>Password</td>
                            <td>
                                <input type="password" name="Password" ng-model="staffModel.password"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Role</td>
                            <td>
                                <select class="form-control border-grey" name="Role" ng-model="staffModel.role">
							      <option value="manager">Manager</option>
							      <option value="admin">Admin</option>
							      <option value="operator">Operator</option>
							      <option value="driver">Driver/Warehouse</option>
							    </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Birthday</td>
                            <td>
                                <input id="datepicker_birthday" name="Birthday" ng-model="staffModel.birthday" />
                            </td>
                        </tr>
                        <tr>
                            <td>Date hired</td>
                            <td>
                                <input id="datepicker_datehire"  name="Datehired" ng-model="staffModel.datehired" />
                            </td>
                        </tr>
                        <tr>
                            <td>SS</td>
                            <td>
                                <input name="SS" type="number" ng-model="staffModel.ss" />
                            </td>
                        </tr>
                        <tr>
                            <td>Driver license</td>
                            <td>
                                <input name="DriverLicense" ng-model="staffModel.driverlicense" />
                            </td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td>
                                <input name="Phone" ng-model="staffModel.phone" />
                            </td>
                        </tr>
                        <tr>
                            <td>Salary</td>
                            <td>
                                <input name="Salary" type="number" ng-model="staffModel.salary" />
                            </td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td>
                                <input name="Address" ng-model="staffModel.address" />
                            </td>
                        </tr>
                        <tr>
                            <td>City</td>
                            <td>
                                <input name="City" ng-model="staffModel.city" />
                            </td>
                        </tr>
                        <tr>
                            <td>Zip Code</td>
                            <td>
                                <input name="Zipcode" type="number" ng-model="staffModel.zipcode" />
                            </td>
                        </tr>
                        <tr>
                            <td>Position</td>
                            <td>
                                <input  name="Position" ng-model="staffModel.position" />
                            </td>
                        </tr>
                        <tr>
                            <td>Comments</td>
                            <td>
                                <input  name="Comments" ng-model="staffModel.comments" />
                            </td>
                        </tr>
                    </table>
                        <p class="error paddingL20" ng-repeat="err in errors" ng-bind="err"></p>
                    <div class="row" style="padding:0;margin:12px 0;">
                        <button class="cus-btn pull-right" type="button" ng-click="upsertStaff()">Save</button>
                        <button ng-show="titleSection == 'Create Staff'" class="cus-btn pull-right" type="button" ng-click="resetForm()">Reset</button>
                    </div>
                <form>
            </div>
        </article>
        <!-- end edit staff -->
    </div>
    <!-- end side part -->
</section>
<!-- end page content -->

@stop @section('scripts')
<script type="text/javascript" src='{{ asset("bk/js/bootstrap.min.js") }}'></script>
<script type="text/javascript" src='{{ asset("bk/js/bootstrap-datetimepicker.js") }}'></script>
<script type="text/javascript">
    $(function() {
        $('#datepicker_birthday').datetimepicker({
            format: 'YYYY/MM/DD'
        });
        $('#datepicker_datehire').datetimepicker({
            format: 'YYYY/MM/DD'
        });
    });
</script>
<script>
    function triggerCreateStaff(status){
        document.getElementById('triggerCreateStaff').checked = status;
    }
</script>
@stop