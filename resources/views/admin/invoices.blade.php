@extends('layouts.admin')

@section('title') Invoice List

@stop @section('content')

<!-- page content -->
<section class="main-container" id="client-page" ng-controller="InvoicesController">
    <!-- main part -->
    <div class="col-xs-12 col-md-8 main-part">
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">
                    <!-- header bar -->
                    <div class="row header-bar">
                        <div class="col-md-12 col-lg-3">
                            <h3>Invoices</h3>
                        </div>
                        <div class="col-md-12 col-lg-9">
                            <form action="/admin/invoices" method="get" class="search-group">

                                <div class="cell">
                                    <div class="display-table col-md-6 col-xs-12">
                                        <!-- <div class="cell labelContainerInvoice">Date</div> -->
                                        <div class="cell">
                                            <input name="from" id="datepicker_from" placeholder="Date From" class="form-control calendar" value="">
                                        </div>
                                    </div>

                                    <div class="display-table col-md-6 col-xs-12">
                                        <!-- <div class="cell labelContainerInvoice">To</div> -->
                                        <div class="cell">
                                            <input name="to" id="datepicker_to" placeholder="Date To" class="form-control calendar" value="">
                                        </div>
                                    </div>

                                </div>

                                <div class="cell">
                                    <input name="search" type="text" class="form-control nobor-r" placeholder="Number or Name" value="{{ isset($arr['search']) ? $arr['search'] : '' }}">
                                </div>

                                <div class="cell btnContainer">
                                    <button type="submit" class="btnSearch nobor-l">
                                        <i></i>
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                    <!-- end header bar -->
                    <!-- data container -->
                    <div class="data-container">
                        <table class="data-table">
                            <thead>
                                <tr>
                                    <!-- <th class="hidden-sm hidden-xs">UNIT</th> -->
                                    <th>BG Number</th>
                                    <th>Stripe Number</th>
                                    <th>Date</th>
                                    <th>Customer Name</th>
                                    <th>Items Billed</th>
                                    <th>Payment Type</th>
                                    <th>Invoice Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($invoices as $k => $invoice)
                                    <tr id="invoice_{{ $invoice->_id }}"
                                        ng-click="getInvoiceInfo('{{ $invoice->_id }}')" class="invoice-class">

                                        <!-- <td class="hidden-sm hidden-xs">{{ ($k + 1) }}</td>getInvoiceInfo -->
                                        <td><span class="cl-0">{{ $invoice->invoice_num }}</span></td>
                                        <td class="wb-b">{{ $invoice->invoice_id }}</td>
                                        <td class="wb-b">{{ $invoice->dayspay }}</td>
                                        <td class="wb-b">{{ $invoice->clientname }}</td>
                                        <td class="wb-b">{{ $invoice->items }}</td>
                                        <td class="hidden-sm hidden-xs">{{ $invoice->payby }}</td>
                                        <td><span class="cl-0">{{ $invoice->amount }} {{ $invoice->currency }}</span></td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div>
                            {!! $invoices->render() !!}
                        </div>
                    </div>
                    <!-- end data container -->
                </div>
            </div>
        </div>
    </div>
    <!-- end main part -->

    <!-- side part -->
    <div class="col-md-4 information-part hidden" ng-init="getInvoiceDefault('{{ count($invoices) > 0 ? $invoices[0]['_id'] : 0 }}')">
        <article id="print-invoice" class="client-information invoices-cus" ng-show="invoice.invoice_id != ''">
                <div id="invoice-container" style="padding: 0 15px;">
					<h1 style="text-align: center;">INVOICE</h1>
					<table width="100%">
						<tr>
							<td width="55%"></td>
							<td>
								<div style="font-weight: bold;">Date: <span ng-bind="invoice.dayspay"></span></div>
								<div style="font-weight: bold;">Invoice Number: <span ng-bind="invoice.invoice_num"></span></div>
							</td>
						</tr>
					</table><br />

					<table width="100%">
						<tr>
							<td width="13%" style="vertical-align: top; font-weight: bold;">Bill to:</td>
							<td width="45%">
								<div ng-bind="invoice.clientname"></div>
								<div ng-bind="invoice.clientaddress"></div>
							</td>
							<td style="vertical-align: top;">
								<div><span style="font-weight: bold;">Phone:</span> <span ng-bind="invoice.clientphone"></span></div>
								<div><span style="font-weight: bold;">Customer Number:</span> <span ng-bind="invoice.user_id"></span></div>
							</td>
						</tr>
					</table><br />

					<table style="border: 1px solid #ddd; border-collapse: collapse; width: 100%; max-width: 100%; margin-bottom: 20px;">
						<tr style="background: #5A5B5D !important; color: white; font-size: 16px;">
							<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">Item</th>
							<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">Description</th>
							<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">Quantity</th>
							<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">Unit Cost</th>
							<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">Subtotal</th>
						</tr>

						<tr style="text-align: center;" ng-repeat="item in invoice.data">
							<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;" >@{{ item.itemType }}</th>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top; text-align: justify;">@{{ (item.itemType == 'L') ? 'Large Bin' : ((item.itemType == 'E') ? 'Extra Large Bin' : 'Other Stuff') }}</td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">@{{ item.quantity }}</td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">@{{ item.amountOneItemFormat }}</td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">@{{ item.amount }}</td>
						</tr>

						<tr style="text-align: center;" ng-if="invoice.itemType == 'deliver' && invoice.quantity > 0">
							<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">Other</th>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top; text-align: justify;">Bin Fee - @{{ invoice.description }}</td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">@{{ invoice.quantity }}</td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">@{{ invoice.amount }}</td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">@{{ invoice.amount }}</td>
						</tr>

						<tr style="background-color: #f9f9f9; text-align: center;" ng-if="invoice.itemType == 'purchase' && invoice.quantity > 0">
							<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">Other</th>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top; text-align: justify;">@{{ invoice.description }}</td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">@{{ invoice.quantity }}</td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">@{{ invoice.amountNoTax }}</td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">@{{ invoice.amountNoTax }}</td>
						</tr>

						<tr style="background-color: #f9f9f9; text-align: center;" ng-if="invoice.itemType == 'purchase' && invoice.quantity > 0">
							<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">Tax</th>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top; text-align: justify;">Tax on bin purchase (8.25%)</td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">@{{ invoice.quantity }}</td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">@{{ invoice.tax }}</td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">@{{ invoice.tax }}</td>
						</tr>

						<tr style="background-color: #f9f9f9; text-align: center;" ng-if="invoice.discount != 0">
							<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">Discount</th>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top; text-align: justify;">Code: @{{ invoice.discount_key }}</td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;"></td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">@{{ invoice.discount }}%</td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;">- $@{{ (invoice.totalAmountNoFormat * invoice.discount / (100 - invoice.discount)) / 100 | number : 2 }}</td>
						</tr>

						<tr style="text-align: center;">
							<th style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;"></th>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;"></td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;"></td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;"><span style="font-weight: bold;">Total</span></td>
							<td style="border: 1px solid #ddd; padding: 8px; line-height: 1.42857143; vertical-align: top;"><span style="font-weight: bold;" ng-bind="invoice.amount"></span></td>
						</tr>
					</table>
					<h3>
						<b>Paid by:</b><span> Visa Credit Card</span> <span style="margin-left: 5px;">**** **** **** <span ng-bind="invoice.last4"></span></span> <span style="margin-left: 10px;"><span ng-bind="invoice.amount"></span></span>
					</h3>

            </div>
        </article>
        <div class="trigger-label-container"  ng-show="invoice.invoice_id != ''">
        	<span class="bin-track-btn print-big" ng-click="ngprintinvoice('print-invoice')">
            	<img src="{{ asset('bk/images/green_print.png') }}" />
            	Print
            </span>
        	<!-- <label class="staff-print" ng-click="ngprintinvoice('print-invoice')"></label> -->
		</div>
    </div>
    <!-- end side part -->

</section>
<!-- end page content -->

@stop @section('scripts')
<script type="text/javascript" src='{{ asset("bk/js/bootstrap.min.js") }}'></script>
<script type="text/javascript" src='{{ asset("bk/js/bootstrap-datetimepicker.js") }}'></script>
<script type="text/javascript">
    $(function() {
        $('#datepicker_from').datetimepicker({
        	format: 'MMM D.YYYY',
        	defaultDate: "{{ (isset($arr['from']) && $arr['from'] != '') ? date('m/d/Y', strtotime($arr['from'])) : '' }}"
        });
        $('#datepicker_to').datetimepicker({
        	format: 'MMM D.YYYY',
        	defaultDate: "{{ (isset($arr['to']) && $arr['to'] != '') ? date('m/d/Y', strtotime($arr['to'])) : '' }}"
        });
    });
</script>
@stop