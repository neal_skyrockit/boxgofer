@extends('layouts.admin')

@section('title') Warehouse @stop

@section('content')
<!-- page content -->
<section class="main-container" id="warehouse-page" ng-controller="WarehouseController">
    <!-- main part -->
    <div class="col-xs-8 main-part">
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">

					<!-- header bar -->
                    <div class="row header-bar">
                    	<div class="col-md-12 col-lg-2">
                            <h3>Warehouse</h3>
                        </div>
                        <div class="col-md-12 col-lg-10">
                            <form action="/admin/warehouse" method="get" class="search-group">
                            	<div class="cell input-search pull-right">
                                        <div class="cell warehouse-select">
                                            <select name="city_code" class="form-control city-code" class="slt-city-code">
                                            	@foreach($warehouses as $w)
                                            		@foreach($w->city_code as $value)
                                            			<option value="{{$value}}">{{$value}}</option>
                                            		@endforeach
                                            	@endforeach
						                    </select>
                                        </div>

                                        <div class="cell warehouse-select">
                                            <select name="warehouse_code" class="form-control warehouse-code">
                                            	@foreach($warehouses as $w)
                                            		@foreach($w->warehouse_code as $value)
                                            			<option value="{{$value}}">{{$value}}</option>
                                            		@endforeach
                                            	@endforeach
						                    </select>
                                        </div>

                                        <div class="cell">
                                            <input type="number" name="aisle_num" placeholder="Aisle#" class="form-control inp-aisle" value="{{ isset($arr['aisle_num']) ? $arr['aisle_num'] : '' }}">
                                        </div>

                                        <div class="cell">
                                            <input type="number" name="section_num" placeholder="Section#" class="form-control inp-section" value="{{ isset($arr['section_num']) ? $arr['section_num'] : '' }}">
                                        </div>

                                        <div class="cell">
                                            <input type="number" name="shelf_num" placeholder="Shelf#" class="form-control inp-shelf" value="{{ isset($arr['shelf_num']) ? $arr['shelf_num'] : '' }}">
                                        </div>

                                        <div class="cell">
		                                    <input name="search" type="text" class="form-control nobor-r inp-search" placeholder="Search" value="{{ isset($arr['search']) ? $arr['search'] : '' }}">
		                                </div>
                                        <div class="cell btnContainer">
		                                    <button type="submit" class="btnSearch nobor-l">
		                                        <i></i>
		                                    </button>
		                                </div>
                                </div>


                            </form>
                        </div>
                    </div>
                    <!-- end header bar -->
                    <!-- data container -->
                    <div ng-init="listBarcodeImg = '{{$listBarcode}}'"></div>
                    <div class="data-container">
                        <table class="data-table">
                            <thead>
                                <tr>
                                    <th class="hidden-xs">ORDER</th>
                                    <th>Bin code</th>
                                    <th>Warehouse code</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Name</th>
                                    <th>Time</th>
                                    <th>Status</th>
                                    <th>Warehouse</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($storage as $k => $t)
                                <tr id="storage_{{$t->_id}}" ng-click="getStorageInfo('{{ $t->_id }}')" class="storage-class">
                                    <td class="ta-c hidden-xs" @if($k == 0)ng-init="getStorageInfo('{{$t->_id}}')"@endif>{{ ($k == 0) ? $t->transactions->transcode : ($t->trans_id != $storage[$k - 1]->trans_id ? $t->transactions->transcode : '') }}</td>
                                    <td class="wb-b cl-0">{{ $t->barcode }}</td>
                                    <td class="wb-b cl-0">{{ $t->warehouse_barcode }}</td>
                                    <td><span>{{ $t->phone }}</span></td>
                                    <td class="wb-b"><span>{{ $t->address }}</span></td>
                                    <td>{{ $t->relationUser->name }}</td>
                                    <td class="hidden-xs"><span>{{ CommonHelper::processTime($t->processtime) }}</span></td>
                                    <td>{{ ($t->status == 0 || $t->status == 1 || $t->status == 3) ? 'Drop-off' : ($t->status == 4 ? 'Pick-up' : ($t->status == 5 ? 'Cancel' : 'In-storage') ) }}</td>
                                    <td class="cl-0 ta-c"><span>{{ $t->workingstatus }}</span></td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                        <div>
                            {!! $storage->render() !!}
                        </div>
                    </div>
                    <!-- end data container -->

                </div>
            </div>
        </div>
    </div>
    <!-- end main part -->
    <!-- side part -->
    <div class="col-xs-4 information-part hidden">
		<div class="client-information" ng-show="storage != ''">
            <div class="current-bin storage-bin">
                <h3>Warehouse code</h3>
                <p class="ta-c" ng-bind="storage.warehouse_barcode"></p>
            </div>

			<table class="custom-table">
                <tr class="bg-grey">
                    <td>Day start</td>
                    <td class="w150"><span ng-bind="storage.daystart"></span></td>
                </tr>
            </table>
            <h3 class="fs18 cl-0 nomargin padding2020">Activity :</h3>
            <table class="custom-table custom-table-new">
                <thead>
                    <tr class="clean bg-grey">
                        <th class="ta-c" style="width: 50%">Drop-off</th>
                        <th class="ta-c">Pick-up</th>
                    </tr>
                </thead>
                <tbody class="ct-new">
                    <tr ng-repeat="dt in storage.dropofftime">
                        <td class="w150 ta-c"><span ng-bind="convertDate(dt, false, true)"></span></td>
                        <td class="w150 ta-c"><span ng-bind="convertDate(storage.pickuptime[$index], false, true)"></span></td>
                    </tr>
                </tbody>
               <tfoot ng-if="storage.canceltime">
                    <tr class="clean bg-grey">
                        <td>Cancel:</td>
                        <td class="w150"><span ng-bind="storage.canceltime"></span></td>
                    </tr>
                </tfoot>

            </table>
        </div>
    </div>
    <!-- end side part -->

</section>
<!-- end page content -->

@stop @section('scripts')
<script type="text/javascript" src='{{ asset("bk/js/bootstrap.min.js") }}'></script>
@stop