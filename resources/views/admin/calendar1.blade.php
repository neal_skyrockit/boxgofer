@extends('layouts.admin')

@section('title') Bin List @stop

 @section('content')

<!-- page content -->
<section class="main-container" id="client-page" ng-controller="CalendarController">
    <!-- main part -->
    <div class="col-xs-12 col-md-8 main-part">
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">
                    <!-- header bar -->
                    <div class="row header-bar">
                        <!-- <div class="col-md-12 col-lg-3">
                            <h3>Calendar</h3>
                        </div> -->
                        <div class="col-md-12 col-lg-6 actions ta-l">

                        </div>
                        <div class="col-md-12 col-lg-3">
                        </div>
                    </div>
                    <!-- end header bar -->
                    <div class="row">
                    	<div class="col-md-12 col-lg-7">
							<div class="dropdown">
							  <button class="btn btn-default dropdown-toggle dropdown-date" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							    @{{currentDate}}
							    <span class="caret"></span>
							  </button>
							  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
							    <li ng-repeat="item in availableDate track by $index" ng-class="item == currentDate ? 'bg-primary': ''"><a href="#" ng-bind="item" ng-click="changeMonth($event)"></a></li>
							  </ul>
							</div>
						</div>
                    	<div class="col-md-12 col-lg-5">
                    		<div class="wrap-calendar-right">
                        		<span>Drop-off</span>
								<span class="drop-off-quare"></span>
								<span>Pick-up</span>
								<span class="pick-up-quare"></span>
                        	</div>
                    	</div>
                    </div>
                    <!-- data container -->
                    <div class="data-container">
                        <table class="DCalendar">
				            <thead>
				                <tr>
				                    <td>SUN</td>
				                    <td>MON</td>
				                    <td>TUE</td>
				                    <td>WEB</td>
				                    <td>THR</td>
				                    <td>FRI</td>
				                    <td>SAT</td>
				                </tr>
				            </thead>
				            <tbody>
				            	<tr ng-repeat="weeks in bgcalendar">
				            		<td ng-repeat="week in weeks" ng-click="getCalendar($event)" ng-class="week.currentDay ? 'bg-warning date-active' : ''">
				                        <span class="calendar-date">@{{ week.day }}</span>
				                        <div class="list-schedule">
				                        	<ul>
				                        		<li ng-repeat="foo in week.data" ng-class="week.storagestatus[foo] == 'Drop-off' ? 'color-drop-off' : 'color-pick-up'">@{{ foo | uppercase }}</li>
				                        	</ul>
				                        </div>
				                    </td>
				            	</tr>
				            </tbody>
				        </table>
                    </div>
                    <!-- end data container -->
                </div>
            </div>
        </div>
    </div>
    <!-- end main part -->

    <!-- side part -->
    <div class="col-xs-4 information-part">
        <div class="calendar-right hidden">
			<div id="print-content">
				<div class='calendar-right-header'>
	        		<span ng-bind="dayformat"></span>
	        		<span class="bin-track-btn print-big" ng-click="printCalendar()">
	            		<img src="{{ asset('bk/images/green_print.png') }}" />
	            		Print
	            	</span>
	        		<!-- <label class="staff-print" ng-click="printCalendar()"></label> -->
	        	</div>
				<div class="calendar-item" ng-repeat="storage in storages">
					<table class="main-wrap">
						<tr>
							<td ng-class="storage.storagestatus == 'Drop-off' ? 'color-drop-off' : 'color-pick-up' " ng-bind="storage.time"></td>
							<td>@{{storage.storagestatus}} @{{storage.totalbin}} @{{storage.totalbin == 1 ? 'bin': 'bins'}} (@{{storage.itemdescript}}) at @{{storage.address}}</td>
						</tr>
					</table>

					<table class="sub-wrap">
						<tr>
							<td></td>
							<td class="customer-name">
								@{{storage.username}}
								<div>ORDER @{{storage.orderid}}</div>
							</td>
						</tr>
						<tr>
							<td>Of bins size</td>
							<td>@{{formatItemType(storage.itemdescript)}}</td>
						</tr>
						<tr>
							<td>Phone</td>
							<td>@{{storage.phone}}</td>
						</tr>
						<tr>
							<td>Address</td>
							<td>@{{storage.address}}</td>
						</tr>
						<tr>
							<td>Description of Other Stuff</td>
							<td>@{{storage.otherdes}}</td>
						</tr>
						<tr>
							<td>Special request</td>
							<td>@{{storage.content}}</td>
						</tr>
					</table>
	        	</div>
			</div>
    	</div>
    </div>
    <!-- end side part -->

</section>
<!-- end page content -->

@stop
@section('scripts')
	<script type="text/javascript" src='{{ asset("bk/js/bootstrap.min.js") }}'></script>
@stop