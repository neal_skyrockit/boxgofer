@extends('layouts.admin')

@section('title') Dashboard @stop

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="panel-body">

			<h2>Users</h2>

		    @if ($errors->has())
		        @foreach ($errors->all() as $error)
		            <div class='bg-danger alert'>{{ $error }}</div>
		        @endforeach
		    @endif

		    <nav class="navbar navbar-default navbar-custom" role="navigation">
			    <form class="navbar-form navbar-left" role="search" method="get">
			    	<div class="form-group">
						<span>From</span><input name="from" id="datepicker-from" type="text" id="datepicker-from" class="form-control input-sm" placeholder="Updated From" value="{{ isset($search['from']) ? $search['from'] : '' }}">
					</div>
					<div class="form-group">
						<span>To</span><input name="to" id="datepicker-to" type="text" id="datepicker-to" class="form-control input-sm" placeholder="Updated To" value="{{ isset($search['to']) ? $search['to'] : '' }}">
					</div>

					<div class="form-group">
						<span>Status</span>
						<select class="form-control input-sm" name="status">
							<option {{ (!isset($search['status']) || ($search['status'] == "")) ? 'selected="selected"' : '' }} value="">All</option>
							<option {{ (isset($search['status']) && $search['status'] == 1) ? 'selected="selected"' : '' }} value="1">Active</option>
							<option {{ (isset($search['status']) && $search['status'] == 0) && $search['status'] != "" ? 'selected="selected"' : '' }} value="0">Inactive</option>
						</select>
					</div>

					<div class="form-group">
						<span>Search by</span><input name="search" type="text" class="form-control input-sm" placeholder="Id or Name or Email or Phone" value="{{ isset($search['search']) ? $search['search'] : '' }}" />
					</div>
					<button type="submit" class="btn btn-primary btn-sm">Search</button>
					<a href="/admin/userlist" class="btn btn-default btn-sm">Reset</a>
				</form>
			</nav>

			<table class="table table-hover table-responsive">
				<thead>
					<tr>
						<th>Id</th>
						<th>Name</th>
						<th>Email</th>
						<th>Phone</th>
						<th>Status</th>
						<th>Created</th>
						<th>Updated</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $foo)
					<tr>
						<td>{{ $foo->_id }}</td>
						<td>{{ $foo->name }}</td>
						<td>{{ $foo->email }}</td>
						<td>{{ $foo->phone }}</td>
						<td>{{ $foo->status }}</td>
						<td>{{ $foo->created_at }}</td>
						<td>{{ $foo->updated_at }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			<div>
				{!! $users->render() !!}
			</div>

		</div>
	</div>
</div>
<script>
jQuery(document).ready(function($){
	$('#datepicker-from').datepicker({
	    format: 'yyyy-mm-dd'
	});
	$('#datepicker-to').datepicker({
	    format: 'yyyy-mm-dd'
	});
});
</script>

@stop