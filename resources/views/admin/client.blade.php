@extends('layouts.admin')

@section('title') Customer List @stop

@section('content')

<!-- page content -->
<section class="main-container" id="client-page" ng-controller="ClientController">
    <!-- main part -->
    <div class="col-xs-12 col-md-8 main-part">
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">
                    <!-- header bar -->
                    <div class="row header-bar">
                        <div class="col-md-12 col-lg-2">
                            <h3>Customers</h3>
                        </div>
                        <div class="col-md-12 col-lg-10">
                            <form action="/admin/customers" method="get" class="search-group">
                                <div class="cell">
                                    <div class="display-table col-md-6 col-xs-12">
                                        <div class="cell labelContainer">Date</div>
                                        <div class="cell">
                                            <input name="from" id="datepicker_from" value="{{ isset($arr['from']) ? $arr['from'] : '' }}" class="form-control calendar">
                                        </div>
                                    </div>
                                    <p class="visible-xs"></p>
                                    <div class="display-table col-md-6 col-xs-12">
                                        <div class="cell labelContainer">To</div>
                                        <div class="cell">
                                            <input name="to" id="datepicker_to" value="{{ isset($arr['to']) ? $arr['to'] : '' }}" class="form-control calendar">
                                        </div>
                                    </div>
                                </div>
                                <div class="cell">
                                    <input name="search" type="text" class="form-control nobor-r" value="{{ isset($arr['search']) ? $arr['search'] : '' }}" placeholder="Search Name">
                                </div>
                                <div class="cell btnContainer">
                                    <button type="submit" class="btnSearch nobor-l">
                                        <i></i>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end header bar -->
                    <!-- data container -->
                    <div class="data-container">
                        <table class="data-table">
                            <thead>
                                <tr>
                                    <!-- <th class="hidden-xs">UNIT</th> -->
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Items</th>
                                    <th class="hidden-xs">Day Started</th>
                                    <th>Total Order</th>
                                    <!-- <th>Status</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $u)
                                <tr id="cl_{{$u->_id}}" ng-click="getClientInfo('{{$u->_id}}')">
                                    <td class="cl-0"><a href="/admin/orders/{{ $u->_id }}">{{$u->name}}</a></td>
                                    <td class="wb-b">{{$u->phone}}</td>
                                    <td class="wb-b"><span>{{$u->email}}</span></td>
                                    <td>
                                        {{ isset($StorageInfo[$u->_id]['l']) ? $StorageInfo[$u->_id]['l'] : 0 }}L - {{ isset($StorageInfo[$u->_id]['e']) ? $StorageInfo[$u->_id]['e'] : 0 }}X - {{ isset($StorageInfo[$u->_id]['o']) ? $StorageInfo[$u->_id]['o'] : 0 }}O
                                    </td>
                                    <td class="hidden-xs"><span>{{ date("M d.Y", strtotime($u->created_at))  }}</span></td>
                                    <td class="cl-0 ta-r"><span class="currency">{{ $u->totalorder }}</span></td>
                                    <!-- <td class="action-group">
                                        <span class="action" ng-click="changeStatus('{{$u->_id}}')" data-status="{{$u->status == 1 ? 'ok' : 'deny' }}"></span>
                                    </td> -->
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div>
                            {!! $users->render() !!}
                        </div>
                    </div>
                    <!-- end data container -->
                </div>
            </div>
        </div>
    </div>
    <!-- end main part -->
    <!-- side part -->
    <input type="checkbox" id="triggerHideInformationPart" class="hidden" />
    <div class="col-md-4 information-part hidden">
    @if(count($users) > 0)
        <label for="triggerHideInformationPart" class="triggerHideInformationPart hidden-md hidden-lg"></label>
        <div>
            <h3 class="header" ng-bind="clientInfo.info.name"></h3>
            <div class="client-information">
                <button id="savebtn" class="btn savebtn pull-right" ng-click="save()"></button>
                <button id="deletebtn" class="btn deletebtn margin-right-15 pull-right" ng-click="delete()"></button>
                <div id="mess-show" ng-show="messageshow.ishide != true" class="@{{ messageshow.nameclass }} col-md-12">@{{ messageshow.mess }}</div>
                <table class="custom-table editfields" ng-init="getClientInfo('{{ $users[0] !== null ? $users[0]->_id : 0 }}')">
                    <tr>
                        <td>Name</td>
                        <td><input ng-model="clientInfo.info.name" /></td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td><input id="client-phone" placeholder="(123) 456-7890" maxlength="14" ng-model="clientInfo.info.phone" /></td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td><input ng-model="clientInfo.info.physicalAddress" /></td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td>
                        	<input ng-model="clientInfo.info.city" />
                        </td>
                    </tr>
                    <tr>
                        <td>Zip Code</td>
                        <td>
                        	<input ng-model="clientInfo.info.zipcode" />
                        </td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td class="wb-b"><input ng-model="clientInfo.info.email" /></td>
                    </tr>
                    <tr>
                        <td>Day Started</td>
                        <td><span ng-bind="clientInfo.info.createdat"></span></td>
                    </tr>
                    <tr>
                        <td>Items Rented</td>
                        <td><span ng-bind="clientInfo.storage.l"></span>L
                        - <span ng-bind="clientInfo.storage.e"></span>X
                        - <span ng-bind="clientInfo.storage.o"></span>O</td>
                    </tr>
                    <tr>
                        <td>Items Renturned</td>
                        <td><span ng-bind="clientInfo.info.returned.l"></span>L
                        - <span ng-bind="clientInfo.info.returned.e"></span>X
                        - <span ng-bind="clientInfo.info.returned.o"></span>O</td>
                    </tr>
                    <tr>
                        <td>Last Payment Date</td>
                        <td><span ng-bind="clientInfo.info.lastPayment"></span></td>
                    </tr>
                    <tr>
                        <td>Last Payment Amount</td>
                        <td><span ng-bind="clientInfo.info.lastAmount"></span></td>
                    </tr>
                    <tr>
                        <td>Monthly Amount</td>
                        <td><span ng-bind="clientInfo.info.monthAmount"></span></td>
                    </tr>
                    <tr>
                        <td>Ip address</td>
                        <td><span ng-bind="clientInfo.info.ipaddress"></span></td>
                    </tr>
                    <tr>
                        <td>Registered</td>
                        <td><span ng-bind="clientInfo.info.created_at"></span></td>
                    </tr>
                </table>
                <a href="/admin/orders/@{{clientInfo.info._id}}" class="btn fullbtn">VIEW ORDERS</a>
            </div>
        </div>
        @else
        <div class="client-information">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
    	@endif
    </div>
    <!-- end side part -->
</section>
<!-- end page content -->

@stop @section('scripts')

<script type="text/javascript" src='{{ asset("bk/js/bootstrap.min.js") }}'></script>
<script type="text/javascript" src='{{ asset("bk/js/bootstrap-datetimepicker.js") }}'></script>
<script type="text/javascript">
	var selectedFromDay = '{{ isset($arr["from"]) ? $arr["from"] : "" }}',
		selectedToDay = '{{ isset($arr["to"]) ? $arr["to"] : "" }}';
	
    $(function() {
        $('#datepicker_from').datetimepicker({
        	format: 'MMM D.YYYY'
        });
        $('#datepicker_to').datetimepicker({
        	format: 'MMM D.YYYY'
        });

        if (selectedFromDay != '') $('#datepicker_from').val(selectedFromDay);
        if (selectedToDay != '') $('#datepicker_to').val(selectedToDay);
    });
</script>
@stop