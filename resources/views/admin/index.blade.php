@extends('layouts.admin')

@section('title') Dashboard @stop

@section('content')

<section class="main-container" id="dashboard-page" ng-controller="ChartController">

    <!-- main part -->
    <div class="col-xs-8 main-part">
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">
                    <h3 class="title">Bins in Storage</h3>
                    <div id="BinChart" style="height:300px"></div>
                </div>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-md-12 part">
                <div class="content">
                    <h3 class="title">Cash on Hand</h3>
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">
                    <h3 class="title">Daily Schedule Bar Chart</h3>
                    <div id="DailyChart" style="height:300px">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">
                    <h3 class="title">Ordered Bins by Zip Code</h3>
                    <div id="ZipcodeChart" style="height:300px"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">
                    <h3 class="title">Web Traffic</h3>
					<div style="display:none;" id="embed-api-auth-container"></div>
					<div style="display:none;" id="view-selector-container"></div>
					<div style="display:none;" id="active-users-container"></div>
					<div id="chart-1-container"></div>
					<div id="legend-1-container"></div>
					<div id="view-name"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- end main part -->

    <!-- side part -->
    <div class="col-xs-4 information-part">
        <h3 class="header">Schedule</h3>
        <hr/>
        <div class="schedule-group">
            <p class="datetime"> Today, {{ $arr['today'][0] }}</p>
            <div class="tasks">
			@if(count($arr['today'][1]) > 0)
				@foreach($arr['today'][1] as $foo)
                <div class="task">
                    <p class="time">{{ $foo['time'] }}</p>
                    <p class="content">{{ $foo['storagestatus'] }} {{ ($foo['totalbin'] == 1) ? $foo['totalbin'] . ' bin' : $foo['totalbin'] . ' bins' }} ({{ CommonHelper::processSpecialStr($foo['itemtype']) }}) at {{ $foo['address'] }} </p>
                </div>
                @endforeach
			@endif
            </div>
        </div>
        <div class="schedule-group">
            <p class="datetime">{{ $arr['tomorrow'][0] }}</p>
            <div class="tasks">
			@if(count($arr['tomorrow'][1]) > 0)
                @foreach($arr['tomorrow'][1] as $foo)
                <div class="task">
                    <p class="time">{{ CommonHelper::processTime($foo['datetime']) }}</p>
                    <p class="content">{{ $foo['storagestatus'] }} {{ ($foo['totalbin'] == 1) ? $foo['totalbin'] . ' bin' : $foo['totalbin'] . ' bins' }} ({{ CommonHelper::processSpecialStr($foo['itemtype']) }}) at {{ $foo['address'] }} </p>
                </div>
                @endforeach
			@endif
            </div>
        </div>
        <div class="schedule-group">
            <p class="datetime">{{ $arr['aftertomorrow'][0] }}</p>
            <div class="tasks">
			@if(count($arr['aftertomorrow'][1]) > 0)
                @foreach($arr['aftertomorrow'][1] as $foo)
                <div class="task">
                    <p class="time">{{ CommonHelper::processTime($foo['datetime']) }}</p>
                    <p class="content">{{ $foo['storagestatus'] }} {{ ($foo['totalbin'] == 1) ? $foo['totalbin'] . ' bin' : $foo['totalbin'] . ' bins' }} ({{ CommonHelper::processSpecialStr($foo['itemtype']) }}) at {{ $foo['address'] }} </p>
                </div>
                @endforeach
			@endif
            </div>
        </div>
    </div>
    <!-- end side part -->
</section>
<!-- Step 2: Load the library. -->

<script>
(function(w,d,s,g,js,fs){
  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
  js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
  js.src='https://apis.google.com/js/platform.js';
  fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
}(window,document,'script'));
</script>

<!-- This demo uses the Chart.js graphing library and Moment.js to do date
     formatting and manipulation. -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

<!-- Include the ViewSelector2 component script. -->
<script src="{{ asset('public/web/gg/view-selector2.js') }}"></script>

<!-- Include the DateRangeSelector component script. -->
<script src="{{ asset('public/web/gg/date-range-selector.js') }}"></script>

<!-- Include the ActiveUsers component script. -->
<script src="{{ asset('public/web/gg/active-users.js') }}"></script>

<script>

// == NOTE ==
// This code uses ES6 promises. If you want to use this code in a browser
// that doesn't supporting promises natively, you'll have to include a polyfill.

gapi.analytics.ready(function() {

  /**
   * Authorize the user immediately if the user has already granted access.
   * If no access has been created, render an authorize button inside the
   * element with the ID "embed-api-auth-container".
   */
  gapi.analytics.auth.authorize({
    container: 'embed-api-auth-container',
    clientid: '883235351480-ac87f1si9m632thjtkeu7s3c681786ri.apps.googleusercontent.com'
  });

  /**
   * Create a new ActiveUsers instance to be rendered inside of an
   * element with the id "active-users-container" and poll for changes every
   * five seconds.
   */
  var activeUsers = new gapi.analytics.ext.ActiveUsers({
    container: 'active-users-container',
    pollingInterval: 5
  });

  /**
   * Add CSS animation to visually show the when users come and go.
   */
  activeUsers.once('success', function() {
    var element = this.container.firstChild;
    var timeout;
    this.on('change', function(data) {
      var element = this.container.firstChild;
      var animationClass = data.delta > 0 ? 'is-increasing' : 'is-decreasing';
      element.className += (' ' + animationClass);

      clearTimeout(timeout);
      timeout = setTimeout(function() {
        element.className =
            element.className.replace(/ is-(increasing|decreasing)/g, '');
      }, 3000);
    });
  });

  /**
   * Create a new ViewSelector2 instance to be rendered inside of an
   * element with the id "view-selector-container".
   */
  var viewSelector = new gapi.analytics.ext.ViewSelector2({
    container: 'view-selector-container',
  })
  .execute();

  /**
   * Update the activeUsers component, the Chartjs charts, and the dashboard
   * title whenever the user changes the view.
   */
  viewSelector.on('viewChange', function(data) {
    var title = document.getElementById('view-name');
    title.innerHTML = data.property.name + ' (' + data.view.name + ')';

    // Start tracking active users for this view.
    activeUsers.set(data).execute();

    // Render all the of charts for this view.
    renderWeekOverWeekChart(data.ids);
  });

  /**
   * Draw the a chart.js line chart with data from the specified view that
   * overlays session data for the current week over session data for the
   * previous week.
   */
  function renderWeekOverWeekChart(ids) {

    // Adjust `now` to experiment with different days, for testing only...
    var now = moment(); // .subtract(3, 'day');

    var thisWeek = query({
      'ids': ids,
      'dimensions': 'ga:date,ga:nthDay',
      'metrics': 'ga:sessions',
      'start-date': moment(now).subtract(1, 'day').day(0).format('YYYY-MM-DD'),
      'end-date': moment(now).format('YYYY-MM-DD')
    });

    var lastWeek = query({
      'ids': ids,
      'dimensions': 'ga:date,ga:nthDay',
      'metrics': 'ga:sessions',
      'start-date': moment(now).subtract(1, 'day').day(0).subtract(1, 'week')
          .format('YYYY-MM-DD'),
      'end-date': moment(now).subtract(1, 'day').day(6).subtract(1, 'week')
          .format('YYYY-MM-DD')
    });

    Promise.all([thisWeek, lastWeek]).then(function(results) {

      var data1 = results[0].rows.map(function(row) { return +row[2]; });
      var data2 = results[1].rows.map(function(row) { return +row[2]; });
      var labels = results[1].rows.map(function(row) { return +row[0]; });

      labels = labels.map(function(label) {
        return moment(label, 'YYYYMMDD').format('ddd');
      });

      var data = {
        labels : labels,
        datasets : [
          {
            label: 'Last Week',
            fillColor : 'rgba(220,220,220,0.5)',
            strokeColor : 'rgba(220,220,220,1)',
            pointColor : 'rgba(220,220,220,1)',
            pointStrokeColor : '#fff',
            data : data2
          },
          {
            label: 'This Week',
            fillColor : 'rgba(151,187,205,0.5)',
            strokeColor : 'rgba(151,187,205,1)',
            pointColor : 'rgba(151,187,205,1)',
            pointStrokeColor : '#fff',
            data : data1
          }
        ]
      };

      new Chart(makeCanvas('chart-1-container')).Line(data);
      generateLegend('legend-1-container', data.datasets);
    });
  }

  /**
   * Extend the Embed APIs `gapi.analytics.report.Data` component to
   * return a promise the is fulfilled with the value returned by the API.
   * @param {Object} params The request parameters.
   * @return {Promise} A promise.
   */
  function query(params) {
    return new Promise(function(resolve, reject) {
      var data = new gapi.analytics.report.Data({query: params});
      data.once('success', function(response) { resolve(response); })
          .once('error', function(response) { reject(response); })
          .execute();
    });
  }

  /**
   * Create a new canvas inside the specified element. Set it to be the width
   * and height of its container.
   * @param {string} id The id attribute of the element to host the canvas.
   * @return {RenderingContext} The 2D canvas context.
   */
  function makeCanvas(id) {
    var container = document.getElementById(id);
    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');

    container.innerHTML = '';
    canvas.width = container.offsetWidth;
    canvas.height = (container.offsetHeight < 300) ? 300 : container.offsetHeight;//container.offsetHeight;
    container.appendChild(canvas);

    return ctx;
  }

  /**
   * Create a visual legend inside the specified element based off of a
   * Chart.js dataset.
   * @param {string} id The id attribute of the element to host the legend.
   * @param {Array.<Object>} items A list of labels and colors for the legend.
   */
  function generateLegend(id, items) {
    var legend = document.getElementById(id);
    legend.innerHTML = items.map(function(item) {
      var color = item.color || item.fillColor;
      var label = item.label;
      return '<li><i style="background:' + color + '"></i>' + label + '</li>';
    }).join('');
  }

  // Set some global Chart.js defaults.
  Chart.defaults.global.animationSteps = 60;
  Chart.defaults.global.animationEasing = 'easeInOutQuart';
  Chart.defaults.global.responsive = true;
  Chart.defaults.global.maintainAspectRatio = false;

});
</script>
<script type="text/javascript" src='{{ asset("bk/js/canvasjs.js") }}'></script>
<script>

    function dataPointObject(value,name){
        return {
            y: value,
            label: name
        }
    }

    function dataObject(color,listDataPoint,legendText){
        return {
            type: "stackedColumn",
            showInLegend: "true",
            color:color,
            legendText: legendText || "",
			name:legendText || "",
            dataPoints:listDataPoint || []
        };
    }

 	// Bin
    function BinChart(){
        var _ = this;

        _.axisY = ['orderedL','orderedXL','orderedOther','unusedL','unusedXL'];
        _.legend = ['Ordered - L','Ordered - XL','Ordered - Other','Unused - L','Unused - XL'];
        _.color = ['#4e81bd','#c1504c','#9bbb5a','#8064a1','#4aadc7'];
        _.listData = [];
    }

    BinChart.prototype.render = function(id){
        var _ = this;
        _.chart = new CanvasJS.Chart(id,
                {
                    data : _.listData ,
                    toolTip:{
                        content: "{name}: {y}{zc}"
                    }
                });
        _.chart.render();
    }

    BinChart.prototype.mappingData = function(datas){
        var _ = this;
        _.listData = [];

        for(var i = 0; i < 5;i++){
            var obj = dataObject(_.color[i],datas[_.axisY[i]],_.legend[i]) ;
            _.listData.push(obj);
        }

    }

    // Daily
    function DailyChart(){
        var _ = this;
        _.axisY = ['L','XL','Other'];
        _.legend = ['Large','XL','Other items'];
        _.color = ['#4e81bd','#c1504c','#9bbb5a'];
        _.listData = [];
    }

    DailyChart.prototype = BinChart.prototype;

    // Zipcode
    function ZipcodeChart(){
        var _ = this;
        _.axisY = ['Ordered - L','Ordered - XL','Ordered - Other'];
        _.legend = ['Ordered - L','Ordered - XL','Ordered - Other'];
        _.color = ['#9bbb5a','#8064a1','#4aadc7'];
        _.listData = [];
    }

    ZipcodeChart.prototype.mappingData = function(datas){
        var _ = this;
        _.listData = [];

        for(var i = 0; i < 5;i++){
            var obj = dataObject(_.color[i],datas[_.axisY[i]],_.legend[i]) ;
            _.listData.push(obj);
        }

    }

    ZipcodeChart.prototype.render = function(id){
        var _ = this;
        _.chart = new CanvasJS.Chart(id,
                {
                    data : _.listData ,
                    toolTip:{
                        content: "{name}: {y}{zc}"
                    },
                    axisX: {
                        interval: 1,
                        labelAngle: -70
                    }
                });
        _.chart.render();
    }

</script>
@stop