@extends('layouts.admin')

@section('title') Bin tracking @stop

@section('content')
<!-- page content -->
<section class="main-container" id="transaction-page" ng-controller="StaffTransactionController">
    <!-- main part -->
    <div class="col-xs-8 main-part">
        <div class="row">
            <div class="col-md-12 part">
                <div class="content">

                    <!-- header bar -->
                    <div class="row header-bar">
                        <div class="col-md-12 col-lg-3">
                            <h3>Bin Tracking</h3>
                        </div>
                        <div class="col-md-12 col-lg-9">
                            <form action="/admin/bintracking" method="get" class="search-group">
                            	<div class="cell">
                            		<div class="display-table col-md-6 col-xs-12">
                                        <div id="ad-select" class="cell">
						                    <select name="warehouse" id="st-status" class="form-control">
						                    	<option {{ (!isset($arr['warehouse']) || $arr['warehouse']== 'all') ? 'selected="selected"' : '' }} value="all">All Warehouse</option>
						                    	 <option {{ (isset($arr['warehouse']) && $arr['warehouse']== 'IT1 - In Transit') ? 'selected="selected"' : '' }} value="IT1 - In Transit">IT1</option>
						                    	 <option {{ (isset($arr['warehouse']) && $arr['warehouse']== 'AD1 - At Doorstep') ? 'selected="selected"' : '' }} value="AD1 - At Doorstep">AD1</option>
						                    	 <option {{ (isset($arr['warehouse']) && $arr['warehouse']== 'IT2 - In Transit') ? 'selected="selected"' : '' }} value="IT2 - In Transit">IT2</option>
						                    	 <option {{ (isset($arr['warehouse']) && $arr['warehouse']== 'IS - In Storage') ? 'selected="selected"' : '' }} value="IS - In Storage">IS</option>
						                    	 <option {{ (isset($arr['warehouse']) && $arr['warehouse']== 'IT3 - In Transit') ? 'selected="selected"' : '' }} value="IT3 - In Transit">IT3</option>
						                    	 <option {{ (isset($arr['warehouse']) && $arr['warehouse']== 'AD2 - At Doorstep') ? 'selected="selected"' : '' }} value="AD2 - At Doorstep">AD2</option>
						                    	 <option {{ (isset($arr['warehouse']) && $arr['warehouse']== 'IT4 - In Transit') ? 'selected="selected"' : '' }} value="IT4 - In Transit">IT4</option>
						                    </select>
                                        </div>
                                    </div>

                                    <div class="display-table col-md-6 col-xs-12">
                                        <div class="cell">
                                            <input name="date" id="datepicker_date" placeholder="Date" class="form-control calendar" value="{{ isset($arr['date']) ? $arr['date'] : '' }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="cell">
                                    <input name="search" type="text" class="form-control nobor-r" placeholder="Search" value="{{ isset($arr['search']) ? $arr['search'] : '' }}">
                                </div>
                                <div class="cell btnContainer">
                                    <button type="submit" class="btnSearch nobor-l">
                                        <i></i>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end header bar -->

                    <!-- data container -->
                    <div class="data-container">
                        <table class="data-table">
                            <thead>
                                <tr>
                                    <th class="hidden-xs">ORDER</th>
                                    <th class="mobile-hide">Bin</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Name</th>
                                    <th>Time</th>
                                    <th>Status</th>
                                    <th>Warehouse location</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($storage as $k => $t)
                                	@if(isset($t->transactions->transcode))
		                                <tr id="storage_{{$t->_id}}" ng-click="getStorageInfo('{{ $t->_id }}')" class="storage-class">

		                                    <td class="ta-c hidden-xs">{{ ($k == 0) ? $t->transactions->transcode : ($t->trans_id != $storage[$k - 1]->trans_id ? $t->transactions->transcode : '') }}</td>
		                                    <td class="wb-b cl-0 mobile-hide">{{ $t->barcode }}</td>
		                                    <td class="wb-b cl-0 crop">{{ $t->relationUser->email }}</td>
		                                    <td><span>{{ $t->phone }}</span></td>
		                                    <td class="wb-b"><span>{{ $t->address }}</span></td>
		                                    <td>{{ $t->relationUser->name }}</td>
		                                    <td class="hidden-xs"><span>{{ CommonHelper::processTime($t->processtime) }}</span></td>

		                                    @if(!empty($t->workingstatus))
		                                    <td class="cl-0 ta-c working-status"><span>{{ $t->formartWorkingStatusShortly($t->workingstatus) }}</span></td>
		                                    @elseif($t->status == 9)
		                                    <td class="text-danger ta-c working-status"><span>Done</span></td>
		                                    @else
		                                    <td class="cl-0 ta-c working-status"><span></span></td>
		                                    @endif

		                                    <td class="wb-b cl-0">{{ $t->warehouse_barcode }}</td>
		                                </tr>
                                	@endif
                                @endforeach

                            </tbody>
                        </table>
                        <div>
                            {!! $storage->render() !!}
                        </div>
                    </div>
                    <!-- end data container -->
                </div>
            </div>
        </div>
    </div>
    <!-- end main part -->
    <!-- side part -->
    <div ng-init="listBarcodeImg = '{{$listBarcode}}'"></div>
    <div class="col-xs-4 information-part hidden" ng-init="getStorageInfo('{{ isset($storage[0]) ? $storage[0]->_id : 0 }}')">
        <div class="client-information">
            <h3 class="header bin-track">
            	<span ng-bind="storage.transcode" class="order-num font-size-18">ORDER 2T6EXJF</span>
            	@if(Auth::guard('managers')->user()->role != 'operator')
            	<span class="print-all-btn" ng-click="ngprintbarcode()">
            		<img src="{{ asset('bk/images/white_print.png') }}" />
            		Print All
            	</span>
            	@endif
            </h3>
            <div class="current-bin storage-bin">
                <h3>Bin</h3>
                <b><p class="ta-c" ng-bind="storage.barcode"></p></b>
            </div>
            <div ng-show="storage.status != 2" class="warehouse-btn">
            	<input id="upload-image-trigger" name="image-trigger" type="radio" class="hidden" />
            	<input id="list-image-trigger" name="image-trigger" type="radio" class="hidden" />
            	@if(Auth::guard('managers')->user()->role != 'operator')
            	<span onclick="triggerClick('upload-image-trigger')" class="bt-img-trigger"><img class="bin-track-btn bin-track-img" src="{{ asset('bk/images/add photo.png') }}" /></span>
            	<span onclick="triggerClick('list-image-trigger')" class="bt-list-img-trigger"><img class="bin-track-btn bin-track-img" src="{{ asset('bk/images/viewphoto.png') }}" /></span>
            	<span class="bin-track-btn print-big" ng-click="ngprintbarcode({size:'bigger'})">
            		<img src="{{ asset('bk/images/green_print.png') }}" />
            		Print
            	</span>
            	@endif

            	<div class="list-image-group">
                    <div class="list-image-container">
                        <div class="list-image">
                        	<div ng-repeat="image in storage.images">
                            	<img class="upimage" src="/public/uploads/@{{image}}" /><img ng-click="deleteimage(storage._id, image);" class="delimage" width="20" height="20" src="{{ asset('public/web/images/imgpsh_fullsize.png') }}" />
                            </div>
                        </div>
                        <button class="btn fullbtn btn-close" onclick="closeImageTrigger('list-image-trigger')">Close</button>
                    </div>
                </div>
                <div class="upload-image-group">
                    <input id="file" type="file" class="hidden" onchange="chooseImage(this)" value="" />
                    <div class="upload-image-container">
                        <table id="image-information">
                            <tr>
                                <td style="width:50px">
                                    <img id="bin_upload_image" class="disimg" />
                                </td>
                                <td style="width:50px" class="wb-b-i name"></td>
                            </tr>
                        </table>
                        <button class="btn btn-success" ng-click="uploadImage(storage)">Upload</button>
                        <button class="btn" onclick="closeImageTrigger('upload-image-trigger')">Cancel</button>
                    </div>
                </div>
            </div>

            <div class="warehouse-btn bin-track-btn-2">
            	<div class="dropdown display-inline-block working-status-drow" ng-show="storage.list_scan.length">
				  	<button id="satusLabel" type="button" class="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    	Status
				    	<span class="caret"></span>
				  	</button>
				  	<ul class="dropdown-menu" aria-labelledby="satusLabel">
				  		<li ng-repeat="item in storage.list_scan" ng-click="changeWorkingStatus(item)"><a>@{{ item }}</a></li>
				  	</ul>
				</div>
				<button class="btn" ng-click="chargePopup()">Charge</button>
				<button class="btn" data-toggle="modal" data-target="#warehouseModal"><img src="{{ asset('bk/images/location.png') }}" /> Location</button>

				<!-- Chuong: Comment for now because it access by scan IT4
				<button class="btn cancel-btn act-cancel" ng-click="cancelRequest()">Cancel</button>
				-->
            </div>

            <table class="custom-table">
                <tr class="bg-grey">
                    <td>Start Date:</td>
                    <td class="w150"><span ng-bind="storage.daystart"></span></td>
                </tr>
            </table>
            <h3 class="fs18 cl-0 nomargin padding2020">Activity :</h3>
            <table class="custom-table custom-table-new order-edit-table">
                <thead>
                    <tr class="clean bg-grey">
                        <th class="ta-c" style="width: 70%">Date Time</th>
                        <th class="ta-c">Status</th>
                    </tr>
                </thead>
                <tbody class="ct-new">
                    <tr ng-repeat="dt in storage.timelist">
                        <td class="ta-c"><span ng-bind="dt.datetime"></span></td>
                        <td class="ta-c" ng-bind-html="to_html(dt.status)"></td>
                    </tr>
                </tbody>
               <tfoot ng-show="storage.canceltime">
                    <tr class="clean bg-grey">
                        <td colspan="2">Cancel Date:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span ng-bind="storage.canceltime"></span></td>
                    </tr>
                </tfoot>

            </table>
        </div>
    </div>
    <!-- end side part -->

    <!-- start model -->
	@include('modals.charge')
	@include('modals.warehouse')
	@if(count($storage))
		@include('modals.admincancelbin')
	@endif

	<!-- end model -->

</section>
<!-- end page content -->

@stop @section('scripts')
<script type="text/javascript" src='{{ asset("bk/js/bootstrap.min.js") }}'></script>
<script type="text/javascript" src='{{ asset("bk/js/bootstrap-datetimepicker.js") }}'></script>
<script type="text/javascript">
    $(function() {
        $('#datepicker_date').datetimepicker({
        	format: 'MMM D.YYYY',
        });
     	// set time
    	if (selectedDay != '') $('#datepicker_date').val(selectedDay);
    });
    var selectedDay = '{{ isset($arr["date"]) ? $arr["date"] : "" }}';
</script>
<script>
    function closeImageTrigger(strEle) {
        document.getElementById(strEle).checked = false;
    }

    function chooseImage(ele) {
        var file = ele.files[0];
        var ti = document.getElementById('image-information');
        var img = document.getElementById('bin_upload_image');
        if (file != null && file != undefined) {
            ti.querySelector('.name').innerHTML = file.name;

            var reader = new FileReader();
            reader.onload = (function(theFile) {
                return function(e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    img.src = e.target.result;
                };
            })(file);
            // Read in the image file as a data URL.
            reader.readAsDataURL(file);
            document.getElementById('upload-image-trigger').checked = true;
        }else{
            img.removeAttribute('src');
             ti.querySelector('.name').innerHTML = '';
        }
    }

    function triggerClick(strEle) {
        var ele = document.getElementById(strEle);

        if (strEle == 'upload-image-trigger') {
            var ele = document.getElementById('file');
            var file = ele.files[0];
            if (file != null && file != undefined)
                document.getElementById('upload-image-trigger').checked = true;

            ele.click();
        } else {
            if (ele.checked === true) {
                ele.checked = false;
            } else {
                ele.checked = true;
            }
        }
    }

    function clearFile(){
        document.getElementById('file').value = "";
         document.getElementById('file').onchange();
         var inps = document.querySelectorAll('input[name="image-trigger"]') ;
         [].forEach.call(inps,function(obj){
             obj.checked = false;
         });
    }

    @if(count($storage))
    $(function() {
		// Use for cancel
        $('#datepickerca').datetimepicker({
        	useCurrent: false,
            minDate: moment().add(1, 'd').format('MM/DD/YYYY'),
            defaultDate: moment().add(1, 'd').format('MM/DD/YYYY'),
            format: 'MM/DD/YYYY',
            daysOfWeekDisabled: [0]
        });
    });

    var listCityXZipcodes = {};
    var listZipcodeXCity = {};
    var addressFull = {};
    var initZipcode = {{$storage[0]->relationUser->zipcode}};
    var cityca = document.getElementById('cityca');

    cityca.onchange = function(){
        var curCity = cityca.value;
        if(curCity === ""){
            zipcode.innerHTML = "";
            return;
        }
        var options = function(){
            var retVal = "";
            listCityXZipcodes[curCity].forEach(function(obj){
                if(obj==initZipcode)
                    retVal += "<option value="+obj+" selected>"+obj+"</option>";
                else
                    retVal += "<option value="+obj+" >"+obj+"</option>";
            });
            return retVal;
        }

        zipcodeca.innerHTML = options();
    }

    function setZipcodeXCity(c,zipcodes){
        zipcodes.forEach(function(obj){
            listZipcodeXCity['z'+obj] = c;
        });
    }

    function setAddressFull(foo) {
        Object.keys(foo).forEach(function(key) {
        	addressFull[key] = foo[key];
        });
    }

    function init(){
        var refCity = listZipcodeXCity['z'+initZipcode];
        if(refCity === undefined
        || refCity === null ){
            return;
        }
        cityca.value = refCity;
        cityca.onchange();
    }

     @foreach($places as $place)
        listCityXZipcodes['{{$place->name}}'] = <?php echo json_encode($place->zipcodes); ?> ;
        setZipcodeXCity('{{$place->name}}',<?php echo json_encode($place->zipcodes); ?>);
        setAddressFull(<?php echo json_encode($place->full); ?>);
     @endforeach

     init();
     @endif
</script>
@stop