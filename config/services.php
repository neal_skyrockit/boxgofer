<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'paymentday' => [
	    'renewal'	=> 3,//If = 30 is month in StripeFuns
	    'trailend'	=> 2
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        // Customer key test
        'ApiKey' => env('STRIPE_API_KEY', 'sk_test_Q1DOi3AYp0x7cy7xDX58c9hY'),
        //'ApiKey' => env('STRIPE_API_KEY', 'sk_test_4CbY9XRNfM4D2S2svuqa4fzG'),
		//'ApiKey' => env('STRIPE_API_KEY', 'sk_live_QRbrEsxwNHBrsyB8YZyMgCuj'),
    	'deliverAmount' => 14.99
    ],

    'facebook'	=> [
	    'client_id' => env('FACEBOOK_CLIENT_ID'),
	    'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
	    'redirect' => env('CALLBACK_URL')
    ],

    'codereader'	=> [
	    'api_key' => '101774ef7ea393563f44ae1d65d3a78d',
	    'api_url' => 'https://api.codereadr.com/api'
		//'api_url' => 'https://barcode.codereadr.com/api'
    ],

    'paypal'	=> [
    	'client_id' => 'Aa4bfx2i8SGb6P9Zr2ShiPe_LG-JSHA93rSacxA-McyCBETOCdHh8gF3rkZ9ilqy68X57L7EtIkGFUd_',
    	'secret' => 'EOGwDOfhYFu5tlfrG2WsioXgpZd1SeKiHR5nWzPx4ZbbSn5iQ7UXeIcfqRkmpEnOk_7fPslaxVBevReD',
    	/**
    	 * SDK configuration
    	 */
    	'settings' => array(
    			/**
    			 * Available option 'sandbox' or 'live'
    	 */
    			'mode' => 'sandbox',

    			/**
    			 * Specify the max request time in seconds
    	 */
    			'http.ConnectionTimeOut' => 30,

    			/**
    			 * Whether want to log to a file
    	 */
    			'log.LogEnabled' => true,

    			/**
    			 * Specify the file that want to write on
    	 */
    			'log.FileName' => storage_path() . '/logs/paypal.log',

    			/**
    			 * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
    	 *
    	 * Logging is most verbose in the 'FINE' level and decreases as you
    	 * proceed towards ERROR
    	 */
    			'log.LogLevel' => 'FINE'
    	),
    ]

];
