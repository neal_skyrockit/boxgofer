<?php

return array(
    // set your paypal credential - manhtest@test.com
    'client_id' => 'Aa4bfx2i8SGb6P9Zr2ShiPe_LG-JSHA93rSacxA-McyCBETOCdHh8gF3rkZ9ilqy68X57L7EtIkGFUd_',
    'secret' => 'EOGwDOfhYFu5tlfrG2WsioXgpZd1SeKiHR5nWzPx4ZbbSn5iQ7UXeIcfqRkmpEnOk_7fPslaxVBevReD',

    /**
     * SDK configuration
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',

        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,

        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,

        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);
