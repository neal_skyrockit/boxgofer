var Boxgofer = angular.module('Boxgofer', [])
.config(function ($httpProvider) {
    $httpProvider.interceptors.push(function ($rootScope, $q) {
        return {
            request: function (config) {
                config.timeout = 100000;
                return config;
            },
            responseError: function (rejection) {
                switch (rejection.status){
                    case 408 :
                        console.log('connection timed out');
                        break;
                }
                return $q.reject(rejection);
            }
        }
    })
});

Boxgofer.directive('groupnumber', function () {

    return {
        restrict: 'E',
        scope: {
            amount: '=',
            max: '=',
        },
        template: '<div id="Item-Large" class="number-group">'
        + '<button class="decrease" ng-click="decrement()"></button>'
        + '<input class="amount" ng-model="amount" ng-change="change()"/>'
        + '<button class="increase" ng-click="increase()"></button>'
        + '<div class="message" ng-if="active == true" ng-click="hide()"><p>{{"Bins remain : " + max}}</p></div>'
        + '</div>',
        link: function (scope, iElement, iAttrs) {
        	
            scope.hide = function () {
                scope.active = false;
            }
            scope.change = function () {
                scope.active = false;
                if (scope.amount === "") {
                    scope.amount = 0;
                } else if (isNaN(scope.amount)) {
                    scope.amount = 0;
                } else if (scope.amount > scope.max) {
                    scope.amount = scope.max;
                    scope.active = true;
                }
            };

            scope.increase = function () {
                if (isNaN(scope.amount))
                    scope.amount = 1;
                else
                    if (scope.amount < scope.max)
                        scope.amount++;
                    else {
                        scope.active = true;
                    }

            }
            scope.decrement = function () {
                scope.active = false;
                if (isNaN(scope.amount))
                    scope.amount = 1;
                else
                    if (scope.amount > 0)
                        scope.amount--;

            }
            //Enter
            $('input.amount').keyup(function(e) {
        	    if(e.keyCode == 13) {
        	    	$(this).val(Number($(this).val()));
        	        $(this).next().focus();
        	    }
        	});
            
            $('input.amount').blur(function(e) {
    	    	$(this).val(Number($(this).val()));
        	});
            
        }
    };
})

Boxgofer.controller('ZipcodeController', ['$scope', '$http', function ($scope, $http) {

    $scope.suggestemail = '';
    $scope.placesuggest = false;

    $scope.checkZipCode = function (token) {
    	if ( $scope.zipcode != undefined 
    			&& $scope.zipcode != '' 
    				&& !isNaN($scope.zipcode)) 
    	{
    		$http({
                method : 'post',
                url: '/api/checkZipCode',
                data: { 'zipcode': $scope.zipcode, 'token': token },
                cache: false,
                timeout: 5000
            })
            .success(function (res) {
                if (res != 0) {
                    // window.location.replace('/register/' + $scope.zipcode + '/' + token);
                    window.location.replace('/order/' + $scope.zipcode + '/' + token);
                } else {
                    $scope.placesuggest = true;
                }
            });
    	} 
    	else 
    	{
    		alert('Check your zipcode.');
    	}
    };

    $scope.sendSuggestion = function () {
    	
    	if(isNaN($scope.zipcode)) {
    		alert('Please re-check your zip code !!!');
    		return false;
    	}

        if (validateEmail($scope.suggestemail)) {

            $http.post('/api/sendSuggestion', { 'zipcode': $scope.zipcode, 'email': $scope.suggestemail })
                .success(function (res) {
                    if (res == 1) {
                        alert('Your e-mail has been successfully sent.');
                        window.location.replace('/home');
                    } else {
                        alert(res.error);
                    }
                });

        } else {
            alert('Not a valid e-mail format, fat fingers. \nPlease enter a valid e-mail address');
        }
    }

}]).controller('MystuffController', ['$scope', '$http', function ($scope, $http) {
		
		$scope.availableTime = null;
		$scope.availableTime2 = null;
		$scope.availableTimeca = null;
		$scope.isShowTimeca = false;
		$scope.isShowTime = false;
		$scope.isShowTime2 = false;
		$scope.storagedkey = '';
		$scope.current = { address : 'Not yet', addressca : 'Not yet' };
		$scope.isShow = false;
		$scope.errormessage = '';
		$scope.errormessageca = '';
		$scope.form = {
			address : '',
			date : '',
			time : '',
			option : 1,
			addressca : '',
			dateca : '',
			timeca : '',
			addressfull : '',
			addressfullca : ''
		};
		
		$('.act-deliver').on("click", function () {
			$scope.isShow = false;
		    var key = $(this).data('id');
		    $scope.storagedkey = key;
		    $scope.form.address = '';
		    $scope.form.password = '';
		    $scope.form.phone = '';
		    $scope.errormessage = '';
		    
		    $("#datepicker").val('');
		    $("#timepicker").val('');
		    $("#datepicker2").val('');
		    $("#timepicker2").val('');
		    
		    $('input:radio[name=drop-pick-option][value=1]').prop('checked', true);
		    $('#drop-off-options').find('.pickuptime').hide();
		    $scope.form.option = '1';
		    
		    if (key == 'All') {
		    	key = $($('.act-deliver')[0]).attr('data-id');	// Change key to storage id if deliver all
		    }
		    
		    $http.post('/api/getStorageAddress', { 'key': key })
            .success(function (res) {
                 if (res.error) {
                	 alert(res.error);
                 } else {
                	 $scope.current.address = res;
                	 $('#myModalDeliver').modal('show');
                 }
             }); 
		});
		
		$(".myuploadimg").change(function () {
			var key = $(this).data('id');
			var file = this.files[0];
            if(file != null){
            	$(".save_" + key).show();
            	$(".name_" + key).text(file['name']);
            }
            else {
                $http.post('/api/getStorageAddress', { 'key': key })
                .success(function (res) {
                    if (res.error) {
                        alert(res.error);
                    } else {
                        $scope.current.address = res;
                        $('#myModalDeliver').modal('show');
                    }
                });
            }

        });
		
		// Deliver
		$scope.deliver = function(storagedkey) {
			
			var cuskey = $('#cuskey').val();
			if (cuskey == undefined || cuskey == '') {
				alert('Got error !');
				return false;
			}
			
			if (($scope.form.address != '') 
				&& ($scope.form.password == ''))
			{
				$scope.errormessage = 'Enter your password.';
			}
			else if (($scope.form.address != '')
				&& ($scope.form.password != ''))
			{
				$scope.form.addressfull = $scope.form.address + ', ' + addressFull[$('#zipcode').val()];	
			}
			//================
			$scope.form.date = $("#datepicker").val();
			$scope.form.time = $("#timepickerval").val();
			$scope.form.timeshow = $("#timepicker").val();
			$scope.form.date2 = $("#datepicker2").val();
			$scope.form.time2 = $("#timepicker2val").val();
			$scope.form.time2show = $("#timepicker2").val();
			$scope.form.storagedkey = storagedkey;
			
			if ($scope.form.option == 1) {
				if ($scope.form.date == '' || $scope.form.time == '') {
					$scope.errormessage = 'Enter your date and time for Drop-off.';
					return;
				}
			} 
			else 
			{
				if ($scope.form.date2 == '' || $scope.form.time2 == '') {
					$scope.errormessage = 'Enter your date and time for Pick-up.';
					return;
				}
				// Check datetime pickup
				var one = new Date($scope.form.date + ' ' + $scope.form.time);
				var two = new Date($scope.form.date2 + ' ' + $scope.form.time2);
				if (two.getTime() < one.getTime()) {
					$scope.errormessage = 'Date and time for pick-up must be later than the drop-off time. Please check and confirm the pick-up time.';
					return;
				}
			}

			$scope.errormessage = '';
			if ($scope.form.addressfull != '') {
				var findAddress = $http.get('https://api.smartystreets.com/street-address?'
			        +'auth-id=ef98fd2a-a28d-3de6-db0e-2a5b6d845e92'
			        +'&auth-token=839CwxIjpjq5Txwi27rQ'
			        +'&candidates=10'
			        +'&street=' + encodeURI($scope.form.addressfull)
		        );
		        
		        findAddress.then(function(a){
		            if(a.status !== 200){
		            	$scope.errormessage = "Your address is invalid (1)";
		            	return;
		            }
		        
		            if(a.status === 200 && a.data.length === 0){
		            	$scope.errormessage = "Your address is invalid (2)";
		            	return;
		            }
		            //
					$("img.loading-img").removeClass('hidden');
					$("button.submit-btn").addClass('hidden');
					$http.post('/api/deliverPickup', { 'form' : $scope.form, 'cuskey' : cuskey })
		            .success(function (res) {
		            	if (res.error) {
		            		$scope.errormessage = res.error;
		                } else {
		                	setTimeout(function(){
		                		window.location.href = window.location.href;
	                		}, 5000);
		                	$scope.isShow = true;
		                	$scope.errormessage = '';
		                }
		            	$("img.loading-img").addClass('hidden');
						$(".submit-btn").removeClass('hidden');
		            });
		        },function(b){
		        	$scope.errormessage = "Your address is invalid (3)";
		        });
				
			}
			else
			{
				$('#overlay').show();
				$("button.submit-btn").addClass('hidden');
				 $http({
	                method : 'post',
	                url: '/api/deliverPickup',
	                data: { 'form': $scope.form, 'cuskey' : cuskey },
	                cache: false
	            })
	            .success(function (res) {
	            	//console.log(res);
	            	if (res.error) {
	            		$scope.errormessage = res.error;
	                } else {
	                	setTimeout(function(){
	                		window.location.href = window.location.href;
                		}, 1000);
	                	$scope.isShow = true;
	                	$scope.errormessage = '';
	                }
	            	$('#overlay').hide();
					$(".submit-btn").removeClass('hidden');
	            })
                .error(function (err) {
                	$('#overlay').hide();
                    console.log(err);
                    window.location.href = window.location.href;
                });
			}
		};
		
		//Cancel
		$('.act-cancel').on("click", function () {
			var key = $(this).data('id');
		    $scope.storagedkey = key;
		    $scope.form.addressca = '';
		    $scope.form.passwordca = '';
		    $scope.form.phoneca = '';
		    $("#datepicker").val('');
		    $("#timepicker").val('');
		    
		    if (key == 'All') {
		    	key = $($('.act-deliver')[0]).attr('data-id');	// Change key to storage id if cancel all
		    }
		    	
	    	$http.post('/api/getStorageAddress', { 'key': key })
            .success(function (res) {
                 if (res.error) {
                	 $scope.errormessageca = res.error;
                 } else {
                	 $scope.current.addressca = res;
                	 $('#myModalCancel').modal('show');
                 }
             }); 
		});
		// Cancel requesting
		$scope.cancelRequesting = function(storagedkey) {
			
			var cuskey = $('#cuskey').val();
			if (cuskey == undefined || cuskey == '') {
				alert('Got error !');
				return false;
			}
			
			$scope.form.dateca = $("#datepickerca").val();
			$scope.form.timeca = $("#timepickercaval").val();
			$scope.form.timecashow = $("#timepickerca").val();
			$scope.form.storagedkey = storagedkey;
			
			if (($scope.form.addressca != '') 
				&& ($scope.form.passwordca == '')) 
			{
				$scope.errormessageca = 'Enter your password.';
			} 
			else if (($scope.form.addressca != '') 
					&& ($scope.form.passwordca != '')) 
			{
				$scope.form.addressfullca = $scope.form.addressca + ', ' + addressFull[$('#zipcodeca').val()];
			}
			
			//
			if ($scope.form.addressfullca != '') 
			{
				var findAddress = $http.get('https://api.smartystreets.com/street-address?'
			        +'auth-id=ef98fd2a-a28d-3de6-db0e-2a5b6d845e92'
			        +'&auth-token=839CwxIjpjq5Txwi27rQ'
			        +'&candidates=10'
			        +'&street=' + encodeURI($scope.form.addressfullca)
		        );
		        
		        findAddress.then(function(a){
		            if(a.status !== 200){
		            	$scope.errormessageca = "Your address is invalid (1)";
		            	return;
		            }
		        
		            if(a.status === 200 && a.data.length === 0){
		            	$scope.errormessageca = "Your address is invalid (2)";
		            	return;
		            }
		            //
		            if ($scope.form.dateca != '' && $scope.form.timeca != '') 
		            {
		            	$("img.loading-img").removeClass('hidden');
						$("button.submit-btn").addClass('hidden');
						$http.post('/api/cancelRequesting', { 'form': $scope.form, 'cuskey' : cuskey })
			            .success(function (res) {
			            	if (res.error != undefined) {
			            		$scope.errormessageca = res.error;
			                } else {
			                	setTimeout(function(){
			                		window.location.href = window.location.href;
		                		}, 1000);
			                	$scope.isShow = true;
			                	$scope.errormessageca = '';
			                }
			            	$("img.loading-img").addClass('hidden');
							$(".submit-btn").removeClass('hidden');
			            })
			            .error(function (err) {
	                        console.log(err);
	                    });
		            }
		            else 
		            {
		            	$scope.errormessageca = 'Enter your date and time.';
		            }
		        },function(b){
		        	$scope.errormessageca = "Your address is invalid (3)";
		        });
				
			}
			else if ($scope.form.dateca != '' && $scope.form.timeca != '') 
            {
				$('#overlay').show();
				$("button.submit-btn").addClass('hidden');
				$http.post('/api/cancelRequesting', { 'form': $scope.form, 'cuskey' : cuskey })
	            .success(function (res) {
	            	//console.log(res)
	            	if (res.error) {
	            		$scope.errormessageca = res.message;
	                } else {
	                	setTimeout(function(){
	                		window.location.href = window.location.href;
                		}, 1000);
	                	$scope.isShow = true;
	                	$scope.errormessageca = '';
	                }
	            	$('#overlay').hide();
					$(".submit-btn").removeClass('hidden');
	            });
            }
            else 
            {
            	$scope.errormessageca = 'Enter your date and time.';
            }
		}

		// Delete image
		$scope.deleteimage = function(storage, imgname) {
			
			$http.post('/api/deleteimage', { 'storage': storage, 'image': imgname })
            .success(function (res) {
            	//console.log(res);
            	if (!res.error) {
            		window.location.href = "/mystuff";
            	} else {
            		alert(res.error);
            	}
            })
            .error(function (err) {
                alert('Error !!!');
            });
		};
		//
        $scope.getAvailableTime = function(date, idname){
			$http
			.get('/api/getAvailableTime?date=' + date)
			.success(function (data) {
			 	//console.log(data);
			 	if (data.error) {
			 		$scope.errors.push(data.error);
			 	} else if (idname == 'datepicker') {
			 		$('#timepicker').val('');
		 			$scope.availableTime = data;
		 		} else if (idname == 'datepicker2') {
		 			$('#timepicker2').val('');
		 			$scope.availableTime2 = data;
		 		} else if (idname == 'datepickerca') {
		 			$('#timepickerca').val('');
		 			$scope.availableTimeca = data;
		 		} else {
		 			$scope.availableTime = data;
		 			$scope.availableTime2 = data;
		 			$scope.availableTimeca = data;
		 		}
			})
			.error(function (err) {
				//console.log(err);
		        $scope.errors.push(err);
		    });
		};
		// Time available
		var now = moment().add(1, 'd').format('MM/DD/YYYY');
		$scope.getAvailableTime(now, '');
		//
   	 	$('#datepicker, #datepicker2, #datepickerca').on("dp.change", function(e) {
   	 		var date = $(this).val();
   	 		var idname = $(this).attr('id');
   	 		// Set minDate of pick-up datepicker when change drop-off datepicker  
   	 		if ($(this).attr('id') == 'datepicker') {
   	 			$('#drop-off-options').find('.pickuptime').hide();
   	 			$('input:radio[name=drop-pick-option][value=1]').prop('checked', true);
	   	 		$scope.$apply(function () {
	   	 			$scope.form.option = '1';
		        });
   	 			$('#datepicker2').val(date);
	 			$('#datepicker2').data("DateTimePicker").minDate($('#datepicker').data("DateTimePicker").date());
   	 			$('#datepicker2').data("DateTimePicker").date($('#datepicker').data("DateTimePicker").date());
   	 			$scope.getAvailableTime(date, 'datepicker2');
   	 			$("#timepicker2val").val('');
   	 			$("#timepickerval").val('');
	   	 		
	 		}
   	 		$scope.isShowTime = false;
 			$scope.isShowTime2 = false;
 			$scope.isShowTimeca = false;
   	 		$scope.getAvailableTime(date, idname);
        });
   	 	
   	 	/*
   	 	 * Athor: Chuong
   	 	 * Check date and time drop-off before change to option pick-up date and time
   	 	 */
   	 	$('input:radio[name=drop-pick-option]').on('change', function() {
			if ($(this).attr('id') == 'drop-pick-option-1') {
				$(this).closest('div').find('.pickuptime').hide();
				$scope.$apply(function () {
	   	 			$scope.form.option = '1';
		        });
			} else {
				if ($('#datepicker').val() == '' || $('#timepicker').val() == '') {
					$('input:radio[name=drop-pick-option][value=1]').prop('checked', true);
					$scope.$apply(function () {
						$scope.errormessage = 'Enter your date and time for Drop-off.';
						$scope.form.option = '1';
			        });
				} else {
					$scope.$apply(function () {
						$scope.errormessage = '';
			        });
					
					$(this).closest('div').find('.pickuptime').show();
				}
			}			
		});
   	 	
		//
   	 	$scope.onSelect = function(time, text, idname){
        	if (text == undefined) {
            	$('#' + idname).val(time[1]);
            	$('#' + idname + 'val').val(time[0]);
            	$scope.isShowTime = false;
     			$scope.isShowTime2 = false;
     			$scope.isShowTimeca = false;
        	}
        };
        //
        $scope.processTime = function(idname) {
        	
        	if (idname == 'timepicker') {
        		if ($('#datepicker').val() == '') return false;
        		$scope.isShowTime = ! $scope.isShowTime && ($scope.availableTime != null);
     			$scope.isShowTime2 = false;
     			$scope.isShowTimeca = false;
	 		} else if (idname == 'timepicker2') {
	 			if ($('#datepicker2').val() == '') return false;
	 			$scope.isShowTime = false;
     			$scope.isShowTime2 = ! $scope.isShowTime2 && ($scope.availableTime2 != null);
     			$scope.isShowTimeca = false;
	 		} else if (idname == 'timepickerca') {
	 			if ($('#datepickerca').val() == '') return false;
	 			$scope.isShowTime = false;
     			$scope.isShowTime2 = false;
     			$scope.isShowTimeca = ! $scope.isShowTimeca && ($scope.availableTimeca != null);
	 		} else {
	 			$scope.isShowTime = false;
     			$scope.isShowTime2 = false;
     			$scope.isShowTimeca = false;
	 		}
        }
		
        /*$scope.handleBlur = function(a) {
        	alert(a);
        };*/
		//
		$('#form-phone, #form-phoneca').keydown(function (e) {
    		var key = e.charCode || e.keyCode || 0;
    		$phone = $(this);
    		// Auto-format- do not expose the mask as the user begins to type
    		if (key !== 8 && key !== 9) {
    			if ($phone.val().length === 1 || $phone.val().length === 0) {
    				$phone.val('(');
    			}
    			if ($phone.val().length === 4) {
    				$phone.val($phone.val() + ')');
    			}
    			if ($phone.val().length === 5) {
    				$phone.val($phone.val() + ' ');
    			}			
    			if ($phone.val().length === 9) {
    				$phone.val($phone.val() + '-');
    			}
    		}

    		// Allow numeric (and tab, backspace, delete) keys only
    		return (key == 8 || 
    				key == 9 ||
    				key == 46 ||
    				(key >= 48 && key <= 57) ||
    				(key >= 96 && key <= 105));	
    	})
    	.bind('focus click', function () {
    		$phone = $(this);
    		
    		if ($phone.val().length === 0) {
    			$phone.val('(');
    		}
    		else {
    			var val = $phone.val();
    			$phone.val('').val(val); // Ensure cursor remains at the end
    		}
    	})
    	.blur(function () {
    		$phone = $(this);
    		
    		if ($phone.val() === '(') {
    			$phone.val('');
    		}
    	});
		
		// Change content
		$scope.changeContent = function(storagedkey) {
			var getvalue = $('#pwd_' + storagedkey).val();
			if (getvalue != '') {
				var storeVal = $('.show-content-' + storagedkey).html();
				$('.show-content-' + storagedkey).html(getvalue);
				$http.post('/api/changeContent', { 'storagedkey' : storagedkey, 'content': getvalue })
	            .success(function (res) {
	            	if (res.error) {
	                	alert(res.error);
	                	$('.show-content-' + storagedkey).html(storeVal);
	                } else {
	                	$('.desc-content-' + storagedkey).html(getvalue);
	                	 $( ".show-content-" + storagedkey ).html(getvalue)
	                	$('.desc-editor-' + storagedkey).css('display', 'none');
	                	$( ".show-desc-" + storagedkey ).bind({
        	    	    	mouseenter: function() {
        	        			$('.desc-text-' + storagedkey).css('display', 'block');
        	    			}
        	    	    });
        	    	    
        	    	    $( ".show-content-" + storagedkey ).bind({
        	    	    	mouseenter: function() {
        	        			$('.desc-content-' + storagedkey).css('display', 'block');
        	    			}
        	    	    });
	                }
	            });
			}
		}
		
		// Show popup with large image when click on image
	    $('.item-his-popup .upimage, .barcode-image').on('click', function() {
	    	var newImg = new Image();
	    	newImg.src = $(this).attr('src');
	    	var width = $(document).width() < 768 ? 'auto' : newImg.width +'px';
	    	$('.imagepreview').attr('src', $(this).attr('src'));
	    	if (width != 'auto') {
	    		$('#imagemodal .modal-dialog').css('width', width)
	    	}	        
			$('#imagemodal').modal('show');
	    });

	    $( ".item-mystuff-show-desc" ).bind({
	    	mouseenter: function() {
    			$(this).parent().find('.item-mystuff-desc-text').css('display', 'block');
			}, 
			mouseleave: function() {
				$(this).parent().find('.item-mystuff-desc-text').css('display', 'none');
			}
	    });
	    
	    $( ".item-mystuff-show-content" ).bind({
	    	mouseenter: function() {
    			$(this).parent().find('.item-mystuff-desc-content').css('display', 'block');
			}, 
			mouseleave: function() {
				$(this).parent().find('.item-mystuff-desc-content').css('display', 'none');
			}
	    });
	    
	    $( ".item-mystuff-show-desc, .label-cancel" ).click(function(){
	    	id = $(this).data("id");
	    	t = $('.desc-editor-' + id);
	    	if (t.is(':visible')) {
	    		t.css('display', 'none');
	    		$( ".show-desc-" + id ).bind({
	    	    	mouseenter: function() {
	        			$('.desc-text-' + id).css('display', 'block');
	    			}
	    	    });
	    	    
	    	    $( ".show-content-" + id ).bind({
	    	    	mouseenter: function() {
	        			$('.desc-content-' + id).css('display', 'block');
	    			}
	    	    });
	    	} else {
	    		t.css('display', 'block');
		    	$( ".show-desc-" + id ).unbind('mouseenter');
		    	$( ".show-content-" + id ).unbind('mouseenter');
	    	}
	    	
	    });
	    
    }])
    
    .controller('HistoryController', ['$scope', '$http', function ($scope, $http) {
    	$('.barcode-image').on('click', function() {
	    	var newImg = new Image();
	    	newImg.src = $(this).attr('src');
	    	var width = $(document).width() < 768 ? 'auto' : newImg.width +'px';
	    	$('.imagepreview').attr('src', $(this).attr('src'));
	    	if (width != 'auto') {
	    		$('#imagemodal .modal-dialog').css('width', width)
	    	}
			$('#imagemodal').modal('show');
	    });
    }])

    .controller('LoginController', ['$scope', '$http', function ($scope, $http) {
    	$scope.isshowpass = false;
    	$scope.emailerror = '';
    	$scope.emailsuccess = '';
		$scope.forgotpw = function(){
			if (validateEmail($scope.fgpwemail)) {
				
				$('.btn-submit').prop('disabled', true);
				$('.btn-submit').addClass('bg-loading');
				
				$http.post('/api/forgotpw', { 'email': $scope.fgpwemail })
                .success(function (res) {
                	$('.btn-submit').removeClass('bg-loading');
                    if (res.error) {
                    	$scope.emailerror = res.error;
                    	$('.btn-submit').prop('disabled', false);
                    } else if(res == 1) {
                    	$scope.emailsuccess = 'Password reset e-mail has been sent';
                    	setTimeout(function(){ 
                    		$('#myModalForgotpw').modal('hide'); 
                		}, 5000);
                    } else {
                    	console.log(res);
                    }
                });
				
			} else {
				$scope.emailerror = 'Email is invalid';
			}
    	}
    }])
    .controller('SettingController', ['$scope', '$http', function ($scope, $http) {
    
        //
    	$('#setting-phone').keydown(function (e) {
    		var key = e.charCode || e.keyCode || 0;
    		$phone = $(this);
    		// Auto-format- do not expose the mask as the user begins to type
    		if (key !== 8 && key !== 9) {
    			if ($phone.val().length === 1 || $phone.val().length === 0) {
    				$phone.val('(');
    			}
    			if ($phone.val().length === 4) {
    				$phone.val($phone.val() + ')');
    			}
    			if ($phone.val().length === 5) {
    				$phone.val($phone.val() + ' ');
    			}			
    			if ($phone.val().length === 9) {
    				$phone.val($phone.val() + '-');
    			}
    		}

    		// Allow numeric (and tab, backspace, delete) keys only
    		return (key == 8 || 
    				key == 9 ||
    				key == 46 ||
    				(key >= 48 && key <= 57) ||
    				(key >= 96 && key <= 105));	
    	})
    	.bind('focus click', function () {
    		$phone = $(this);
    		
    		if ($phone.val().length === 0) {
    			$phone.val('(');
    		}
    		else {
    			var val = $phone.val();
    			$phone.val('').val(val); // Ensure cursor remains at the end
    		}
    	})
    	.blur(function () {
    		$phone = $(this);
    		
    		if ($phone.val() === '(') {
    			$phone.val('');
    		}
    	});
    	
    }])
    .controller('OrderController', ['$scope', '$http', function ($scope, $http) {
    	
    	$scope.orderbutton = 'Next';
    	$scope.availableTime = null;
        $scope.isShowTime = false;
        $scope.partial = 1;
        $scope.init = false;
        $scope.zipcode = "";
        $scope.city = "";
    	$scope.addressend = "";
        //order 1       
        $scope.order = {
        	promo : "",
        	aptsuite : "",
        	zipcode : "",
            large: 0,
            extlarge: 0,
            otherstuff: 0,
            dropoffDate: "",
            formatdropoffDate: function () {
                var daysInWeeks = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                var monthsInYear = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                var date = new Date($scope.order.dropoffDate);
                if (isNaN(date.getTime())) {
                    date = new Date();
                }
                return daysInWeeks[date.getDay()]
                    + ", " + monthsInYear[date.getMonth()]
                    + " " + date.getDate()
                    + ", " + date.getFullYear();
            },
            dropoffTime: "",
            dropoffTimeShow : "",
            address: "",
            addressfull: "",
            physicalAddress : "",
            phone: "",
            specialRequest: "",
            totalMonthly: function () {
                var money = ($scope.items["large"].price * $scope.order.large)
                    + ($scope.items["extlarge"].price * $scope.order.extlarge)
                    + ($scope.items["otherstuff"].price * $scope.order.otherstuff);
                return isNaN(money) ? 0 : money;
            },
            card: {
                card_number: "",
                exp: "",               
                cvc: "",
                expM: function () {
                    if($scope.order.card.exp.indexOf('/') > -1)
                        return $scope.order.card.exp.split('/')[0];
                    else if($scope.order.card.exp.indexOf(' ') > -1)
                        return $scope.order.card.exp.split(' ')[0];
                    else
                        return $scope.order.card.exp.substring(0,2);
                },
                expY: function () {
                    if($scope.order.card.exp.indexOf('/') > -1)
                        return $scope.order.card.exp.split('/')[1];
                    else if($scope.order.card.exp.indexOf(' ') > -1)
                        return $scope.order.card.exp.split(' ')[1];
                    else
                        return $scope.order.card.exp.substring(2,$scope.order.card.exp.length);
                }          
            },
            account:{
            	full_name	: "",
            	email 		: ""
            }
        }

        $scope.items = {
            large: {
                price: 0,
                total: 0,
                total_rented: 0
            },
            extlarge: {
                price: 0,
                total: 0,
                total_rented: 0
            },
            otherstuff: {
                price: 0,
                total: 0,
                total_rented: 0
            },
            max: function (type) {
            	return 99;
                //return $scope.items[type].total - $scope.items[type].total_rented;
            },
        };

        $scope.errors = [];

        var valid = {
            Step1: function () {
            	
            	$scope.passStep = true;
            	var binTotal = parseInt($scope.order.large) + parseInt($scope.order.extlarge) + parseInt($scope.order.otherstuff);
                if (binTotal > 34) {
                    // $scope.errors.push("Sorry ! For all items less than 35 lbs");
                    toastr["error"]('Sorry! For all items less than 35 lbs.');
                    $scope.passStep = false;
                }

                if (($scope.order.large > $scope.items.max('large'))
                    || ($scope.order.extlarge > $scope.items.max('extlarge'))
                    || ($scope.order.otherstuff > $scope.items.max('otherstuff'))
                    ) {
                    // $scope.errors.push("Sorry ! Large Bins is out");
                	toastr["error"]('Sorry! Large Bins is out.');
                    $scope.passStep = false;
                }

                if ($scope.order.totalMonthly() == 0) {
                    // $scope.errors.push("How many Bins do you need ?");
                    toastr["error"]('How many Bins do you need?');
                    $scope.passStep = false;
                }

                var dropoffDate = document.getElementById('datepicker').value;
                if (dropoffDate.trim() === "") {
                    // $scope.errors.push("When (date) are you free to receive your empty bins ?");
                    toastr["error"]('When (date) are you free to receive your empty bins?');
                    document.getElementById('datepicker').setAttribute('data-error', '');
                    $scope.passStep = false;
                } else {
                    document.getElementById('datepicker').removeAttribute('data-error');
                }

                var dropoffTimeShow = document.getElementById('timepicker').value;
                var dropoffTime = document.getElementById('timepickerval').value;
                if (dropoffTime.trim() === "") {
                    // $scope.errors.push("When (time) are you free to receive your empty bins ?");
                    toastr["error"]('When (time) are you free to receive your empty bins?');
                    document.getElementById('timepicker').setAttribute('data-error', '');
                    $scope.passStep = false;
                } else {
                    document.getElementById('timepicker').removeAttribute('data-error');
                }

                if ($scope.order.physicalAddress == '') {
                	if ($scope.order.address.trim() === "") {
                        // $scope.errors.push("Where will all deliveries take place ?");
                        toastr["error"]('Where will all deliveries take place?');
                        document.getElementById('form-address').setAttribute('data-error', '');
                        $scope.passStep = false;
                    } else {
                        document.getElementById('form-address').removeAttribute('data-error');
                    }
                }

                if ($scope.order.phone.trim() === "" || !validatePhone($scope.order.phone)) {
                    // $scope.errors.push("Check your phone and format (123) 456-7890 ?");
                    toastr["error"]('Check your phone and format (123) 456-7890?');
                    document.getElementById('form-phone').setAttribute('data-error', '');
                    $scope.passStep = false;
                } else {
                    document.getElementById('form-phone').removeAttribute('data-error');
                }
                
                // Check Other stuff description if have other stuff
                if ($scope.order.otherstuff && ! $scope.order.otherDes) {
                	document.getElementById('form-other-des').setAttribute('data-error', '');
                	// $scope.errors.push("Other stuff description is required!");
                	toastr["error"]('Other stuff description is required!');
                    $scope.passStep = false;
                } else {
                	document.getElementById('form-other-des').removeAttribute('data-error');
                }
                
                //API check address
                if ($scope.passStep) {
                	
                	var aptSuite = ($scope.order.aptsuite != "") ? ', ' + $scope.order.aptsuite : '';
                	var getAddress = ($scope.order.address.trim() != '') ? $scope.order.address.trim() + aptSuite : $scope.order.physicalAddress;
                	$scope.order.addressfull = getAddress + ', '+ $scope.addressend;
                	$scope.order.cityzipcode = $scope.addressend;
                	$scope.order.addressstreet = ($scope.order.address.trim() != '') ? $scope.order.address.trim() : $scope.order.physicalAddress;
                	
                	var findAddress = $http.get('https://api.smartystreets.com/street-address?'
                    +'auth-id=ef98fd2a-a28d-3de6-db0e-2a5b6d845e92'
                    +'&auth-token=839CwxIjpjq5Txwi27rQ'
                    +'&candidates=10'
                    +'&street=' + encodeURI($scope.order.addressfull)
                    );
                    
                    findAddress.then(function(a){
                        if(a.status !== 200){
                            // $scope.errors.push("Your address invalid");
                            toastr["error"]('Your address invalid.');
                            $scope.passStep = false;
                        }   
                    
                        if(a.status === 200 && a.data.length === 0){
                            // $scope.errors.push("We can't find your address");
                            toastr["error"]('We can\'t find your address.');
                            $scope.passStep = false;
                        }
                        
                        if ($scope.passStep == false) return false;
                        
                        $scope.order.dropoffDate = dropoffDate
                        $scope.order.dropoffTime = dropoffTime;
                        $scope.order.dropoffTimeShow = dropoffTimeShow; 
                        $scope.partial++;
                        
                    },function(b){
                        // $scope.errors.push("We can't find your address");
                        toastr["error"]('We can\'t find your address.');
                    });
                	
                }    
            },
            Step2: function () {
                $scope.partial++;
            },
            Step3: function () {
                $scope.passStep = true;

                if($('#full_name').length > 0){
	                if ($scope.order.account.full_name == "") {
	                    document.getElementById('full_name').setAttribute('data-error', '');
	                    toastr["error"]('Please input name.');
	                    $scope.passStep = false;
	                } else 
	                	document.getElementById('full_name').removeAttribute('data-error', '');
	            }

	            if($('#email').length > 0){
	                if ($scope.order.account.email == "") {
	                    document.getElementById('email').setAttribute('data-error', '');
	                    toastr["error"]('Please input email.');
	                    $scope.passStep = false;
	                } else
	                	document.getElementById('email').removeAttribute('data-error', '');
	            }

                if (!Stripe.card.validateCardNumber($scope.order.card.card_number)) {
                    document.getElementById('cardNumber').setAttribute('data-error', '');
                    toastr["error"]('Please input card number.');
                    $scope.passStep = false;
                } else
                    document.getElementById('cardNumber').removeAttribute('data-error', '');

                if (!Stripe.card.validateCVC($scope.order.card.cvc)) {
                    document.getElementById('cardCVC').setAttribute('data-error', '');
                    toastr["error"]('Please input card cvc.');
                    $scope.passStep = false;
                } else
                    document.getElementById('cardCVC').removeAttribute('data-error', '');

                if (!Stripe.card.validateExpiry($scope.order.card.exp)) {
                    document.getElementById('cardExpiration').setAttribute('data-error', '');
                    toastr["error"]('Please input card expiration.');
                    $scope.passStep = false;
                } else
                    document.getElementById('cardExpiration').removeAttribute('data-error', '');
                
                if ( ($scope.order.promo != '') && (! promotionCodeValid($scope.order.promo)) ) 
                {
                	document.getElementById('order-promo-code').setAttribute('data-error', '');
                	toastr["error"]('Invalid promotion code.');
                    $scope.passStep = false;
                } 
                else if ($scope.order.promo != '') 
                {
                	$('.order-promo-checking').removeClass('hidden');
                	$http({
                        url : "/api/checkPromo",
                        method : 'POST',
                        async : false,
                        params : {'promo' : $scope.order.promo}
                    }).success(function(data) {
                    	if (data.error != undefined) {
                    		// alert(data.error);
                    		toastr["error"](data.error);
                    		document.getElementById('order-promo-code').setAttribute('data-error', '');
                        	$scope.passStep = false;
                    	} else {
                    		
                    		document.getElementById('order-promo-code').removeAttribute('data-error', '');
                    		
                    		// Check accept T&C checkbox
                            if ($scope.passStep && ! $('#orderaccept').prop( "checked" )) {
                            	$('.chb-tc label').css('color', 'red');
                            	$scope.passStep = false;
                            	// alert('You must agree to our Terms & Conditions before we can finalize your order.');
                            	toastr["error"]('You must agree to our Terms & Conditions before we can finalize your order.');
                            }

                            if ($scope.passStep != true) return;

                            // Post
                            // $scope.postOrder();
                            $scope.createAccount();

                    	}
                    	
                    	$('.order-promo-checking').addClass('hidden');
                    	
                    }).error(function (err) {
                    	console.log(err);
                    	$('.order-promo-checking').addClass('hidden');
                    	document.getElementById('order-promo-code').setAttribute('data-error', '');
                    	$scope.passStep = false;
                    });
                	
                } else {
                	document.getElementById('order-promo-code').removeAttribute('data-error', '');
                	
                	// Check accept T&C checkbox
                    if ($scope.passStep && ! $('#orderaccept').prop( "checked" )) {
                    	$('.chb-tc label').css('color', 'red');
                    	$scope.passStep = false;
                    	// alert('You must agree to our Terms & Conditions before we can finalize your order.');
                    	toastr["error"]('You must agree to our Terms & Conditions before we can finalize your order.');
                    }

                    if ($scope.passStep != true) return;
                    
                   	$scope.createAccount();

                   	// $scope.postOrder();
                }
            },
            Step4: function () {
                $scope.postOrder();
            },
            Step5: function () {
                window.location.href = "/mystuff";
            }
        }
        
        //Post order create account

        $scope.createAccount = function () {

        	if($('#email').length > 0){
            	document.getElementById('email').setAttribute('data-error', '');
            }

            $scope.order.zipcode = $scope.zipcode;
            $http.post("/api/create-account", $scope.order)
            .success(function (data) {
            	if(data.status == 'success'){
            		if( data.action == 'userCreate' ){
            			toastr["success"](data.message);
            			valid["Step4"]();
            		} else {
            			valid["Step4"]();
            		}
            	} else {
            		toastr["error"](data.message);
            	}
            })
            .error(function (err) {
            	console.log(err);
            });

        }

        // Post Order Process
        $scope.postOrder = function(){
        	
        	$('#payment-mess').hide();
            $('#overlay').show();

        	$scope.order.zipcode = $scope.zipcode;
            $http.post("/api/order", $scope.order)
            .success(function (data) {

				// if (data['trancode'] != undefined) {
				// 	$scope.order.trancode = data['trancode'];
				// 	$scope.partial = 4;
				// 	$scope.orderbutton = 'My Account';
				// } else {
				// 	alert('Got error');
				// }

            	if(data.status == 'success'){
            		if( data.transcode != '' ){
            			$scope.order.trancode = data.transcode;
	                    $scope.partial 		  = 5;
	                    $scope.orderbutton 	  = 'My Account';
	                    toastr["success"](data.message)
            		}
            	} else {
            		toastr["error"](data.message)

            		if(data.error_field){
            			if(data.error_field != ''){
            				document.getElementById(data.error_field).setAttribute('data-error', '');
            			}
            		}

            		if( data.redirect ) {
	            		if( data.redirect != '' ){
	            			window.setTimeout(function(){
								window.location.href = data.redirect;
							}, 1000);
	            		}
	            	}
            	}
            	$('#overlay').hide();
            })
            .error(function (err) {
            	console.log(err);
            	$('#overlay').hide();
            	if (err.message) {
            		$scope.errors.push(err.message);
                    if (err.code === 1) {
                        $scope.items['large'] = err.data['large'];
                        $scope.items['extlarge'] = err.data['extlarge'];
                        $scope.items['otherstuff'] = err.data['otherstuff'];
                    }
                    $scope.partial = err.step;
            	} else {
            		alert('Error timeout.');
            	}
                
            });

        }

        $scope.next = function () {
        	console.log('test');
            $scope.errors = [];
            valid["Step" + $scope.partial]();
        }

        $scope.back = function () {
            if ($scope.partial > 1) $scope.partial--;
        }
       
        //init
        $http
            .post('/api/getItemPrice')
            .success(function (data) {
                $scope.items['large'] = data['large'];
                $scope.items['extlarge'] = data['extlarge'];
                $scope.items['otherstuff'] = data['otherstuff'];
                $scope.init = true;
            })
            .error(function (err) {
                $scope.errors.push(err['message']);
            });
        
        $scope.removeSlashes = function(text) {
        	if (text != undefined) {
        		return text.replace(/\\/g, "");
        	}
        };
        
        // Time available
        $scope.getAvailableTime = function(){
   	 		var date = $('#datepicker').val();
			$http
			.get('/api/getAvailableTime?date=' + date)
			.success(function (data) {
			 	if (data.error) {
			 		$scope.errors.push(data.error);
			 	} else {
			 		$scope.availableTime = data;
			 	}
			})
			.error(function (err) {
		        $scope.errors.push(err);
		    });
		};
		// Init
        $scope.getAvailableTime();
   	 	$('#datepicker').on("dp.change", function(e) {
   	 		$('#timepicker').val('');
   	 		$('#timepickerval').val('');
   	 		$scope.availableTime = null;
   	 		$scope.getAvailableTime();
        });
		//
   	 	$scope.onSelect = function(time, text){
   	 		//alert(time);
        	if (text == undefined) {
            	$('#timepicker').val(time[1]);
            	$('#timepickerval').val(time[0]);
            	$scope.isShowTime = ! $scope.isShowTime;
        	}
        };
        //
        $('#form-phone').keydown(function (e) {
    		var key = e.charCode || e.keyCode || 0;
    		$phone = $(this);
    		// Auto-format- do not expose the mask as the user begins to type
    		if (key !== 8 && key !== 9) {
    			if ($phone.val().length === 1 || $phone.val().length === 0) {
    				$phone.val('(');
    			}
    			if ($phone.val().length === 4) {
    				$phone.val($phone.val() + ')');
    			}
    			if ($phone.val().length === 5) {
    				$phone.val($phone.val() + ' ');
    			}			
    			if ($phone.val().length === 9) {
    				$phone.val($phone.val() + '-');
    			}
    		}

    		// Allow numeric (and tab, backspace, delete) keys only
    		return (key == 8 || 
    				key == 9 ||
    				key == 46 ||
    				(key >= 48 && key <= 57) ||
    				(key >= 96 && key <= 105));	
    	})
    	.bind('focus click', function () {
    		$phone = $(this);
    		
    		if ($phone.val().length === 0) {
    			$phone.val('(');
    		}
    		else {
    			var val = $phone.val();
    			$phone.val('').val(val); // Ensure cursor remains at the end
    		}
    	})
    	.blur(function () {
    		$phone = $(this);
    		
    		if ($phone.val() === '(') {
    			$phone.val('');
    		}
    	});
        
        $('#cardExpiration').keydown(function (e) {
    		var key 		= e.charCode || e.keyCode || 0,
    			$expiration = $(this);
    		
    		// Make sure maxlength is 5 character
    		if (key !== 8 && key !== 9) {
    			if ($expiration.val().length === 2) {
    				$expiration.val($expiration.val() + '/');
    			}
    			
    			if ($expiration.val().length === 5) {
    				$expiration.val($expiration.val().slice(0, -1));
    			}
    		}    		
    		
    		// Allow numeric (and tab, backspace, delete) keys only
    		return (key == 8 || 
    				key == 9 ||
    				key == 46 ||
    				(key >= 48 && key <= 57) ||
    				(key >= 96 && key <= 105));
    	})
    	.bind('focus click', function () {
    		var $expiration = $(this),
    			val 		= $expiration.val();
    		
			$expiration.val('').val(val); // Ensure cursor remains at the end
    	})

    }])
    .controller('ReferController', ['$scope', '$http', function ($scope, $http) {
        $scope.referEmail = "";
        $scope.errors = [];
        $scope.share = function () {
            $scope.errors = [];
            if (!validateEmail($scope.referEmail)) {
                document.getElementById('email').setAttribute('data-error', '');
                $scope.errors.push('Email is not valid');
                return;
            } else
                document.getElementById('email').removeAttribute('data-error', '');

            $scope.processing = true;
            $http.post("/api/referral", { email: $scope.referEmail })
                .success(function (data) {
                    $scope.processing = false;
                    $scope.success = true;
                })
                .error(function (err) {
                    $scope.processing = false;
                })

        }
    }]);

function promotionCodeValid( code ) {
    var re = /^[0-9a-zA-Z]+$/;
    return re.test(code);
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validatePhone(phone) {
	var re = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
	return re.test(phone);
}