function NunmberGroup(selector) {
    var _ = this;
    var group = selector;
    var input = group.querySelector('input.amount');
    var decrease = group.querySelector('.decrease');
    var increase = group.querySelector('.increase');
    var regex = /^\d+$/;

    function valid() {
        if (regex.test(input.value)){
            return true;
        }
            
        return false;
    }

    decrease.onclick = function () {
        if (valid()) {
            var n = parseInt(input.value);
            if (n <= 0)
                input.value = 0;
            else
                input.value = n - 1;
        }else{
            input.value = 0;
        }
    }

    increase.onclick = function () {
        if (valid()) {
            var n = parseInt(input.value);
            input.value = n + 1;
        }else{
            input.value = 0;
        }
    }   
}

$(document).ready(function() {
	/**
	 * For all Datepicker when change month we update disable date
	 * @author: chuong
	 */
	
	$('#datepicker, #datepicker2, #datepickerca').on("dp.update", function(e) {
		if (e.change == 'M') {
			var newDisableDate = [],
				firstDateView = $(this).closest('div').find('.bootstrap-datetimepicker-widget .datepicker-days table tr:first-child td:first-child').attr('data-day');

			$.get('/api/getdisabledate?date='+ firstDateView, function (res) {
                 if (res['disable_date'].length) {
                	 for(var x in res['disable_date']) {
                		 var item = res['disable_date'][x];
                		 newDisableDate.push(new Date(item));
                	 }
                 }
                 
                 $('#datepicker').data("DateTimePicker").disabledDates(newDisableDate);
             }); 
        }
	});

    toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": true,
      "positionClass": "toast-bottom-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "3000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
});