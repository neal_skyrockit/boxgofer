var BoxgoferAdmin = angular.module('BoxgoferAdmin', []);
BoxgoferAdmin.controller('ClientController', ['$scope', '$http', function ($scope, $http) {
	$scope.messageshow = {
			ishide : true,
			nameclass : 'bg-danger',
			mess : 'Cannot update'
	}
	
    $scope.clientInfo = {
        info:{},
        storage:{}
    };
	var currentEmail = "";
    var currentRowId = "";
    $scope.getClientInfo = function(id){
        $scope.processing = true;
        if(id == '0')
            return ;
              
        if(currentRowId === id)
            return;
            
        if(currentRowId !== "")
            document.getElementById('cl_'+currentRowId).className = "";
            
        currentRowId = id;
        document.getElementById('cl_'+currentRowId).className = "choose";
        $http.post('/api/getClientInfo',{id:id})
        .success(function(data){
            $scope.clientInfo = data;
            $scope.clientInfo.info.created_at = moment(data.info.created_at).format('MMMM D, YYYY hh:mm:ss');
            currentEmail = $scope.clientInfo.info.email;
            $scope.processing = false;
            $('.information-part').removeClass('hidden');
        })
        .error(function(err){
            $scope.processing = false;
        });
    }
    
    // Delete
    $scope.delete = function() {
    	var cf = confirm("Are you sure delete this customer?");
    	if (cf == true) {
    		$scope.changeStatus($scope.clientInfo.info._id);
    	}
    }
    
    // Save
    $scope.save = function(){
    	
    	$scope.messageshow.ishide = true;
    	
    	if ($scope.clientInfo.info.email == undefined 
    			|| $scope.clientInfo.info.email == '' 
    				|| !validateEmail($scope.clientInfo.info.email)) 
    	{
    		$scope.messageshow.ishide = false;
        	$scope.messageshow.nameclass = 'bg-danger text-danger';
        	$scope.messageshow.mess = "Your email is invalid";
        	return false;
    	}
    	
    	if ($scope.clientInfo.info.phone == undefined || $scope.clientInfo.info.phone == ''
    		|| !validatePhone($scope.clientInfo.info.phone)) {
    		$scope.messageshow.ishide = false;
        	$scope.messageshow.nameclass = 'bg-danger text-danger';
        	$scope.messageshow.mess = "Your phone is invalid";
        	return false;
    	}
    	
    	// set zipcode
    	$scope.clientInfo.info.full = $scope.clientInfo.info.physicalAddress + ', ' 
    		+ $scope.clientInfo.info.city + ', TX ' 
    		+ $scope.clientInfo.info.zipcode;
    	
    	var findAddress = $http.get('https://api.smartystreets.com/street-address?'
		        +'auth-id=ef98fd2a-a28d-3de6-db0e-2a5b6d845e92'
		        +'&auth-token=839CwxIjpjq5Txwi27rQ'
		        +'&candidates=10'
		        +'&street=' + encodeURI($scope.clientInfo.info.full)
	        );
	        
        findAddress.then(function(a){

            if(a.status !== 200){
            	$scope.messageshow.ishide = false;
            	$scope.messageshow.nameclass = 'bg-danger text-danger';
            	$scope.messageshow.mess = "Your address is invalid (1)";
            	return false;
            }
        
            if(a.status === 200 && a.data.length === 0){
            	$scope.messageshow.ishide = false;
            	$scope.messageshow.nameclass = 'bg-danger text-danger';
            	$scope.messageshow.mess = "Your address is invalid (2)";
            	return false;
            }
            //
            if (currentEmail == $scope.clientInfo.info.email) {
            	saveData();
            } 
            else 
            {	
            	$http.post('/api/checkEmail', {'email' : $scope.clientInfo.info.email})
                .success(function(data){
                	if (data == 1) {
                		saveData();
                	} else {
                		$scope.messageshow.ishide = false;
                    	$scope.messageshow.nameclass = 'bg-danger text-danger';
                    	$scope.messageshow.mess = data.error;
                	}
                })
                .error(function(err){
                	$scope.messageshow.ishide = true;
                	$scope.messageshow.nameclass = 'bg-danger text-danger';
            		$scope.messageshow.mess = 'Error request';
                    $scope.processing = false;
                });
            }
        },function(b){
        	$scope.errormessage = "Your address is invalid (3)";
        });
    }
    
    // Function save data
    var saveData = function(){
    	//$scope.clientInfo.info.full = fullAddress;
        $scope.processing = true;
        $http.post('/api/saveClientInfo', $scope.clientInfo.info)
        .success(function(data){
        	$scope.messageshow.ishide = true;
        	$scope.messageshow.nameclass = 'bg-danger text-danger';
    		$scope.messageshow.mess = 'Cannot update';
        	if (data == 1) {
        		$scope.messageshow.ishide = false;
        		$scope.messageshow.nameclass = 'bg-success text-success'
        		$scope.messageshow.mess = 'Done'
        	}
            $scope.processing = false;
        })
        .error(function(err){
        	$scope.messageshow.ishide = true;
        	$scope.messageshow.nameclass = 'bg-danger text-danger';
    		$scope.messageshow.mess = 'Error request';
            $scope.processing = false;
        });
    }
    
    // Change status
    $scope.changeStatus = function(id) {
    	$http.post('/api/changeStatusClient', {'id' : id})
        .success(function(data){
        	if (data == 1) {
        		alert('Status is changed.');
        		location.reload(); 
        	} else {
        		alert(data.error);
        	}
        })
        .error(function(err){
        	alert(err);
        });
    }
    
    $('#client-phone').keydown(function (e) {
		var key = e.charCode || e.keyCode || 0;
		$phone = $(this);
		// Auto-format- do not expose the mask as the user begins to type
		if (key !== 8 && key !== 9) {
			if ($phone.val().length === 1 || $phone.val().length === 0) {
				$phone.val('(');
			}
			if ($phone.val().length === 4) {
				$phone.val($phone.val() + ')');
			}
			if ($phone.val().length === 5) {
				$phone.val($phone.val() + ' ');
			}			
			if ($phone.val().length === 9) {
				$phone.val($phone.val() + '-');
			}
		}

		// Allow numeric (and tab, backspace, delete) keys only
		return (key == 8 || 
				key == 9 ||
				key == 46 ||
				(key >= 48 && key <= 57) ||
				(key >= 96 && key <= 105));	
	})
	.bind('focus click', function () {
		$phone = $(this);
		
		if ($phone.val().length === 0) {
			$phone.val('(');
		}
		else {
			var val = $phone.val();
			$phone.val('').val(val); // Ensure cursor remains at the end
		}
	})
	.blur(function () {
		$phone = $(this);
		
		if ($phone.val() === '(') {
			$phone.val('');
		}
	});
    
}])
.controller('TransactionController', ['$scope', '$http', function ($scope, $http) {
    $scope.transaction = {
        info:{},
        bins:[]
    };
    $scope.bins = [];
    $scope.bin = {};
    $scope.availableTime = null;
    $scope.isShowTime = false;
    $scope.currentBinIndex = 0;
    
    var currentRowId = "";
    $scope.getTransInfo = function(transid){ 
        if(transid == '0')
            return ;
        
        $scope.currentTransId = transid;
        if(currentRowId != "")
            document.getElementById('tras_'+currentRowId).className = "";   
                
        currentRowId = transid;
        document.getElementById('tras_'+currentRowId).className = "choose";
        
        $http.post('/api/getTransInfo',{transid:transid})
        .success(function(data){     
            //console.log(data);      
            $scope.transaction.info = data['info'];
            $scope.transaction.bins = data['bins'];
            $scope.detail($scope.transaction.bins[0],1);
            $('.information-part').removeClass('hidden');
        })
        .error(function(err){
            console.log(err);
        });
    }
    
    $scope.setEditDatetime = function(bin) {
    	$scope.editDropOff = false;
    	$scope.editPickUp = false;
    	$scope.editDropOffDate = false;
    	$scope.editPickUpDate = false;
    	var formatDropoff = '';
    	
    	if (bin == null || bin == undefined) {
    		return;
    	}
    	
    	var dropoffList = bin['dropofftime'],
    		pickupList = bin['pickuptime'],
    		now = moment().startOf('day')._d;
    	
    	if (dropoffList.length) {
    		var lastDropoffDate = dropoffList[dropoffList.length - 1],
    			dropoffDate = lastDropoffDate.split(' ')[0];
    		
    		formatDropoff = dropoffDate.split('/')[2] +'-'+ dropoffDate.split('/')[0] +'-'+ dropoffDate.split('/')[1] 
    		
    		var formatDropoffObj = moment(formatDropoff)._d;
    		formatDropoffObj.setHours(0,0,0,0);
    		if (formatDropoffObj > now) {
    			$scope.editDropOffDate = lastDropoffDate;
    		}
    	}
    	
    	if (pickupList.length) {
    		var lastPickupDate = pickupList[pickupList.length - 1],
				pickupDate = lastPickupDate.split(' ')[0],
				pickupDropoff = pickupDate.split('/')[2] +'-'+ pickupDate.split('/')[0] +'-'+ pickupDate.split('/')[1]
    		
    		var pickupDropoffObj = moment(pickupDropoff)._d;
    		pickupDropoffObj.setHours(0,0,0,0);
    		if ( pickupDropoffObj > now) {
    			$scope.editPickUpDate = lastPickupDate;
    		}
    	}    	
    }
    
    $scope.detail = function(b,index){
        if(b === null || b === undefined){
            return ;
        }
        $scope.setEditDatetime(b);
        $scope.bin = b;
        $scope.bin['idx'] = index;
        $scope.currentBinIndex = index - 1;
        clearFile();
    }
    // Delete image
	$scope.deleteimage = function(storage, imgname) {
		$http.post('/api/deleteimage', { 'storage': storage, 'image': imgname })
        .success(function (res) {
        	//console.log(res);
        	if (!res.error) {
        		$scope.bin.images = res;
        	} else {
        		alert(res.error);
        	}
        })
        .error(function (err) {
            alert('Error !!!');
        });
	};
    //
    $scope.uploadImage = function(bin){
        var ele = document.getElementById('file');
        var file = ele.files[0];
        if (file === null || file === undefined)
            return false;
        
        var fd = new FormData();      
        fd.append('file', file);
        
        $http.post('/api/uploadBinImage/'+bin._id, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
        	
        	if( Object.prototype.toString.call( bin.images ) !== '[object Array]' ) {
        		var arr = Object.keys(bin.images).map(function (key) {return bin.images[key]});
        		bin.images = arr;
        	}
        	
        	$scope.bin.images.push(data.image);
            clearFile();
            alert('Done !');
        })
        .error(function(err){
            console.log(err);
            alert('Error !!!');
        });
        
    }
    
    // formatDate option for date with string like "07/30/2016 - 1:00 am"
    $scope.convertDate = function(d, haveTime, formatDate){
        if(d === undefined || d.length == 0)
            return "";
    	
    	var dateStr = d.split(" ")[0];
    	if (formatDate) {
    		dateStr = dateStr.split("/")[2] +'-'+ dateStr.split("/")[0] +'-'+ dateStr.split("/")[1];
    	}
    
	    var date = moment(dateStr),
	    	time = '';
	    
	    if (haveTime) {
	    	time = ' - '+ d.split(" ")[2] +' '+ d.split(" ")[3];
	    }
	    
	    return date.format('MMMM D.YYYY') + time;
    }
    
    $scope.cancelOrder = function(id) {
    	var r = confirm('Are you sure cancel this Order ?');
    	if (r == true) {
    		$('#overlay-admin').show();
    		$http.post('/api/adminCancelOrder', { 'id': id })
            .success(function (res) {
                 if (res.error !== undefined) {
                	 alert(res.error);
                 } else {
                	 location.reload();
                 }
                 $('#overlay-admin').hide();
             }).error(function(err){
                 console.log(err);
                 alert('Error !!!');
             });
    	}
	}
    
    $scope.openDateTimeModal = function(storage_id, status) {
    	$("#datepicker").val('');
		$("#timepicker").val('');
		$scope.errormessage = '';
		$('#datepicker').data("DateTimePicker").date(null);
    	$scope.currentStorageStatus = status;
    	$scope.currentStorageId = storage_id;
    	$('#myModalBin').modal('show');
    }
    
    $scope.changeStorageDateTime = function() {
    	$scope.errormessage = '';
    	var request = {};
    	request.date = $("#datepicker").val();
    	request.time = $("#timepickerval").val();
    	request.storage_id = $scope.currentStorageId;
    	request.storage_status = $scope.currentStorageStatus;
    	
    	if (! request.date || ! request.time) {
    		$scope.errormessage = 'Enter your date and time for '+ request.storage_status +'.';
    		return;
    	}
    	
    	$('#overlay-admin').show();
    	$http.post('/api/changebindatetime', request)
        .success(function(res){
        	$('#overlay-admin').hide();
        	if (res.error) {
        		$scope.errormessage = res.error;
        	} else {
        		$('#overlay-admin').hide();
        		$('#myModalBin').modal('hide');
        		
        		$http.post('/api/getTransInfo',{transid:$scope.currentTransId})
                .success(function(data){     
                    $scope.transaction.info = data['info'];
                    $scope.transaction.bins = data['bins'];
                    $scope.detail($scope.transaction.bins[$scope.currentBinIndex], $scope.currentBinIndex + 1);
                    $('.information-part').removeClass('hidden');
                })
                .error(function(err){
                    console.log(err);
                });
        	}
        })
        .error(function(err){
        	$('#overlay-admin').hide();
            console.log(err);
            alert('Error !!!');
        });
    	
    }
    
    $scope.onSelect = function(time, text, idname){
    	if (text == undefined) {
        	$('#' + idname).val(time[1]);
        	$('#' + idname + 'val').val(time[0]);
        	$scope.isShowTime = false;
    	}
    };
    //
    $scope.processTime = function(idname) {
    	
    	if (idname == 'timepicker') {
    		if ($('#datepicker').val() == '') return false;
    		$scope.isShowTime = ! $scope.isShowTime && ($scope.availableTime != null);
 		} else {
 			$scope.isShowTime = false;
 		}
    }
    
    $scope.getAvailableTime = function(date, idname){
		$http
		.get('/api/getAvailableTime?date=' + date +'&checkcalendar=false')
		.success(function (data) {
		 	//console.log(data);
		 	if (data.error) {
		 		$scope.errors.push(data.error);
		 	} else if (idname == 'datepicker') {
		 		$('#timepicker').val('');
	 			$scope.availableTime = data;
	 		} else {
	 			$scope.availableTime = data;
	 		}
		})
		.error(function (err) {
			//console.log(err);
	        $scope.errors.push(err);
	    });
	};
	
	$('#datepicker').on("dp.change", function(e) {
	 		var date = $(this).val();
	 		var idname = $(this).attr('id');
	 		$scope.isShowTime = false;
	 		$scope.getAvailableTime(date, idname);
	 		$("#timepickerval").val('');
    });
	
	// Time available
	var now = moment().add(1, 'd').format('MM/DD/YYYY');
	$scope.getAvailableTime(now, '');
    
}]).controller('StaffController', ['$scope', '$http', function ($scope, $http) {
    var createNewStaff = function(){
        return {
        	'first_name':''
            ,'name':''
            ,'email':''
            ,'password':''
            ,'role':''
            ,'birthday':''
            ,'ss':''
            ,'driverlicense':''
            ,'phone':''
            ,'salary':0
            ,'address':''
            ,'city':''
            ,'zipcode':''
            ,'position':''
            ,'datehired':''
            ,'comments':''
        };
    }
    $scope.errors = [];
    $scope.staff = {};
    $scope.titleSection = '';
    $scope.action = '';
    
    $scope.staffModel = createNewStaff();
    var currentRowId = "";
    $scope.getStaffInfo = function(staffid){
        if(staffid == 0)
            return;
        
        if(currentRowId != "")
            document.getElementById('staff_'+currentRowId).className = ""; 
                  
        currentRowId = staffid;
        document.getElementById('staff_'+currentRowId).className = "choose";
        
        $http.post('/api/getStaffInfo', {staffid:staffid})
        .success(function(data){
            $scope.staff = data;
            $scope.staffModel = $scope.staff;
            $('.information-part').removeClass('hidden');
        })
        .error(function(err){
            console.log(err);
        });
    };
    
    $scope.upsertStaff = function(){
        $scope.staffModel.birthday = document.getElementById('datepicker_birthday').value;
        $scope.staffModel.datehired = document.getElementById('datepicker_datehire').value;
        $scope.staffModel.action = $scope.action;
        
         $scope.errors = [];
         $http.post('/api/upsertStaff', $scope.staffModel)
         .success(function(data){
            location.reload();
            $scope.staff = data;
         })
         .error(function(err){
        	 for (var index in err['message']) {
        		 $scope.errors.push(err['message'][index]);
        	 }
         });
    }
    
    $scope.resetForm = function(){
        $scope.errors = [];
        $scope.staffModel = createNewStaff();
    }
    
    $scope.deleteStaff = function(id) {
    	var cf = confirm("Are you sure delete this staff?");
    	if (cf == true) {
    		$http.post('/api/deletestaff', {id: id})
            .success(function(data){
               if (data.status) {
            	   location.reload();
               } else {
            	   alert(data.message);
               }
            })
            .error(function(err){
            	alert(err.message);
            });
    	}
    }
    
    $scope.changeToEdit = function() {
    	$scope.titleSection = 'Edit Staff';
    	$scope.action = 'edit';
    	$scope.errors = [];
    	
    	setTimeout(function(){
    		document.getElementById('triggerCreateStaff').checked = true;
    	}, 10);
    }
    
    $scope.changeToCreate = function() {
    	$scope.titleSection = 'Create Staff';
    	$scope.action = 'create';
    	$scope.resetForm();
    }
        
}]).controller('BinController', ['$scope', '$http', function ($scope, $http) {
	
    $scope.bins = [];
    $scope.changes = [];
    $scope.bin_type = [
        {value: 1, name: 'Large'},
        {value: 2, name: 'Extra'}
    ];
    $scope.bs = {
    	type: $scope.bin_type[0],
    	add_num: '',
    	remove_num: '',
    	desc: ''
    }
    
    $scope.editBin = function(id,idx,$event){
        var eleBtn = $event.target;
        var tr = document.getElementById('bin_'+id);
        
        var fields = {
        	total_remain : tr.querySelector('input[name="total_remain"]'),
            price : tr.querySelector('input[name="price"]'),
            description : tr.querySelector('textarea[name="description"]')
        }
        
        eleBtn.disabled = true;
        
        fields.total_remain.removeAttribute('data-error');
        fields.price.removeAttribute('data-error');
        $scope.bins[idx]['err_total'] = undefined;
        $scope.bins[idx]['err_price'] = undefined;
        
        var pdata = {
            binid : id,
            price : fields.price.value.substring(1),
            total_remain : fields.total_remain.value,
            description : fields.description.value
        };
        
        $http.post('/api/updateBin', pdata)
         .success(function(data){
            $scope.bins[idx] = data;
            eleBtn.disabled = false;
         })
         .error(function(err){
            eleBtn.disabled = false;
            $scope.bins[idx]['err_'+err.field] = err.message;
         });
    }
    $scope.changes = [];
    $scope.getChanges = function(changes){
    	$http.post('/api/getchanges')
        .success(function(data){
        	$scope.changes = data ? data : [];
        	$('.information-part').removeClass('hidden');
        })
        .error(function(err){
            console.log(err);
        });
    }
    
    $scope.mappingBin = function(id,name,total,rented,description){
        return {
            id:id,
            name:name,
            total:total,
            total_rented:rented,
            description:description || ""
        }
    }
    
    $scope.bs_submit = function () {
    	var date = $('#bs-datepicker_date').val(),
    		valid = true;
    	if (! date) {
    		$('#bs-datepicker_date').addClass('error-field');
    		valid = false;
    	} else {
    		$('#bs-datepicker_date').removeClass('error-field');
    	}
    	
    	if (isNaN($('#bs-add-num').val())) {
    		$('#bs-add-num').addClass('error-field');
    		valid = false;
    	} else {
    		$('#bs-add-num').removeClass('error-field');
    	}
    	
    	if (isNaN($('#bs-remove-num').val())) {
    		$('#bs-remove-num').addClass('error-field');
    		valid = false;
    	} else {
    		$('#bs-remove-num').removeClass('error-field');
    	}
    	
    	if (valid) {
    		var data = angular.copy($scope.bs);
        	data.type = data.type['value'];
        	data.date = date;
        	
        	$http.post('/api/addBinTrans', data)
            .success(function(data){
	        	$scope.bins[data['type'] - 1] = data;
	        	$scope.bins[data['type'] - 1]['id'] = data['_id'];
	        	
	        	var lastChange = data['changes'][data['changes'].length - 1];
	        	$scope.changes.push(lastChange);
	        })
	        .error(function(err){
	            console.log(err);
	        });
    	}
    	
    	
    }
    
    $scope.getChanges();
    MS = $scope;
    
}]).controller('ChartController', ['$scope', '$http', function ($scope, $http) {
    var binChart = new BinChart();
    var daiyChart = new DailyChart();
    var zipcodeChart = new ZipcodeChart();
    // Manh custom chart
    $http.post('/api/getChartData')
    .success(function(data){
    	//console.log(data);
        if (data.error != undefined) {
        	alert(data.error);
        } else {
        	// Bin
        	binChart.mappingData(data.bin);
            binChart.render('BinChart');
            // Daily
            daiyChart.mappingData(data.daily);
            daiyChart.render('DailyChart');
            // Zipcode
            zipcodeChart.mappingData(data.zipcode);
            zipcodeChart.render('ZipcodeChart');
        }
    })
    .error(function(err){
        console.log(err);
    });

}])
.controller('InvoicesController', ['$scope', '$http', function ($scope, $http) {

	$scope.invoice = {
		_id : '',
		dayspay : '',
		invoice_id : '',
		invoice_num : '',
		clientname : '',
		payby : '',
		items : '',
		clientaddress : '',
		amount : '0 usd'
	}

	$scope.getInvoiceDefault = function (id) 
	{
		$('#invoice_' + id).addClass('choose');
		$scope.getInvoiceInfo(id);
	}

	$scope.getInvoiceInfo = function(id){
        $http.post('/api/getInvoiceInfo', {'id' : id})
        .success(function(data){
        	console.log(data);
        	if (data.error == undefined) {
        		
        		$('.invoice-class').removeClass('choose');
        		$('#invoice_' + data._id).addClass('choose');
        		var itemString = data.items,
					type_item = itemString.substring(itemString.length - 5),
					num_item = itemString.substring(0, itemString.indexOf(type_item));
        		
        		$scope.invoice = {
    				_id : data._id,
    				dayspay : data.dayspay,
    				invoice_id : data.invoice_id,
    				invoice_num : data.invoice_num,
    				clientname : data.clientname,
    				payby : data.payby,
    				items : data.items,
    				clientaddress : data.clientaddress,
    				clientphone : data.clientphone,
    				type_item: type_item,
    				num_item: num_item,
    				amount : data.amount,
    				user_id: typeof(data.user_id) != 'undefined' ? data.user_id : '',
    				last4: typeof(data.last4) != 'undefined' ? data.last4 : '****',
    				data : data.data,
    				amountNoTax: data.amountNoTax,
	    			tax: data.tax,
	    			totalAmount: data.totalAmount,
	    			description: data.description,
	    			quantity: data.quantity,
	    			amount: data.amount,
	    			itemType: data.itemType,
	    			totalAmountNoFormat: (data.totalAmountNoFormat != undefined) ? data.totalAmountNoFormat : 0,
	    			discount: (data.discount != undefined) ? data.discount : 0,
	    			discount_key: (data.discount_key != undefined) ? data.discount_key : ''
    			}
        		
        		$('.information-part').removeClass('hidden');
        	} 
        	else { console.log(data.error); }
        	
        })
        .error(function(err){
            console.log(err);
        });
    }
	
	// Print invoice
	$scope.ngprintinvoice = function(id){
		var html = $('#' + id).html();
		var popupWinindow = window.open('', '_blank', 'width=700,height=500,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><body onload="window.print(); window.close();"><center>' + html + '</center></body></html>');
        popupWinindow.document.close();
        //popupWinindow.focus();
	}
	
}])
.controller('StaffTransactionController', ['$scope', '$http', '$sce', function ($scope, $http, $sce) {
	$scope.listBarcodeImg = '';
	$scope.storage = {
		_id : '',
		phone : '',
		address : '',
		time : '',
		status : 0,
		warehouse : '',
		daystart : '',
		dropofftime : '',
		pickuptime : '',
		canceltime : '',
		transcode : '',
		images : '',
		barcode : '',
		barcodeimage : '',
		paymentkey : '',
		paymenttype: 0,
		listbarcodeimages : '',
		list_scan : []
	}
	$scope.charge = {
			amount : '',
			des : '',
			mess : ''
	}
	$scope.availableTimeca = null;
	$scope.errormessageca = '';
	$scope.form = {
		address : '',
		date : '',
		time : '',
		option : 1,
		addressca : '',
		dateca : '',
		timeca : '',
		addressfull : '',
		addressfullca : ''
	};
	
	$scope.chargePopup = function() {
		$scope.charge.amount = '';
		$scope.charge.des = '';
		$scope.charge.mess = '';
		$('#myModalCharge').modal('show');
	}
	
	// Charge
	$scope.chargeCustomer = function() {
		
		if ($scope.storage.paymentkey != ''
			&& $scope.charge.amount > 0
				&& $scope.charge.des != '') {
			
			$http.post('/api/chargeCustomer', {
				'paymentkey': $scope.storage.paymentkey, 
				'amount': $scope.charge.amount,
				'des': $scope.charge.des,
				'storageid': $scope.storage['_id']
			})
	        .success(function (res) {
	        	//console.log(res);
	        	if (res.error != undefined) {
	        		$scope.charge.mess = res.error;
	        	} else {
	        		alert('Success !');
	        		$('#myModalCharge').modal('hide');
	        	}
	        })
	        .error(function (err) {
	        	$scope.charge.mess = 'Error 1 !!!';
	        });
			
		} else {
			$scope.charge.mess = 'Error 2 !!!';
		}	
	}

	$scope.getStorageInfo = function(id) {
		
		if (id == 0) return false;
			
        $http.post('/api/getStorageInfo', {'id' : id})
        .success(function(data) {
        	if (data.error == undefined) {
        		$('.storage-class').removeClass('choose');
        		$('#storage_' + id).addClass('choose');
        		$scope.currentStorageId = id;
        		
        		$scope.storage = {
    				_id : id,
    				phone : data.phone,
    				address : data.address,
    				status : data.status,
    				warehouse : data.workingstatus,
    				daystart : $scope.convertDate(data.created_at),
    				timelist : $scope.formatlist( data.dropofftime, data.pickuptime ),
    				dropofftime : data.dropofftime,
    				pickuptime : data.pickuptime,
    				canceltime : $scope.convertDate(data.canceltime, false, true),
    				transcode : 'ORDER ' + data.transcode,
    				images : data.images,
    				barcode : data.barcode,
    				barcodeimage : data.barcodeimage,
    				paymentkey : data.paymentkey,
    				paymenttype: data.paymenttype,
    				trans_id : data.trans_id,
    				warehouse_city : data.warehouse_city,
    				warehouse_code : data.warehouse_code,
    				warehouse_aisle : data.warehouse_aisle,
    				warehouse_section : data.warehouse_section,
    				warehouse_shelf: data.warehouse_shelf,
    				zipcode: data.zipcode,
    				cuskey: data.cuskey,
    				userid: data.userid,
    				list_scan: data.list_scan,
    				item_type: data.item_type
    			
    			}
        		
        		$('.information-part').removeClass('hidden');
        	}
        	else { alert(data.error); }
        	
        })
        .error(function(err){
            console.log(err);
        });
	}
        
    $scope.editWarehouse = function(){
         $scope.warehouseErrors = [];
         $http.post('/api/editWarehouse', $scope.storage)
         .success(function(data){
            location.reload();
         })
         .error(function(err){
        	 for (var index in err['message']) {
        		 $scope.warehouseErrors.push(err['message'][index]);
        	 }
         });
    }
    
    $scope.form = {
    	'addressca': '',
    	'passwordca': '',
    	'phoneca': '',
    }
    $scope.current = {
    	'addressca': ''
    }
        
        /*$scope.cancelRequest = function() {
        	var key = $scope.currentStorageId;
		    $scope.storagedkey = key;
		    $scope.form.addressca = '';
		    $scope.form.passwordca = '';
		    $scope.form.phoneca = '';
		    $("#datepicker").val('');
		    $("#timepicker").val('');
		    
		    if (key == 'All') {
		    	$scope.current.addressca = $(this).data('address');
		    	$('#myModalCancel .current-address').html($scope.current.addressca);
		    	$('#myModalCancel').modal('show');
		     } 
		     else {
		    	
		    	$http.post('/api/getStorageAddress', { 'key': key })
	            .success(function (res) {
	                 if (res.error) {
	                	 $scope.errormessageca = res.error;
	                 } else {
	                	 $scope.current.addressca = res;
	                	 $('#myModalCancel').modal('show');
	                 }
	             }); 
		     }	
        }*/
    $scope.formatlist = function( dropoff, pickup ){
    	var len = dropoff.length;
    	var result = [];
    	for(i = 0; i < len; i ++) {
    		result.push({ "datetime": $scope.convertDate(dropoff[i], true, true), "status": $scope.convertDate(dropoff[i], true, true, true) + ' (Drop-off)' });
    		if (pickup[i-1] != undefined) {
    			result.push({ "datetime": $scope.convertDate(pickup[i-1], true, true), "status": $scope.convertDate(pickup[i-1], true, true, true) + ' (Pick-up)' });
    		}
    	}
    	//console.log(result);
    	return result;
    }
    
    $scope.to_html = function(html_code) {
        return $sce.trustAsHtml(html_code);
    }
	// formatDate option for date with string like "07/30/2016 - 1:00 am"
	/*$scope.convertDate = function(d, haveTime, formatDate){
        if(d === undefined || d.length == 0)
            return "";
    	
    	var dateStr = d.split(" ")[0];
    	if (formatDate) {
    		dateStr = dateStr.split("/")[2] +'-'+ dateStr.split("/")[0] +'-'+ dateStr.split("/")[1];
    	}
    
	    var date = moment(dateStr),
	    	time = '';
	    
	    if (haveTime) {
	    	time = ' - '+ d.split(" ")[2] +' '+ d.split(" ")[3];
	    }
	    
	    return date.format('MMMM D.YYYY') + time;
    }*/
	
	// Delete image
	$scope.deleteimage = function(storage, imgname) {
		$http.post('/api/deleteimage', { 'storage': storage, 'image': imgname })
        .success(function (res) {
        	if (!res.error) {
        		$scope.storage.images = res;
        	} else {
        		alert(res.error);
        	}
        })
        .error(function (err) {
            alert('Error !!!');
        });
	};
			
	// Upload image
	$scope.uploadImage = function(bin){
        var ele = document.getElementById('file');
        var file = ele.files[0];
        if (file === null || file === undefined)
            return false;
        
        var fd = new FormData();      
        fd.append('file', file);
        
        $http.post('/api/uploadBinImage/' + bin._id, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
        	if( Object.prototype.toString.call( bin.images ) !== '[object Array]' ) {
        		var arr = Object.keys(bin.images).map(function (key) {return bin.images[key]});
        		bin.images = arr;
        	}
        	
        	bin.images.push(data.image);
            clearFile();
            alert('Done !');
        })
        .error(function(err){
            alert(err.message);
        });
        
    }
	
	// formatDate option for date with string like "07/30/2016 - 1:00 am OR 07/30/2016 - 1:00 am - IT1"
	$scope.convertDate = function(d, haveTime, formatDate, getstatus){
    	
		if (d != undefined && d.length > 0 ) {
			
			var arr = d.split(" ");
			
			if (getstatus != undefined && getstatus) {
				return (arr[5] != undefined) ? '<span class="cl-0">' + arr[5] + '</span>' : '';
			}
			
			var dateStr = arr[0];
			
	    	if (formatDate) {
	    		dateStr = dateStr.split("/")[2] +'-'+ dateStr.split("/")[0] +'-'+ dateStr.split("/")[1];
	    	}
	    
		    var date = moment(dateStr),
		    	time = '';
		    
		    if (haveTime) {
		    	time = ' - '+ d.split(" ")[2] +' '+ d.split(" ")[3];
		    }
		    
		    return date.format('MMMM D.YYYY') + time;
		}
    }
	
	// Print barcode
	$scope.ngprintbarcode = function($option){
		var html = '';
		// If have option size => just print selected bin barcode and replace barcode size image
		if ($scope.storage.barcodeimage != '' && $option && typeof $option['size'] != 'undefined') {
			html = '<img width="600" src="' + $scope.storage.barcodeimage + '" />'
		} else if ($scope.listBarcodeImg != '') {
			// Get list barcode image
    		var listBacode = JSON.parse($scope.listBarcodeImg);
			if (listBacode != '') {
				angular.forEach(listBacode[$scope.storage.trans_id], function(value, key) {
					html += '<img style="width: 400px; height: 160px;" src="' + value + '" /><div style="height: 0px;"></div>';
				});
			}
		} else {
			alert('Error api, re-print please.');
			return false;
		}
		var popupWinindow = window.open('', '_blank', 'scrollbars=yes,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
		popupWinindow.document.open();
        popupWinindow.document.write('<html></head><body onload="window.print(); window.close();" style="margin: 0px; padding: 0px;"><center><div>' + html + '</div></center></body></html>');
        popupWinindow.document.close();
	}
	
	$scope.changeWorkingStatus = function(workingStatus) {
		var data = {
				tid : $scope.storage.barcode,
				answers : [workingStatus],
				item_type : $scope.storage.item_type,
		};
		
		$('#overlay-admin').show();
		$http.post('/api/adminchangeworkingstatus', data)
        .success(function(res){
        	$('#overlay-admin').hide();
        	if (res['status'] == 1) {
        		var formatWorkingStatus = (workingStatus == 'Empty - Inventory') ? 'Returned' : workingStatus.split('-')[0];
        		$('#storage_' + $scope.storage._id).find('.working-status').html('<span>'+ formatWorkingStatus +'</span>');
        		$scope.getStorageInfo($scope.storage._id);	// Update status list after change status success
        		alert('Done');
        	} else {
        		alert('Change status failed.');
        		//location.reload();
        	}
        }).error(function (err) {
			console.log(err);
	    });
	}
		
	$scope.getAvailableTime = function(date, idname){
		$http
		.get('/api/getAvailableTime?date=' + date)
		.success(function (data) {
		 	if (data.error) {
		 		$scope.errors.push(data.error);
		 	} else if (idname == 'datepickerca') {
	 			$('#timepickerca').val('');
	 			$scope.availableTimeca = data;
	 		} else {
	 			$scope.availableTimeca = data;
	 		}
		})
		.error(function (err) {
			//console.log(err);
	        $scope.errors.push(err);
	    });
	};
	
	$('#datepickerca').on("dp.change", function(e) {
	 		var date = $(this).val();
	 		var idname = $(this).attr('id');
	 		
			$scope.isShowTimeca = false;
	 		$scope.getAvailableTime(date, idname);
    });
	
	$scope.processTime = function(idname) {
    	
    	if (idname == 'timepickerca') {
 			if ($('#datepickerca').val() == '') return false;
 			$scope.isShowTimeca = ! $scope.isShowTimeca && ($scope.availableTimeca != null);
 		} else {
 			$scope.isShowTimeca = false;
 		}
    }
	
	$scope.onSelect = function(time, text, idname){
    	if (text == undefined) {
        	$('#' + idname).val(time[1]);
        	$('#' + idname + 'val').val(time[0]);
 			$scope.isShowTimeca = false;
    	}
    };
    
    // Cancel requesting
	$scope.cancelRequesting = function(storagedkey) {
		var cuskey = $scope.storage.cuskey;
		if (cuskey == undefined || cuskey == '') {
			alert('Got error !');
			return false;
		}
		
		$scope.form.dateca = $("#datepickerca").val();
		$scope.form.timeca = $("#timepickercaval").val();
		$scope.form.timecashow = $("#timepickerca").val();
		$scope.form.storagedkey = storagedkey;
		$scope.form.addressfullca = '';
		$scope.form.userid = $scope.storage.userid;
		
		if ($scope.form.addressca != '') 
		{
			$scope.form.addressfullca = $scope.form.addressca + ', ' + addressFull[$('#zipcodeca').val()];
		}
		
		//
		if ($scope.form.addressfullca && $scope.form.addressfullca != '') 
		{
			var findAddress = $http.get('https://api.smartystreets.com/street-address?'
		        +'auth-id=ef98fd2a-a28d-3de6-db0e-2a5b6d845e92'
		        +'&auth-token=839CwxIjpjq5Txwi27rQ'
		        +'&candidates=10'
		        +'&street=' + encodeURI($scope.form.addressfullca)
	        );
	        
	        findAddress.then(function(a){
	            if(a.status !== 200){
	            	$scope.errormessageca = "Your address is invalid (1)";
	            	return;
	            }
	        
	            if(a.status === 200 && a.data.length === 0){
	            	$scope.errormessageca = "Your address is invalid (2)";
	            	return;
	            }
	            //
	            if ($scope.form.dateca != '' && $scope.form.timeca != '') 
	            {
	            	$("img.loading-img").removeClass('hidden');
					$("button.submit-btn").addClass('hidden');
					$http.post('/api/cancelRequesting', { 'form': $scope.form, 'cuskey' : cuskey })
		            .success(function (res) {
		            	if (res.error != undefined) {
		            		$scope.errormessageca = res.error;
		                } else {
		                	setTimeout(function(){
		                		window.location.href = window.location.href;
	                		}, 1000);
		                	$scope.isShow = true;
		                	$scope.errormessageca = '';
		                }
		            	$("img.loading-img").addClass('hidden');
						$(".submit-btn").removeClass('hidden');
		            })
		            .error(function (err) {
                        console.log(err);
                    });
	            }
	            else 
	            {
	            	$scope.errormessageca = 'Enter your date and time.';
	            }
	        },function(b){
	        	$scope.errormessageca = "Your address is invalid (3)";
	        });
			
		}
		else if ($scope.form.dateca != '' && $scope.form.timeca != '') 
        {
			$('#overlay-admin').show();
			$("button.submit-btn").addClass('hidden');
			$http.post('/api/adminCancelRequesting', { 'form': $scope.form, 'cuskey' : cuskey })
            .success(function (res) {
            	//console.log(res)
            	console.log(res);return true;
            	if (res.error) {
            		$scope.errormessageca = res.message;
                } else {
                	setTimeout(function(){
                		window.location.href = window.location.href;
            		}, 1000);
                	$scope.isShow = true;
                	$scope.errormessageca = '';
                }
            	$('#overlay-admin').hide();
				$(".submit-btn").removeClass('hidden');
            });
        }
        else 
        {
        	$scope.errormessageca = 'Enter your date and time.';
        }
	}
	
	$('#form-phoneca').keydown(function (e) {
		var key = e.charCode || e.keyCode || 0;
		$phone = $(this);
		// Auto-format- do not expose the mask as the user begins to type
		if (key !== 8 && key !== 9) {
			if ($phone.val().length === 1 || $phone.val().length === 0) {
				$phone.val('(');
			}
			if ($phone.val().length === 4) {
				$phone.val($phone.val() + ')');
			}
			if ($phone.val().length === 5) {
				$phone.val($phone.val() + ' ');
			}			
			if ($phone.val().length === 9) {
				$phone.val($phone.val() + '-');
			}
		}

		// Allow numeric (and tab, backspace, delete) keys only
		return (key == 8 || 
				key == 9 ||
				key == 46 ||
				(key >= 48 && key <= 57) ||
				(key >= 96 && key <= 105));	
	})
	.bind('focus click', function () {
		$phone = $(this);
		
		if ($phone.val().length === 0) {
			$phone.val('(');
		}
		else {
			var val = $phone.val();
			$phone.val('').val(val); // Ensure cursor remains at the end
		}
	})
	.blur(function () {
		$phone = $(this);
		
		if ($phone.val() === '(') {
			$phone.val('');
		}
	});
	
	// Time available
	var now = moment().add(1, 'd').format('MM/DD/YYYY');
	$scope.getAvailableTime(now, '');
}])

.controller('WarehouseController', ['$scope', '$http', function ($scope, $http) {
	$scope.storage = '';
	$scope.listBarcodeImg = '';
	
	$scope.getStorageInfo = function(id) {
		
		if (id == 0) return false;
			
        $http.post('/api/getStorageInfo', {'id' : id})
        .success(function(data) {
        	if (data.error == undefined) {
        		
        		$('.storage-class').removeClass('choose');
        		$('#storage_' + id).addClass('choose');
        		
        		$scope.storage = {
    				_id : id,
    				phone : data.phone,
    				address : data.address,
    				status : data.status,
    				warehouse_barcode : data.warehouse_barcode,
    				daystart : $scope.convertDate(data.created_at),
    				dropofftime : data.dropofftime,
    				pickuptime : data.pickuptime,
    				canceltime : data.canceltime,
    				transcode : 'ORDER ' + data.transcode,
    				images : data.images,
    				barcode : data.barcode,
    				barcodeimage : data.barcodeimage,
    				paymentkey : data.paymentkey,
    				paymenttype: data.paymenttype,
    				trans_id : data.trans_id
    			}
        		$('.information-part').removeClass('hidden');
        	}
        	else { alert(data.error); }
        	
        })
        .error(function(err){
            console.log(err);
        });
    }
		
	$scope.convertDate = function(d, haveTime, formatDate){
        if(d === undefined)
            return "";
    	
    	var dateStr = d.split(" ")[0];
    	if (formatDate) {
    		dateStr = dateStr.split("/")[2] +'-'+ dateStr.split("/")[0] +'-'+ dateStr.split("/")[1];
    	}
    
	    var date = moment(dateStr),
	    	time = '';
	    
	    if (haveTime) {
	    	time = ' - '+ d.split(" ")[2] +' '+ d.split(" ")[3];
	    }
	    
	    return date.format('MMMM D.YYYY') + time;
    }
}])
.controller('CalendarController', ['$scope', '$http', function ($scope, $http) {
	var Months = ['January', 'Febuary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		toDay = new Date(moment().tz("America/Chicago").format("YYYY-MM-DD"));
	
	$scope.dayformat = Months[toDay.getMonth()].substring(0, 3) +' '+ ('0' + toDay.getDate()).slice(-2) +' .'+ toDay.getFullYear();
	$scope.bgcalendar = [];
	$scope.currentDate = Months[toDay.getMonth()] +' '+ toDay.getFullYear();
	
	$scope.changeMonth = function($event) {
		var selectDate = angular.element($event.currentTarget).text();
		$scope.currentDate = selectDate;
		$scope.setAvailableDate(new Date(selectDate));
		$scope.getDataByMonth($scope.currentDate);
	}
	
	$scope.setAvailableDate = function(date) {
		$scope.availableDate = [];
		for (var i = -6; i <=6; i++) {
			var item = moment(date).add(i,'month')._d;
			$scope.availableDate.push(Months[item.getMonth()] +' '+ item.getFullYear());
		}
	}
	
	$scope.getDataByMonth = function(monthyear) {
		$http.post('/api/getCalendarMonth', {'monthyear' : monthyear})
        .success(function(data) {
        	if (data.error == undefined) {
        		$scope.bgcalendar = data;
        	}
        	else { alert(data.error); }
        })
        .error(function(err){
            console.log(err);
        });
	}
	
	$scope.getDataByMonth($scope.currentDate);

	$scope.formatItemType = function(str) {
		return str.replace(/LG|XL|OS/g, function ($0, $1, $2) // $3, $4... $n for captures
			{
			    if ($0 == "LG")
			        return " Large";
			    else if ($0 == "XL")
			        return " Extra Large";
			    else if ($0 == "OS")
			        return " Other Item";
			});
	}
	
	/*
	 * param data: format dd/mm/yyyy
	 * */
	$scope.getCalendarByDate = function(date) {
		$scope.storages = [];
		$http.post('/api/getCalendarByDate', {'date' : date})
        .success(function(data) {
        	$scope.storages = data;
        	$('.calendar-right').removeClass('hidden');
        })
        .error(function(err){
            console.log(err);
        });
	}
	
	$scope.getCalendar = function($event) {
		var day = angular.element($event.currentTarget).find('.calendar-date').text();
		if (day) {
			var dateObj = new Date(new Date($scope.currentDate).setDate(day));
			$scope.dayformat = Months[dateObj.getMonth()].substring(0, 3) +' '+ ('0' + dateObj.getDate()).slice(-2) +' .'+ dateObj.getFullYear();
			$scope.getCalendarByDate($scope.formatDateString(dateObj));
		}
	}
	
	$scope.printCalendar = function() {
		var style = '<style>';
		style += '.calendar-right-header {';
		style += '    margin-bottom: 10px;';
		style += '}';
		style +='.calendar-right-header span {';
		style +='	padding: 3px 10px;';
		style +='	margin-right: 20px;';
		style +='	background: #46B197;';
		style +='	color: white;';
		style +='	-webkit-border-radius: 3px; ';
		style +='	-moz-border-radius: 3px; ';
		style +='	border-radius: 3px; ';
		style +='}';
		style +='.calendar-right-header label {';
		style +='	vertical-align: middle;';
		style +='}';
		style +='.calendar-item {';
		style +='	border-top: 1px solid #A6A6A8;';
		style +='}';
		style +='.calendar-item a {';
		style +='	text-decoration: none;';
		style +='	color: #58585b;';
		style +='}';
		style +='.customer-name {';
		style +='	color: #71B748;';
		style +='	font-size: 18px;';
		style +='}';
		style +='.calendar-item table {';
		style +='	width: 100%;';
		style +='	margin: auto;';
		style +='}';
		style +='.calendar-item table td {';
		style +='	padding-top: 10px;';
		style +='}';
		style +='.calendar-item table td:first-child {';
		style +='	width: 30%;';
		style +='}';
		style +='.calendar-item table td:nth-child(2) {';
		style +='	font-weight: bold;';
		style +='}';
		style +='.calendar-item .main-wrap,';
		style +='.calendar-item .sub-wrap {';
		style +='	margin-bottom: 10px;';
		style +='}';
		style +='.calendar-item .main-wrap {';
		style +='	cursor: pointer;';
		style +='}';
		style +='.calendar-item .sub-wrap {';
		style +='	display: table !important;';
		style +='}';
		style +='.color-drop-off {';
		style +='	color: #71B748;';
		style +='	font-weight: bold;';
		style +='}';
		style +='.color-pick-up {';
		style +='	color: #C6A82C;';
		style +='	font-weight: bold;';
		style +='}';
		style +='.staff-print {';
		style +='	display: none;';
		style +='}';
		style +='</style>';
		
		var body = document.getElementById('print-content').innerHTML;
		var mywindow = window.open('', 'my div');

		mywindow.document.write('<html><head>'+ style);
		mywindow.document.write('</head><body><div id="invoice-container">'+ body +'</div></body></html>');

		mywindow.document.close(); // necessary for IE >= 10
		mywindow.focus(); // necessary for IE >= 10

		mywindow.print();
		mywindow.close();

		return true;
	}
	
	$(document)
		.on('click', '.main-wrap', function() {
			$(this).closest('.calendar-item').find('.sub-wrap').toggle();
			$(this).closest('.calendar-item').siblings().find('.sub-wrap').toggle(false); 
		})
		.on('click', '.DCalendar tbody td', function() {
			$('.DCalendar tbody td').removeClass('date-active');
			$(this).addClass('date-active');
		})
		
	$scope.formatDateString = function(date) {
		return ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth()+1)).slice(-2) + '/'+ date.getFullYear();
	}
	$scope.getCalendarByDate($scope.formatDateString(toDay));
	$scope.setAvailableDate(toDay);
}])
.controller('PromoController', ['$scope', '$http', function ($scope, $http) {
	
	$scope.promo = $scope.add = {
		code : '', 
		percent : ''
	};
	
	// Get by id
	$scope.getPromoInfo = function(id) {
		if (id != '') {
			$http.post('/api/getPromoInfo', {'id' : id})
	        .success(function(data){
	        	if (data.error == undefined) {
	        		
	        		$('.promo-class').removeClass('choose');
	        		$('#promo_' + data.id).addClass('choose');
	        		$scope.promo = {
	    				code : data.id, 
	    				percent : data.percent
	    			};
	        	} 
	        	else { 
	        		alert(data.error); 
	        	}
	        	
	        })
	        .error(function(err){
	            console.log(err);
	        });
		}
	}
	
	// Add
	$scope.promo_submit = function() {
		var isSubmit = true;
		if ( ! promotionCodeValid($scope.add.code) ) {
			$('.promo-form-code').addClass('has-error');
			isSubmit = false;
		} else {
			$('.promo-form-code').removeClass('has-error');
		}
		if ( ! isNumeric($scope.add.percent) || $scope.add.percent < 0 || $scope.add.percent > 100 ) {
			$('.promo-form-percent').addClass('has-error');
			isSubmit = false;
		} else {
			$('.promo-form-percent').removeClass('has-error');
		}

		// Submit
		if (isSubmit) {
			$http.post('/api/processPromo', {'data' : $scope.add, 'type' : 'add'})
	        .success(function(data){
	        	if (data.error != undefined) {
	        		alert(data.error);
	        	}
	        	else { 
	        		window.location.href = window.location.href; 
	        	}
	        })
	        .error(function(err){
	            console.log(err);
	        });
		}
	}
	
	// Delete
	$scope.promo_deletesubmit = function(id) {
		
		var r = confirm("Are you sure to delete this Promotion ?");
		if (r == true) {
			$http.post('/api/processPromo', {'data' : id, 'type' : 'delete'})
	        .success(function(data){
	        	if (data.error != undefined) {
	        		alert(data.error);
	        	} 
	        	else {
	        		alert('Done !');
	        		window.location.href = window.location.href; 
	        	}
	        	
	        })
	        .error(function(err){
	            alert(err.message);
	        });
		}
	}
	
}]);

function promotionCodeValid( code ) {
    var re = /^[0-9a-zA-Z]+$/;
    return re.test(code);
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validatePhone(phone) {
	var re = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
	return re.test(phone);
}

function isNumeric(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}