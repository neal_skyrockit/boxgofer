<!DOCTYPE html>
<html>

<head>
    <title>Account Setting Personal Information</title>
    <meta charset="utf-8" />
    <!--css-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/mainhong.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/responsive.css' rel='stylesheet' type='text/css'>
</head>

<body>
    <div id="wrapper">
        <!--header-->
        <header>
            <nav class="clearfix container">
                <a id="logo"><img src="/public/html/images/logo.png" /></a>
                <div>
                    <ul class="quickaction">
                        <li><a class="">Order Now</a></li>
                        <li>
                            <input id="trigger-user-actions" type="checkbox" class="hidden trigger-user-actions">
                            <label for="trigger-user-actions" class="user-actions">DDD</label>                  
                            <ul class="sub-menu">
                                <li><a>My Stuff</a></li>
                                <li><a>Account Settings</a></li>
                                <li><a>Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="clearfix">
                        <li><a>Customer Support : <span class="color">855 999 650</span></a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--end header-->

        <!--main content-->
        <main>
            <div class="container">
                <!--page personal information-->
                <section class="page" id="page-per-info">
                    <h2>Account Settings</h2>
                    <div class="route-acc">
                        <a href="accPerInfo.php" class="a-perInfo active">Personal Information</a>
                        <a href="accHistory.php" class="a-history">History</a>
                    </div>
                    <form class="frm-eidtprofile" method="post" action="">
                        <div class="form-group">
                            <input name="name" class="form-control" id="name" value="Kevin">
                        </div>
                        <div class="form-group">
                            <input name="username" class="form-control" id="email" value="kevin@gmail.com">
                        </div>
                        <div class="form-group">
                            <input name="password" class="form-control" id="password" value="*******">
                        </div>
                        <div class="form-group">
                            <input name="phone" class="form-control" id="phone" value="0945345678">
                        </div>
                        <div class="form-group">
                            <input name="address" class="form-control" id="address" value="Physical Address">
                        </div>
                        <div class="form-group" id="select-city">
                            <select class="form-control" >
                                <option>City</option>
                                <option>Hanoi</option>
                                <option>Hochiminh</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-save">Save change</button>
                        </div>
                    </form>
                </section>
                <!--end page-->
            </div>
        </main>
        <!--end main content-->

        <!--footer-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Boxgofer</h3>
                        <nav>
                            <ul>
                                <li><a>FAQ</a></li>
                                <li><a>About Us</a></li>
                                <li><a>Privacy</a></li>
                                <li><a>Terms & Conditions</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-4">
                        <h3>Contact</h3>
                        <ul>
                            <li>2801 Ladona Dr</li>
                            <li>Fort Worth, TX 76133, Dallas, Texas (Head Office)</li>
                            <li>+852 3565 6850</li>
                            <li><a href="mailto:support@boxgofer.com">support@boxgofer.com</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3>Customer Service</h3>
                        <ul>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 6 pm</li>
                            <li>Delivery Hours:</li>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 9 pm</li>
                            <li>*Closed during public holidays</li>
                        </ul>
                    </div>
                </div>
                <h3>
                    Connect with us: 
                    <a class="icon icon-facebook"></a> 
                    <a class="icon icon-twitter"></a>
                </h3>
                <p class="ta-center">2015 Quodisys alright reserved</p>
            </div>
        </footer>
        <!--end footer-->
    </div>
</body>

</html>