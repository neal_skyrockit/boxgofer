﻿<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta charset="utf-8" />
    <!--css-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/main.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/responsive.css' rel='stylesheet' type='text/css'>
</head>
<body>
    <div id="wrapper">
        <!--header-->
        <header>
            <nav class="clearfix container">
                <a id="logo"><img src="/public/html/images/logo.png" /></a>
                <div>
                    <ul class="quickaction">
                        <li><a class="">Order Now</a></li>
                        <li><a class="">Login</a></li>
                    </ul>
                    <ul class="clearfix">
                        <li><a>How it work</a></li>
                        <li><a>FAQ</a></li>
                        <li><a>Customer Support : <span class="color">855 999 650</span></a></li>
                    </ul>
                </div>           
            </nav>
        </header>
        <!--end header-->

        <!--main content-->
        <main>
            <div class="container">
                <!--login page-->
                <section id="page-login">
                    <div class="row">
                        <!--create account section-->
                        <div class="section col-md-6">
                            <form class="frm-login" method="post" action="">
                                <h3 class="">CREATE AN ACCOUNT</h3>
                                <div class="form-group">
                                    <input name="name"  
                                           class="form-control" 
                                           id="name" 
                                           placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <input name="username" 
                                           class="form-control" 
                                           id="email" 
                                           placeholder="Email/Username">
                                </div>
                                <div class="form-group">
                                    <input name="password" 
                                           class="form-control" 
                                           id="email" 
                                           placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn button-signup">Sign up</button>
                                </div>
                                <div class="form-group">
                                    <p class="txt1">Already registered with BoxGofer? <a class="color">Log in</a></p>
                                </div>
                                <div class="form-group">
                                    <button class="btn button-singup-facebook">Sign up with Facebook</button>
                                </div>
                            </form>
                            
                        </div>
                        <!--description section-->
                        <div class="section col-md-6">
                            <div class="page-login-description">
                                <div class="row">                       
                                    <div class="content content-1">
                                        <h3>STORAGE AT YOUR DOORSTEP</h3>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,. </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="content content-2">
                                        <h3>Free Delivery</h3>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,. </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="content content-3">
                                        <h3>Secure Warehouse Storage</h3>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>              
                <!--end login page-->
            </div>
        </main>
        <!--end main content-->

        <!--footer-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Boxgofer</h3>
                        <nav>
                            <ul>
                                <li><a>FAQ</a></li>
                                <li><a>About Us</a></li>
                                <li><a>Privacy</a></li>
                                <li><a>Terms & Conditions</a></li>
                            </ul>
                        </nav>                    
                    </div>
                    <div class="col-md-4">
                        <h3>Contact</h3>
                        <ul>
                            <li>2801 Ladona Dr</li>
                            <li>Fort Worth, TX 76133, Dallas, Texas (Head Office)</li>
                            <li>+852 3565 6850</li>
                            <li><a href="mailto:support@boxgofer.com">support@boxgofer.com</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3>Customer Service</h3>
                        <ul>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 6 pm</li>
                            <li>Delivery Hours:</li>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 9 pm</li>
                            <li>*Closed during public holidays</li>
                        </ul>
                    </div>
                </div>
                <h3>
                    Connect with us: 
                    <a class="icon icon-facebook"></a> 
                    <a class="icon icon-twitter"></a>
                </h3>
                <p class="ta-center">2015 Quodisys alright reserved</p>
            </div>
        </footer>
        <!--end footer-->
    </div>
</body>
</html>
