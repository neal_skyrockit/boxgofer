<!DOCTYPE html>
<html>

<head>
    <title>Login</title>
    <meta charset="utf-8" />
    <!--css-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/main.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/responsive.css' rel='stylesheet' type='text/css'>
</head>

<body>
    <div id="wrapper">
        <!--header-->
        <header>
            <nav class="clearfix container">
                <a id="logo"><img src="/public/html/images/logo.png" /></a>
                <div>
                    <ul class="quickaction">
                        <li><a class="">Order Now</a></li>
                        <li>
                            <input id="trigger-user-actions" type="checkbox" class="hidden trigger-user-actions">
                            <label for="trigger-user-actions" class="user-actions">DDD</label>                  
                            <ul class="sub-menu">
                                <li><a>My Stuff</a></li>
                                <li><a>Account Settings</a></li>
                                <li><a>Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="clearfix">
                        <li><a>Customer Support : <span class="color">855 999 650</span></a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--end header-->

        <!--main content-->
        <main>
            <!--order page-->
            <section class="page" id="page-section">
                <div class="order-line-section">
                    <div class="container">
                        <p class="order-line">
                            <label class="step schedule-step active">1</label>
                            <label class="step review-step">2</label>
                            <label class="step billing-step">3</label>
                            <label class="step confirm-step">4</label>
                        </p>
                    </div>
                </div>
                <div class="container">
                    <div class="order-view">
                        <!-- row 1-->
                        <div class="row">
                            <div class="col-xs-3">
                                <p class="color">How many Bins do you need?</p>
                            </div>
                            <div class="col-xs-9">
                                <div class="grey-section">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-6 nowrap">
                                            <span>Large</span>
                                            <div class="number-group">
                                                <button class="decrease"></button>
                                                <input class="amount" />
                                                <button class="increase"></button>
                                            </div>
                                            <div class="tooltip-group">
                                                <input id="tooltip-large" class="hidden trigger-tooltip" type="checkbox" />
                                                <label for="tooltip-large" class="guide" data-tooltip="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,"></label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-6 nowrap">
                                            <span>Extra Large</span>
                                            <div class="number-group">
                                                <button class="decrease"></button>
                                                <input class="amount" />
                                                <button class="increase"></button>
                                            </div>
                                            <div class="tooltip-group">
                                                <input id="tooltip-ex-large" class="hidden trigger-tooltip" type="checkbox" />
                                                <label for="tooltip-ex-large" class="guide" data-tooltip="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row 1 -->
                        <!--row 2-->
                        <div class="row">
                            <div class="col-xs-3">
                                <p>We'll charge $6/mo for all items less than 50 lbs that one Gofer can carry.</p>
                            </div>
                            <div class="col-xs-9">
                                <div class="grey-section">
                                    <div class="row">
                                        <div class="col-md-12 nowrap">
                                            <span>Large Items</span>
                                            <div class="number-group">
                                                <button class="decrease"></button>
                                                <input class="amount" />
                                                <button class="increase"></button>
                                            </div>
                                            <div class="tooltip-group">
                                                <input id="tooltip-large-item" class="hidden trigger-tooltip" type="checkbox" />
                                                <label for="tooltip-large-item" class="guide" data-tooltip="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row 2 -->
                        <!--row 3-->
                        <div class="row">
                            <div class="col-xs-3">
                                <p class="color">Total Monthly Price</p>
                            </div>
                            <div class="col-xs-9">
                                <div class="grey-section">
                                    <div class="row">
                                        <div class="col-md-12 nowrap">
                                            <p class="label-price" ></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row 3 -->
                        <!--row 4-->
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="button-next pull-right">Next</button>
                            </div>             
                        </div>
                        <!-- end row 4 -->
                    </div>
                </div>
            </section>

            <!--end order page-->
        </main>
        <!--end main content-->

        <!--footer-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Boxgofer</h3>
                        <nav>
                            <ul>
                                <li><a>FAQ</a></li>
                                <li><a>About Us</a></li>
                                <li><a>Privacy</a></li>
                                <li><a>Terms & Conditions</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-4">
                        <h3>Contact</h3>
                        <ul>
                            <li>2801 Ladona Dr</li>
                            <li>Fort Worth, TX 76133, Dallas, Texas (Head Office)</li>
                            <li>+852 3565 6850</li>
                            <li><a href="mailto:support@boxgofer.com">support@boxgofer.com</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3>Customer Service</h3>
                        <ul>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 6 pm</li>
                            <li>Delivery Hours:</li>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 9 pm</li>
                            <li>*Closed during public holidays</li>
                        </ul>
                    </div>
                </div>
                <h3>
                    Connect with us: 
                    <a class="icon icon-facebook"></a> 
                    <a class="icon icon-twitter"></a>
                </h3>
                <p class="ta-center">2015 Quodisys alright reserved</p>
            </div>
        </footer>
        <!--end footer-->
    </div>
</body>

</html>