<!DOCTYPE html>
<html>

<head>
    <title>Login</title>
    <meta charset="utf-8" />
    <!--css-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/main.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/responsive.css' rel='stylesheet' type='text/css'>
    <link href='css/bootstrap-datetimepicker.css' rel='stylesheet' type='text/css'>
</head>

<body>
    <div id="wrapper">
        <!--header-->
        <header>
            <nav class="clearfix container">
                <a id="logo"><img src="/public/html/images/logo.png" /></a>
                <div>
                    <ul class="quickaction">
                        <li><a class="">Order Now</a></li>
                        <li>
                            <input id="trigger-user-actions" type="checkbox" class="hidden trigger-user-actions">
                            <label for="trigger-user-actions" class="user-actions">DDD</label>
                            <ul class="sub-menu">
                                <li><a>My Stuff</a></li>
                                <li><a>Account Settings</a></li>
                                <li><a>Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="clearfix">
                        <li><a>Customer Support : <span class="color">855 999 650</span></a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--end header-->

        <!--main content-->
        <main>
            <!--order page-->
            <section class="page" id="page-section">
                <div class="order-line-section">
                    <div class="container">
                        <p class="order-line">
                            <label class="step schedule-step active">1</label>
                            <label class="step review-step">2</label>
                            <label class="step billing-step">3</label>
                            <label class="step confirm-step">4</label>
                        </p>
                    </div>
                </div>
                <div class="container">
                    <div class="order-view">
                        <!-- row 1-->
                        <div class="row">
                            <div class="col-xs-3">
                                <p class="color">Bins Drop-off</p>
                                <p>When are you free to receive your empty bins?</p>
                            </div>
                            <div class="col-xs-9">
                                <div class="grey-section">
                                    <div class="row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="input-table" >
                                                <input id="datepicker" type="text" class="form-control w120" placeholder="Date" />
                                                <label for="datepicker" class="icon green icon-calendar"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="input-table">
                                                <input id="timepicker" type="text" class="form-control w120" placeholder="Time" />
                                                <label for="timepicker" class="icon green icon-clock"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row 1 -->
                        <!-- row 2-->
                        <div class="row">
                            <div class="col-xs-3">
                                <p class="color">Address</p>
                                <p>Where will all deliveries take place?</p>
                            </div>
                            <div class="col-xs-9">
                                <div class="grey-section">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input class="form-control" placeholder="Address" />
                                        </div>
                                        <div class="col-sm-3">
                                            <select class="form-control">
                                                <option>City</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <input class="form-control" placeholder="Phone" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row 2 -->
                        <!-- row 3-->
                        <div class="row">
                            <div class="col-xs-3">
                                <p class="color">Special Request</p>
                                <p>Do you have any other request?</p>
                            </div>
                            <div class="col-xs-9">
                                <div class="grey-section">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <textarea class="form-control" rows="5"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row 3 -->     
                        <!--row 4-->
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="button-next pull-right">Next</button>
                                <button class="button-back pull-right">Back</button>
                            </div>             
                        </div>
                        <!-- end row 4 -->                  
                    </div>
                </div>
            </section>

            <!--end order page-->
        </main>
        <!--end main content-->

        <!--footer-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Boxgofer</h3>
                        <nav>
                            <ul>
                                <li><a>FAQ</a></li>
                                <li><a>About Us</a></li>
                                <li><a>Privacy</a></li>
                                <li><a>Terms & Conditions</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-4">
                        <h3>Contact</h3>
                        <ul>
                            <li>2801 Ladona Dr</li>
                            <li>Fort Worth, TX 76133, Dallas, Texas (Head Office)</li>
                            <li>+852 3565 6850</li>
                            <li><a href="mailto:support@boxgofer.com">support@boxgofer.com</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3>Customer Service</h3>
                        <ul>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 6 pm</li>
                            <li>Delivery Hours:</li>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 9 pm</li>
                            <li>*Closed during public holidays</li>
                        </ul>
                    </div>
                </div>
                <h3>
                    Connect with us: 
                    <a class="icon icon-facebook"></a> 
                    <a class="icon icon-twitter"></a>
                </h3>
                <p class="ta-center">2015 Quodisys alright reserved</p>
            </div>
        </footer>
        <!--end footer-->
    </div>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript">
        $(function () {
                $('#datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('#timepicker').datetimepicker({
                    format: 'LT'
                });
                
        });
    </script>
</body>

</html>