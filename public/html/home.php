﻿<!DOCTYPE html>
<html>

<head>
    <title>Home page</title>
    <meta charset="utf-8" />
    <!--css-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/main.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/responsive.css' rel='stylesheet' type='text/css'>
</head>

<body class="single-page">
    <div id="wrapper">
        <!--header-->
        <header>
            <nav class="clearfix container">
                <a id="logo"><img src="/public/html/images/logo.png" /></a>
                <div>
                    <ul class="quickaction">
                        <li><a class="">Order Now</a></li>
                        <li><a class="">Login</a></li>
                    </ul>
                    <ul class="clearfix">
                        <li><a>How it work</a></li>
                        <li><a>FAQ</a></li>
                        <li><a>Customer Support : <span class="color">855 999 650</span></a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--end header-->
        <!--main content-->
        <main>
            <section id="section-1" class="section">
                <div class="content-container">
                    <div class="content">
                        <h4>STORAGE AT YOUR DOORSTEP</h4>
                        <p>We pick up, store, and bring back your stuff.</p>
                        <button class="btn">Get started</button>
                    </div>
                </div>
            </section>
            <section id="section-2" class="section">
                <h3>How it works</h3>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="image-circle image-circle-1"></div>
                            <p class="text">Free durable bins to your doorstep for you to pack</p>
                        </div>
                        <div class="col-md-4">
                            <div class="image-circle image-circle-2"></div>
                            <p class="text">We take it to our secure storage facility.</p>
                        </div>
                        <div class="col-md-4">
                            <div class="image-circle image-circle-3"></div>
                            <p class="text">We bring it back whenever you want.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="section-3" class="section">
                <h3>Simple Pricing 4-6-6 month</h3>
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <div>
                                <img src="images/section-3-image1.png" style="max-width:100%" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                                the industry's standard dummy text ever since the 1500s, </p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="section-4" class="section">
                <div class="container">
                    <span class="image1">Barcode Scanned</span>
                    <span class="image2">Fast & Free Pickup</span>
                    <span class="image3">Durable Bins</span>
                    <span class="image4">Safe & Secure Storage</span>
                    <span class="image5">Attach Pictures</span>
                </div>
            </section>
            <section id="section-5" class="section">
                <div class="content">
                    <h3>Now Serving</h3>
                    <p>Dallas</p>
                    <p>Park Cities</p>
                    <p>Plano</p>
                    <p>Richardson</p>
                </div>
            </section>
            <section id="section-6" class="section">
                <div class="content">
                    <h5>Schedule storage at your doorstep</h3>
                      <a>Get Started</a>
                </div>       
            </section>
        </main>
        <!--end main content-->
        <!--footer-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Boxgofer</h3>
                        <nav>
                            <ul>
                                <li><a>FAQ</a></li>
                                <li><a>About Us</a></li>
                                <li><a>Privacy</a></li>
                                <li><a>Terms & Conditions</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-4">
                        <h3>Contact</h3>
                        <ul>
                            <li>2801 Ladona Dr</li>
                            <li>Fort Worth, TX 76133, Dallas, Texas (Head Office)</li>
                            <li>+852 3565 6850</li>
                            <li><a href="mailto:support@boxgofer.com">support@boxgofer.com</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3>Customer Service</h3>
                        <ul>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 6 pm</li>
                            <li>Delivery Hours:</li>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 9 pm</li>
                            <li>*Closed during public holidays</li>
                        </ul>
                    </div>
                </div>
                <h3>
                    Connect with us:
                    <a class="icon icon-facebook"></a>
                    <a class="icon icon-twitter"></a>
                </h3>
                <p class="ta-center">2016 Box Gofer all rights reserved</p>
            </div>
        </footer>
        <!--end footer-->
    </div>
</body>
</html>