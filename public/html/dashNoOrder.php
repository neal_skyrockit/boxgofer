<!DOCTYPE html>
<html>

<head>
    <title>Account Setting Personal Information</title>
    <meta charset="utf-8" />
    <!--css-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/mainhong.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/responsive.css' rel='stylesheet' type='text/css'>
</head>

<body>
    <div id="wrapper">
        <!--header-->
        <header>
            <nav class="clearfix container">
                <a id="logo"><img src="/public/html/images/logo.png" /></a>
                <div>
                    <ul class="quickaction">
                        <li><a class="">Order Now</a></li>
                        <li>
                            <input id="trigger-user-actions" type="checkbox" class="hidden trigger-user-actions">
                            <label for="trigger-user-actions" class="user-actions">DDD</label>                  
                            <ul class="sub-menu">
                                <li><a>My Stuff</a></li>
                                <li><a>Account Settings</a></li>
                                <li><a>Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="clearfix">
                        <li><a>Customer Support : <span class="color">855 999 650</span></a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--end header-->

        <!--main content-->
        <main>
            <div class="container">
                <!--page dashboard no order-->
                <section class="page" id="page-dashnoorder">
                    <h2>Welcome Kevin</h2>
                    
                    <div class="wrap-dash">
                        <div class="wrap-dash-left">
                            <a class="icon-dash"><img src="images/icon-storage.png"></a>
                            <p class="status">0 Items in Storage</p>
                        </div>
                        <div class="wrap-dash-mid">
                            <a class="icon-dash"><img src="images/icon-transit.png"></a>
                            <p class="status">0 Items in Transit from Your Doorstep</p>
                        </div>
                        <div class="wrap-dash-right">
                            <a class="icon-dash"><img src="images/dash-home.png"></a>
                            <p class="status">0 Items at Your Doorstep</p>
                        </div>
                    </div>
                    <div class="wrap-dash-content">
                        <div class="wrap-dash-top">
                            <h3>Your items (0)</h3>
                        </div>
                        <div class="wrap-dash-inner">
                            <img src="images/section-2-image1.jpg">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <button class="btn btn-order">Order Bins</button>
                        </div>
                    </div>
                    
                </section>
                <!--end page-->
            </div>
        </main>
        <!--end main content-->

        <!--footer-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Boxgofer</h3>
                        <nav>
                            <ul>
                                <li><a>FAQ</a></li>
                                <li><a>About Us</a></li>
                                <li><a>Privacy</a></li>
                                <li><a>Terms & Conditions</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-4">
                        <h3>Contact</h3>
                        <ul>
                            <li>2801 Ladona Dr</li>
                            <li>Fort Worth, TX 76133, Dallas, Texas (Head Office)</li>
                            <li>+852 3565 6850</li>
                            <li><a href="mailto:support@boxgofer.com">support@boxgofer.com</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3>Customer Service</h3>
                        <ul>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 6 pm</li>
                            <li>Delivery Hours:</li>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 9 pm</li>
                            <li>*Closed during public holidays</li>
                        </ul>
                    </div>
                </div>
                <h3>
                    Connect with us: 
                    <a class="icon icon-facebook"></a> 
                    <a class="icon icon-twitter"></a>
                </h3>
                <p class="ta-center">2015 Quodisys alright reserved</p>
            </div>
        </footer>
        <!--end footer-->
    </div>
</body>

</html>