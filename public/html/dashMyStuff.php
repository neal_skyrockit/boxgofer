<!DOCTYPE html>
<html>

<head>
    <title>Account Setting Personal Information</title>
    <meta charset="utf-8" />
    <!--css-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/mainhong.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/responsive.css' rel='stylesheet' type='text/css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>

<body>
    <div id="wrapper">
        <!--header-->
        <header>
            <nav class="clearfix container">
                <a id="logo"><img src="/public/html/images/logo.png" /></a>
                <div>
                    <ul class="quickaction">
                        <li><a class="">Order Now</a></li>
                        <li>
                            <input id="trigger-user-actions" type="checkbox" class="hidden trigger-user-actions">
                            <label for="trigger-user-actions" class="user-actions">DDD</label>                  
                            <ul class="sub-menu">
                                <li><a>My Stuff</a></li>
                                <li><a>Account Settings</a></li>
                                <li><a>Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="clearfix">
                        <li><a>Customer Support : <span class="color">855 999 650</span></a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--end header-->

        <!--main content-->
        <main>
            <div class="container">
                <!--page dashboard my stuff-->
                <section class="page" id="page-dashmystuff">
                    <h2>Kevin's Items</h2>
                    
                    <div class="wrap-dash">
                        <div class="wrap-dash-left">
                            <a class="icon-dash"><img src="images/icon-storage.png"></a>
                            <p class="status">0 Items in Storage</p>
                        </div>
                        <div class="wrap-dash-mid">
                            <a class="icon-dash"><img src="images/icon-transit.png"></a>
                            <p class="status">0 Items in Transit from Your Doorstep</p>
                        </div>
                        <div class="wrap-dash-right">
                            <a class="icon-dash"><img src="images/dash-home.png"></a>
                            <p class="status">0 Items at Your Doorstep</p>
                        </div>
                    </div>
                    <div class="wrap-dash-content">
                        <div class="wrap-dash-top">
                            <h3>Your items 3 </h3>
                            <button class="btn btn-order">Order Bins</button>
                        </div>
                        <div class="wrap-dashmystuff-inner">
                            <div class="item-mystuff">
                                <div class="col-md-3 col-sm-3 item-mystuff-img">
                                    <img src="images/img_ex.png">
                                </div>
                                <div class="col-md-6 col-sm-6 item-mystuff-inner">
                                    <p class="item-mystuff-desc">Beach Ball, Skiing Board, Hat, toys,....</p>
                                    <div class="">
                                        <img src="images/img_barcode.png">
                                        <span class="item-mystuff-barcode">0123456789</span>
                                        <div class="item-mystuff-group">
                                            <p class="item-mystuff-popup">
                                                <img style="margin-bottom:10px" src="images/img_his_01.png">
                                                <img src="images/img_his_02.png">
                                            </p>
                                        </div>
                                        <div class="add-img"><button class="btn btn-add-img"></button> </div>   
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 item-mystuff-deliver">
                                    <button class="btn btn-deliver" data-toggle="modal" data-target="#myModalDeliver">Deliver to My Doorstep</button>
                                </div>
                                
                            </div>
                            <div class="item-mystuff">
                                <div class="col-md-3 col-sm-3 item-mystuff-img">
                                    <img src="images/img_ex.png">
                                </div>
                                <div class="col-md-6 col-sm-6 item-mystuff-inner">
                                    <p class="item-mystuff-desc">Beach Ball, Skiing Board, Hat, toys,....</p>
                                    <div class="">
                                        <img src="images/img_barcode.png">
                                        <span class="item-mystuff-barcode">0123456789</span>
                                        <div class="item-mystuff-group">
                                            <p class="item-mystuff-popup">
                                                <img style="margin-bottom:10px" src="images/img_his_01.png">
                                                <img src="images/img_his_02.png">
                                            </p>
                                        </div>
                                        <div class="add-img"><button class="btn btn-add-img"></button> </div>   
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 item-mystuff-deliver">
                                    <button class="btn btn-deliver">Deliver to My Doorstep</button>
                                </div>
                                
                            </div>
                            <div class="item-mystuff">
                                <div class="col-md-3 col-sm-3 item-mystuff-img">
                                    <img src="images/img_ex.png">
                                </div>
                                <div class="col-md-6 col-sm-6 item-mystuff-inner">
                                    <p class="item-mystuff-desc">Beach Ball, Skiing Board, Hat, toys,....</p>
                                    <div class="">
                                        <img src="images/img_barcode.png">
                                        <span class="item-mystuff-barcode">0123456789</span>
                                        <div class="item-mystuff-group">
                                            <p class="item-mystuff-popup">
                                                <img style="margin-bottom:10px" src="images/img_his_01.png">
                                                <img src="images/img_his_02.png">
                                            </p>
                                        </div>
                                        <div class="add-img"><button class="btn btn-add-img"></button> </div>   
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 item-mystuff-deliver">
                                    <button class="btn btn-deliver">Deliver to My Doorstep</button>
                                </div>
                                
                            </div>
                            <div class="deliver-all"><button class="btn btn-deliver">Deliver All Items to My Doorstep</button></div>
                        </div>
                    </div>
                    
                </section>
                <!--end page-->
            </div>
        </main>
        <!--end main content-->

        <!-- Modal -->
        <div id="myModalDeliver" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="before-submit">
                            <h2>Deliver to</h2>
                            <p>Current Address</p>
                            <div class="current-address">2801 Ladona Dr, Fort Worth, TX 76133, Dallas</div>
                            <p>or change address</p>
                            <div class="form-group"><input type="text" name="address" placeholder="New Address" class="form-control"></div>
                            <div class="form-group"><input type="password" name="password" placeholder="Password confirm" class="form-control"></div>
                            <button class="btn btn-save">Submit</button>
                        </div>
                        <div class="after-submit">
                            <img src="images/section-4-image3.png"><span>Yours items are on the ways.</span>
                        </div>
                    </div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>
        <!--end modal--->

        <!--footer-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Boxgofer</h3>
                        <nav>
                            <ul>
                                <li><a>FAQ</a></li>
                                <li><a>About Us</a></li>
                                <li><a>Privacy</a></li>
                                <li><a>Terms & Conditions</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-4">
                        <h3>Contact</h3>
                        <ul>
                            <li>2801 Ladona Dr</li>
                            <li>Fort Worth, TX 76133, Dallas, Texas (Head Office)</li>
                            <li>+852 3565 6850</li>
                            <li><a href="mailto:support@boxgofer.com">support@boxgofer.com</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3>Customer Service</h3>
                        <ul>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 6 pm</li>
                            <li>Delivery Hours:</li>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 9 pm</li>
                            <li>*Closed during public holidays</li>
                        </ul>
                    </div>
                </div>
                <h3>
                    Connect with us: 
                    <a class="icon icon-facebook"></a> 
                    <a class="icon icon-twitter"></a>
                </h3>
                <p class="ta-center">2015 Quodisys alright reserved</p>
            </div>
        </footer>
        <!--end footer-->
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#myModalDeliver .btn-save").click(function(){
                $("#myModalDeliver input[type=password]").show();
            });
        });
    </script>
</body>

</html>