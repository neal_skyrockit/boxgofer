<!DOCTYPE html>
<html>

<head>
    <title>Login</title>
    <meta charset="utf-8" />
    <!--css-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/main.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/responsive.css' rel='stylesheet' type='text/css'>
</head>

<body>
    <div id="wrapper">
        <!--header-->
        <header>
            <nav class="clearfix container">
                <a id="logo"><img src="/public/html/images/logo.png" /></a>
                <div>
                    <ul class="quickaction">
                        <li><a class="">Order Now</a></li>
                        <li>
                            <input id="trigger-user-actions" type="checkbox" class="hidden trigger-user-actions">
                            <label for="trigger-user-actions" class="user-actions">DDD</label>                  
                            <ul class="sub-menu">
                                <li><a>My Stuff</a></li>
                                <li><a>Account Settings</a></li>
                                <li><a>Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="clearfix">
                        <li><a>Customer Support : <span class="color">855 999 650</span></a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--end header-->

        <!--main content-->
        <main>
            <!--order page-->
            <section class="page" id="page-section">
                <div class="order-line-section">
                    <div class="container">
                        <p class="order-line">
                            <label class="step schedule-step">1</label>
                            <label class="step review-step active">2</label>
                            <label class="step billing-step">3</label>
                            <label class="step confirm-step">4</label>
                        </p>
                    </div>
                </div>
                <div class="container">
                    <div class="order-view">
                        <!-- row 1-->
                        <div class="row">
                            <div class="col-xs-3">
                                <p class="color">How many Bins do you need?</p>
                            </div>
                            <div class="col-xs-9">
                                <div class="grey-section nopadding">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <span class="icon icon-smallpackage"></span>  
                                            <span class="text">3 Large</span>           
                                        </div>
                                        <p class="margin0300 visible-xs col-xs-12"></p>
                                        <div class="col-xs-12 col-sm-6">
                                            <span class="icon icon-bigpackage"></span>  
                                            <span class="text">1 Extra Large</span>           
                                        </div>
                                    </div>
                                </div>
                                <div class="grey-section nopadding">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <span class="icon icon-bicycle"></span>  
                                            <span class="text">3 Large Items</span>           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row 1 -->
                        <!-- row 2-->
                        <div class="row">
                            <div class="col-xs-3">
                                <p class="color">Bins Drop-off</p>
                                <p>When are you free to receive your empty bins?</p>
                            </div>
                            <div class="col-xs-9">
                                <div class="grey-section nopadding">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <span class="icon icon-calendarV2"></span>  
                                            <span class="text">Thursday, 31 Dec. 2015 </span>           
                                        </div>
                                        <p class="margin0300 visible-xs col-xs-12"></p>
                                        <div class="col-xs-12 col-sm-6">
                                            <span class="icon icon-clockV2"></span>  
                                            <span class="text">15:00</span>           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row 2 -->
                        <!-- row 3-->
                        <div class="row">
                            <div class="col-xs-3">
                                <p class="color">Address</p>
                                <p>Where will all deliveries take place?</p>
                            </div>
                            <div class="col-xs-9">
                                <div class="grey-section nopadding">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <span class="icon icon-home"></span>  
                                            <span class="text">2801 Ladona Dr, Fort Worth, TX 76133, Dallas</span>           
                                        </div>
                                    </div>
                                </div>
                                <div class="grey-section nopadding">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <span class="icon icon-contact-phone"></span>  
                                            <span class="text">+852 8193 4025</span>           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row 3 -->
                        <!-- row 4-->
                        <div class="row">
                            <div class="col-xs-3">
                                <p class="color" style="padding-top:18px;">Total Monthly Price</p>
                            </div>
                            <div class="col-xs-9">
                                <div class="grey-section">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p class="color" style="padding:0 0 0 50px;">$50/month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row 4 -->
                        <!--row 5-->
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="button-next pull-right">Next</button>
                                <button class="button-back pull-right">Back</button>
                            </div>             
                        </div>
                        <!-- end row 5 -->
                    </div>
                </div>
            </section>

            <!--end order page-->
        </main>
        <!--end main content-->

        <!--footer-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Boxgofer</h3>
                        <nav>
                            <ul>
                                <li><a>FAQ</a></li>
                                <li><a>About Us</a></li>
                                <li><a>Privacy</a></li>
                                <li><a>Terms & Conditions</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-4">
                        <h3>Contact</h3>
                        <ul>
                            <li>2801 Ladona Dr</li>
                            <li>Fort Worth, TX 76133, Dallas, Texas (Head Office)</li>
                            <li>+852 3565 6850</li>
                            <li><a href="mailto:support@boxgofer.com">support@boxgofer.com</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3>Customer Service</h3>
                        <ul>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 6 pm</li>
                            <li>Delivery Hours:</li>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 9 pm</li>
                            <li>*Closed during public holidays</li>
                        </ul>
                    </div>
                </div>
                <h3>
                    Connect with us: 
                    <a class="icon icon-facebook"></a> 
                    <a class="icon icon-twitter"></a>
                </h3>
                <p class="ta-center">2015 Quodisys alright reserved</p>
            </div>
        </footer>
        <!--end footer-->
    </div>
</body>

</html>