<!DOCTYPE html>
<html>

<head>
    <title>Account Setting Personal Information</title>
    <meta charset="utf-8" />
    <!--css-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/mainhong.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/responsive.css' rel='stylesheet' type='text/css'>
</head>

<body>
    <div id="wrapper">
        <!--header-->
        <header>
            <nav class="clearfix container">
                <a id="logo"><img src="/public/html/images/logo.png" /></a>
                <div>
                    <ul class="quickaction">
                        <li><a class="">Order Now</a></li>
                        <li>
                            <input id="trigger-user-actions" type="checkbox" class="hidden trigger-user-actions">
                            <label for="trigger-user-actions" class="user-actions">DDD</label>                  
                            <ul class="sub-menu">
                                <li><a>My Stuff</a></li>
                                <li><a>Account Settings</a></li>
                                <li><a>Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="clearfix">
                        <li><a>Customer Support : <span class="color">855 999 650</span></a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--end header-->

        <!--main content-->
        <main>
            <div class="container">
                <!--page personal information-->
                <section class="page" id="page-acc-history">
                    <h2>Account Settings</h2>
                    <div class="route-acc">
                        <a href="accPerInfo.php" class="a-perInfo">Personal Information</a>
                        <a href="accHistory.php" class="a-history active">History</a>
                    </div>
                    <div class="wrap-his">
                        <div class="wrap-his-head">
                            <p class="col-md-6 col-sm-6"></p>
                            <p class="col-md-3 col-sm-3">Drop-off</p>
                            <p class="col-md-3 col-sm-3">Pick-up</p>
                        </div>
                        <div class="item-his">
                            <div class="col-md-2 col-sm-2 item-his-img">
                                <img src="images/img_ex.png">
                            </div>
                            <div class="col-md-4 col-sm-4 item-his-inner">
                                <p class="item-his-desc">Beach Ball, Skiing Board, Hat, toys,....</p>
                                <div class="">
                                    <img src="images/img_barcode.png">
                                    <span class="item-his-barcode">0123456789</span>
                                    <div class="item-his-group">
                                        <p class="item-his-popup">
                                            <img style="margin-bottom:10px" src="images/img_his_01.png">
                                            <img src="images/img_his_02.png">
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 item-his-inner">
                                <p class="item-his-date">Thursday, 12 Nov. 2015</p>
                                <p class="item-his-hours">12:00 - 15:00</p>
                            </div>
                            <div class="col-md-3 col-sm-3 item-his-inner">
                                <p class="item-his-date">Monday, 31 Dec. 2015</p>
                                <p class="item-his-hours">06:30 PM</p>
                            </div>
                        </div>
                        <div class="item-his">
                            <div class="col-md-2 col-sm-2 item-his-img">
                                <img src="images/img_ex.png">
                            </div>
                            <div class="col-md-4 col-sm-4 item-his-inner">
                                <p class="item-his-desc">Beach Ball, Skiing Board, Hat, toys,....</p>
                                <div>
                                    <img src="images/img_barcode.png">
                                    <span class="item-his-barcode">0123456789</span>
                                    <div class="item-his-group">
                                        <p class="item-his-popup">
                                            <img style="margin-bottom:10px" src="images/img_his_01.png">
                                            <img src="images/img_his_02.png">
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 item-his-inner">
                                <p class="item-his-date">Thursday, 12 Nov. 2015</p>
                                <p class="item-his-hours">12:00 - 15:00</p>
                            </div>
                            <div class="col-md-3 col-sm-3 item-his-inner">
                                <p class="item-his-date">Monday, 31 Dec. 2015</p>
                                <p class="item-his-hours">06:30 PM</p>
                            </div>
                        </div>
                        <div class="item-his">
                            <div class="col-md-2 col-sm-2 item-his-img">
                                <img src="images/img_ex.png">
                            </div>
                            <div class="col-md-4 col-sm-4 item-his-inner">
                                <p class="item-his-desc">Beach Ball, Skiing Board, Hat, toys,....</p>
                                <div>
                                    <img src="images/img_barcode.png">
                                    <span class="item-his-barcode">0123456789</span>
                                    <div class="item-his-group">
                                        <p class="item-his-popup">
                                            <img style="margin-bottom:10px" src="images/img_his_01.png">
                                            <img src="images/img_his_02.png">
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 item-his-inner">
                                <p class="item-his-date">Thursday, 12 Nov. 2015</p>
                                <p class="item-his-hours">12:00 - 15:00</p>
                            </div>
                            <div class="col-md-3 col-sm-3 item-his-inner">
                                <p class="item-his-date">Monday, 31 Dec. 2015</p>
                                <p class="item-his-hours">06:30 PM</p>
                            </div>
                        </div>
                    </div>
                    
                </section>
                <!--end page-->
            </div>
        </main>
        <!--end main content-->

        <!--footer-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Boxgofer</h3>
                        <nav>
                            <ul>
                                <li><a>FAQ</a></li>
                                <li><a>About Us</a></li>
                                <li><a>Privacy</a></li>
                                <li><a>Terms & Conditions</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-4">
                        <h3>Contact</h3>
                        <ul>
                            <li>2801 Ladona Dr</li>
                            <li>Fort Worth, TX 76133, Dallas, Texas (Head Office)</li>
                            <li>+852 3565 6850</li>
                            <li><a href="mailto:support@boxgofer.com">support@boxgofer.com</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3>Customer Service</h3>
                        <ul>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 6 pm</li>
                            <li>Delivery Hours:</li>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 9 pm</li>
                            <li>*Closed during public holidays</li>
                        </ul>
                    </div>
                </div>
                <h3>
                    Connect with us: 
                    <a class="icon icon-facebook"></a> 
                    <a class="icon icon-twitter"></a>
                </h3>
                <p class="ta-center">2015 Quodisys alright reserved</p>
            </div>
        </footer>
        <!--end footer-->
    </div>
</body>

</html>