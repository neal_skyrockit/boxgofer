﻿<!DOCTYPE html>
<html>
<head>
    <title>Your Zip Code</title>
    <meta charset="utf-8" />
    <!--css-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/main.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/responsive.css' rel='stylesheet' type='text/css'>
</head>
<body>
    <div id="wrapper">
        <!--header-->
        <header>
            <nav class="clearfix container">
                <a id="logo"><img src="/public/html/images/logo.png" /></a>
                <div>
                    <ul class="quickaction">
                        <li><a class="">Order Now</a></li>
                        <li><a class="">Login</a></li>
                    </ul>
                    <ul class="clearfix">
                        <li><a>How it work</a></li>
                        <li><a>FAQ</a></li>
                        <li><a>Customer Support : <span class="color">855 999 650</span></a></li>
                    </ul>
                </div>           
            </nav>
        </header>
        <!--end header-->

        <!--main content-->
        <main>
            <div class="container">
                <!--login page-->
                <section id="page-yourzipcode">
                    <form class="frm-yourzipcode">
                        <h3>ENTER YOUR ZIPCODE</h3>
                        <div class="zipcodesection">
                            <div class="row">
                                <div class="part col-xs-3"><label>Zip Code</label></div>
                                <div class="part col-xs-5"><input class="form-control bor-color" /></div>
                                <div class="part col-xs-4"><button class="btn bg-color">Submit</button></div>
                            </div>
                        </div>                   
                        <div class="supportsection">
                            <p>Our Gofer's aren't serving your home yet, but please enter your e-mail below and we will notify you when our Gofer's are heading your way.</p>
                            <div class="row">
                                <div class="part col-xs-8"><input class="form-control bor-color" placeholder="Email"/></div>
                                <div class="part col-xs-4"><button class="btn bg-color">Send</button></div>
                            </div>
                        </div>
                    </form>

                </section>
                <!--end login page-->
            </div>
        </main>
        <!--end main content-->

        <!--footer-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Boxgofer</h3>
                        <nav>
                            <ul>
                                <li><a>FAQ</a></li>
                                <li><a>About Us</a></li>
                                <li><a>Privacy</a></li>
                                <li><a>Terms & Conditions</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-4">
                        <h3>Contact</h3>
                        <ul>
                            <li>2801 Ladona Dr</li>
                            <li>Fort Worth, TX 76133, Dallas, Texas (Head Office)</li>
                            <li>+852 3565 6850</li>
                            <li><a href="mailto:support@boxgofer.com">support@boxgofer.com</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3>Customer Service</h3>
                        <ul>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 6 pm</li>
                            <li>Delivery Hours:</li>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 9 pm</li>
                            <li>*Closed during public holidays</li>
                        </ul>
                    </div>
                </div>
                <h3>
                    Connect with us:
                    <a class="icon icon-facebook"></a>
                    <a class="icon icon-twitter"></a>
                </h3>
                <p class="ta-center">2015 Quodisys alright reserved</p>
            </div>
        </footer>
        <!--end footer-->
    </div>
</body>
</html>
