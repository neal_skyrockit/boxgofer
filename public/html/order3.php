<!DOCTYPE html>
<html>

<head>
    <title>Login</title>
    <meta charset="utf-8" />
    <!--css-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/main.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/responsive.css' rel='stylesheet' type='text/css'>
</head>

<body>
    <div id="wrapper">
        <!--header-->
        <header>
            <nav class="clearfix container">
                <a id="logo"><img src="/public/html/images/logo.png" /></a>
                <div>
                    <ul class="quickaction">
                        <li><a class="">Order Now</a></li>
                        <li>
                            <input id="trigger-user-actions" type="checkbox" class="hidden trigger-user-actions">
                            <label for="trigger-user-actions" class="user-actions">DDD</label>
                            <ul class="sub-menu">
                                <li><a>My Stuff</a></li>
                                <li><a>Account Settings</a></li>
                                <li><a>Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="clearfix">
                        <li><a>Customer Support : <span class="color">855 999 650</span></a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--end header-->

        <!--main content-->
        <main>
            <!--order page-->
            <section class="page" id="page-section">
                <div class="order-line-section">
                    <div class="container">
                        <p class="order-line">
                            <label class="step schedule-step">1</label>
                            <label class="step review-step">2</label>
                            <label class="step billing-step active">3</label>
                            <label class="step confirm-step">4</label>
                        </p>
                    </div>
                </div>
                <div class="container">
                    <div class="order-view">
                        <!-- row 1-->
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                <p class="color fs24">Billing Details</p>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="grey-section billing-step">
                                    <!-- row -->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p class="color">Secure Payment</p>
                                        </div>
                                    </div>
                                    <!-- row -->
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <span>Credit Card</span>
                                        </div>
                                        <div class="col-xs-4">
                                            <label class="payment payment-credit" for="payment-credit"></label>
                                        </div>
                                        <div class="col-xs-4">
                                            <label class="payment payment-paypal">
                                                <form id="paypal-form" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                                    <input name="cmd" value="_s-xclick" type="hidden">
                                                    <input name="hosted_button_id" value="L6AVWMM8XWYX8" type="hidden">
                                                    <input src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" name="submit" alt="PayPal - The safer, easier way to pay online!" border="0" type="image">
                                                    <img alt="" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" border="0" height="1" width="1">
                                                </form>
                                            </label>
                                        </div>
                                    </div>
                                    <!-- row -->
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <span>Card Number</span>
                                        </div>
                                        <div class="col-xs-8">
                                            <input class="form-control" />
                                        </div>
                                    </div>
                                    <!-- row -->
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <span>Expiration</span>
                                        </div>
                                        <div class="col-xs-3">
                                            <input class="form-control" />
                                        </div>
                                        <div class="col-xs-2">
                                            <span>CW</span>
                                        </div>
                                        <div class="col-xs-3">
                                            <input class="form-control" />
                                        </div>
                                    </div>
                                    <!-- row -->
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <span>Name on Card</span>
                                        </div>
                                        <div class="col-xs-8">
                                            <input class="form-control" />
                                        </div>
                                    </div>
                                    <!-- row -->
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <span>Billing Address</span>
                                        </div>
                                        <div class="col-xs-8">
                                            <input class="form-control" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p style="margin-top:20px">Your details are securely stored with our payment processing partner using bank-grade encryption.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row 1 -->
                        <!-- row 2-->
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="grey-section billing-step">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p>Monthly cost : <span class="color fs24">$30</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row 2 -->
                        <!--row 5-->
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="button-next pull-right">Next</button>
                                <button class="button-back pull-right">Back</button>
                            </div>
                        </div>
                        <!-- end row 5 -->
                    </div>
                </div>
            </section>

            <!--end order page-->
        </main>
        <!--end main content-->

        <!--footer-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Boxgofer</h3>
                        <nav>
                            <ul>
                                <li><a>FAQ</a></li>
                                <li><a>About Us</a></li>
                                <li><a>Privacy</a></li>
                                <li><a>Terms & Conditions</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-4">
                        <h3>Contact</h3>
                        <ul>
                            <li>2801 Ladona Dr</li>
                            <li>Fort Worth, TX 76133, Dallas, Texas (Head Office)</li>
                            <li>+852 3565 6850</li>
                            <li><a href="mailto:support@boxgofer.com">support@boxgofer.com</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3>Customer Service</h3>
                        <ul>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 6 pm</li>
                            <li>Delivery Hours:</li>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 9 pm</li>
                            <li>*Closed during public holidays</li>
                        </ul>
                    </div>
                </div>
                <h3>
                    Connect with us: 
                    <a class="icon icon-facebook"></a> 
                    <a class="icon icon-twitter"></a>
                </h3>
                <p class="ta-center">2015 Quodisys alright reserved</p>
            </div>
        </footer>
        <!--end footer-->
    </div>
</body>

</html>