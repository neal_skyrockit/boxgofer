<!DOCTYPE html>
<html>

<head>
    <title>Login</title>
    <meta charset="utf-8" />
    <!--css-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/main.css' rel='stylesheet' type='text/css'>
    <link href='/public/html/css/responsive.css' rel='stylesheet' type='text/css'>
</head>

<body>
    <div id="wrapper">
        <!--header-->
        <header>
            <nav class="clearfix container">
                <a id="logo"><img src="/public/html/images/logo.png" /></a>
                <div>
                    <ul class="quickaction">
                        <li><a class="">Order Now</a></li>
                        <li>
                            <input id="trigger-user-actions" type="checkbox" class="hidden trigger-user-actions">
                            <label for="trigger-user-actions" class="user-actions">DDD</label>
                            <ul class="sub-menu">
                                <li><a>My Stuff</a></li>
                                <li><a>Account Settings</a></li>
                                <li><a>Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="clearfix">
                        <li><a>Customer Support : <span class="color">855 999 650</span></a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--end header-->

        <!--main content-->
        <main>
            <!--order page-->
            <section class="page" id="page-section">
                <div class="order-line-section">
                    <div class="container">
                        <p class="order-line">
                            <label class="step schedule-step">1</label>
                            <label class="step review-step ">2</label>
                            <label class="step billing-step">3</label>
                            <label class="step confirm-step active">4</label>
                        </p>
                    </div>
                </div>
                <div class="container">
                    <div class="order-view">
                        <!--row 1-->
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                <p class="color fs24">Confirm</p>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <p class="color">Thank you for ordering Storage at Your Doorstep</p>
                                <p>Your Appointment Confirmation Number:<span class="color fs18"> JNFVT </span></p>
                            </div>
                        </div>
                        <!--end row 1-->
                        <!-- row 1-->
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="grey-section nopadding">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <span class="icon icon-smallpackage"></span>
                                            <span class="text">3 Large</span>
                                        </div>
                                        <p class="margin0300 visible-xs col-xs-12"></p>
                                        <div class="col-xs-12 col-sm-6">
                                            <span class="icon icon-bigpackage"></span>
                                            <span class="text">1 Extra Large</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="grey-section nopadding">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <span class="icon icon-bicycle"></span>
                                            <span class="text">3 Large Items</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="grey-section nopadding">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <span class="icon icon-calendarV2"></span>
                                            <span class="text">Thursday, 31 Dec. 2015 </span>
                                        </div>
                                        <p class="margin0300 visible-xs col-xs-12"></p>
                                        <div class="col-xs-12 col-sm-6">
                                            <span class="icon icon-clockV2"></span>
                                            <span class="text">15:00</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="grey-section nopadding">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <span class="icon icon-home"></span>
                                            <span class="text">2801 Ladona Dr, Fort Worth, TX 76133, Dallas</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="grey-section nopadding">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <span class="icon icon-contact-phone"></span>
                                            <span class="text">+852 8193 4025</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="grey-section nopadding">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p style="padding:12px 0 12px 66px;margin:0">Monthly cost : <span class="color fs24">$30</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row 1 -->
                        <!--row 5-->
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="button-next pull-right" onclick="congrat.show()">Next</button>
                                <span class="pull-right color refer-text">Refer a Friend and Receive a Month Free</span>
                            </div>
                        </div>
                        <!-- end row 5 -->
                        <div id="congrat" class="message-group">
                            <div class="blur-layer" data-hide></div>
                            <div class="message-body">
                                <img src="images/logo-message.png" height="" />
                                <h3>Congratulations!</h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                                <button class="button-next" data-hide>Done</button>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

            <!--end order page-->
        </main>
        <!--end main content-->
        <!--footer-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Boxgofer</h3>
                        <nav>
                            <ul>
                                <li><a>FAQ</a></li>
                                <li><a>About Us</a></li>
                                <li><a>Privacy</a></li>
                                <li><a>Terms & Conditions</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-4">
                        <h3>Contact</h3>
                        <ul>
                            <li>2801 Ladona Dr</li>
                            <li>Fort Worth, TX 76133, Dallas, Texas (Head Office)</li>
                            <li>+852 3565 6850</li>
                            <li><a href="mailto:support@boxgofer.com">support@boxgofer.com</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3>Customer Service</h3>
                        <ul>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 6 pm</li>
                            <li>Delivery Hours:</li>
                            <li>Monday to Sunday,</li>
                            <li>9 am to 9 pm</li>
                            <li>*Closed during public holidays</li>
                        </ul>
                    </div>
                </div>
                <h3>
                    Connect with us: 
                    <a class="icon icon-facebook"></a> 
                    <a class="icon icon-twitter"></a>
                </h3>
                <p class="ta-center">2015 Quodisys alright reserved</p>
            </div>
        </footer>
        <!--end footer-->
    </div>
    <script>
        function MessageController(selector){
            var _ = this;
            _.selector = selector;
            _.hide = function(){
                _.selector.style.display = "none";
            }
            
            var hideActionDOM = _.selector.querySelectorAll("*[data-hide]");
            [].forEach.call(hideActionDOM,function(obj){             
                obj.onclick = function(){
                    _.hide();
                }
            })
        }
        
        MessageController.prototype.show = function(){
            var _ = this;
            _.selector.style.display = "block";
        }
        
        MessageController.prototype.hide = function(){
            var _ = this;
            _.hide();
        }
        
        var congrat = new MessageController(document.getElementById('congrat'));
        
    </script>
</body>

</html>