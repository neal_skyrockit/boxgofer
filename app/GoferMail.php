<?php namespace App;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Config;

class GoferMail
{
	/**
	 * sendMail
	 * $data = ['data' =>{}, 'buyername' => 'Manh Vu', 'buyeremail' => 'manh@quodisys.com']
	 * $view = 'emails.buyaccount'
	 */
	static function sendMail($to, $subject, $view, $data = array())
	{
		$mail = Config::get('mail.from');
		return Mail::send ( $view, $data, function ($message) use($to, $subject, $mail) {
			// note: if you don't set this, it will use the defaults from config/mail.php
			$message->from ( $mail['address'], $mail['name'] );
			$message->to ( $to );
			$message->subject ( $subject );
		} );
	}

	/**
	 * testSendMail
	 */
	static function testSendMail($data = array())
	{
		// $mail = Config::get('mail.from');
		$mail = Config::get('mail.from2');
		return Mail::send ( "emails.testemail", $data, function ($message) use($mail) {
			// note: if you don't set this, it will use the defaults from config/mail.php
			$message->from ( $mail['address'], $mail['name'] );
			// $message->to ("manh@quodisys.com");
			$message->to ("neal@globerunnerseo.com");
			$message->subject ( "Test cron mail - " . date('Y-m-d h:i:s') );
		} );
	}

	/**
	 * bgEncrypt
	 * Manh created 20160219
	 * Use Forgot password
	 */
	static function bgEncrypt($email, $salt = "boxgofer.com")
	{
		return urlencode(trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $email, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)))));
	}

	/**
	 * bgDecrypt
	 * Manh created 20160219
	 * Use Forgot password
	 */
	static function bgDecrypt($strEncrypt, $salt = "boxgofer.com")
	{
		$strEncrypt = urldecode($strEncrypt);
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($strEncrypt), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
	}
}