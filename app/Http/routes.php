<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Manh add web pages - 20160105
Route::get('/', 'WebController@index');
Route::get('/home', 'WebController@home');
Route::get('/zipcode', 'WebController@zipcode');
Route::match(array('GET', 'POST'), '/register/{code}/{token?}', 'WebController@register');
Route::match(array('GET', 'POST'), '/settings', 'WebController@settings');
Route::get('/history', 'WebController@history');
//Route::get('/refer', 'WebController@refer');
Route::get('/mystuff', 'WebController@mystuff');
Route::post('/uploadImages', 'WebController@uploadImages');
Route::match(array('GET', 'POST'), '/resetpassword', 'WebController@resetPassword');
Route::get('/dummyData', 'WebController@dummyData');
Route::get('/terms', 'WebController@terms');
Route::get('/privacy', 'WebController@privacy');
Route::get('/aboutus', 'WebController@aboutUs');
Route::get('/faq', 'WebController@faq');
// Route::get('/order', 'WebController@order');

// Added - 03/09/2017 - mm/dd/yyyy
Route::get('/order/{code}/{token?}', 'WebController@orderNow');


// DDD add admin pages - 20160216
Route::get('/admin', 'AdminController@index');
Route::match(array('GET', 'POST'),'/admin/customers', 'AdminController@client');
Route::match(array('GET', 'POST'),'/admin/staff', 'AdminController@staff');
Route::match(array('GET', 'POST'),'/admin/orders/{userid?}', 'AdminController@transList');
Route::get('/admin/login', 'AdminController@login');
Route::post('/admin/upsertStaff', 'AdminController@upsertStaff');
Route::get('/admin/bin', 'AdminController@bin');
Route::post('/admin/upload_excel', 'AdminController@upload_excel');
Route::get('/admin/calendar', 'AdminController@calendar');
Route::get('/admin/chart', 'AdminController@chart');
Route::get('/admin/invoices', 'AdminController@invoices');
Route::get('/admin/bintracking', 'AdminController@staffTransaction');
Route::get('/admin/warehouse', 'AdminController@warehouse');
Route::get('/admin/promotion', 'AdminController@promotion');

//Ajax
Route::get('/api/getAvailableTime', 'AjaxController@getAvailableTime');
Route::get('/api/getdisabledate', 'AjaxController@getDisableDate');
Route::post('/api/barcode/postbackApi', 'AjaxController@postbackApi');
Route::post('/api/barcode/postbackOtherStuffApi', 'AjaxController@postbackOtherStuffApi');
Route::post('/api/barcode/postbackWarehouseApi', 'AjaxController@postbackWarehouseApi');
Route::post('/api/checkZipCode', 'AjaxController@checkZipCode');
Route::post('/api/sendSuggestion', 'AjaxController@sendSuggestion');
Route::post('/api/uploadimages', 'AjaxController@uploadImages');
Route::post('/api/getStorageAddress', 'AjaxController@getStorageAddress');
Route::post('/api/deliverPickup', 'AjaxController@deliverPickup');
//Route::post('/api/cancelRequesting', 'AjaxController@cancelRequesting');
//Route::post('/api/adminCancelRequesting', 'AjaxController@adminCancelRequesting');
Route::post('/api/adminCancelOrder', 'AjaxController@adminCancelOrder');
Route::post('/api/changeContent', 'AjaxController@changeContent');
Route::post('/api/forgotpw', 'AjaxController@forgotpw');
Route::post('/api/changeStatusClient', 'AjaxController@changeStatusClient');
Route::post('/api/getInvoiceInfo', 'AjaxController@getInvoiceInfo');
Route::post('/api/getStorageInfo', 'AjaxController@getStorageInfo');
Route::post('/api/deleteimage', 'AjaxController@deleteImage');
Route::post('/api/checkEmail', 'AjaxController@checkEmail');
Route::post('/api/chargeCustomer', 'AjaxController@chargeCustomer');
Route::post('/api/editWarehouse', 'AjaxController@editWarehouse');
Route::post('/api/getCalendarMonth', 'AjaxController@getCalendarMonth');
Route::post('/api/getCalendarByDate', 'AjaxController@getCalendarByDate');
Route::post('/api/adminchangeworkingstatus', 'AjaxController@adminChangeWorkingStatus');
Route::post('/api/changebindatetime', 'AjaxController@changeBinDateTime');
// Paypal - remove paypal 20160502
Route::post('/api/stripe/callback', 'AjaxController@stripeCallback');
Route::post('/api/getItemPrice', 'AjaxController@getItemPrice');
// Route::post('/api/order', 'AjaxController@order');
Route::post('/api/order', 'AjaxController@orderNow');
Route::post('/api/create-account', 'AjaxController@createAccount');
Route::post('/api/referral', 'AjaxController@referral');
Route::post('/api/getClientInfo', 'AjaxController@getClientInfo');
Route::post('/api/saveClientInfo', 'AjaxController@saveClientInfo');
Route::post('/api/getTransInfo', 'AjaxController@getTransInfo');
Route::post('/api/uploadBinImage/{binid?}', 'AjaxController@uploadBinImage');
Route::post('/api/getStaffInfo', 'AjaxController@getStaffInfo');
Route::post('/api/upsertStaff', 'AjaxController@upsertStaff');
Route::post('/api/deletestaff', 'AjaxController@deleteStaff');
Route::post('/api/updateBin', 'AjaxController@updateBin');
Route::post('/api/getChartData', 'AjaxController@getChartData');
Route::post('/api/addBinTrans', 'AjaxController@addBinTrans');
Route::post('/api/getchanges', 'AjaxController@getChanges');
Route::post('/api/getPromoInfo', 'AjaxController@getPromoInfo');
Route::post('/api/processPromo', 'AjaxController@processPromo');
Route::post('/api/checkPromo', 'AjaxController@checkPromo');

// Add crontab send invoice at the end of day
//Route::get('/api/bgsendinvoicemail', 'AjaxController@bgSendInvoiceMail');
Route::get('/cron/bgsendinvoicemail', 'CronController@bgSendInvoiceMail');
Route::get('/cron/updateStripeDaily', 'CronController@updateStripeDaily');
Route::get('/cron/crondeliveries', 'CronController@cronDeliveries');
Route::get('/dowload/csv', 'CronController@downloadDeliveries');
Route::get('/cron/crontestmail', 'CronController@crontestmail');
// Test renewal subscription
Route::post('/test/testRenewSubscription', 'TestController@testRenewSubscription');
Route::post('/test/stripeCallbackManually', 'TestController@stripeCallbackManually');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => ['web']], function () {

	//Manh admin - 20160111
	Route::get('/admin', 'AdminController@index');
	Route::get('/admin/userlist', 'AdminController@userList');
	Route::get('/admin/translist', 'AdminController@transList');
	Route::get('/admin/storagelist', 'AdminController@storageList');

	// Facebook
	Route::get('fb/login', 'Auth\AuthboxgoferController@getFbLogin');
	Route::get('fb/callback', 'Auth\AuthboxgoferController@getFbCallback');

	Route::get('/login', 'WebController@login');
	Route::post('/login', 'Auth\AuthboxgoferController@postLogin');
	Route::get('/logout', 'Auth\AuthboxgoferController@getLogout');
	Route::post('/postregister', 'Auth\AuthboxgoferController@postRegister');

	//Manh add auth for backend - 20160501
	Route::post('auth/login', 'Auth\AuthController@postLogin');
	Route::get('auth/logout', 'Auth\AuthController@getLogout');

	Route::controllers ( [
		'authboxgofer' => 'Auth\AuthboxgoferController',
		'auth' 		=> 'Auth\AuthController',
		'password' 	=> 'Auth\PasswordController'
	] );
});