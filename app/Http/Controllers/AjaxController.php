<?php
/**
 * AjaxController
 * - Use this controller for all ajax request
 *
 * @author Manh Vu
 * @since 1.0 2016/01/13
 * $Id: $
 */

namespace App\Http\Controllers;

use Monolog\Handler\Mongo;

use PhpParser\Node\Expr\Array_;

use Illuminate\Support\Facades\File;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use \DateTime;
use App\GoferMail;
use App\Models\Places;
use App\Models\PlacesSuggested;
use App\Models\Items;
use App\Models\Transactions;
use App\Models\Storage;
use App\Models\Users;
use App\Models\Staff;
use App\Models\History;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Routing\ResponseFactory;
use Excel;
use App\StripeFuns;
use App\Http\Helpers\CommonHelper;
use App\Models\Bginvoices;
use Config;
use App\Models\Calendars;
use App\Models\Warehouses;
use App\Models\Mailinvoices;
use App\Models\Promos;
use Session;

class AjaxController extends Controller {

    protected $guard = 'user';
    protected $auth;

    /**
     * contruct
     */
    public function __construct() {

        $this->middleware('authboxgofer', ['except' =>
        		[
                    'uploadBinImage', 'deleteImage', 'checkZipCode', 'sendSuggestion', 'uploadImages',
        			'getClientInfo', 'saveClientInfo', 'getTransInfo', 'forgotpw', 'getStaffInfo', 'upsertStaff',
        			'deleteStaff', 'updateBin', 'upload_excel', 'getChartData', 'postbackApi', 'postbackOtherStuffApi',
        			'postbackWarehouseApi', 'stripeCallback', 'paypalWebhook', 'getInvoiceInfo', 'getStorageInfo',
        			'checkEmail', 'chargeCustomer', 'addBinTrans', 'getChanges', 'createWarehouseDb', 'editWarehouse',
        			'getCalendarMonth', 'getCalendarByDate', 'changeStatusClient', 'bgSendInvoiceMail', 'getStorageAddress',
        			'getAvailableTime', 'cancelRequesting', 'adminChangeWorkingStatus', 'adminCancelOrder', 'changeBinDateTime',
        			'getDisableDate', 'getPromoInfo', 'processPromo', 'checkPromo', 'getItemPrice', 'orderNow', 'createAccount'
        		] 
        	]);

        $this->places = new Places();
        $this->suggest = new PlacesSuggested();
        $this->items = new Items();
        $this->staff = new Staff();
        $this->transaction = new Transactions();
        $this->auth = Auth::guard('managers');
    }

    /**
     * changeContent
     */
    public function changeContent() {

    	$post = Input::all();

    	if (empty($post['storagedkey']) || empty($post['content'])) {
    		return ['error' => 'Got error'];
    	}

    	$storage = Storage::find($post['storagedkey']);
    	if ($storage) {
    		$storage->content = $post['content'];
    		$storage->save();

    		return 1;
    	}

    	return ['error' => 'Cannot find this storage'];
    }

	/**
	 * sendSuggestion
	 */
	public function sendSuggestion() {

		$post = Input::all();

		if (!isset($post['zipcode']) || !is_numeric($post['zipcode'])
				|| !isset($post['email']) || empty($post['email'])) {
			return ['code' => 0, 'error' => 'Please re-check the zip code or email.'];
		}

		if (!filter_var($post['email'], FILTER_VALIDATE_EMAIL) !== false) {
			return ['code' => 0, 'error' => 'Email is invalid.'];
		}

		return $this->suggest->sendSuggestion($post['zipcode'], $post['email']);
	}

	/**
	 * checkZipCode
	 */
	public function checkZipCode() {

		$post = Input::all();

		if (!isset($post['zipcode']) || empty($post['zipcode'])) {
			return 0;
		}

		return $this->places->checkZipCode((int)$post['zipcode']);
	}

	/**
	 * checkDateTime
	 * Use to check Pick-up or Drop-off time is available or not
	 */
	public function checkDateTime() {

		$post = Input::all();

		if (!isset($post['date']) || empty($post['time'])) {
			return ['error' => 'Need input Date and Time'];
		}

		$calendar = new Calendars();

		return $calendar->isAvailable($post);
	}

	/**
	 * getStorageAddress
	 */
	public function getStorageAddress() {

		$post = Input::all();

		if (!isset($post['key']) || empty($post['key'])) {
			return 0;
		}

		$storage = new Storage();

		return $storage->getStorageAddressById($post['key']);
	}

	/**
	 * forgotpw
	 */
	public function forgotpw() {

		$post = Input::all();
		$url = Config::get('app.url');

		if (!isset($post['email']) || empty($post['email'])) {
			return ['error' => 'Enter your email.'];
		}

		$obj = Users::where('email', $post['email'])->first();
		if (!$obj) return ['error' => 'Your email is not exist.'];
		$str = date('mdY') . '-' . $post['email'];

		$data = array(
				'title'=> 'You have requested to reset your password',
				'content' => 'To reset your password, click the following link.',
				'url' => $url . '/resetpassword?token=' . GoferMail::bgEncrypt($str));

		$email = $post['email'];

		if(GoferMail::sendMail($email, 'Box Gofer Password Reset Request', 'emails.forgotpw', $data)) {
			return 1;
		}

		return ['error' => 'Got an error'];
	}

	/**
	 * cancelRequesting
	 * Pausing
	 */
	/* public function cancelRequesting() {

		$user = Auth::guard($this->guard)->user();
		$post = Input::all();

		if ($user && isset($post['form']['storagedkey']) && isset($post['cuskey']))
		{
			// Check address
			if (!isset($post['form']['addressfullca']) || $post['form']['addressfullca'] != ''
					&& !Hash::check($post['form']['passwordca'], $user->password))
			{
				return [
	                'error' => true,
	                'message' => 'Password not match'
            	];
			}
			// Address
			if (isset($post['form']['addressfullca']) && $post['form']['addressfullca'] != '')
			{
				$post['form']['addressca'] = $post['form']['addressfullca'];
			}

			if (empty($post['form']['dateca']) || empty($post['form']['timeca']))
				return [
	                'error' => true,
	                'message' => 'Enter your date and time'
            	];

			// Update Storage
			$obj = new Storage();
			// Manh add userid (custom) for create Calendar
			$post['form']['userid'] = $user->userid;

			$result = ($post['form']['storagedkey'] != 'All') ?
					$obj->cancelStorageByIdAjax($post['form']) : $obj->cancelStorageUserAjax($post['form'], $user->_id);

			// Send mail
			if (!isset($result['error'])) {
				$storage = $obj::find($post['form']['storagedkey']);

				$metaData = [
						'phone' => $storage->phone,
						'address' => $storage->address,
						'customername' => $storage->customername,
						'email' => $storage->relationUser->email,
						'chargetype' => 'deliver',
						'addressstreet' => $storage->addressstreet,
						'cityzipcode' => $storage->cityzipcode,
						'aptsuite' => $storage->aptsuite,
						'userid' => $storage->relationUser->userid,
						'orderId' => $storage->transactions->transcode,
				];

				// Charge money
				$stripe = Config::get('services.stripe');
				$charge = $this->stripeCharge($post['cuskey'], $stripe['deliverAmount'], 'Charge for Cancel drop-off', $metaData);
				if (isset($charge['error'])) {
					return [
					'error' => true,
					'message' => $charge['error']
					];
				}

				$item = is_array($result) ? implode(', ', $result) : $result;

				$data = array('title'=> 'Cancellable item(s): ' . $item , 'timestamp' => 'Cancellable time: ' . $post['form']['dateca'] . ' ' . $post['form']['timecashow']);

				if(GoferMail::sendMail($user->email, 'Boxgofer - Cancellable ' . $item, 'emails.timestamp', $data)) {
					return 1;
				}

				return [
		                'error' => true,
		                'message' => 'Error send mail'
            		];
			}

			return $result;
		}

		return ['error' => 'Got error'];
	} */

	/**
	 * adminCancelOrder
	 *
	 * @author Manh
	 */
	public function adminCancelOrder() {

		$post = Input::all();

		if (isset($post['id']))
		{
			// Update Storage
			$obj = new Transactions();
			$result = $obj->cancelOrderById($post['id']);

			return $result;

		}

		return ['error' => 'Got error'];
	}

	/**
	 * adminCancelRequesting
	 * author: Chuong
	 */
	/* public function adminCancelRequesting() {

		$post = Input::all();
		$user = Users::getUserById($post['form']['userid']);

		if ($user && isset($post['form']['storagedkey']) && isset($post['cuskey']))
		{
			// Address
			if (isset($post['form']['addressfullca']) && $post['form']['addressfullca'] != '')
			{
				$post['form']['addressca'] = $post['form']['addressfullca'];
			}

			if (empty($post['form']['dateca']) || empty($post['form']['timeca']))
				return [
						'error' => true,
						'message' => 'Enter your date and time'
				];

				// Update Storage
				$obj = new Storage();
				// Manh add userid (custom) for create Calendar
				$post['form']['userid'] = $user->userid;

				$result = ($post['form']['storagedkey'] != 'All') ?
				$obj->cancelStorageByIdAjax($post['form']) : $obj->cancelStorageUserAjax($post['form'], $user->_id);

				// Send mail
				if (!isset($result['error'])) {
					$storage = $obj::find($post['form']['storagedkey']);

					$metaData = [
							'phone' => $storage->phone,
							'address' => $storage->address,
							'customername' => $storage->customername,
							'email' => $storage->relationUser->email,
							'chargetype' => 'deliver',
							'addressstreet' => $storage->addressstreet,
							'cityzipcode' => $storage->cityzipcode,
							'aptsuite' => $storage->aptsuite,
							'userid' => $storage->relationUser->userid,
					];

					// Charge money
					$stripe = Config::get('services.stripe');
					$charge = $this->stripeCharge($post['cuskey'], $stripe['deliverAmount'], 'Charge for Cancel drop-off', $metaData);
					if (isset($charge['error'])) {
						return [
								'error' => true,
								'message' => $charge['error']
						];
					}

					$item = is_array($result) ? implode(', ', $result) : $result;

					$data = array('title'=> 'Cancellable item(s): ' . $item , 'timestamp' => 'Cancellable time: ' . $post['form']['dateca'] . ' ' . $post['form']['timecashow']);

					if(GoferMail::sendMail($user->email, 'Boxgofer - Cancellable ' . $item, 'emails.cancel', $data)) {
						return 1;
					}

					return [
							'error' => true,
							'message' => 'Error send mail'
					];
				}

				return $result;
		}

		return ['error' => 'Got error'];
	} */

	/**
	 * deliverPickup
	 */
	public function deliverPickup() {

		$user = Auth::guard($this->guard)->user();
		$post = Input::all();

		if ($user && isset($post['form']['storagedkey']))
		{
			// Check address
			if (!isset($post['form']['address']) || $post['form']['address'] != ''
					&& !Hash::check($post['form']['password'], $user->password))
			{
				return [
						'error' => true,
						'message' => 'Password not match'
					];
			}
			// Check date time
			$flgDatetime = ($post['form']['option'] == 1) ?
					(!empty($post['form']['date']) && !empty($post['form']['time'])) : (!empty($post['form']['date']) && !empty($post['form']['time'])
							&& !empty($post['form']['date2']) && !empty($post['form']['time2']));

			if (!$flgDatetime)
				return [
					'error' => true,
					'message' => 'Enter your date and time'
				];

			// Update Storage
			$obj = new Storage();
			// Manh add userid (custom) for create Calendar
			$post['form']['userid'] = $user->userid;

			$result = ($post['form']['storagedkey'] != 'All') ?
					$obj->updateStorageByIdAjax($post['form']) : $obj->updateStorageUserAjax($post['form'], $user->_id);

			// Send mail
			if (!isset($result['error'])) {

				$storage = $result[0];

				$metaData = [
					'phone' => $storage->phone,
					'address' => $storage->address,
					'customername' => $storage->customername,
					'email' => $storage->relationUser->email,
					'addressstreet' => $storage->addressstreet,
					'cityzipcode' => $storage->cityzipcode,
					'aptsuite' => $storage->aptsuite,
					'chargetype' => 'deliver',
					'userid' => $storage->relationUser->userid,
					'orderId' => $storage->transactions->transcode,
				];

				// Charge money
				$stripe = Config::get('services.stripe');
				$charge = $this->stripeCharge($user->stripe_id, $stripe['deliverAmount'], 'Charge for Delivery drop-off', $metaData);

				/* if (isset($charge['error'])) {
					return [
					'error' => true,
					'message' => $charge['error']
					];
				}

				$item = $this->getDeliverItems($result);

				$data = array('title'=> 'Delivery item(s): ' . $item , 'timestamp' => 'Delivery time: ' . $post['form']['date'] . ' ' . $post['form']['timeshow']);
				//$email = 'manh@quodisys.com';//$user->email;

				if(GoferMail::sendMail($user->email, 'Boxgofer - Delivery ' . $item, 'emails.deliver', $data)) {
					return 1;
				}

				return [
					'error' => true,
					'message' => 'Error send mail'
				]; */
				// Done
				return 1;

			}
			// return error
			return $result;
		}

		return [
			'error' => true,
			'message' => 'Got error'
		];
	}

	/**
	 * getDeliverItems
	 */
	/* private function getDeliverItems($data) {
		$items = '';
		foreach($data as $foo) {
			if ($items == '') {
				$items = $foo->transactions->transcode;
			} else {
				$items .= ',' . $foo->transactions->transcode;
			}
		}

		return $items;
	} */

	/**
	 * deleteImage
	 */
	public function deleteImage() {

		$post = Input::all();

		if (!isset($post['image']) || !isset($post['storage'])) return ['error' => 'Cannot found this image.'];

		$obj = Storage::find($post['storage']);

		if (empty($obj)) return ['error' => 'Cannot found this Storage.'];

		$temp = $obj->images;

		if(($key = array_search($post['image'], $temp)) !== false)
		{
			unset($temp[$key]);

			$fullpath = base_path().'/public/uploads/' . $post['image'];
			File::Delete($fullpath);

			$obj->images = $temp;
			$obj->save();

			return $obj->images;
		}

		return ['error' => 'Cannot delete.'];
	}

    /**
     * uploadImages
     */
    public function uploadImages() {

        $image = Input::file('image');

        if ($image) {

            // getting all of the post data
            $file = array('image' => $image);
            // setting up rules
            $rules = array('image' => 'required', ); //mimes:jpeg,bmp,png and for max size max:10000
            // doing the validation, passing post data, rules and the messages
            $validator = Validator::make($file, $rules);
            // Validate rule
            if ($validator->fails()) {
                // send back to the page with the input data and errors
                return ['code' => 2, 'error' => 'Choose the image'];
                //return Redirect::to('upload')->withInput()->withErrors($validator);
            }

            // Validate type
            if ($image->isValid()) {
                //$destinationPath = 'uploads'; // upload path
                $destinationPath = base_path().'/uploads';
                $fileName = date('Ymdhis');
                $fileName = $fileName.'_'.$image->getClientOriginalName();
                $image->move($destinationPath, $fileName); // uploading file to given path
                // sending back with message
                return ['code' => 1, 'success' => 'Success to upload'];
            }

        }

        return ['code' => 0, 'error' => 'Images is invalid'];
    }

    /**
     * DDD
     * getItemPrice
     */
    public function getItemPrice()
    {
    	$result = [];
        $listItem = $this->items->getItemsList();

        for($i = 0; $i < 3; $i ++) {
            $obj = [
                    'name' => $listItem[$i]->name,
                    'type' => $listItem[$i]->type,
                    'price' => $listItem[$i]->price,
                    'description' => $listItem[$i]->description,
                    'total' => $listItem[$i]->total,
                    'total_rented' => $listItem[$i]->total_rented,
            ];

            $key = $listItem[$i]->type === Items::ItemTypeLarge ? 'large'
                    :( $listItem[$i]->type === Items::ItemTypeExtraLarge ? 'extlarge' : 'otherstuff');
            $result[$key] = $obj;

        }
        return $result;
    }

    /**
     * getAvailableTime
     */
    public function getAvailableTime() {

    	$post = Input::all();
    	$result = ['error' => 'Cannot get available time.'];

    	if (isset($post['date'])) {

    		$result = CommonHelper::timeDefined();
    		$checkCalendar = isset($post['checkcalendar']) && $post['checkcalendar'] == 'false' ? 0 : 1;

    		// Saturday start 9 am - 5 pm
    		if (date('w', strtotime($post['date'])) == 6) {
    			array_shift($result);
    			array_pop($result);
    		}

			if ($checkCalendar) {
				$obj = new Calendars();
				$calendar = $obj->getCalendarsByDate($post['date']);

				if (!empty($calendar)) {
					foreach ($result as $key => $foo) {
						if (in_array($foo['time'][0], $calendar))
							$result[$key]['text'] = 'text-inactive';
					}
				}
			}
    	}

    	return $result;
    }

    /**
     * DDD
     * order
     */
    public function validateDate($date) {
        $d = DateTime::createFromFormat('m/d/Y', $date);
        return $d && $d->format('m/d/Y') == $date;
    }

    public function validTime($time) {
        $regexTime = '/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9][ ](am|pm)$/';
        return preg_match($regexTime, $time, $matches);
    }

    public function order() {
        try {

            $user = Auth::guard($this->guard)->user();
            $post = Input::all();

            //Check bins valid
            if (!isset($post['large'])
            || !isset($post['extlarge'])
            || !isset($post['otherstuff']))
                return response()->json([
                        'step' => 1
                        ,'message' => 'Bins invalid'
                        ], 500);

            $large = is_numeric($post['large']) ? $post['large'] : 0;
            $extlarge = is_numeric($post['extlarge']) ? $post['extlarge'] : 0;
            $otherstuff = is_numeric($post['otherstuff']) ? $post['otherstuff'] : 0;
            $otherstuffDes = isset($post['otherDes']) ? $post['otherDes'] : '';

            if (($large + $extlarge + $otherstuff) === 0)
                return response()->json([
                        'step' => 1
                        ,'message' => 'Bins invalid'
                        ], 500);

            $listItem = $this->items->getItemsList();
            $orderBins = array();

            for($i=0; $i < 3; $i++) {

            	if (!isset($listItem[$i]) || empty($listItem[$i])) return false;

                if($listItem[$i]->type === Items::ItemTypeLarge){
                	$quanlity = $large;
                	$remain['large'] = $listItem[$i]->total - $listItem[$i]->total_rented;
                	$l = $listItem[$i];
                }
                else if($listItem[$i]->type === Items::ItemTypeExtraLarge){
                	$quanlity = $extlarge;
                	$e = $listItem[$i];
                }
                else{
                    $quanlity = $otherstuff;
                    $o = $listItem[$i];
                }

                $orderBins[] = [
	                '_id' => $listItem[$i]->_id,
	                'type' => $listItem[$i]->type,
	                //'plan_id' => $listItem[$i]->StripePlanId,
	                'quantity' => $quanlity,
                	'price' => $listItem[$i]->price,
                ];

            }

            if( ($large > $this->items->remainLarge($l) )
                || ($extlarge > $this->items->remainExtLarge($e))
                || ($otherstuff > $this->items->otherstuff($o))
              )
                return response()->json([
                    'code' => 1
                    ,'step' => 1
                    ,'message' => 'Sorry ! Large Bins is out'
                    ,'data' => ['large' => $l, 'extlarge' => $e, 'otherstuff' => $o]
                     ], 500);

            //Check date valid
            $dropoffDate = isset($post['dropoffDate']) ? $post['dropoffDate'] : '';
            if (!$this->validateDate($dropoffDate))
                return response()->json([
                    'step' => 1
                    ,'message' => 'Dropoff date invalid'
                    ], 500);

            //Check time valid
            $dropoffTime = isset($post['dropoffTime']) ? $post['dropoffTime'] : '';
            $dropoffTimeShow = isset($post['dropoffTimeShow']) ? $post['dropoffTimeShow'] : '';
            if(!$this->validTime($dropoffTime))
                return response()->json([
                    'step' => 1
                    ,'message' => 'Dropoff time invalid'
                    ], 500);

            //Check address valid
            $address = isset($post['addressfull']) ? $post['addressfull'] : '';
            $addressstreet = isset($post['addressstreet']) ? $post['addressstreet'] : '';
            $cityzipcode = isset($post['cityzipcode']) ? $post['cityzipcode'] : '';
            $aptsuite = isset($post['aptsuite']) ? $post['aptsuite'] : '';
            if (trim($address) === '' || $addressstreet === '' || $cityzipcode === '')
                return response()->json([
                    'step' => 1
                    ,'message' => 'Address is invalid'
                    ], 500);

            //Check phone valid
            $phone = isset($post['phone']) ? $post['phone'] : '';
            if (trim($phone) === '')
                return response()->json([
                        'step' => 1
                        ,'message' => 'Phone is invalid'
                        ], 500);

            $specialRequest = isset($post['specialRequest']) ? $post['specialRequest'] : '';

            //
            $setData['username'] = $user->name;
            $setData['email'] = $user->email;
            $setData['address'] = $post['addressfull'];
            $setData['phone'] = $phone;
            $setData['addressstreet'] = $addressstreet;
            $setData['cityzipcode'] = $cityzipcode;
            $setData['aptsuite'] = $aptsuite;
			$setData['userid'] = $user->userid;

			// Manh - 20160628 change to keep info on server then create subcription after scanned
			$arr['number'] = $post['card']['card_number'];
			$getExp = explode('/', $post['card']['exp']);
			$arr['month'] = isset($getExp[0]) ? trim($getExp[0]) : 0;
			$arr['year'] = isset($getExp[1]) ? trim($getExp[1]) : 0;
			$arr['cvc'] = $post['card']['cvc'];
			$arr['user_id'] = $user->_id;
			$arr['user_stripe_id'] = $user->stripe_id;
			// Manh add promotion code - 20160928
			$arr['promo_code'] = isset($post['promo']) ? $post['promo'] : '';

            $payment = new StripeFuns();
            $paymentInfo = $payment->Charge($arr, $orderBins, $setData, $dropoffDate, $dropoffTime);

            if (isset($paymentInfo['error'])) {
            	return response()->json([
            		'step' => 1
            		,'message' => $paymentInfo['error']
            	], 500);
            } else {
            	// Update user
            	if (empty($user->physicalAddress)) {
            		$user->physicalAddress = $addressstreet;
            	}
            	$user->phone = empty($user->phone) ? $phone : '';
            	$user->stripe_id = $paymentInfo['customer_id'];
            	$user->stripe_token = $paymentInfo['token_id'];
            	$user->save();
            }

            $transaction = Transactions::createTransactions($user->_id, $specialRequest, $paymentInfo, $otherstuffDes);
            //$count = 0;
            $dropoff = $dropoffDate . ' - ' . $dropoffTime;
            $dropoffShow = $dropoffDate . ' ' . $dropoffTimeShow;

            foreach($orderBins as $bin){

                if($bin['quantity'] > 0){

                    for ($i = 1; $i <= $bin['quantity']; $i++) {

                    	$itemCode =  ($bin['type'] == Items::ItemTypeLarge) ? 'LG' : ( $bin['type'] == Items::ItemTypeExtraLarge ? 'XL' : 'OS' );
                    	$barcode = 'DAL' . CommonHelper::randomGen(3) . '-' . $itemCode . '-' . $user->userid;
                        $code = Storage::createStorage($user->_id, $transaction->_id, $bin['_id'], $barcode, $dropoff,
                        		$address, $phone, $post['zipcode'], $transaction->transcode, $user->name, $addressstreet,
                        		$cityzipcode, $aptsuite);
                        //$count++;

                        //Create Calendar
                        $params['userid'] = $user->userid;// This is custom userid
                        $params['datetime'] = date('d/m/Y', strtotime($dropoffDate)) . ' - ' . trim($dropoffTime);
                        $params['date'] = date('d/m/Y', strtotime($dropoffDate));
                        $params['datesearch'] = strtotime($dropoffDate);
                        $params['time'] = trim($dropoffTime);
                        $params['address'] = $post['addressfull'];
                        $params['storagestatus'] = 'Drop-off';
                        $params['itemtype'] = $itemCode;
                        $params['storageid'] = $code->_id;
                        $calendar = Calendars::createCalendar($params);
                    }

                    $ite =  $bin['type'] == Items::ItemTypeLarge ? $l
                            : ( $bin['type'] == Items::ItemTypeExtraLarge ? $e : $o );
                    $ite->total_rented += $bin['quantity'];
                    $ite->save();
                }

            }

            //$user->save();

            //Update referral point - Manh 20160219 - delay
            /* if ($user->referral) {
            	$obj = Users::find($user->referral);
            	if ($obj) {
            		$obj->point = $obj->point + $large + $extlarge + $otherstuff;
            		$obj->save();
            	}
            } */

            //Send mail
            $data = array(
                'title'=> 'Box Gofer: Bin Dropoff',
            	'timestamp' => $dropoffShow,
            	'address' => $post['addressfull'],
            	'name' => $user->name,
            	'phone' => $phone,
            	'formatDateEmail' => CommonHelper::formatDateForEmail($dropoff)

            );

            GoferMail::sendMail($user->email, 'Box Gofer Order Confirmation: Bin Drop-Off Schedule', 'emails.timestamp', $data);
            $this->setCurrentChange($listItem);

            return ["trancode" => $transaction->transcode, "request" => $post];

        } catch (Exception $e) {
            return response()->json([
                        'step' => 3
                        ,'message' => $e->getMessage()
                        ], 500);
        }

    }

    public function createAccount( Request $request ){

    	if ( !Auth::guard('user')->user() ) {

	    	$user = new Users();
			$place = $this->places->getPlaceByZipcode($request->zipcode);

			// Places
			if ($place == null)
				return redirect('zipcode');

			// Post processing
			if( $request->isMethod('post') ){

				$validator = Validator::make((array)$request->all(), [
					"zipcode"  				=> 'required',
					"phone" 				=> 'required',
					"cityzipcode" 			=> 'required',
					"addressstreet" 		=> 'required',
					"account.full_name"		=> 'required',
					"account.email" 		=> 'required|email|unique:users,email'
		        ]);

		        $error = $validator->errors();

		        if( count($error) > 0 ){
		        	if($error->has('account.email')){
		        		return response()->json(['message' => 'The email has already registered. If you have created an account, please login.', 'status' => '', 'error_field' => 'email'  ]);
		        	} else {
		        		return response()->json(['message' => $error->first(), 'status' => '']);
		        	}
		        }

		         // User Id
		       	$fullname 	= $request->account['full_name'];
				$nameStr 	= '';
				$postname 	= preg_replace('/[^ \w]+/', '', $fullname); // Removes special chars.
				$arr 		= explode(' ', $postname);
				$nameStr 	= $arr[count($arr) - 1];
				$newstr 	= '';
				for($i = 0; $i < 4; $i ++) {
					$newstr .= isset($nameStr[$i]) ? $nameStr[$i] : '_';
				}
				$nameStr 	= strtoupper($newstr) . rand(100, 999) . '-' . $request->zipcode;

				// User Create
				$user->userid 		= $nameStr;
				$user->zipcode 		= $request->zipcode;
	            $user->city 		= $place['name'];
	            $user->full 		= $place['full'][$request->zipcode];
				$user->name 		= $fullname;
				$user->email 		= $request->account['email'];
				$user->password 	= bcrypt('123456789'); //str_random(16)
				$user->status 		= 1;
	            $user->totalorder 	= 0;
	            $user->ipaddress 	= CommonHelper::getRealIpAddr();

	            // User token
				if ($request->token) {
					$user->referral = $request->token;
				}

	            // Save user
				$user->save();

				// GoferMail::sendMail($user->email, 'Box Gofer User Registration', 'emails.createUser', $user);

				// Set login
				Auth::guard($this->guard)->loginUsingId($user->_id, true);

				return response()->json(['message' => 'Account has been created. ', 'status' => 'success', 'action' => 'userCreate']);
			}

		}
		return response()->json(['message' => 'The user is logged in...', 'status' => 'success', 'action' => 'userLogin']);
    }

    public function orderNow( Request $request ) {

    	$user = Auth::guard($this->guard)->user();

		// Post processing
		if( $request->isMethod('post') ){
    		$validator = Validator::make((array)$request->all(), [
    			"large" 				=> 'required',
    			"extlarge" 				=> 'required',
    			"otherstuff" 			=> 'required',
				"zipcode"  				=> 'required',
				"dropoffDate" 			=> 'required',
				"dropoffTimeShow" 		=> 'required',
				"phone" 				=> 'required',
				"cityzipcode" 			=> 'required',
				"addressstreet" 		=> 'required',
				"card.card_number"		=> 'required',
				"card.exp"				=> 'required'
	        ]);

	        $error = $validator->errors();

	        if( count($error) > 0 ){
	        	return response()->json(['message' => $error->first(), 'status' => '']);
	        }

			//Check bins valid
            if (!isset($request->large)
             || !isset($request->extlarge)
             || !isset($request->otherstuff))
            	return response()->json(['step' => 1, 'message' => 'Bins invalid', 'status' => '']);

            $large 			= is_numeric($request->large) ? $request->large : 0;
            $extlarge 		= is_numeric($request->extlarge) ? $request->extlarge : 0;
            $otherstuff 	= is_numeric($request->otherstuff) ? $request->otherstuff : 0;
            $otherstuffDes 	= isset($request->otherDes) ? $request->otherDes : '';

            if (($large + $extlarge + $otherstuff) === 0)
            	return response()->json(['step' => 1, 'message' => 'Bins invalid', 'status' => '']);

            $listItem = $this->items->getItemsList();
            $orderBins = array();

            for($i=0; $i < 3; $i++) {

            	if (!isset($listItem[$i]) || empty($listItem[$i])) return false;

                if($listItem[$i]->type === Items::ItemTypeLarge){
                	$quanlity = $large;
                	$remain['large'] = $listItem[$i]->total - $listItem[$i]->total_rented;
                	$l = $listItem[$i];
                }
                else if($listItem[$i]->type === Items::ItemTypeExtraLarge){
                	$quanlity = $extlarge;
                	$e = $listItem[$i];
                }
                else{
                    $quanlity = $otherstuff;
                    $o = $listItem[$i];
                }

                $orderBins[] = [
	                '_id' => $listItem[$i]->_id,
	                'type' => $listItem[$i]->type,
	                //'plan_id' => $listItem[$i]->StripePlanId,
	                'quantity' => $quanlity,
                	'price' => $listItem[$i]->price,
                ];

            }

            // Check available stocks
            if( ($large > $this->items->remainLarge($l) )
                || ($extlarge > $this->items->remainExtLarge($e))
                || ($otherstuff > $this->items->otherstuff($o))
              )
            	return response()->json(['code' => 1, 'step' => 1, 'message' => 'Sorry ! Large Bins is out.', 'data' => ['large' => $l, 'extlarge' => $e, 'otherstuff' => $o], 'status' => '']);

            //Check date
            if (!$this->validateDate($request->dropoffDate))
            	return response()->json(['step' => 1, 'message' => 'Dropoff date is invalid.', 'status' => '']);

            //Check time
            if (!$this->validTime($request->dropoffTime))
            	return response()->json(['step' => 1, 'message' => 'Dropoff time is invalid.', 'status' => '']);

            //Check address
            if (trim($request->addressfull) === '' || $request->addressstreet === '' || $request->cityzipcode === '')
            	return response()->json(['step' => 1, 'message' => 'Address is invalid.', 'status' => '']);

            //Check phone valid
            if (trim($request->phone) === '')
            	return response()->json(['step' => 1, 'message' => 'Phone is invalid.', 'status' => '']);

            // Set data
			$setData['username'] 		= $user->name;
            $setData['email'] 			= $user->email;
            $setData['address'] 		= $request->addressfull;
            $setData['phone'] 			= $request->phone;
            $setData['addressstreet'] 	= $request->addressstreet;
            $setData['cityzipcode'] 	= $request->cityzipcode;
            $setData['aptsuite'] 		= $request->aptsuite;
			$setData['userid'] 			= $user->userid;

			// Manh - 20160628 change to keep info on server then create subcription after scanned
			$arr['number'] 			= $request->card['card_number'];
			$getExp 				= explode('/', $request->card['exp']);
			$arr['month'] 			= isset($getExp[0]) ? trim($getExp[0]) : 0;
			$arr['year'] 			= isset($getExp[1]) ? trim($getExp[1]) : 0;
			$arr['cvc'] 			= $request->card['cvc'];
			$arr['user_id'] 		= $user->_id;
			$arr['user_stripe_id'] 	= $user->stripe_id;
			// Manh add promotion code - 20160928
			$arr['promo_code'] 		= isset($request->promo) ? $request->promo : '';

            $payment = new StripeFuns();
            $paymentInfo = $payment->Charge($arr, $orderBins, $setData, $request->dropoffDate, $request->dropoffTime);

            if (isset($paymentInfo['error'])) {
            	return response()->json(['step' => 1, 'message' => $paymentInfo['error'], 'status' => '']);
            } else {
            	// Update user
            	if (empty($user->physicalAddress)) {
            		$user->physicalAddress = $request->addressstreet;
            	}

            	$user->phone 		= empty($user->phone) ? $request->phone : '';
            	$user->stripe_id 	= $paymentInfo['customer_id'];
            	$user->stripe_token = $paymentInfo['token_id'];
            	$user->save();
            }

            $transaction = Transactions::createTransactions($user->_id, $request->specialRequest, $paymentInfo, $request->otherstuffDes);

            $dropoff 	 = $request->dropoffDate . ' - ' . $request->dropoffTime;
            $dropoffShow = $request->dropoffDate . ' ' . $request->dropoffTimeShow;

            foreach($orderBins as $bin){

                if($bin['quantity'] > 0){

                    for ($i = 1; $i <= $bin['quantity']; $i++) {

                    	$itemCode =  ($bin['type'] == Items::ItemTypeLarge) ? 'LG' : ( $bin['type'] == Items::ItemTypeExtraLarge ? 'XL' : 'OS' );
                    	$barcode  = 'DAL' . CommonHelper::randomGen(3) . '-' . $itemCode . '-' . $user->userid;
                        $code 	  = Storage::createStorage($user->_id, $transaction->_id, $bin['_id'], $barcode, $dropoff, $request->addressfull, $request->phone, $request->zipcode, $transaction->transcode, $user->name, $request->addressstreet, $request->cityzipcode, $request->aptsuite);
                        //$count++;

                        //Create Calendar
                        $params['userid'] 		 = $user->userid;// This is custom userid
                        $params['datetime'] 	 = date('d/m/Y', strtotime($request->dropoffDate)) . ' - ' . trim($request->dropoffTime);
                        $params['date'] 		 = date('d/m/Y', strtotime($request->dropoffDate));
                        $params['datesearch'] 	 = strtotime($request->dropoffDate);
                        $params['time'] 		 = trim($request->dropoffTime);
                        $params['address']		 = $request->addressfull;
                        $params['storagestatus'] = 'Drop-off';
                        $params['itemtype'] 	 = $itemCode;
                        $params['storageid'] 	 = $code->_id;
                        $calendar = Calendars::createCalendar($params);
                    }

                    $ite =  $bin['type'] == Items::ItemTypeLarge ? $l
                            : ( $bin['type'] == Items::ItemTypeExtraLarge ? $e : $o );
                    $ite->total_rented += $bin['quantity'];
                    $ite->save();
                }

            }

			//Send mail
			$data = array(
				'title' 		  => 'Box Gofer: Bin Dropoff',
				'timestamp' 	  => $dropoffShow,
				'address' 		  => $request->addressfull,
				'name' 			  => $user->name,
				'phone' 		  => $request->phone,
				'formatDateEmail' => CommonHelper::formatDateForEmail($dropoff)
			);

            // GoferMail::sendMail($user->email, 'Box Gofer Order Confirmation: Bin Drop-Off Schedule', 'emails.timestamp', $data);
            $this->setCurrentChange($listItem);

            return response()->json(["transcode" => $transaction->transcode, 'message' => 'Transaction successfully completed.', 'request' => $request, 'status' => 'success']);
	    }

	    return response()->json(['message' => 'There is an error upon processing the order.', 'status' => '', 'redirect' => url('/zipcode')]);

    }

    /**
     * DDD
     * referral
     */
    public function referral() {
        $user = Auth::guard($this->guard)->user();
        $post = Input::all();
        if (!filter_var($post['email'], FILTER_VALIDATE_EMAIL))
            return response()->json(['message' => 'Invalid email'], 500);

        $data = array('email'=> $_SERVER['SERVER_NAME'] . '/zipcode?token=' . $user->_id, 'username' => $user->name);
        $subject = 'Your Friend ' . $user->name . ' has Signed Up with Box Gofer – Come Check Us Out';

        if(GoferMail::sendMail($post['email'], $subject, 'emails.referral', $data)){
            return ["message" => 'Success'];
        } else {
            return response()->json(['message' => 'Error'], 500);
        }

    }

    /**
     * DDD
     * getClientInfo
     */
    public function getClientInfo() {

        $post = Input::all();
        //return $post;
        if(!isset($post['id']))
            return response()->json(['message' => 'Invalid request'], 500);

        $lastPayment = "( Not Yet )";

        $users = Users::getUserFieldById($post['id']);
        $storage = Storage::getStorageByUserId($post['id']);
        $StorageInfo = array(
            'l'=>0,
            'e'=>0,
            'o'=>0
        );
        $itemReturned = array(
        		'l'=>0,
        		'e'=>0,
        		'o'=>0
        );

        foreach($storage as $us){
           $type = ($us->item->type == 1) ? 'l'
                 :(($us->item->type == 2) ? 'e' : (($us->item->type == 3) ? 'o' : ''));
           if($type !== '')
               $StorageInfo[$type] = $StorageInfo[$type] + 1;

           if ($us->status == 9) $itemReturned[$type] = $itemReturned[$type] + 1;
        }

        $users['createdat'] = date("M d.Y", strtotime($users->created_at));
        $lastPayment = $this->transaction->getLastpayment($post['id']);
        $amount = isset($lastPayment->paymentInfo['total']) ? $lastPayment->paymentInfo['total'] : 0;

        if($lastPayment !== null) {
        	$lastPayment = date("M d.Y", strtotime($lastPayment->updated_at));
        }

        $users['lastPayment'] = $lastPayment;
        $users['lastAmount'] = '$' . $amount;
        $users['returned'] = $itemReturned;

        //
        $monthAmount = 0;
        $trans = Transactions::getTransByUserIdAndCurrentMonth($post['id']);
        foreach ($trans as $foo) {
			if (isset($foo->paymentInfo['total'])) $monthAmount = $monthAmount + $foo->paymentInfo['total'];
        }
        $users['monthAmount'] = '$' . $monthAmount;

        //return $users;
        return [
            'info' => $users,
            'storage' => $StorageInfo
        ];
    }

    /**
     * DDD
     * saveClientInfo
     */
     public function saveClientInfo() {

         $post = Input::all();

         if(!isset($post['_id']))
            return response()->json(['message' => 'Invalid request'], 500);

         $users = Users::getUserById($post['_id']);
         if ($users == null)
            return response()->json(['message' => 'Invalid User'], 500);

         $place = $this->places->getPlaceByZipcode($post['zipcode']);

         if ($place == null) {
         	return response()->json(['message' => 'Invalid Zipcode'], 500);
         }

         if (isset($post['phone']) && $post['phone'] != '') {
         	$users->phone = $post['phone'];
         }

     	 if (isset($post['physicalAddress']) && $post['physicalAddress'] != '') {
         	$users->physicalAddress = $post['physicalAddress'];
         }

         if (isset($post['zipcode']) && $post['zipcode'] != '') {
         	$users->zipcode = $post['zipcode'];
         }

         if (isset($post['city']) && $post['city'] != '') {
         	$users->city = $post['city'];
         }

         if (isset($post['full']) && $post['full'] != '') {
         	$users->full = $post['full'];
         }

         if (isset($post['email']) && $post['email'] != '') {
         	$users->email = $post['email'];
         }

         if (isset($post['name']) && $post['name'] != '') {
         	$users->name = $post['name'];
         }

         $users->save();

         return 1;
     }

    /**
     * DDD - 20160217
     * getTransInfo
     */
    public function getTransInfo() {

        $post = Input::all();
        //return $post;
        if(!isset($post['transid']))
            return response()->json(['message' => 'Invalid request'], 500);

        $trans = $this->transaction->getById($post['transid']);
        $storage = Storage::getStorageByTransId($post['transid']);
        return [
            'info' => $trans,
            'bins' => $storage
        ] ;
    }

    /**
     * DDD - 20160217
     * uploadBinImage
     */
    public function uploadBinImage($binid = null){
    	if ($this->auth->user()->role == 'operator') {
    		response()->json(['message' => 'Permission denied'], 500);
    	}

        $image = Input::file('file');

        if($binid === null)
            return response()->json(['message' => 'The image is invalid'], 500);

        if (!$image)
            return response()->json(['message' => 'The image is invalid'], 500);

         // getting all of the post data
         $file = array('image' => $image);
         // setting up rules
         $rules = array('image' => 'mimes:jpeg,bmp,png', ); //mimes:jpeg,bmp,png and for max size max:10000
            // doing the validation, passing post data, rules and the messages
         $validator = Validator::make($file, $rules);
            // Validate rule
         if ($validator->fails())
            // send back to the page with the input data and errors
            return response()->json(['message' => 'Choose image'], 500);

         // Validate type
         if (!$image->isValid())
            return response()->json(['message' => 'The image is invalid'], 500);

         $storage = new Storage();
         $sto = $storage->getStorageById($binid) ;

         if($sto === null)
             return response()->json(['message' => 'Bin not valid'], 500);

         //$destinationPath = 'uploads'; // upload path
         $destinationPath = base_path().'/public/uploads';
         //$extension = $image->getClientOriginalExtension(); // getting image extension
         //$fileName = rand(11111,99999).'.'.$extension; // renameing image
         $fileName = date('Ymdhis');
         $fileName = $fileName.'_'.$image->getClientOriginalName();
         $image->move($destinationPath, $fileName); // uploading file to given path
         // sending back with message
         $a = $sto->images;
         array_push($a, $fileName);
         $sto->images = $a;
         $sto->save();

         return ['image' => $fileName];
    }

    /**
     * DDD - 20160218
     * upsertStaff
     */
    public function upsertStaff() {
    	if ($this->auth->user()->role != 'owner' && $this->auth->user()->role != 'manager') return redirect('admin');

        $post = Input::all();
        $action = $post['action'];

        $validator = $this->staffValidator($post, $action);
        $message = array();

        if ($validator->fails()) {
        	foreach ($validator->messages()->getMessages() as $mess) {
        		$message[] = $mess[0];
        	}
        }

        if (!$this->staff->IsNullOrEmptyString($post['birthday']))
        	if (!$this->staff->validateDate($post['birthday'], 'Y/m/d'))
        		$message[] = 'Birthday invalid format';

        if (!$this->staff->IsNullOrEmptyString($post['datehired']))
        	if (!$this->staff->validateDate($post['datehired'], 'Y/m/d'))
        		$message[] = 'Date hired invalid format';

        if (! empty($message)) {
        	return response()->json(['message' => $message], 500);
        }

        // Check address
        $addressFull = $post['address'] .', '. $post['city'] .', TX '. $post['zipcode'];

        if (! CommonHelper::checkAddress($addressFull)) {
        	$message[] = "We can't find your address";
        	return response()->json(['message' => $message], 500);
        }

        if ($action == 'create') {
        	$post['password'] = bcrypt($post['password']);
        } else {
        	unset($post['password']);
        }

        return $this->staff->upsert($post);
    }

    /**
     * DDD - 20160218
     * getStaffInfo
     */
    public function getStaffInfo() {
        $staff = Input::all();

        $retVal = $this->staff->getById($staff['staffid']);

        if($retVal === null)
           return response()
                    ->json(['message' => 'Staff not valid'], 500);

        return $retVal;
    }

    /**
     * DDD - 20160219
     * updateBin
     */
    public function updateBin() {
        $bin = Input::all();

        if(!is_numeric($bin['total_remain']) || $bin['total_remain'] < 0 )
            return response()->json([
                'message' => 'Total Inventory not valid',
                'field'=>'total'
                ], 500);

        if(!is_numeric($bin['price']))
            return response()->json([
                'message' => 'Price not valid',
                'field'=>'price'
                ], 500);

        $retVal = $this->items->getById($bin['binid']);

        if($retVal === null)
           return response()->json([
               'message' => 'Bin not valid',
               'field'=>'other'], 500);

		if ($retVal->type == 3) {
			$total_calculate = $retVal->total_rented + $bin['total_remain'];
		} else {
			$total_calculate = $retVal->total;
		}

        $total = intval($total_calculate);
        $oldTotal = $retVal->total;
        $retVal->total = $total;

        if($retVal->type == 3 && $total != $oldTotal) {

          	$change = isset($retVal->changes) ? $retVal->changes : array();
           	$change[] = [
           			'total' => $oldTotal,
           			'date' => intval((date("Y") .''. date("m"))),
           			'year' => date("Y"),
           			'month' => date("M"),
           			'current_total' => $total
           	];
           	$retVal->changes = $change;
        }

        $retVal->price = floatval($bin['price']);
        $retVal->description = addslashes($bin['description']);
        $retVal->save();

        return $retVal;

    }


    /**
     * DDD - 20160222
	 * upload_excel
	 */
    public function upload_excel(){
        $file = Input::file('excelfile');

        $reader = Excel::selectSheetsByIndex(0)->load($file)->get();

        $emails = $this->staff->getListEmail();
        $obj_e = array();
        $failStaff = array();
        foreach($emails as $e){
            $obj_e[$e[0]] = '' ;
        }

        foreach($reader->toArray() as $row){
            $valid = $this->staff->validExcelPost($row);
            if($valid !== true){
                $failStaff[] = array(
                    'email'=> $row['email'],
                    'error' => $valid
                ) ;
                continue;
            }

            if(isset($obj_e[$row['email']])){
                $failStaff[] = array(
                        'email'=> $row['email'],
                        'error' => 'Email has already exist'
                    ) ;
                continue;
            }

            $this->staff->upsert($row);
            $obj_e[$row['email']] = '' ;
        }

        return [
            'fail'=>$failStaff,
            'html'
        ];

        die($retVal);
    }

    /**
     * getChartData
     * For dashboard analytics
     */
    public function getChartData() {

    	$data['bin'] = $this->chartBinsInStorage();
    	$data['daily'] = $this->chartDailySchedule();
    	$data['zipcode'] = $this->chartZipcodeSchedule();

    	return $data;
    }

    /**
     * chartZipcodeSchedule
     * Get Zipcode data for analytics
     */
    private function chartZipcodeSchedule() {

    	/** Will get at storage zipcode **/
    	$result = array();

    	$axisX = ['75209', '75225', '75205', '75219', '75204', '75206', '75214', '75231', '75081',
    		'75080', '75075', '75248', '75001', '75244', '75252', '75287', '75093',
    		'75024', '75025', '75044', '75082', '75074', '75023'];
    	$axisY = ['Ordered - L', 'Ordered - XL', 'Ordered - Other'];

    	for($a = 0; $a < count($axisY); $a++){
            $data = array();
            for($i=0; $i < count($axisX); $i++){
               $data[] = array(
                   	'y' => Storage::totalByZipcodeAndType($axisX[$i], $a + 1),
                   	'label' => $axisX[$i],
              		'zc' => ' (' . $axisX[$i] . ')',
               );
            }
            $result[$axisY[$a]] = $data;
         }

    	return $result;

    }

    /**
     * chartBinsInStorage
     *
     * Analytics bin
	 */
     private function chartBinsInStorage(){

     	$half = date("M") < 7 ? 0 : 1;

         $returnData = array();

         $axisX = $half == 0
                ? [1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun']
                : [7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec'];

         $axisY = [ 1 => 'orderedL', 2 => 'orderedXL', 3 => 'orderedOther'];

         foreach ($axisY as $type => $vY) {

         	$ordered = array();
         	$unused = array();

         	foreach ($axisX as $month => $vX) {
         		$getData = Storage::totalProcessing($type, $month);
         		$ordered[] = array(
         			'y' => isset($getData['ordered']) ? $getData['ordered'] : 0,
         			'label' => $vX
         		);
         		if ($type != 3) {
         			$unused[] = array(
         				'y' => isset($getData['unused']) ? $getData['unused'] : 0,
         				'label' => $vX
         			);
         		}
         	}

         	$returnData[$vY] = $ordered;
         	if ($type != 3) {
         		$returnData[($type == 1) ? 'unusedL' : 'unusedXL'] = $unused;
         	}

         }

         /* for($a = 0;$a < count($axisY);$a++){
            $data = array();
            for($i=0;$i < count($axisX);$i++){
               $data[] = array(
                   'y'=>rand(0,500),
                   'label'=>$axisX[$i]
               );
            }
            $dummyData[$axisY[$a]] = $data;
         } */

         return $returnData;
     }

    /**
     * chartDailySchedule
     * Analytics daily
	 */
    private function chartDailySchedule() {

         $result = array();
         $h = new History();

         $getData = History::getHistoryByCurrentDay();

         if (!$getData) return $result;

         $axisY = ['L', 'XL', 'Other'];

         foreach ($axisY as $foo) {

         	if (isset($getData->$foo)) {

         		$data = array();

         		foreach ($getData->$foo as $key => $val) {
         			$data[] = array(
         				'y' => $val,
         				'label' => $key
         			);
         		}

         		$result[$foo] = $data;
         	}

         }

         return $result;
    }

     /**
      * getInvoiceInfo
      */
     public function getInvoiceInfo() {
     	$post = Input::all();
     	if (isset($post['id'])) {
			$bginvoice = new Bginvoices();
			$obj = $bginvoice->getInvoiceById($post['id']);
			if ($obj) {
				return $obj;
			}
     	}

     	return ['error' => 'Error to get invoice infomation'];
     }

     /**
      * getStorageInfo
      *
      */
     public function getStorageInfo() {
     	$post = Input::all();
     	if (isset($post['id'])) {
     		$storage = new Storage();
     		$obj = $storage->getCustomStorageById($post['id']);
     		if ($obj) {
     			return $obj;
     		}
     	}

     	return ['error' => 'Error to get storage infomation'];
     }

    /**
     * postbackApi
	 * Manh add for barcode API - 20160303
	 */
	public function postbackApi() {

		$result = $this->processScanBins();

		header("Content-type: text/xml");
		echo '<?xml version="1.0" encoding="UTF-8"?>';
		echo '<xml>';
		echo '    <message>';
		echo '        <status>' . $result['status'] . '</status>';
		echo '        <text>' . $result['mess'] . '</text>';
		echo '    </message>';
		echo '</xml>';
	}

	/**
	 * postbackOtherStuffApi
	 * Chuong add for barcode API - 20160516
	 */
	public function postbackOtherStuffApi() {
		$result = $this->processOtherStuffScan();

		header("Content-type: text/xml");
		echo '<?xml version="1.0" encoding="UTF-8"?>';
		echo '<xml>';
		echo '    <message>';
		echo '        <status>' . $result['status'] . '</status>';
		echo '        <text>' . $result['mess'] . '</text>';
		echo '    </message>';
		echo '</xml>';
	}

	/*
	 * adminChangeWorkingStatus
	 */
	public function adminChangeWorkingStatus() {

		$post = Input::all();

		if ($post['item_type'] == 3) {
			return $this->processOtherStuffScan();
		}

		return $this->processScanBins();
	}

	/*
	 * processScanBins
	 */
	private function processScanBins() {

    	$post = Input::all();
    	$status = 0;
    	$mess = 'The barcode is invalid';

    	if (isset($post['tid']) && isset($post['answers'])) {
    		$obj = new Storage();
    		$storage = $obj->getStorageByBarcode($post['tid']);
    		//Get answer
    		$answer = array_values($post['answers']);

    		if($storage && $storage->item->type != 3) {

    			$continue = false;

    			switch ($storage->status) {
    				case 0:
    					if ($answer[0] == 'IT1 - In Transit')
    					{
    						$continue = true;
    						$storage->intransit = 1;
    					}
    					else if ($answer[0] == 'AD1 - At Doorstep')
    					{
    						History::upsertHistory($storage->item->type, 'DropoffEmpty', 1);
    						// Update transactions
    						//Transactions::updateTransactionsAD1($storage->transactions->_id, $storage->item->type);
    						// Use for check charge and find in CronController
    						$storage->isCharged = 2;
    						$storage->status = 1;
    						$continue = true;
    						$emailTemplate = 'ad1';
    						$subject = 'Box Gofer: Bins at your Doorstep!';
    						// Reset
    						$storage->intransit = 0;
    						// Update transactions trial_end
    						if (empty($storage->transactions->trial_end)) {
    							$setingday = Config::get('services.paymentday');
    							$storage->transactions->trial_end = strtotime('+' . $setingday['trailend'] . ' days');
    							$storage->transactions->save();
    						}
    					}
    					break;

    				case 1:
    					if ($answer[0] == 'IT2 - In Transit' && $storage->intransit != 2)
    					{
    						$continue = true;
    						$storage->intransit = 2;
    						// Update transactions
    						Transactions::updateScannedTransactions($storage->transactions->_id, $storage->item->type, $storage->isCharged);
    						// Charged
    						$storage->isCharged = 1;
    						//scannedcron
    						//$stripe = new StripeFuns();
    						//$ex = $stripe->excuteSubscription($storage->transactions->paymentInfo, $storage->item->type);
    					}
    					else if ($answer[0] == 'IS - In Storage' && $storage->intransit == 2)
    					{
    						History::upsertHistory($storage->item->type, 'PickupFull', 1);
    						$storage->status = 2;
    						$continue = true;
    						$emailTemplate = 'is';
    						$subject = 'Box Gofer: Bins Have Arrived Safely in Storage!';
    						// Reset
    						$storage->intransit = 0;
    					}
    					break;

    				case 2:
    					// This step for user change to status = 3
    					break;

    				case 3:
    					// Allow cancel (scan to IT4) after delivery
    					if ($storage->isCancel == 1) {
    						$mess = 'This bin is processing for Empty.';
    					} else if ($answer[0] == 'IT4 - In Transit' && $storage->intransit == 3) {
    						$date = date('m/d/Y - H:00 a');//Ex: 04/21/2016 - 9:00 am
    						$storage->intransit = 4;
    						// Flag cancel
    						$storage->isCancel = 1;
    						// Process time here
    						$storage->processtime = $date;
    						$storage->canceltime = $date;
    						$storage->searchdate = CommonHelper::processDate($date);
    						$continue = true;
    						$emailTemplate = 'it4';
    						$subject = 'Box Gofer: Empty Bins Received!';
    						History::upsertHistory($storage->item->type, 'PickupEmpty', 1);
    					}
    					else if ($answer[0] == 'IT3 - In Transit' && $storage->intransit != 3) {
    						$continue = true;
    						$emailTemplate = 'it3';
    						$subject = 'Box Gofer: Return Bins Request';
    						$storage->intransit = 3;
    					}
    					else  if ($answer[0] == 'AD2 - At Doorstep' && $storage->intransit == 3) {
    						History::upsertHistory($storage->item->type, 'DropoffFull', 1);
    						$storage->status = 4;
    						$continue = true;
    						// Reset
    						$storage->intransit = 0;
    					}
    					break;

    				case 4:
    					// Cancel bin
    					if ($storage->isCancel == 1) {
    						$mess = 'This bin is processing for cancel.';
    					} else if ($answer[0] == 'IT4 - In Transit' && $storage->intransit != 4) {
    						$date = date('m/d/Y - H:00 a');//Ex: 04/21/2016 - 9:00 am
    						//$result = $this->it4Cancel($storage->_id);
    						//if(!isset($result['error'])) {
    							$storage->intransit = 4;
	    						// Flag cancel
	    						$storage->isCancel = 1;
	    						// Process time here
	    						$storage->processtime = $date;
	    						$storage->canceltime = $date;
	    						$storage->searchdate = CommonHelper::processDate($date);
	    						$continue = true;
	    						$emailTemplate = 'it4';
	    						$subject = 'Box Gofer: Empty Bins Received!';
	    						History::upsertHistory($storage->item->type, 'PickupEmpty', 1);
    						//}
    					}
    					else if ($answer[0] == 'IT2 - In Transit')
    					{
    						$continue = true;
    						$storage->intransit = 2;
    					}
    					else if ($answer[0] == 'IS - In Storage')
    					{
    						History::upsertHistory($storage->item->type, 'PickupFull', 1);
    						$storage->status = 2;
    						$continue = true;
    						$emailTemplate = 'is';
    						$subject = 'Box Gofer: Bins Have Arrived Safely in Storage!';
    						// Reset
    						$storage->intransit = 0;
    					}

    					break;

    				case 5:// This is empty bin
    					/* if ($answer[0] == 'Empty - Inventory') {
    						// Process time here
    						$storage->processtime = $storage->canceltime;
    						$storage->searchdate = CommonHelper::processDate($storage->canceltime);
    						$storage->status = 9;
    						$storage->item->total_rented --;
    						$storage->item->save();
    						$continue = true;
    						// Reset
    						$storage->intransit = 0;
    					} */
    					break;

    				case 9:
    					if ($answer[0] == 'Empty - Inventory') {
    						$continue = true;
    						$storage->intransit = 0;
    					}
    					break;
    			}

    			if ($continue) {
    				$storage->workingstatus = $answer[0];

    				if ($storage->save()) {
    					$dateformail = date('Ymd');

    					if ($storage->transactions->sentmaildate != $dateformail) {
    						$storage->transactions->sentmailtemplate = [];
    					}

    					if (isset($emailTemplate) && (! in_array($emailTemplate, $storage->transactions->sentmailtemplate)) ) {
    						// Update
    						$storage->transactions->sentmaildate = $dateformail;
    						$sentMailTemplate = isset($storage->transactions->sentmailtemplate) ? $storage->transactions->sentmailtemplate : array();
    						$sentMailTemplate[] = $emailTemplate;
    						$storage->transactions->sentmailtemplate = $sentMailTemplate;
    						$storage->transactions->save();

    						$emailSubject = isset($subject) ? $subject : 'Boxgofer - Your bin is processing (' . $answer[0] . ') ';
    						$emailSubject .= ' - Order: '. $storage->transactions->transcode;
    						$data = array(
    								'binstatus'=> $answer[0],
    								'binmessage' => '[Message to customer here]',
    								'storage' => $storage,
    								'formatDateEmail' => ($emailTemplate == 'it4') ? '' : CommonHelper::formatDateForEmail($storage->processtime),
    								'subject' => $emailSubject
    						);

    						GoferMail::sendMail($storage->relationUser->email, $emailSubject, 'emails.' . $emailTemplate, $data);
    					}

    					return [
    							'status' => 1,
    							'mess' => 'The barcode is valid',
    					];

    				}
    				else
    				{
    					$mess = 'Got an error save the answer please try again.';
    				}

    			}/*
    			else
    			{
    				$mess = 'The answer is wrong.';
    			} */

    		} elseif ($storage->item->type == 3) {
    			$mess = 'Only work for Large and Extra Large.';
    		}
    	}

		return [
    			'status' => $status,
    			'mess' => $mess,
    	];
    }

    /**
     *
     */
    private function processOtherStuffScan() {
    	$post = Input::all();
    	$status = 0;
    	$mess = 'The barcode is invalid';

    	if (isset($post['tid']) && isset($post['answers'])) {
    		$obj = new Storage();
    		$storage = $obj->getStorageByBarcode($post['tid']);
    		//Get answer
    		$answer = array_values($post['answers']);

    		if($storage && $storage->item->type == 3) {
    			$continue = false;
    			//$emailTemplate = 'barcodemail';

    			switch ($storage->status) {
    				case 0:
    					if ($answer[0] == 'IT2 - In Transit' && $storage->intransit != 2)
    					{
    						$continue = true;
    						$storage->intransit = 2;

    						// Update transactions
    						Transactions::updateScannedTransactions($storage->transactions->_id, $storage->item->type);
    						//$stripe = new StripeFuns();
    						//$ex = $stripe->excuteSubscription($storage->transactions->paymentInfo, $storage->item->type);
    						// Update transactions trial_end
    						if (empty($storage->transactions->trial_end)) {
    							$setingday = Config::get('services.paymentday');
    							$storage->transactions->trial_end = strtotime('+' . $setingday['trailend'] . ' days');
    							$storage->transactions->save();
    						}
    					}
    					else if ($answer[0] == 'IS - In Storage' && $storage->intransit == 2)
    					{
    						History::upsertHistory($storage->item->type, 'PickupFull', 1);
    						$storage->status = 2;
    						$continue = true;
    						$emailTemplate = 'is';
    						$subject = 'Box Gofer: Bins Have Arrived Safely in Storage!';
    						// Reset
    						$storage->intransit = 0;
    					}
    					break;

    				case 3:
    					// Cancel bin
    					if ($storage->isCancel == 1)
    					{
    						$mess = 'This bin is processing for Empty.';
    					}
    					else if ($answer[0] == 'IT4 - In Transit' && $storage->intransit == 3)
    					{
    						//$result = $this->it4Cancel($storage->_id);
    						$date = date('m/d/Y - H:00 a');//Ex: 04/21/2016 - 9:00 am
    						$storage->intransit = 4;
    						// Flag cancel
    						$storage->isCancel = 1;
    						//if(!isset($result['error'])) {
    						// Process time here
    						$storage->processtime = $date;
    						$storage->canceltime = $date;
    						$storage->searchdate = CommonHelper::processDate($date);
    						//$storage->item->total_rented --;
    						//$storage->item->save();
    						$continue = true;
    						$emailTemplate = 'it4';
    						$subject = 'Box Gofer: Empty Bins Received!';
    						//$storage->intransit = 4;
    						History::upsertHistory($storage->item->type, 'PickupEmpty', 1);
    						//}
    					} else if ($answer[0] == 'IT3 - In Transit' && $storage->intransit != 3) {
    						$continue = true;
    						$emailTemplate = 'it3';
    						$subject = 'Box Gofer: Return Bins Request';
    						$storage->intransit = 3;
    					}
    					else  if ($answer[0] == 'AD2 - At Doorstep' && $storage->intransit == 3) {
    						History::upsertHistory($storage->item->type, 'DropoffFull', 1);
    						$storage->status = 4;
    						$continue = true;
    						$emailTemplate = 'it4';
    						$subject = 'Box Gofer: Empty Bins Received!';
    						// Reset
    						$storage->intransit = 0;
    					}
    					break;

    				case 4:
    					// Cancel bin
    					if ($storage->isCancel == 1)
    					{
    						$mess = 'This bin is processing for Empty.';
    					}
    					else if ($answer[0] == 'IT4 - In Transit' && $storage->intransit != 4)
    					{
    						//$result = $this->it4Cancel($storage->_id);
    						$date = date('m/d/Y - H:00 a');//Ex: 04/21/2016 - 9:00 am
    						$storage->intransit = 4;
    						// Flag cancel
    						$storage->isCancel = 1;
    						//if(!isset($result['error'])) {
    							// Process time here
    							$storage->processtime = $date;
    							$storage->canceltime = $date;
    							$storage->searchdate = CommonHelper::processDate($date);
    							//$storage->item->total_rented --;
    							//$storage->item->save();
    							$continue = true;
    							$emailTemplate = 'it4';
    							$subject = 'Box Gofer: Empty Bins Received!';
    							//$storage->intransit = 4;
    							History::upsertHistory($storage->item->type, 'PickupEmpty', 1);
    						//}
    					}
    					else if ($answer[0] == 'IT2 - In Transit')
    					{
    						$continue = true;
    						$storage->intransit = 2;
    					}
    					else if ($answer[0] == 'IS - In Storage')
    					{
    						History::upsertHistory($storage->item->type, 'PickupFull', 1);
    						$storage->status = 2;
    						$continue = true;
    						$emailTemplate = 'is';
    						$subject = 'Box Gofer: Bins Have Arrived Safely in Storage!';
    						// Reset
    						$storage->intransit = 0;
    					}
    					break;

    				case 5:
    					/* if ($answer[0] == 'IT4 - In Transit') {
    						// Process time here
    						$storage->processtime = $storage->canceltime;
    						$storage->searchdate = CommonHelper::processDate($storage->canceltime);
    						$continue = true;
    						$storage->intransit = 4;
    						History::upsertHistory($storage->item->type, 'PickupEmpty', 1);
    					} */
    					break;

    				case 9:
    					if ($answer[0] == 'Empty - Inventory') {
    						$continue = true;
    						$storage->intransit = 0;
    					}
    					break;
    			}

    			if ($continue) {

    				$storage->workingstatus = $answer[0];

    				if ($storage->save()) {

    					$dateformail = date('Ymd');

    					if ($storage->transactions->sentmaildate != $dateformail) {
    						$storage->transactions->sentmailtemplate = [];
    					}

    					if (isset($emailTemplate) && (! in_array($emailTemplate, $storage->transactions->sentmailtemplate)) ) {

    						// Update
    						$storage->transactions->sentmaildate = $dateformail;
    						$sentMailTemplate = isset($storage->transactions->sentmailtemplate) ? $storage->transactions->sentmailtemplate : array();
    						$sentMailTemplate[] = $emailTemplate;
    						$storage->transactions->sentmailtemplate = $sentMailTemplate;
    						$storage->transactions->save();

    						$emailSubject = isset($subject) ? $subject : 'Boxgofer - Your bin is processing (' . $answer[0] . ') ';

    						$data = array(
    								'binstatus'=> $answer[0],
    								'binmessage' => '[Message to customer here]',
    								'storage' => $storage,
    								'formatDateEmail' => CommonHelper::formatDateForEmail($storage->processtime),
    								'subject' => $emailSubject
    						);

    						GoferMail::sendMail($storage->relationUser->email, $emailSubject, 'emails.' . $emailTemplate, $data);
    					}

    					$status = 1;
    					$mess = 'The barcode is valid';

    				}
    				else
    				{
    					$mess = 'Got an error save the answer please try again.';
    				}

    			}
    			/* else
    			{
    				$mess = 'The answer is wrong.';
    			} */
    		} elseif ($storage->item->type != 3) {
    			$mess = 'Only work for Other Stuff.';
    		}
    	}

    	return [
    			'status' => $status,
    			'mess' => $mess,
    	];
    }

    /**
     * postbackWarehouseApi
     * Chuong add for Warehouse location API - 20160517
     */
    public function postbackWarehouseApi() {
    	$post = Input::all();
    	$status = 0;
    	$mess = 'The barcode is invalid';

    	header("Content-type: text/xml");
    	echo '<?xml version="1.0" encoding="UTF-8"?>';
    	echo '<xml>';
    	echo '    <message>';
    	echo '        <status>' . $status . '</status>';
    	echo '        <text>' . $mess . '</text>';
    	echo '    </message>';
    	echo '</xml>';
    }

    /**
     * changeStatusClient
     */
    public function changeStatusClient() {
    	$post = Input::all();
    	if (isset($post['id'])) {
    		$user = Users::getUserById($post['id']);
    		if ($user) {
    			$user->status = ($user->status == 1) ? 0 : 1;
    			$user->save();

    			return 1;
    		}
    	}

    	return ['error' => 'Error request'];
    }

    /**
     * stripeCallback
     * This is a webhook callback proccessing
     */
	public function stripeCallback() {

		$stripe = new StripeFuns();
		$post = Input::all();

		if (isset($post['type']) && $post['type'] == "invoice.payment_succeeded")
		{
			if ($post['data']['object']['total'] == 0) {
				return http_response_code(201);
			}

			if (isset($post['data']['object']['charge']) && $post['data']['object']['charge'] == null) {
    			return http_response_code(201);
    		}
    		$k = 0;
    		foreach($post['data']['object']['lines']['data'] as $val)
    		{
    			if (!empty($val['metadata'])) break;
    			$k ++;
    		}

			$email = $post['data']['object']['lines']['data'][$k]['metadata']['email'];
			$phone = isset($post['data']['object']['lines']['data'][$k]['metadata']['phone']) ? $post['data']['object']['lines']['data'][$k]['metadata']['phone'] : '';
			$quantity = $post['data']['object']['lines']['data'][$k]['quantity'];
			// Sending your customers the amount in pennies is weird, so convert to dollars
			//$amountOneItem = $post['data']['object']['lines']['data'][$k]['plan']['amount'];
			$amountOneItem = $post['data']['object']['total'];
			$amountOneItemFormat = sprintf('$%0.2f', $amountOneItem / 100.0);
			$amount = $amountOneItem * $quantity;
			$amountFormat = sprintf('$%0.2f', $amount / 100.0);
			// Get items
			$listitems = isset($post['data']['object']['lines']['data'][$k]['metadata']['listitems']) ? $post['data']['object']['lines']['data'][$k]['metadata']['listitems'] : '';
			$customerId = $post['data']['object']['customer'];
			// Discount
			$discount = !empty($post['data']['object']['discount']) ? $post['data']['object']['discount']['coupon']['percent_off'] : 0;
			$discountKey = !empty($post['data']['object']['discount']) ? $post['data']['object']['discount']['coupon']['id'] : '';

			// Create invoice in DB
			$customerRetrieve = $stripe->customerRetrieve($customerId);
			$params['last4'] = $customerRetrieve['sources']['data'][$k]['last4'];
			$params['user_id'] = isset($post['data']['object']['lines']['data'][$k]['metadata']['userid']) ? $post['data']['object']['lines']['data'][$k]['metadata']['userid'] : '';
			$params['clientName'] = isset($post['data']['object']['lines']['data'][$k]['metadata']['username']) ? $post['data']['object']['lines']['data'][$k]['metadata']['username'] : '';
			$params['clientEmail'] = $email;
			$params['clientAddress'] = isset($post['data']['object']['lines']['data'][$k]['metadata']['address']) ? $post['data']['object']['lines']['data'][$k]['metadata']['address'] : '';
			$params['item'] = isset($post['data']['object']['lines']['data'][$k]['metadata']['item']) ? $post['data']['object']['lines']['data'][$k]['metadata']['item'] : '';
			$params['payby'] = 'Stripe';
			$params['dayspay'] = date("F j, Y", $post['data']['object']['date']);
			$params['currency'] = $post['data']['object']['currency'];
			$params['amount'] = $amountFormat;
			$params['invoice_num'] = '1101' . CommonHelper::randomGen(5);
			$params['invoice_id'] = $post['data']['object']['id'];
			$params['phone'] = $phone;
			$params['listitems'] = $listitems;
			$params['addressstreet'] = isset($post['data']['object']['lines']['data'][$k]['metadata']['addressstreet']) ? $post['data']['object']['lines']['data'][$k]['metadata']['addressstreet'] : '';
			$params['cityzipcode'] = isset($post['data']['object']['lines']['data'][$k]['metadata']['cityzipcode']) ? $post['data']['object']['lines']['data'][$k]['metadata']['cityzipcode'] : '';
			$params['aptsuite'] = isset($post['data']['object']['lines']['data'][$k]['metadata']['aptsuite']) ? $post['data']['object']['lines']['data'][$k]['metadata']['aptsuite'] : '';
			$params['card_number'] = "**** **** **** ". $customerRetrieve['sources']['data'][$k]['last4'];
			$params['discount'] = $discount;
			$params['discount_key'] = $discountKey;
			$params['org_amount'] = $amount;

			//Send mail
			$data = array(
					'dayStart' => $params['dayspay'],
					'period' => date("F j, Y", $post['data']['object']['period_start']) . ' to ' . date("F j.Y", $post['data']['object']['period_end']),
					'customer' => $params['clientName'],
					'payBy' => 'Stripe',
					'item' => $params['item'],
					'billingAdd' => $params['clientAddress'],
					'amount' => $amountFormat,
					'org_amount' => $params['org_amount'],
					'invoiceId' => $params['invoice_id'],
					'invoice_num' => $params['invoice_num'],
					'phone' => $phone,
					'quantity' => $quantity,
					'totalAmount' => $amountFormat,
					'customerId' => $customerId,
					'amountOneItemFormat' => $amountOneItemFormat,
					'addressstreet' => $params['addressstreet'],
					'cityzipcode' => $params['cityzipcode'],
					'aptsuite' => $params['aptsuite'],
					'card_number' => $params['card_number'],
					'userid' => $params['user_id'],
					'orderId' => isset($post['data']['object']['lines']['data'][$k]['metadata']['orderId']) ? $post['data']['object']['lines']['data'][$k]['metadata']['orderId'] : '',
					'email' => $email,
					'listitems' => $listitems,
					'discount' => $discount,
					'discount_key' => $discountKey
			);

			$storeInvoice = new Mailinvoices();
			$storeInvoice->createMailinvoice( $data, $params);

			// Update total order of user
			Users::updateUserTotal($params['user_id'], $params['org_amount']);

			return http_response_code(201);

		} //Charge first subcription
		else if (isset($post['type']) && $post['type'] == "charge.succeeded")
		{
			if (empty($post['data']['object']['metadata'])) return http_response_code(201);

			$email = $post['data']['object']['metadata']['email'];
			$amount = $post['data']['object']['amount'];
			$chargetype = $post['data']['object']['metadata']['chargetype'];

			// Get customer retrieve
			$params['last4'] = $post['data']['object']['source']['last4'];
			$params['user_id'] = isset($post['data']['object']['metadata']['userid']) ? $post['data']['object']['metadata']['userid'] : '';
			$params['clientName'] = isset($post['data']['object']['metadata']['customername']) ? $post['data']['object']['metadata']['customername'] : '';
			$params['clientEmail'] = $email;
			$params['clientAddress'] = isset($post['data']['object']['metadata']['address']) ? $post['data']['object']['metadata']['address'] : '';
			$params['payby'] = 'Stripe';
			$params['dayspay'] = date("F j, Y", strtotime('now'));
			$params['currency'] = $post['data']['object']['currency'];
			$params['amount'] = sprintf('$%0.2f', $amount / 100.0);
			$params['invoice_num'] = '1101' . CommonHelper::randomGen(5);
			$params['invoice_id'] = !empty($post['data']['object']['invoice']) ? $post['data']['object']['invoice'] : $post['data']['object']['id'];
			$params['phone'] = $post['data']['object']['metadata']['phone'];
			$params['itemType'] = ($chargetype == 'deliver') ? 'deliver' : 'purchase';
			$params['quantity'] = 1;
			$params['org_amount'] = $amount;
			$params['description'] = $post['data']['object']['description'];
			$params['addressstreet'] = isset($post['data']['object']['metadata']['addressstreet']) ? $post['data']['object']['metadata']['addressstreet'] : '';
			$params['cityzipcode'] = isset($post['data']['object']['metadata']['cityzipcode']) ? $post['data']['object']['metadata']['cityzipcode'] : '';
			$params['aptsuite'] = isset($post['data']['object']['metadata']['aptsuite']) ? $post['data']['object']['metadata']['aptsuite'] : '';
			$params['card_number'] = "**** **** **** " . $params['last4'];
			$params['item'] = isset($post['data']['object']['metadata']['item']) ? $post['data']['object']['metadata']['item'] : '';

			//Send mail
			$data = array(
					'dayStart' => date("F j, Y", strtotime('now')),
					'customer' => $params['clientName'],
					'payBy' => 'Stripe',
					'billingAdd' => $params['clientAddress'],
					'amount' => sprintf('$%0.2f', $amount / 100.0),
					'org_amount' => $amount,
					'invoiceId' => $params['invoice_id'],
					'invoice_num' => $params['invoice_num'],
					'phone' => $params['phone'],
					'itemType' => $params['itemType'],
					'quantity' => 1,
					'totalAmount' => $params['amount'],
					'customerId' => $post['data']['object']['customer'],
					'description' => $params['description'],
					'addressstreet' => $params['addressstreet'],
					'cityzipcode' => $params['cityzipcode'],
					'aptsuite' => $params['aptsuite'],
					'card_number' => $params['card_number'],
					'userid' => $params['user_id'],
					'orderId' => isset($post['data']['object']['metadata']['orderId']) ? $post['data']['object']['metadata']['orderId'] : '',
					'email'	=> $email,
					'item' => $params['item']
			);

			if ($chargetype != 'deliver') {
				$params['amountNoTax'] = $data['amountNoTax'] = sprintf('$%0.2f', $post['data']['object']['metadata']['amountNoTax']);
				$params['tax'] = $data['tax'] = sprintf('$%0.2f', $post['data']['object']['metadata']['tax']);
				$params['totalAmount'] = $data['totalAmount'] = $params['amount'];
			}

			$storeInvoice = new Mailinvoices();
			$storeInvoice->createMailinvoice($data, $params);

			// Update total order of user
			Users::updateUserTotal($params['user_id'], $amount);

			return http_response_code(201);

		}

		return http_response_code(503);
	}

	/**
	 * checkEmail
	 */
	public function checkEmail() {

		$post = Input::all();

		if (isset($post['email'])) {

			$result = Users::isExistedEmail($post['email']);

			return ($result) ? ['error' => 'This email is existed.'] : 1;
		}

		return ['error' => 'Error request'];
	}

	/**
	 * chargeCustomer
	 */
	public function chargeCustomer() {
		if ($this->auth->user()->role == 'operator' || $this->auth->user()->role == 'driver' ) {
			return ['error' => 'Permission denied'];
		}

		$post = Input::all();

		if (isset($post['paymentkey'])
					&& isset($post['amount'])
						&& isset($post['des'])
							&& isset($post['storageid'])) {

			try {

				$obj = new Storage();
				$storage = $obj::find($post['storageid']);

				$metaData = [
						'phone' => $storage->phone,
						'address' => $storage->address,
						'customername' => $storage->customername,
						'email' => $storage->relationUser->email,
						'chargetype' => 'charge customer',
						'addressstreet' => $storage->addressstreet,
						'cityzipcode' => $storage->cityzipcode,
						'aptsuite' => $storage->aptsuite,
						'userid' => $storage->relationUser->userid,
						'orderId' => $storage->transactions->transcode,
				];

				return $this->stripeCharge($post['paymentkey'], $post['amount'], $post['des'], $metaData);

			} catch (Exception $e) {
				return ['error' => 'Error charge this customer'];
			}

		}

		return ['error' => 'Error request'];
	}

	/**
	 * stripeCharge
	 * Processing charge
	 *
	 * @param string $cuskey
	 * @param integer $amount
	 * @param string $des
	 *
	 * @return mixed
	 */
	private function stripeCharge($cuskey, $amount, $des, $metaData = NULL) {

		try {
			if (isset($metaData['chargetype']) && $metaData['chargetype'] != 'deliver') {
				$tax = 0.0825 * $amount;
				$metaData['tax_percent'] = '8.25%';
				$metaData['tax'] = $tax;
				$metaData['amountNoTax'] = $amount;

				$amount += $tax;
				$amount = number_format((float)$amount, 2, '.', '');
				$metaData['totalAmount'] = $amount;
			}

			$stripe = new StripeFuns();
			$result = $stripe->createCharge($cuskey, $amount, $des, $metaData);

			return 1;

		} catch (Exception $e) {
			return ['error' => 'Error charge this customer'];
		}

		return ['error' => 'Error charge this customer'];
	}

	public function addBinTrans() {
		$post = Input::all();
		$date = isset($post['date']) ? $post['date'] : new Date();
		$addNum = isset($post['add_num']) ? (int) $post['add_num'] : 0;
		$removeNum = isset($post['remove_num']) ? (int) $post['remove_num'] : 0;
		$item = $this->items->getItemByType($post['type']);
		$oldTotal = $item->total;

		if ($addNum > 0) {
			$item->total = $item->total + $addNum;
		}

		if ($removeNum > 0) {
			$item->total = $item->total - $removeNum;
		}


        $change = isset($item->changes) ? $item->changes : array();
        $change[] = [
            'total' => $oldTotal,
        	'current_total' => $item->total,
            'date' => intval((date("Y") .''. date("m"))),
            'year' => date("Y"),
            'month' => date("M"),
            'binDate' => date('m/d/Y', strtotime($date)),
            'binAdd' => $addNum,
            'binRemove' => $removeNum,
            'binType' => $post['type'],
            'binDesc' => $post['desc']
           ];
        $item->changes = $change;

        $item->save();

		return $item;
	}

	public function getChanges() {
		$changes = $this->items->getChanges();
		$result = null;
		foreach ($changes as $foo) {
			foreach ($foo->changes as $val) {
				$result[] = $val;
			}
		}

		return $result;
	}

	/**
	 * deleteStaff: delete staff by id, Role access: owner or manager
	 *
	 * @author Chuong
	 */
	public function deleteStaff() {
		$response = array(
				'message' => '',
				'status' => 0
		);
		$post = Input::all();

		if ($this->auth->user()->role != 'owner' && $this->auth->user()->role != 'manager' ) {
			$response['message'] = 'Permission denied.';
		} elseif (isset($post['id']) && ! empty($post['id'])){
			$staff = $this->staff->getById($post['id']);

			if (! empty($staff)) {
				$staff->delete();
				$response['status'] = 1;
			} else {
				$response['message'] = 'Id not match.';
			}
		} else {
			$response['message'] = 'Empty id.';
		}

		return $response;
	}

	/**
	 * editWarehouse: add or edit warehouse location for storage
	 *
	 * @author Chuong
	 */
	public function editWarehouse() {
		$post = Input::all();
		$validator = $this->warehouseValidator($post);

		if ($validator->fails()) {
			$message = array();
			foreach ($validator->messages()->getMessages() as $mess) {
				$message[] = $mess[0];
			}
			return response()->json(['message' => $message], 500);
		}

		// Upsert city code and warehouse code in Warehouse table
		$warehouse = Warehouses::first();
		$saveWarehouse = false;

		if (! in_array($post['warehouse_city'], $warehouse->city_code)) {
			$items = $warehouse->city_code;
			$items[] = $post['warehouse_city'];
			$warehouse->city_code = $items;
			$saveWarehouse = true;
		}

		if (! in_array($post['warehouse_code'], $warehouse->warehouse_code)) {
			$items = $warehouse->warehouse_code;
			$items[] = $post['warehouse_code'];
			$warehouse->warehouse_code = $items;
			$saveWarehouse = true;
		}

		if ($saveWarehouse) {
			$warehouse->save();
		}

		// Update warehouse info to storage
		$storage = new Storage();
     	$obj = $storage::find($post['_id']);
     	$obj->warehouse_city = $post['warehouse_city'];
     	$obj->warehouse_code = $post['warehouse_code'];
     	$obj->warehouse_aisle = $post['warehouse_aisle'];
     	$obj->warehouse_section = $post['warehouse_section'];
     	$obj->warehouse_shelf = $post['warehouse_shelf'];
     	$obj->warehouse_barcode = $post['warehouse_city'] .'-'. $post['warehouse_code'] .'-'.
     							  $post['warehouse_aisle'] .'-'. $post['warehouse_section'] .'-'.
     							  $post['warehouse_shelf'];
     	$obj->save();

		return $warehouse;
	}

	/* getCalendarByDate
	 * author: Chuong
	 * */
	public function getCalendarByDate() {
		$post = Input::all();
		$result = Calendars::getDataByDate($post['date']);
		return $result;
	}

	/*
	 * changeBinDateTime: change Drop-off or Pick-up date time of storage and calendar
	 */
	public function changeBinDateTime() {
		$post = Input::all();

		$result =  Storage::updateDateTime($post);

		if ($result['success'] == 1) {
			GoferMail::sendMail($result['email'], 'Box Gofer: Schedule Change - Order: '. $result['transcode'], 'emails.adminchangeschedule', $result['email_data']);
		}

		return $result;
	}

	/**
	 * setCurrentChange
	 *
	 * @author Chuong
	 */
	private function setCurrentChange($items) {
		foreach ($items as $item) {
			$changes = isset($item->changes) ? $item->changes : array();
			$date = new DateTime();

			foreach ($changes as $index => $change) {
				if ($change['date'] == $date->format('Ym')) {
					$changes[$index]['current_total'] = $item->total;
				}
			}

			$item->changes = $changes;
			$item->save();
		}
	}

	/**
	 * staffValidator
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	private function staffValidator(array $data, $action)
	{
		$messages = [
			'email.unique' => 'E-mail address is already exist.',
			'name.required' => 'The last name field is required.'
		];

		$emailRequired = $action == 'create' ? '|unique:staff' : '';

		$rule = [
			'first_name' => 'required|max:255',
			'name' => 'required|max:255',
			'email' => 'required|email|max:255'. $emailRequired,
			'role' => 'required|max:50',
			'salary' => 'required|numeric',
			'address' => 'required|max:255',
			'city' => 'required|max:255',
			'zipcode' => 'required|max:255',
		];

		if($action == 'create') {
			$rule['password'] = 'required|min:6';
		}

		return Validator::make($data, $rule, $messages);
	}

	/**
	 * warehouseValidator
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	private function warehouseValidator(array $data)
	{
		$messages = [
			'warehouse_city.required' => 'The city code field is required.',
			'warehouse_code.required' => 'The warehouse code field is required.',
			'warehouse_aisle.required' => 'The aisle number field is required.',
			'warehouse_section.required' => 'The section number field is required.',
			'warehouse_shelf.required' => 'The shelf number field is required.',
			'warehouse_city.regex' => 'The city code must be exact 3 characters.',
			'warehouse_code.regex' => 'The warehouse code must be exact 4 digits.',
			'warehouse_aisle.regex' => 'The aisle number must be exact 3 digits.',
			'warehouse_section.regex' => 'The section number must be exact 2 digits.',
			'warehouse_shelf.regex' => 'The shelf number must be exact 2 digits.',
		];

		$rule = [
				'warehouse_city' => 'required|regex:/^[a-zA-Z0-9]{3}$/',
				'warehouse_code' => 'required|regex:/^[0-9]{4}$/',
				'warehouse_aisle' => 'required|regex:/^[0-9]{3}$/',
				'warehouse_section' => 'required|regex:/^[0-9]{2}$/',
				'warehouse_shelf' => 'required|regex:/^[0-9]{2}$/',
		];

		return Validator::make($data, $rule, $messages);
	}

	/**
	 * getCalendarMonth
	 *
	 * @return mixed
	 * @author Manh Vu
	 */
	public function getCalendarMonth() {

		$post = Input::all();

		if (isset($post['monthyear'])) {

			$result = [];

			$day = 0;
			$str = trim($post['monthyear']);
			$firstDay = date('w', strtotime('01 ' . $str));
			$lastDateOfMonth = date('t', strtotime($str));
			$endDay = date('d', strtotime(date('m/t/Y', strtotime($str))));
			$today = date('Ymd');
			// Get data
			$calendar = new Calendars();
			$value = $calendar->getDataByMonthYear($str);
			$weekNum = 5;

			if ( ($firstDay == 5 && $lastDateOfMonth == 31) || ($firstDay == 6 && $lastDateOfMonth >= 30) ) {
				$weekNum ++;
			}

			for($i = 1; $i <= $weekNum; $i ++) {
				for($j = 0; $j < 7; $j ++) {

					if (($i == 1 && $firstDay > $j) || ($day >= $endDay)) {
						$getDay = '';
					}
					else {
						$day ++;
						$getDay = $day;
					}


					$data[$j] = array(
							'day' => $getDay,
							'data' => isset($value[$getDay]['timearr']) ? $value[$getDay]['timearr'] : '',
							'currentDay' => ($today == date('Ymd', strtotime($getDay . ' ' . $str))) ? 1 : 0,
							'storagestatus' => isset($value[$getDay]['storagestatus']) ? $value[$getDay]['storagestatus'] : ''
					);
				}

				$result[] = $data;
			}


			return $result;
		}

		return ['error' => 'Error request'];
	}

	/**
	 * getDisableDate
	 * This is crontab for send invoice
	 */
	public function getDisableDate() {
		$get = Input::all();

		$obj = new Calendars();
		$disableDate = $obj->getDisableDate($get['date']);

		return ['disable_date' => $disableDate];
	}

	/**
	 * bgSendInvoiceMail
	 * This is crontab for send invoice
	 */
	private function fomatData($content) {
		$data = null;
		$i = 0;
		$totalAmount = 0;
		foreach($content as $foo) {
			$data['dayStart'] = $foo['dayStart'];
			$data['invoiceId'] = $foo['invoiceId'];
			$data['customer'] = $foo['customer'];
			$data['addressstreet'] = $foo['addressstreet'];
			$data['cityzipcode'] = $foo['cityzipcode'];
			$data['aptsuite'] = $foo['aptsuite'];
			$data['phone'] = $foo['phone'];
			$data['userid'] = $foo['userid'];
			$data['card_number'] = $foo['card_number'];
			$totalAmount = $totalAmount + $foo['org_amount'];

			$data['data'][$i]['itemType'] = $foo['itemType'];
			$data['data'][$i]['quantity'] = $foo['quantity'];
			$data['data'][$i]['amount'] = $foo['amount'];
			$data['data'][$i]['amountOneItemFormat'] = isset($foo['amountOneItemFormat']) ? $foo['amountOneItemFormat'] : 0;
			$data['data'][$i]['amountNoTax'] = isset($foo['amountNoTax']) ? $foo['amountNoTax'] : 0;
			$data['data'][$i]['description'] = isset($foo['description']) ? $foo['description'] : '';
			$data['data'][$i]['tax'] = isset($foo['tax']) ? $foo['tax'] : 0;
			$i ++;
		}

		$data['totalAmount'] = sprintf('$%0.2f', $totalAmount / 100.0);

		return $data;
	}

	/**
	 * getPromoInfo
	 */
	public function getPromoInfo() {

		$post = Input::all();
		// Fail
		if(!isset($post['id']))
			return response()->json(['message' => 'Invalid request'], 500);

		$object = new Promos();
		$promo = $object->getById($post['id']);

		return $promo;
	}

	/**
	 * processPromo
	 * Use add, edit or delete
	 */
	public function processPromo() {

		$post = Input::all();
		// Fail
		if(!isset($post['data']) || !isset($post['type']))
			return response()->json(['message' => 'Invalid request'], 500);

		$return = ['error' => 'Wrong request'];
		$object = new Promos();
		switch($post['type']) {

			case 'add':
				$return = $object->createPromo($post['data']['code'], $post['data']['percent']);
				$return = !isset($return['error']) ? 1 : $return;
				break;

			case 'delete':
				$return = $object->deleteById($post['data']);
				$return = !isset($return['error']) ? 1 : $return;
				break;
		}

		return $return;
	}

	/**
	 * checkPromo
	 * Use add, edit or delete
	 */
	public function checkPromo() {

		$post = Input::all();
		// Fail
		if(!isset($post['promo']))
			return response()->json(['message' => 'Invalid request'], 500);

		$object = new Promos();
		$isExisted = $object->isExistedCode( $post['promo'] );

		return $isExisted ? 1 : ['error' => 'Promo code is wrong.'];
	}

	/**
	 * it4Cancel
	 * Scan for IT4 then cancel
	 *
	 * @param string $storagedId
	 * @author Manh
	 * @since 20160609
	 */
	private function it4Cancel($storagedId) {

		// Update Storage
		$obj = new Storage();
		$data['storagedkey'] = $storagedId;
		$data['dateca'] = date('F j.Y');
		$data['timeca'] = date('H:00 a');
		//$post['form']['userid'] = $user->userid;
		$result = $obj->cancelStorageByIdAjax($data);

		if (isset($result['error'])) return $result;

		//$result = $obj->getStorageById($storagedId);

		return $data['dateca'] . ' ' . $data['timeca'];
	}
}