<?php

namespace App\Http\Controllers;

use App\Models\Transactions;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\GoferMail;
use App\Models\Mailinvoices;
use App\StripeFuns;
use App\Http\Helpers\CommonHelper;
use App\Models\Users;
use App\Models\Bginvoices;
use App\Models\Storage;
use Config;
use App\Models\Calendars;

class CronController extends Controller
{
    /**
     * contruct
     */
    public function __construct() {
    }

    /**
     * updateStripeDaily
     * This is cron for update stripe daily
     */
    public function updateStripeDaily() {

    	$post = Input::all();

    	if (!isset($post['tk'])
    		|| $post['tk'] != '23b49bfa58e4fbe32aa3928854d3142ca4f4a1a8b18f6c056f0f6c84795d2cad') return 'false';

    	/*
    	 * Process for trial day
    	 */
    	$listTrial = Transactions::getTransactionListByTrialEnd();
    	if (!empty($listTrial)) {
    		foreach ($listTrial as $foo) {
    			$this->processingTrialday($foo);
    		}
    	}

    	/*
    	 * Process for new scan
    	 */
    	$list = Transactions::getTransactionListByStatus(1);
    	if (!empty($list)) {
	    	foreach ($list as $foo) {
				$this->processingSubcription($foo);
	    	}
    	}

    	/*
    	 * Process for cancel Pin (Scan to It4)
    	 */
    	$cancels = Storage::getCancelStorage();
    	$this->processCancel( $cancels );

    	return http_response_code(201);
    }

    /**
     * processCancel
     */
    private function processCancel( $storage ) {

    	$calen = new Calendars();
    	$result = array();

		foreach ($storage as $foo) {
			// Large
			if ($foo->item->type == 1)
			{
				$result[$foo->trans_id]['L']['count'] = isset($result[$foo->trans_id]['L']['count'])
					? ($result[$foo->trans_id]['L']['count'] + 1) : 1;
			}// Extra
			else if ($foo->item->type == 2)
			{
				$result[$foo->trans_id]['E']['count'] = isset($result[$foo->trans_id]['E']['count'])
					? ($result[$foo->trans_id]['E']['count'] + 1) : 1;
			}// Other Stuff
			else
			{
				$result[$foo->trans_id]['O']['count'] = isset($result[$foo->trans_id]['O']['count'])
					? ($result[$foo->trans_id]['O']['count'] + 1) : 1;
			}

			// Update the Item
			$foo->item->total_rented --;
			$foo->item->save();
			$calen->deleteCalendarByStorageId( $foo->_id );
			// Change status = 9 this storage
			$foo->status = 9;
			$foo->save();

		}

		//
		return $this->updateCancel($result);

    }

    /**
     * updateCancel
     */
    private function updateCancel( $result ) {

    	$stripe = new StripeFuns();
    	$transactions = new Transactions();

    	foreach ($result as $key => $foo) {

    		$trans = Transactions::find( $key );
    		if ($trans) {

    			$paymentInfo = $trans->paymentInfo;
    			$amount = 0;
    			$data = array();
    			// Large
    			if (isset($result[$key]['L']) && isset($paymentInfo['Large'])
    					&& ($paymentInfo['Large']['quantity'] >= $result[$key]['L']['count'])) {
    				$paymentInfo['Large']['quantity'] = $paymentInfo['Large']['quantity'] - $result[$key]['L']['count'];
    				$amount += $result[$key]['L']['count'] * $paymentInfo['Large']['price'];
    				$data['remove_items']['Large'] = $result[$key]['L']['count'];
    			}
    			// Extra
    			if (isset($result[$key]['E']) && isset($paymentInfo['Extra'])
    					&& ($paymentInfo['Extra']['quantity'] >= $result[$key]['E']['count'])) {
    				$paymentInfo['Extra']['quantity'] = $paymentInfo['Extra']['quantity'] - $result[$key]['E']['count'];
    				$amount += $result[$key]['E']['count'] * $paymentInfo['Extra']['price'];
    				$data['remove_items']['Extra'] = $result[$key]['E']['count'];
    			}
    			// Other
    			if (isset($result[$key]['O']) && isset($paymentInfo['Other'])
    					&& ($paymentInfo['Other']['quantity'] >= $result[$key]['O']['count'])) {
    				$paymentInfo['Other']['quantity'] = $paymentInfo['Other']['quantity'] - $result[$key]['O']['count'];
    				$amount += $result[$key]['O']['count'] * $paymentInfo['Other']['price'];
    				$data['remove_items']['Other'] = $result[$key]['O']['count'];
    			}

    			if ($amount == 0) continue;

    			$paymentInfo['total'] -= $amount;
    			$trans->users->sub_total -= $amount;

    			// Delete subscription and Transactions
    			if ($trans->users->sub_total <= 0){

    				$cancel = $stripe->cancelOrder( $trans->users->plan_id, $trans->users->sub_id );

    				if (isset($cancel['error'])) continue;

    				// Update user
    				$trans->users->sub_total = 0;
    				$trans->users->sub_date = '';
    				$trans->users->sub_id = '';
    				$trans->users->plan_id = '';
    				// delete transactions
    				$trans->status = 9;
    				$paymentInfo['total'] = 0;

    			} else {
    				//
    				$planArr['id'] = date('Ymdhis');
    				$planArr['name'] = $trans->transcode . '-' . date('Y-m-d h:i:s');
    				$planArr['amount'] = $trans->users->sub_total;
    				$planArr['promo_code'] = null;
    				$trialDay = CommonHelper::getTrialday($trans->users->sub_date);
    				// Update subcription
    				$sub = $stripe->updatePlanSubscription($trans->users->stripe_id, $trans->users->sub_id, $planArr, $trans->users->plan_id, $data, $trialDay);
    				// Update user
    				$trans->users->plan_id = $planArr['id'];

    			}
    			// Update transactions
    			$trans->paymentInfo = $paymentInfo;
    			//
    			$trans->users->save();
    			$trans->save();
    		}
    	}

    	return true;
    }

    /**
     * processingTrialday
     */
    private function processingTrialday($trans) {

    	//$setingday = Config::get('services.paymentday');
    	$stripe = new StripeFuns();
    	$paymentInfo = $trans->paymentInfo;
    	$amount = 0;
    	$item = '';
    	$listitems = '';
    	$date = empty($trans->users->sub_date) ? 'now' : $trans->users->sub_date;
    	$data = $paymentInfo['user'];
    	$data['orderId'] = $paymentInfo['orderId'];

    	// Get storage by transactions Id and isCharged = 2 (AD1)
		$storages = Storage::getStorageByTransIdAndNotCharge( $trans->_id );

		if ($storages->isEmpty()) return null;

		$flg = array();
		foreach ($storages as $foo) {
			$type = $foo->item->name;
			$payAmount = $paymentInfo[$type]['price'];
			$paymentInfo['total'] += $payAmount;
			$paymentInfo[$type]['scanned'] ++;
			// This is total of subscription
			if (empty($trans->users->sub_total)) {
				$trans->users->sub_total = $payAmount;
			} else {
				$trans->users->sub_total += $payAmount;
			}
			// Total of user
			//$trans->users->totalorder += $payAmount;
			$amount = $amount + CommonHelper::getAmountDay($payAmount, $date);
			$flg[$type]['amount'] = $payAmount;
			if ( !isset($flg[$type]['count']) ) {
				$flg[$type]['count'] = 1;
			} else {
				$flg[$type]['count'] ++;
			}
			// Set flag for charged - 2 is charged
			$foo->isCharged = 1;
			$foo->save();
		}

		// LG bin
		if ( isset( $flg['Large']['count'] ) ) {
			$item = $flg['Large']['count'] . 'L';
		}
		// XL bin
		if ( isset( $flg['Extra']['count'] ) ) {
			$item = ($item == '') ? $flg['Extra']['count'] . 'E' : $item . '-' . $flg['Extra']['count'] . 'E';
		}
		// Get item
		if (isset($paymentInfo['Large'])) {
			$listitems = 'L:' . $paymentInfo['Large']['scanned'] . ':' . $paymentInfo['Large']['price'];
		}
		if (isset($paymentInfo['Extra'])) {
			$t = ($listitems != '') ? '-' : '';
			$listitems .= $t . 'E:' . $paymentInfo['Extra']['scanned'] . ':' . $paymentInfo['Extra']['price'];
		}
		if (isset($paymentInfo['Other'])) {
			$t = ($listitems != '') ? '-' : '';
			$listitems .= $t . 'O:' . $paymentInfo['Other']['scanned'] . ':' . $paymentInfo['Other']['price'];
		}
		$data['listitems'] = $listitems;
		$data['item'] = $item;

    	// Plan
    	$planArr['id'] = date('Ymdhis');
    	$planArr['name'] = $paymentInfo['orderId'] . '-' . date('Y-m-d h:i:s');
    	$planArr['amount'] = $trans->users->sub_total;
    	$planArr['promo_code'] = $paymentInfo['promo_code'];

    	// Create subcription
    	if (empty($trans->users->sub_id)) {

    		$sub = $stripe->createSubscription($trans->users->stripe_id, $planArr, $data);

    		if (empty($sub)) return false;

    		// Update discount value for this Order
    		if (!empty($sub[0])) {
    			$paymentInfo['promo_percent_off'] = $sub[0];
    		}

    		// Update transactions
    		$trans->users->sub_id = $sub[1]->id;
    		$trans->users->sub_date = $sub[1]->created;

    	} else {

    		// Charge
    		if ($amount > 0) {

    			$metaData = [
	    			'phone' => $paymentInfo['user']['phone'],
	    			'address' => $paymentInfo['user']['address'],
	    			'customername' => $paymentInfo['user']['username'],
	    			'email' => $paymentInfo['user']['email'],
	    			'chargetype' => 'deliver',// No get tax
	    			'addressstreet' => $paymentInfo['user']['addressstreet'],
	    			'cityzipcode' => $paymentInfo['user']['cityzipcode'],
	    			'aptsuite' => $paymentInfo['user']['aptsuite'],
	    			'userid' => $paymentInfo['user']['userid'],
	    			'orderId' => $paymentInfo['orderId'],
	    			'item' => $item
    			];

    			// Discount for Bin belong Order got Promo code
    			if (isset($paymentInfo['promo_percent_off']) && !empty($paymentInfo['promo_percent_off'])) {
    				$amount = number_format($amount * (1 - ($paymentInfo['promo_percent_off'] / 100)), 2, '.', '');
    			}

    			$stripe->createCharge($trans->users->stripe_id, $amount, $des = 'Partial month bill for pro-rated monthly bin charge', $metaData);
    		}

			$trialDay = CommonHelper::getTrialday($trans->users->sub_date);
    		// Update subcription
    		$sub = $stripe->updatePlanSubscription($trans->users->stripe_id, $trans->users->sub_id, $planArr, $trans->users->plan_id, $data, $trialDay);

    	}

    	// Update users
    	//$trans->users->totalorder += $amount;
    	$trans->users->plan_id = $planArr['id'];
    	$trans->users->save();
    	// Update paymentInfo
    	$trans->paymentInfo = $paymentInfo;
    	//$trans->plan_id = $planArr['id'];
    	// Status = Done
    	$trans->status = 2;
    	$trans->save();
    }

    /**
     * processingSubcription
     */
    private function processingSubcription($trans) {

    	//$setingday = Config::get('services.paymentday');
    	$stripe = new StripeFuns();
    	$paymentInfo = $trans->paymentInfo;
    	$amount = 0;
    	$item = '';// The bin quantity for charge at this times
    	$listitems = '';
    	$date = empty($trans->users->sub_date) ? 'now' : $trans->users->sub_date;
    	$data = $paymentInfo['user'];
    	$data['orderId'] = $paymentInfo['orderId'];

    	if (isset($paymentInfo['Large']['scanned_flag']) && $paymentInfo['Large']['scanned_flag'] > 0) {
    		$payAmount = ($paymentInfo['Large']['scanned_flag'] * $paymentInfo['Large']['price']);
    		$paymentInfo['total'] += $payAmount;

    		// This is total of subscription
			if (empty($trans->users->sub_total)) {
				$trans->users->sub_total = $payAmount;
			} else {
				$trans->users->sub_total += $payAmount;
			}

			// Total of user
			//$trans->users->totalorder += $payAmount;

    		$paymentInfo['Large']['scanned'] += $paymentInfo['Large']['scanned_flag'];
    		$item = $paymentInfo['Large']['scanned_flag'] . 'L';
    		$paymentInfo['Large']['scanned_flag'] = 0;

    		$amount += CommonHelper::getAmountDay($payAmount, $date);
    	}
    	if (isset($paymentInfo['Extra']['scanned_flag']) && $paymentInfo['Extra']['scanned_flag'] > 0) {
    		$payAmount = ($paymentInfo['Extra']['scanned_flag'] * $paymentInfo['Extra']['price']);
    		$paymentInfo['total'] += $payAmount;
    		// This is total of subscription
    		if (empty($trans->users->sub_total)) {
    			$trans->users->sub_total = $payAmount;
    		} else {
    			$trans->users->sub_total += $payAmount;
    		}
    		// Total of user
    		//$trans->users->totalorder += $payAmount;

    		$paymentInfo['Extra']['scanned'] += $paymentInfo['Extra']['scanned_flag'];
    		$item = ($item == '') ? $paymentInfo['Extra']['scanned_flag'] . 'E' : $item . '-' . $paymentInfo['Extra']['scanned_flag'] . 'E';
    		$paymentInfo['Extra']['scanned_flag'] = 0;

    		$amount += CommonHelper::getAmountDay($payAmount, $date);

    	}
    	if (isset($paymentInfo['Other']['scanned_flag']) && $paymentInfo['Other']['scanned_flag'] > 0) {
    		$payAmount = ($paymentInfo['Other']['scanned_flag'] * $paymentInfo['Other']['price']);
    		$paymentInfo['total'] += $payAmount;
    		// This is total of subscription
    		if (empty($trans->users->sub_total)) {
    			$trans->users->sub_total = $payAmount;
    		} else {
    			$trans->users->sub_total += $payAmount;
    		}
    		// Total of user
    		//$trans->users->totalorder += $payAmount;

    		$paymentInfo['Other']['scanned'] += $paymentInfo['Other']['scanned_flag'];
    		$item = ($item == '') ? $paymentInfo['Other']['scanned_flag'] . 'O' : $item . '-' . $paymentInfo['Other']['scanned_flag'] . 'O';
    		$paymentInfo['Other']['scanned_flag'] = 0;

    		$amount += CommonHelper::getAmountDay($payAmount, $date);
    	}

    	if ($item == '') return null;

    	// Get item
    	if (isset($paymentInfo['Large'])) {
    		$listitems = 'L:' . $paymentInfo['Large']['scanned'] . ':' . $paymentInfo['Large']['price'];
    	}
    	if (isset($paymentInfo['Extra'])) {
    		$t = ($listitems != '') ? '-' : '';
    		$listitems .= $t . 'E:' . $paymentInfo['Extra']['scanned'] . ':' . $paymentInfo['Extra']['price'];
    	}
    	if (isset($paymentInfo['Other'])) {
    		$t = ($listitems != '') ? '-' : '';
    		$listitems .= $t . 'O:' . $paymentInfo['Other']['scanned'] . ':' . $paymentInfo['Other']['price'];
    	}

    	$data['listitems'] = $listitems;
    	$data['item'] = $item;
    	$planArr['id'] = date('Ymdhis');
    	$planArr['name'] = $paymentInfo['orderId'] . '-' . date('Y-m-d h:i:s');
    	$planArr['amount'] = $trans->users->sub_total;
    	$planArr['promo_code'] = $paymentInfo['promo_code'];

    	// check subscription ID
    	$retrieveSub = $stripe->subscriptionRetrieve($trans->users->sub_id);

    	// Create subcription
    	if (empty($retrieveSub)) {

    		$sub = $stripe->createSubscription($trans->users->stripe_id, $planArr, $data);

    		if (empty($sub)) return false;

			// Update discount value for this Order
    		if (!empty($sub[0])) {
    			$paymentInfo['promo_percent_off'] = $sub[0];
    		}

    		$trans->users->sub_id = $sub[1]->id;
    		$trans->users->sub_date = $sub[1]->created;

    	} else {

    		$metaData = [
	    		'phone' => $paymentInfo['user']['phone'],
	    		'address' => $paymentInfo['user']['address'],
	    		'customername' => $paymentInfo['user']['username'],
	    		'email' => $paymentInfo['user']['email'],
	    		'chargetype' => 'deliver',// No get tax
	    		'addressstreet' => $paymentInfo['user']['addressstreet'],
	    		'cityzipcode' => $paymentInfo['user']['cityzipcode'],
	    		'aptsuite' => $paymentInfo['user']['aptsuite'],
	    		'userid' => $paymentInfo['user']['userid'],
	    		'orderId' => $paymentInfo['orderId'],
	    		'item' => $item
    		];

    		// Discount for Bin belong Order got Promo code
    		if (isset($paymentInfo['promo_percent_off']) && !empty($paymentInfo['promo_percent_off'])) {
    			$amount = number_format($amount * (1 - ($paymentInfo['promo_percent_off'] / 100)), 2, '.', '');
    		}

    		// Charge
    		$stripe->createCharge($trans->users->stripe_id, $amount, $des = 'Partial month bill for pro-rated monthly bin charge', $metaData);
    		$trialDay = CommonHelper::getTrialday($trans->users->sub_date);
    		// Update subcription
    		$sub = $stripe->updatePlanSubscription($trans->users->stripe_id, $trans->users->sub_id, $planArr, $trans->users->plan_id, $data, $trialDay);
    	}

    	// Update User
    	//$trans->users->totalorder += $amount;
    	$trans->users->plan_id = $planArr['id'];
    	$trans->users->save();
    	// Update paymentInfo
    	$trans->paymentInfo = $paymentInfo;
    	//$trans->plan_id = $planArr['id'];
		// Status = Done
    	$trans->status = 2;
    	$trans->save();
    }

    /**
     * bgSendInvoiceMail
     */
    public function bgSendInvoiceMail() {

		$obj = new Mailinvoices();
		$data = $obj->getListAll();

   		if (!empty($data)) {

   			foreach($data as $foo) {
   				$getData = $this->fomatData( $foo->content, $foo->invoicedata );

				if (!empty($getData)) {

					$email = $getData['email'];
					//$email = 'manh@quodisys.com';
					$format = $getData['formatType'];

					GoferMail::sendMail(
						$email,
						'Box Gofer - Invoice Payment',
						'emails.' . $format,
						$getData
					);
   				}
  			}
			// Remove all data in collection
			$obj->removeAll();
		}

    }

    /**
     * createInvoiceData
     */
    private function createInvoiceData($invoicedata) {
    	$bginvoice = new Bginvoices();
    	$bginvoice->createBginvoice($invoicedata);
    }

    /**
     * fomatData
     */
    private function fomatData($content, $invoicedata) {

    	$i = 0;
    	$data['email'] = $content['email'];
    	$data['dayStart'] = $content['dayStart'];
    	$data['invoiceId'] = $content['invoiceId'];
    	$data['invoice_num'] = $content['invoice_num'];
    	$data['customer'] = $content['customer'];
    	$data['addressstreet'] = $content['addressstreet'];
    	$data['cityzipcode'] = $content['cityzipcode'];
    	$data['aptsuite'] = $content['aptsuite'];
    	$data['phone'] = $content['phone'];
    	$data['userid'] = $content['userid'];
    	$data['card_number'] = $content['card_number'];
    	$data['totalAmount'] = sprintf('$%0.2f', $content['org_amount'] / 100.0);
    	$data['totalAmountNoFormat'] = $content['org_amount'];
    	//$totalAmount = $content['org_amount'];
    	if (isset($content['listitems'])) {
    		$listitems = $content['listitems'];
    		$data['discount'] = $content['discount'];
    		$data['discount_key'] = $content['discount_key'];
    		$data['formatType'] = 'invoicesuccessnew';
    		$arr = (strpos($listitems, '-') !== false) ? explode('-', $listitems) : array($listitems);
    		if (count($arr) > 0) {
    			foreach ($arr as $f) {
    				$temp = explode(':', $f);
    				$data['data'][$i]['itemType'] = $temp[0];
    				$data['data'][$i]['quantity'] = $temp[1];
    				$data['data'][$i]['amountOneItemFormat'] = sprintf('$%0.2f', $temp[2]);
    				$data['data'][$i]['amount'] = sprintf('$%0.2f', ($temp[2] * $temp[1]));
    				$i ++;
    			}
    		}
    	} else {
    		$data['formatType'] = 'invoicesuccesscharge';
    		$data['itemType'] = $content['itemType'];
    		$data['amountNoTax'] = isset($content['amountNoTax']) ? $content['amountNoTax'] : 0;
    		$data['tax'] = isset($content['tax']) ? $content['tax'] : 0;
    		$data['totalAmount'] = isset($content['totalAmount']) ? $content['totalAmount'] : 0;
    		$data['description'] = $content['description'];
    		$data['quantity'] = $content['quantity'];
    		$data['amount'] = $content['amount'];
    		$data['item'] = isset($content['item']) ? $content['item'] : '';
    	}

    	// Create invoice data
    	$this->createInvoiceData($invoicedata);

    	return $data;
    }

    /**
     * cronDeliveries
     */
    public function cronDeliveries() {

    	$post = Input::all();

    	if (!isset($post['tk'])
    			|| $post['tk'] != '23b49bfa58e4fbe32aa3928854d3142ca4f4a1a8b18f6c056f0f6c84795d2cad') return 'false';

    	$date = strtotime('+1 day');

    	$storage = Storage::getDeliveriesWithDay($date);

    	if (count($storage)) {

    		$url = Config::get('app.url');
    		$data = array(
    				'link' => $url . '/dowload/csv?token=' . base64_encode($date),
    				'date' => date("F j.Y", $date)
    			);
    		$email = 'info@boxgofer.com';
    		//$email = 'manh@quodisys.com';
    		GoferMail::sendMail(
    				$email,
    				'Box Gofer - Deliveries - ' . date("F j.Y", $date),
    				'emails.csvmail',
    				$data
    		);

    		return 'done';
    	}

    	return 'Data null';
    }

    /**
     * downloadDeliveries
     */
    public function downloadDeliveries() {

    	$post = Input::all();

    	if (!isset($post['token'])) return 'false';

    	// $date is strtotime('m/d/Y')
    	$date = base64_decode($post['token']);

    	$storage = Storage::getDeliveriesWithDay( $date );

    	if (count($storage)) {
    		$fileName = 'deliveries-' . date('Ymdhis');

    		return CommonHelper::convertToCSV($storage, $fileName);
    	}

    	return 'Null';
    }

    /**
     * crontestmail
     */
    public function crontestmail() {

    	$post = Input::all();

    	if (!isset($post['tk'])
    			|| $post['tk'] != '23b49bfa58e4fbe32aa3928854d3142ca4f4a1a8b18f6c056f0f6c84795d2cad') return 'false';

    	$email = 'manh@quodisys.com';
    	GoferMail::sendMail(
    			$email,
    			'Box Gofer - Test mail - ' . date("F j.Y", strtotime('now')),
    			'emails.testemail'
    	);

    }

}