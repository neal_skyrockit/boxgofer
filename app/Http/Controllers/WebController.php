<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use App\Models\Places;
use App\Models\Items;
use App\Models\Users;
use App\Models\Storage;
use App\Models\Transactions;
use App\Models\Calendars;
use Illuminate\Support\Facades\Validator;
use App\StripeFuns;
use App\GoferMail;
use App\Http\Helpers\CommonHelper;
use App\Models\History;
use Session;

class WebController extends Controller
{
	protected $guard = 'user';
	protected $user = null;

	/**
	 * contruct
	 */
	public function __construct()
	{
		$this->places = new Places();
		$this->user = Auth::guard($this->guard)->user();
		$this->middleware('authboxgofer', ['except' =>
				['logout',
				'login',
				'index',
				'zipcode',
				'register',
				'home',
				'testupload',
				'resetPassword',
				'privacy',
				'aboutUs',
				'faq',
				'terms',
				'orderNow' // Added
				]
			]);

		//@hong add
		$this->items = new Items();
		$this->storage = new Storage();
		$this->transactions = new Transactions();
	}

	/**
	 * Index page
	 */
	public function index()
	{
		/* $this->items->initItems(2);
		die('done'); */
		/* $c = new Calendars();
		$a = $c->getDataByMonthYear('May 2016');
		var_dump($a);die; */
		/* $stripe = new StripeFuns();
		//$customer = $stripe->customerRetrieve('cus_8Sys8HKucDW5my');
		$in = $stripe->invoiceRetrieve('in_18C33CLmYaBSq0iomoHJ7C4j');
var_dump($in);die; */

		return view('web.index');
	}

	/**
	 * Home page
	 */
	public function home()
	{
		$places = $this->places->getPlaceNameList();
		$items = $this->items->getItemsList();

		return view('web.home')->with('places', $places)
				->with('items', $items);
	}

	/**
	 * terms page
	 */
	public function terms() {
		return view('web.terms');
	}

	/**
	 * privacy page
	 */
	public function privacy() {
		return view('web.privacy');
	}

	/**
	 * About Us page
	 */
	public function aboutUs() {
		return view('web.aboutus');
	}

	/**
	 * FAQ page
	 */
	public function faq() {
		return view('web.faq');
	}

	/**
	 * Zipcode page
	 */
	public function zipcode()
	{
		$token = Input::get('token', '');

		if( Auth::guard('user')->user() ){
			$zipcode = $this->user->zipcode;
			return redirect('/order/'.$zipcode.'/'.$token);
		} else {
			return view('web.zipcode')->with('token', $token);
		}
	}

	/**
	 * Reset Password page
	 */
	public function resetPassword()
	{
		$message = '';
		$post = Input::all('token', '');
		$token = urlencode($post['token']);
		$str = GoferMail::bgDecrypt($token);
		$arr = explode('-', $str);
		$dateStr = date('mdY');

		if (!isset($arr[0]) || !isset($arr[1]) || ($arr[0] != $dateStr))
		{
			return response()->view('errors.503', [], 503);
		}

		$obj = Users::where('email', $arr[1])->first();

		if (!$obj) {
			return response()->view('errors.503', [], 503);
		}
		// Submit
		if (isset($post['resetpw'])) {

			// Check null
			if ($post['newpass'] == '') {
				return redirect('/resetpassword?token=' . $token)
					->withErrors(['error' => 'New password not null.'])
					->withInput($post);
			}

			if ($post['newpass'] != $post['confpass']) {
				return redirect('/resetpassword?token=' . $token)
					->withErrors(['error' => 'New password and Confirm new password are not match.'])
					->withInput($post);
			}

			$obj->password = bcrypt($post['newpass']);
			$obj->save();

			$message = 'Your password is changed.';

		}

		return view('web.resetpassword')
				->with('token', $token)
				->with('message', $message);
	}

	/**
	 * register page
	 */
	public function register($zipcode, $token = '')
	{
		$post = Input::all();
		$user = new Users();
		$place = $this->places->getPlaceByZipcode($zipcode);

		// Check token
		if ($token != '' && !$user->isExistedUserId($token)) {
			return redirect('register/' . $zipcode)
					->withErrors(['error' => 'The Token is not exist.'])
					->withInput();
		}

		// Places
		if ($place == null)
			return redirect('zipcode');

		// Post processing
		if (!empty($post))
		{
			$validator = $this->validator($post);
			if ($validator->fails()) {
				$url = ($token != '') ? 'register/' . $zipcode . '/' . $token : 'register/' . $zipcode;
				return redirect($url)
						->withErrors($validator)
						->withInput();
			}

			//
			$nameStr = '';
			$postname = preg_replace('/[^ \w]+/', '', $post['name']); // Removes special chars.
			$arr = explode(' ', $postname);
			$nameStr = $arr[count($arr) - 1];
			$newstr = '';
			for($i = 0; $i < 4; $i ++) {
				$newstr .= isset($nameStr[$i]) ? $nameStr[$i] : '_';
			}
			$nameStr = strtoupper($newstr) . rand(100, 999) . '-' . $zipcode;
			// Create
			$user->userid = $nameStr;
			$user->zipcode = $zipcode;
            $user->city = $place['name'];
            $user->full = $place['full'][$zipcode];
			$user->name = $post['name'];
			$user->email = $post['email'];
			$user->password = bcrypt($post['password']);
			$user->status = 1;
            $user->totalorder = 0;
            $user->ipaddress = CommonHelper::getRealIpAddr();

			if ($token) {
				$user->referral = $token;
			}
			$user->save();
			// Set login
			Auth::guard($this->guard)->loginUsingId($user->_id, true);

			return redirect('/order');

		}

		return view('web.register')
				->with('zipcode', $zipcode)
				->with('token', $token);
	}

	/**
	 * setting page
	 */
	public function settings()
	{
		$post = Input::all();

		if ($post) {

			if (! CommonHelper::checkPhone($post['phone'])) {

				return redirect('settings')
					->withErrors(['error' => 'Your phone is invalid'])
					->withInput();
			}

			$place = $this->places->getPlaceByZipcode($post['zipcode']);
			$addressfull = $post['address'] . ', ' . $place['full'][$post['zipcode']];

			if ( CommonHelper::checkAddress($addressfull) ) {

				$validator = $this->validatorUpdate($post);
				if ($validator->fails()) {
					return redirect('settings')
					->withErrors($validator)
					->withInput();
				}

				// Check zipcode
				if ($place == null)
					return redirect('settings')
					->withErrors(['error' => 'This zipcode is not support'])
					->withInput();

				// Update
				$this->user->email = $post['email'];
				$this->user->zipcode = $post['zipcode'];
				$this->user->city = $place['name'];
				$this->user->full = $place['full'][$post['zipcode']];
				$this->user->name = $post['name'];
				$this->user->password = bcrypt($post['password']);
				$this->user->physicalAddress = $post['address'];
				$this->user->phone = $post['phone'];
				$this->user->save();
				// Set login
				Auth::guard($this->guard)->loginUsingId($this->user->_id, true);
				// Refresh current login user
				$this->user = Auth::guard($this->guard)->user();
			}
			else
			{
				return redirect('settings')
					->withErrors(['error' => 'Your address is invalid'])
					->withInput();
			}

		}

		$places = $this->places->getPlaceNameList();

		return view('web.setting')->withUser($this->user)
								  ->with('places', $places);
	}

	/**
	 * history page for parents
	 * @author: hong
	 */
	public function history()
	{
		$liststore = Storage::getDoneStorageByUser( $this->user->_id );
		return view('web.history')->with('liststore', $liststore);
	}

	/**
	 * refer page for parents
	 */
	public function refer()
	{
		return view('web.refer');
	}

	/**
	 * mystuff page for parents
	 */
	public function mystuff()
	{
		$cons['userId'] = $this->user->_id;
		$cons['status'] = 2;
		$result = $this->storage->search($cons);
		// For charges every requested drop-off
		$cuskey = (isset($result[0]) && $result[0]->transactions->type == 0) ? $result[0]->transactions->paymentInfo['customer_id'] : '' ;
		$getTotal = $this->storage->getTotalInTransitAndDoorstepByUser($this->user->_id);
		/* $doorstep = $this->storage->totalStorageByUserAndStatus($this->user->_id, 1);
		$transit = $this->storage->totalStorageByUserAndStatus($this->user->_id, 0); */
		$places = $this->places->getPlaceNameList();

		$obj = new Calendars();
		$disableDate = $obj->getDisableDate();

		return view('web.mystuff')->with('storage', $result)
								  ->with('cuskey', $cuskey)
								  ->with('transit', $getTotal[0])
								  ->with('doorstep', $getTotal[1])
								  ->with('instorage', $getTotal[2])
								  ->with('username', $this->user->name)
								  ->with('places', $places)
								  ->with('disable_date', $disableDate)
								  ->withUser($this->user);
	}

	/**
	 * login page for parents
	 */
	public function login()
	{
		return view('web.login');
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		$messages = [
			'email.unique' => 'It appears that e-mail address is already registered with us.  Please click the Log-in button at the top of the page and use your account log-in information to access your account.',
			'confirmemail.required' => 'The confirm email is required.',
			'confirmemail.same' => 'The confirm email and email must match.'
		];

		return Validator::make($data, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'confirmemail' => 'required|email|same:email',
			'password' => 'required|min:6',
		], $messages);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validatorUpdate(array $data)
	{
		return Validator::make($data, [
				'name' => 'required|max:255',
				'email' => 'required|email|max:255|unique:users,email,' . $this->user->_id . ',_id',
				'password' => 'required|min:6',
			]);
	}

	/**
	 * testupload
	 */
	public function testupload()
	{
		return view('web.testupload');
	}

    /**
     * DDD
	 * order
	 */
    public function order()
	{
		$user = Auth::guard($this->guard)->user();
		$place = $this->places->getPlaceByZipcode($user->zipcode);
		$obj = new Calendars();
		$disableDate = $obj->getDisableDate();
		return view('web.order')->with('user', $user)->with('address', $place->full[$user->zipcode])->with('disable_date', $disableDate);
	}

	public function orderNow( $zipcode, $token = '')
	{
		if($zipcode){
			$user = Auth::guard($this->guard)->user();
			$place = $this->places->getPlaceByZipcode($zipcode);
	        $obj = new Calendars();
	        $disableDate = $obj->getDisableDate();

	        if($user){
	        	return view('web.order')->with('user', $user)->with('address', $place->full[$user->zipcode])->with('disable_date', $disableDate);
	        } else {
	        	return view('web.order')->with('address', $place)->with('disable_date', $disableDate);
	        }
		}
		return redirect('/zipcode');

	}

	/**
	 * uploadImages
	 */
	public function uploadImages() {

		$image = Input::file('image');
		$id = Input::get('storage', '');
		$storage = $this->storage->getStorageById($id);

		if ($image && $storage) {

			// getting all of the post data
			$file = array('image' => $image);
			// setting up rules
			$rules = array('image' => 'mimes:jpeg,bmp,png',); //mimes:jpeg,bmp,png and for max size max:10000
			// doing the validation, passing post data, rules and the messages
			$validator = Validator::make($file, $rules);
			// Validate rule
			if ($validator->fails()) {
				// send back to the page with the input data and errors
				return redirect('mystuff')
					->withErrors([
						'error' => 'Choose the image.',
					]);
			}

			// Validate type
			if ($image->isValid()) {
				$destinationPath = base_path() . '/public/uploads';
				$fileName = date('Ymdhis');
				$fileName = 'upload_' . $fileName . '_' .$image->getClientOriginalName();
				$image->move($destinationPath, $fileName); // uploading file to given path
				// Processing images
				$getimage = $storage->images;
				if (empty($getimage))
				{
					$getimage = array($fileName);
				} else {
					array_push($getimage, $fileName);
				}
				$storage->images = $getimage;
				$storage->save();

				// sending back with message
				return redirect('mystuff')->with(['success' => 'Success to upload.']);
			}

		}

		return redirect('mystuff')
			->withErrors([
				'error' => 'Images is invalid.',
			]);
	}
}