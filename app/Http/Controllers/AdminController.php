<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use \DateTime;
use \Exception;
use Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Helpers\CommonHelper;
use App\Models\Users;
use App\Models\Transactions;
use App\Models\Storage;
use App\Models\Staff;
use App\Models\Items;
use App\Models\Places;
use App\Models\Bginvoices;
use App\Models\Calendars;
use App\Models\Warehouses;
use App\Models\Promos;

class AdminController extends Controller
{
	protected $guard = 'managers';
	protected $auth;
	protected $users;
	protected $trans;
	protected $storage;
    protected $staff;
    protected $items;
    protected $invoices;
    protected $calendars;
    protected $promos;

	/**
	 * contruct
	 */
	public function __construct(
			Users $users,
			Transactions $trans,
			Storage $storage,
			Staff $staff,
			Items $items,
			Bginvoices $invoices
	)
	{
		$this->middleware('auth');
		$this->auth = Auth::guard($this->guard);
		$this->users = $users;
		$this->trans = $trans;
		$this->storage = $storage;
        $this->staff = $staff;
        $this->items = $items;
        $this->invoices = $invoices;
        $this->calendars = new Calendars();
        $this->promos = new Promos();
	}

	/**
	 * Dashboard page
	 */
	public function index()
	{
		if ($this->auth->user()->role == 'operator') {
			return redirect('admin/customers');
		}

		// Get schedule - calendars
		$arr['today'] = array(date('F j.Y'), $this->calendars->getCalendarByDate(date('d/m/Y')));
		$arr['tomorrow'] = array(date('F j.Y', strtotime('+1 days')), $this->calendars->getCalendarByDate(date('d/m/Y', strtotime("+1 days")))) ;
		$arr['aftertomorrow'] = array(date('F j.Y', strtotime('+2 days')), $this->calendars->getCalendarByDate(date('d/m/Y', strtotime("+2 days"))));

        $model = array(
            'bin' => []
        );

        $listItem = $this->items->getItemsList();

        for($i=0;$i < 3;$i++){
            $bintype = $listItem[$i]->type == Items::ItemTypeLarge ? 'large'
                        : ($listItem[$i]->type == Items::ItemTypeExtraLarge ? 'extra' : 'other') ;

            $model['bin'][$bintype] = [
                'total' => $listItem[$i]->total,
                'rented' => $listItem[$i]->total_rented,
                'rented_percent' => (($listItem[$i]->total_rented / $listItem[$i]->total) * 100),
                'unused' => $listItem[$i]->total - $listItem[$i]->total_rented
            ];
            $model['bin'][$bintype]['unused_percent'] = 100 - $model['bin'][$bintype]['rented_percent'];
        }
        // return $model;

		return view('admin.index')
				->with('model',$model)
				->with('arr', $arr);
	}

	/**
	 * Users page
	 */
	public function client()
	{
		$arr = Input::all();
		$arr = empty($arr) ? null : $arr;

		$arr['withoutadmin'] = true;
		$arr['status'] = isset($arr['status']) ? $arr['status'] : 1;
		$ids = [];
		$StorageInfo = array();
        $users = $this->users->search($arr);

        if (!empty($users)) {

        	foreach($users as $u){

        		$ustorages = $this->storage->getStorageByUserIdAdmin($u->_id);

	        	foreach($ustorages as $us){
	        		$type = ($us->item->type == 1) ? 'l'
	        			:(($us->item->type == 2) ? 'e' : (($us->item->type == 3) ? 'o' : ''));
	        		if($type != '') {
	        			$StorageInfo[$u->_id][$type] = (isset($StorageInfo[$u->_id][$type])) ? $StorageInfo[$u->_id][$type] + 1 : 1;
	        		}
	        	}

        	}

        }

        //
        $places = new Places();
        $obj = $places->getPlaceNameList();

		return view('admin.client')
	        ->withUsers($users)
	        ->with('StorageInfo', $StorageInfo)
		    ->with('search', $arr)
		    ->with('places', $obj)
			->with('arr', $arr);
	}

	/**
	 * Transactions page
	 */
	public function transList($userid = null)
	{
		$arr = Input::all();

		$arr = empty($arr) ? null : $arr;
        if(!empty($userid))
             $arr['userid'] = $userid;

        $trans = $this->trans->search($arr);

        $transids = [];
        $userids = [];
        $StorageInfo = array();
        $UserInfo = array();

        foreach($trans as $t){
            if(!in_array($t->user_id,$userids))
                $userids[] = $t->user_id;

            $transids[] = $t->_id;
            $StorageInfo[$t->_id] = array(
                    'l'=>0,
                    'e'=>0,
                    'o'=>0
            );
        }

        $user = $this->users->getUserByIds($userids);
        $ustorages =  $this->storage->getStorageByTransIds($transids);

        foreach($ustorages as $us){
           $type = ($us->item->type == 1) ? 'l'
                 :(($us->item->type == 2) ? 'e'
                    : (($us->item->type == 3) ? 'o' : ''));
           if($type != '')
               $StorageInfo[$us->trans_id][$type] = $StorageInfo[$us->trans_id][$type] + 1;
        }

        foreach($user as $u){
            $UserInfo[$u->_id] = array(
                    'email' => $u->email,
                    'name' => $u->name,
            		'customerId' => $u->stripe_id,
            );
        }

		return view('admin.translist')
            ->with('trans',$trans)
            ->with('StorageInfo',$StorageInfo)
            ->with('UserInfo',$UserInfo)
			->with('search', $arr);
	}

	/**
	 * Storages page
	 */
	public function storageList()
	{
		$arr = Input::all();
		$arr = empty($arr) ? null : $arr;
		return view('admin.storagelist')->withStorage($this->storage->search($arr))
			->with('search', $arr);
	}

    /**
	 * Staff page
	 */
	public function staff()
	{
		if ($this->auth->user()->role != 'owner' && $this->auth->user()->role != 'manager') return redirect('admin');

        $arr = Input::all();
        $arr = empty($arr) ? null : $arr;

        $staffs = $this->staff->search($arr);
		return view('admin.staff')
               ->with('staffs', $staffs)
				->with('arr', $arr);
	}

    /**
     * DDD - 20160222
	 * upload_excel
	 */
    public function upload_excel(){

    	if ($this->isStaff()) return redirect('admin');

        $file = Input::file('excelfile');

        if(!isset($file))
            return redirect()->action('AdminController@staff');

        $reader = Excel::selectSheetsByIndex(0)->load($file)->get();

        $emails = $this->staff->getListEmail();
        $obj_e = array();
        $failStaff = array();
        foreach($emails as $e){
            $obj_e[$e[0]] = '' ;
        }

        foreach($reader->toArray() as $row){
            $valid = $this->staff->validExcelPost($row);
            if($valid !== true){
                $failStaff[] = array(
                    'email'=> $row['email'],
                    'error' => $valid
                ) ;
                continue;
            }

            if(isset($obj_e[$row['email']])){
                $failStaff[] = array(
                        'email'=> $row['email'],
                        'error' => 'Email has already exist'
                    ) ;
                continue;
            }

            $this->staff->upsert($row);
            $obj_e[$row['email']] = '' ;
        }

        $staffs = $this->staff->search(null);
		return view('admin.staff')
               ->with('staffs', $staffs)
               ->with('failUpload', $failStaff);

    }

    /**
     * DDD - 20160218
     * upsertStaff
     */
    public function upsertStaff() {

    	if ($this->isStaff()) return redirect('admin');

        $post = Input::all();

        $valid = $this->staff->validModel($post);
        if($valid !== true)
            return response()->json(['message' => $valid], 500);

        $this->staff->upsert($post);
        return Redirect::action('AdminController@staff');
    }

    /**
     * DDD - 20160219
     * bin
     */
    public function bin() {

    	if ($this->auth->user()->role == 'operator' || $this->auth->user()->role == 'driver') return redirect('admin');

        $bins = $this->items->getItemsList();

        return view('admin.bin')
               ->with('bins',$bins);
    }

    /**
     * DDD - 20160222
     * calendar
     */
    public function calendar() {
    	// Get schedule - calendars
    	$arr['today'] = array(date('F j.Y'), $this->calendars->getCalendarByDate(date('d/m/Y')));
    	$arr['tomorrow'] = array(date('F j.Y', strtotime('+1 days')), $this->calendars->getCalendarByDate(date('d/m/Y', strtotime("+1 days")))) ;
    	$arr['aftertomorrow'] = array(date('F j.Y', strtotime('+2 days')), $this->calendars->getCalendarByDate(date('d/m/Y', strtotime("+2 days"))));

        return view('admin.calendar1')->with('arr', $arr);
    }

    /**
     * DDD - 20160302
     * chart
     */
    public function chart() {
        return view('admin.chart');
    }

	/**
	 * invoices
	 */
    public function invoices() {

    	if ($this->auth->user()->role == 'driver') return redirect('admin');

    	$arr = Input::all();

    	$arr = empty($arr) ? null : $arr;

    	$invoices = $this->invoices->search($arr);

    	return view('admin.invoices')
			->with('invoices', $invoices)
    		->with('arr', $arr);
    }

    /**
     * Staff Transactions page
     */
    public function staffTransaction()
    {
    	$arr = Input::all();

    	$arr = empty($arr) ? null : $arr;

    	$result = $this->storage->adminSearch($arr);

    	$listBarcode = array();

    	foreach ($result[1] as $st) {
    		// Not show barcode image of storage status is 2
    		if ($st->status == 2) {
    			continue;
    		}

    		if (! isset($listBarcode[$st->trans_id])) {
    			$listBarcode[$st->trans_id] = array();
    		}
    		$listBarcode[$st->trans_id][] = CommonHelper::generateBarcode($st->barcode);
    	}

    	$placesModel = new Places();
    	$places = $placesModel->getPlaceNameList();

    	return view('admin.stafftrans')
		    	->with('storage', $result[0])
		    	->with('listBarcode', json_encode($listBarcode))
		    	->with('arr', $arr)
		    	->with('places', $places);
    }

    /**
     * Warehouse page
     */
    public function warehouse()
    {
    	if ($this->auth->user()->role != 'driver' && $this->auth->user()->role != 'owner' && $this->auth->user()->role != 'manager') return redirect('admin');

    	$arr = Input::all();
    	$warehouses = Warehouses::getAll();
    	$firstWarehouse = $warehouses->first();
    	$listBarcode = array();

    	if (! isset($arr['city_code']) && ! isset($arr['warehouse_code']) && $firstWarehouse) {
    		$arr['city_code'] = $firstWarehouse->city_code[0];
    		$arr['warehouse_code'] = $firstWarehouse->warehouse_code[0];
    	}

    	$result = $this->storage->warehouseSearch($arr);

    	foreach ($result[1] as $st) {
    		if (! isset($listBarcode[$st->trans_id])) {
    			$listBarcode[$st->trans_id] = array();
    		}
    		$warehouse_barcode = isset($st->warehouse_barcode) ? $st->warehouse_barcode : '';
    		$listBarcode[$st->trans_id][] = CommonHelper::generateBarcode($warehouse_barcode);
    	}

    	return view('admin.warehouse', [
    			'arr' => $arr,
    			'warehouses' => $warehouses,
    			'storage' => $result[0],
    			'listBarcode' => json_encode($listBarcode)
    	]);
    }

    /**
     * promotion page
     */
    public function promotion()
    {
    	if ($this->auth->user()->role != 'owner' && $this->auth->user()->role != 'manager') return redirect('admin');

    	$promos = $this->promos->getPromosList();

    	return view('admin.promotion')->with('promos', $promos['data']);
    }

    /**
     * isStaff
     */
    private function isStaff()
    {
    	return $this->auth->user()->role !== 'manager';
    }
}