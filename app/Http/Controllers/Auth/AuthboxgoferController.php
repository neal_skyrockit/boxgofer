<?php
/**
 * AuthboxgoferController
 * - Create authentication check for user
 *
 * @author Manh Vu
 * @since 1.0 2016/01/08
 * $Id: $
 */
namespace App\Http\Controllers\Auth;

//use Illuminate\Support\Facades\Input;
use App\Models\Places;
use App\Models\Users;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Http\Requests\BoxgoferLoginRequest;
use Illuminate\Contracts\Auth\Guard;
use Laravel\Socialite\Facades\Socialite as Socialite;
use Illuminate\Support\Facades\Auth;

class AuthboxgoferController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $loginPath = '/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->authboxgofer = Auth::guard('user');
    	$this->places = new Places();
    }

    /**
     * postLogin
     *
     * @return void
     */
    public function postLogin(BoxgoferLoginRequest $request)
    {
    	$request->merge(['email' => $request->input('email')]);
    	$credentials = $request->only('email', 'password');
    	$credentials['status'] = 1; // Chuong: Only allow user have status 1

    	if ($this->authboxgofer->attempt($credentials, $request->has('remember')))
    	{
    		return redirect('mystuff');
    		//return redirect()->intended($this->redirectTo);
    	}
    	elseif ($this->authboxgofer->validate($request->only('email', 'password')))
    	{
    		return redirect($this->loginPath)
    		->withInput($request->only('email', 'remember'))
    		->withErrors([
    				'error' => 'This log-in and password does not match any current accounts.  Please try again or click "Forgot Password" below.',
    			]);
    	}

    	return redirect($this->loginPath)
	    	->withInput($request->only('email', 'remember'))
	    	->withErrors([
	   			//'error' => $this->getFailedLoginMessage(),
    			'error' => 'This log-in and password does not match any current accounts.  Please try again or click "Forgot Password" below.'
	    	]);
    }

    /**
     * getLogout
     *
     * @return void
     */
    public function getLogout()
    {
    	$this->authboxgofer->logout();
    	return redirect('/login');
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     */
    public function getFbLogin()
    {
    	return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function getFbCallback()
    {
    	try {
    		$user = Socialite::driver('facebook')->user();
    	} catch (Exception $e) {
    		return redirect('/login');
    	}

    	$authUser = $this->findOrCreateUser($user);

    	$this->authboxgofer->loginUsingId($authUser->_id, true);

    	return redirect('/mystuff');
    }

    /**
     * Return user if exists; create and return if doesn't
     *
     * @param $facebookUser
     * @return User
     */
    private function findOrCreateUser($facebookUser)
    {
    	$authUser = Users::where('authData', $facebookUser->id)
    						->where('status', 1)->first();

    	if (!$authUser) {

    		// Get default place
    		$place = $this->places->getDefaultPlace();

    		$authUser = new Users();
    		//$authUser->place_id = '';//$facebookUser->place_id;
    		//
    		$nameStr = '';
    		$arr = explode(' ', $facebookUser->name);
    		$nameStr = $arr[count($arr) - 1];
    		$newstr = '';
    		for($i = 0; $i < 4; $i ++) {
    			$newstr .= isset($nameStr[$i]) ? $nameStr[$i] : '_';
    		}
    		$nameStr = strtoupper($newstr) . rand(100, 999) . '-' . $place->zipcodes[0];

    		$authUser->userid = $nameStr;
    		$authUser->zipcode = $place->zipcodes[0];
    		$authUser->city = $place->name;
    		$authUser->name = $facebookUser->name;
    		$authUser->email = $facebookUser->email;
    		$authUser->authData = $facebookUser->id;
    		$authUser->avatar = $facebookUser->avatar;
    		$authUser->status = 1;// Active
    		$authUser->save();
    	}

    	return $authUser;

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
