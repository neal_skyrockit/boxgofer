<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Http\Requests\LoginRequest;
use Illuminate\Contracts\Auth\Guard;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = 'admin';
    protected $loginPath = '/auth/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->auth = Auth::guard('managers');
    	//$this->auth = $auth;
        //$this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * postLogin
     *
     * @return void
     */
    public function postLogin(LoginRequest $request)
    {
    	$request->merge(['email' => $request->input('email')]);
    	$credentials = $request->only('email', 'password');

    	if ($this->auth->attempt($credentials, $request->has('remember')))
    	{
    		return redirect('admin');
    	}
    	elseif ($this->auth->validate($request->only('email', 'password')))
    	{
    		return redirect($this->loginPath)
    		->withInput($request->only('email', 'remember'))
    		->withErrors([
    				'error' => 'Your account does not match.',
    			]);
    	}

    	return redirect($this->loginPath)
	    	->withInput($request->only('email', 'remember'))
	    	->withErrors([
	   			'error' => $this->getFailedLoginMessage(),
	    	]);
    }

    /**
     * getLogout
     *
     * @return void
     */
    public function getLogout()
    {
    	$this->auth->logout();
    	return redirect('/auth/login');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        /* return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]); */
    }
}
