<?php

namespace App\Http\Helpers;

use Illuminate\Support\Facades\Config;
use \Illuminate\Support\Facades\Response;

class CommonHelper {

	/**
	 * generateBarcode
	 *
	 * @param $value
	 * @param $size
	 */
	public static function generateBarcode($value, $size = 4)
	{
		$key = Config::get('services.codereader');
		$req = $key['api_url'] . "/?section=barcode&action=generate"
			. "&api_key=" . $key['api_key']
			. "&value=" . $value
			. "&barcodetype=pdf417"
			. '&size=' . $size;

		return $req;
	}

	/**
	 * upsertBarcode
	 *
	 * @param $value
	 * @param $database_id
	 * @param $validity
	 */
	public static function upsertBarcode($value, $database_id = '423243', $validity = 1)
	{
		$key = Config::get('services.codereader');
		$req = $key['api_url'] . "?section=databases&action=upsertvalue"
		. "&api_key=" . $key['api_key']
		. "&database_id=423243"
		. "&value=" . $value
		. '&validity=' . $validity;

		$response = file_get_contents($req);

		$response = new \SimpleXMLElement($response);

		return $response->status;
	}

	/**
	 * showvaluesBarcode
	 *
	 * @param $database_id
	 */
	public static function showvaluesBarcode($database_id = '423243')
	{
		$key = Config::get('services.codereader');
		$req = $key['api_url'] . "?section=databases&action=showvalues"
		. "&api_key=" . $key['api_key']
		. "&database_id=" . $database_id;

		$response = file_get_contents($req);

		$response = new \SimpleXMLElement($response);

		return $response;
	}

	/**
	 * randomGen
	 *
	 * @param $length
	 * @return array
	 */
	public static function randomGen($length = 3)
	{
		$digits = '';
		$numbers = range(0,9);
		shuffle($numbers);
		for($i = 0; $i < $length; $i++) {
			$digits .= $numbers[$i];
		}

		return $digits;
	}

	/**
	 * processTime
	 *
	 * @param $str
	 * @return string
	 */
	public static function processTime($str)
	{
		if (strpos($str, '-') !== false) {
			$result = explode('-', $str);
		}

		return isset($result[1]) ? trim($result[1]) : '';
	}

	/**
	 * processDate
	 *
	 * @param $str
	 * @return string
	 */
	public static function processDate($str)
	{
		if (strpos($str, '-') !== false) {
			$result = explode('-', $str);
		} else {
			$result[0] = date('m/d/Y', strtotime($str));
		}

		return trim($result[0]);
	}

	/**
	 * processSpecialStr
	 *
	 * @param $data
	 * @return string
	 */
	public static function processSpecialStr($data)
	{
		$str = '';
		$arr = (strpos($data, ',') !== false) ? explode(',', $data) : array($data);
		if (count($arr) > 0)
		{
			$result = array_count_values($arr);

			if (isset($result['LG'])) {
				$str .=  $result['LG'] . 'LG';
			}
			if (isset($result['XL'])) {
				$str .=  ($str != '') ? ' - ' . $result['XL'] . 'XL' : $result['XL'] . 'XL';
			}
			if (isset($result['OS'])) {
				$str .=  ($str != '') ? ' - ' . $result['OS'] . 'OS' : $result['OS'] . 'OS';
			}
		}

		return $str;
	}

	/**
	 * checkAddress
	 * Check valid an address with Smartystress API
	 *
	 * @param string $address
	 * @return true|false
	 */
	public static function checkAddress($address) {

		$req = "https://api.smartystreets.com/street-address?"
		. "auth-id=ef98fd2a-a28d-3de6-db0e-2a5b6d845e92"
		. "&auth-token=839CwxIjpjq5Txwi27rQ"
		. "&candidates=10"
		. "&street=" . urlencode($address);
		$response = file_get_contents($req);
		$data = json_decode($response, true);

		return empty($data) ? false : true;
	}

	/**
	 * checkPhone
	 * Check valid a phone number with format (123) 456-7890
	 *
	 * @param string $phone
	 * @return true|false
	 */
	public static function checkPhone($phone)
	{
		$pattern = '/^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/';
		return preg_match($pattern, $phone);
	}

	/**
	 * timeDefined
	 * Special format time
	 *
	 * @return array
	 */
	public static function timeDefined()
	{
		return array(
    				['time' => ['9:00 am', '9:00 am - 10:00 am']],
    				['time' => ['10:00 am', '10:00 am - 11:00 am']],
    				['time' => ['11:00 am', '11:00 am - 12:00 pm']],
    				['time' => ['12:00 pm', '12:00 pm - 1:00 pm']],
    				['time' => ['1:00 pm', '1:00 pm - 2:00 pm']],
    				['time' => ['2:00 pm', '2:00 pm - 3:00 pm']],
    				['time' => ['3:00 pm', '3:00 pm - 4:00 pm']],
    				['time' => ['4:00 pm', '4:00 pm - 5:00 pm']],
    				['time' => ['5:00 pm', '5:00 pm - 6:00 pm']]
    		);
	}

	/**
	 * formatDateForEmail
	 * Format storage processtime (04/21/2016 - 9:00 am) to "Day of week, MMMM DD, YYYY - Time"
	 *
	 * @param string $processtime
	 * @return string
	 */
	public static function formatDateForEmail($processtime) {
		$availableTime = self::timeDefined();
		$dateExplode = explode(' ', $processtime);
		$timeStr = $dateExplode[2] .' '. $dateExplode[3];
		$dateArr = explode('/', $dateExplode[0]);
		$year = $dateArr[2];
		$month = $dateArr[0];
		$day = $dateArr[1];
		$daysName = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
		$dayOfWeek = $daysName[date('w', strtotime($year .'-'. $month .'-'. $day))];
		foreach ($availableTime as $item) {
			if ($item['time'][0] == $timeStr) {
				$time = $item['time'][1];
				break;
			}
		}
		return array(
				'date' => $dayOfWeek .', '. date('F d, Y', strtotime($year .'-'. $month .'-'. $day)),
				'time' => isset($time) ? $time : ''
		);
	}

	/**
	 * getRealIpAddr
	 * Get real customer Ip
	 *
	 * @return string
	 */
	public static function getRealIpAddr() {

		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}
		else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		return $ip;
	}

	/**
	 * generateStrongCode
	 *
	 * @param $length
	 * @return string
	 */
	public static function  generateStrongCode ($length = 7)
	{
		$sets = array('ABCDEFGHJKMNPQRSTUVWXYZ', '0123456789');

		$all = '';
		$code = '';
		foreach($sets as $set)
		{
			$code .= $set[array_rand(str_split($set))];
			$all .= $set;
		}

		$all = str_split($all);
		for($i = 0; $i < $length - count($sets); $i++)
		{
		$code .= $all[array_rand($all)];
		}

		return str_shuffle($code);
	}

	/**
	 * getAmountDay
	 * @param float $amount is basicly amount for each Bin = price
	 * @param integer $date is start of scription date
	 * If $date = 'now' is amount = $amount
	 *
	 * @return float Amount
	 */
	public static function getAmountDay($amount, $date) {

		$setingday = Config::get('services.paymentday');

		if ($date == 'now') return $amount;

		$now = date('Y-m-d');
		$date = date('Y-m-d', $date);
		// Now === start sub
		if ($date >= $now) return $amount;
		// Get renewal times
		$t = ceil(((strtotime($now) - strtotime($date)) / (60 * 60 * 24)) / $setingday['renewal']);
		$nextRenewal = strtotime('+' . $t * $setingday['renewal'] . ' days', strtotime($date));
		// Now === renewal
		if (date('Y-m-d', $nextRenewal) <= $now) return $amount;
		// When Now between Start sub and Renewal
		$day = floor(($nextRenewal - strtotime($now)) / (60 * 60 * 24));

		return number_format($amount * ($day / $setingday['renewal']), 2, '.', '');
	}

	/**
	 * getTrialday
	 * @param integer $firstday
	 * @return mixed
	 */
	public static function getTrialday( $firstday ) {

		// Get date
		$setingday = Config::get('services.paymentday');

		$currentDay = date_create(date('Y-m-d'));
		$newfirstday = date_create(date('Y-m-d', $firstday));
		$diff = date_diff($newfirstday, $currentDay);
		// Get renewal times
		$t = ceil($diff->format("%a") / $setingday['renewal']);
		$t = ($t == 0) ? 1 : $t;
		$renewal = strtotime('+' . $t * $setingday['renewal'] . ' days', $firstday);

		if (date('Y-m-d', $renewal) <= date('Y-m-d')) return 'now';

		return $renewal;
	}

	/**
	 * convertToCSV
	 * @param integer $firstday
	 * @return mixed
	 */
	public static function convertToCSV( $data, $fileName ) {

		//$fileName = 'deliveries-' . date('m-d-Y-h:i:s');
		$headers = array(
				'Content-Type' => 'text/csv',
				'Content-Disposition' => 'attachment; filename="' . $fileName . '.csv"'
		);

		// setting the columns for the csv. if columns provided, then fetching the or else object keys
		//$objectKeys = get_object_vars($data[0]);
		$columns = array('Barcode', 'Address', 'Phone', 'User', 'Time');
		$output = implode(',', $columns);
		$output .= "\n";

		// populating the main output string
		foreach ($data as $row) {
			foreach ($columns as $column) {
				if ($column == 'Barcode') {
					$field = $row->barcode;
				} else if ($column == 'Address') {
					$field = $row->address;
				} else if ($column == 'User') {
					$field = $row->relationUser->name;
				} else if ($column == 'Phone') {
					$field = $row->phone;
				} else {
					$field = CommonHelper::processTime($row->processtime);
				}
				$output .= str_replace(',', ' -', $field);
				$output .= ',';
			}
			$output .= "\n";
		}

		// calling the Response class make function inside my class to send the response.
		// if our class is not a controller, this is required.
		return Response::make($output, 200, $headers);
	}
}