<?php
namespace App;

use App\Http\Helpers\CommonHelper;

use Stripe\Token;
use Stripe\Stripe;
use Stripe\Plan;
use Stripe\Customer;
use Stripe\Event;
use Stripe\Invoice as SInvoice;
use Stripe\Charge;
use Stripe\Subscription;
use Stripe\Coupon;
use App\Models\Items;
use App\Models\Transactions;
use Config;

class StripeFuns{

     const large = 'Large';
     const extra = 'Extra';
     const other = 'Other';

     public function __construct(){
     	 $stripe = Config::get('services.stripe');
         Stripe::setApiKey($stripe['ApiKey']);
     }

     /**
      * createToken
      * https://stripe.com/docs/api/php#create_card_token
      *
      * @param array $arr
      * @return Object
      */
     public function createToken( $arr ){
     	try {
     		return Token::create(array(
     			"card" => array(
				    "number" => $arr['number'],
				    "exp_month" => $arr['month'],
				    "exp_year" => $arr['year'],
				    "cvc" => $arr['cvc']
				)
     		));
     	} catch(\Stripe\Error\Card $e) {
     		return null;
     	} catch (\Stripe\Error\RateLimit $e) {
     		return null;
     	} catch (\Stripe\Error\InvalidRequest $e) {
     		return null;
     	} catch (\Stripe\Error\Authentication $e) {
     		return null;
     	} catch (\Stripe\Error\ApiConnection $e) {
     		return null;
     	} catch (\Stripe\Error\Base $e) {
     		return null;
     	} catch (Exception $e) {
     		return null;
     	}

     	return null;
     }

     /**
      * createPlan
      * Remove trial day 20160628
      * Manh edit 20160628
      * https://stripe.com/docs/api/php#create_plan
      * Amount is cents
      *
      * @param array $plan
      * @return Object
      */
     public function createPlan($plan){
     	$setingday = Config::get('services.paymentday');
     	$plan['amount'] = ($plan['amount'] > 0) ? $plan['amount'] : 0;
        return Plan::create(array(
                "id" => $plan['id'],
                "name" => $plan['name'],
                "amount" => $plan['amount'] * 100,
                "currency" => 'USD',
        		"interval" => ($setingday['renewal'] == 30) ? 'month' : 'day',
        		"interval_count" => ($setingday['renewal'] == 30) ? 1 : $setingday['renewal']
               ));
     }

     /**
      * updatePlanName
      * Can not update for amount
      * Manh - 20160628
      */
     public function updatePlanName($id, $name) {

     	try {

     		$obj = Plan::retrieve($id);
	     	$obj->name = $name;
	     	$obj->save();

	     	return $obj;

     	} catch(\Stripe\Error\Card $e) {
     		// Since it's a decline, \Stripe\Error\Card will be caught
     		$body = $e->getJsonBody();
     		$err  = $body['error'];
     		return 'Message is:' . $err['message'];
     	} catch (\Stripe\Error\RateLimit $e) {
     		// Too many requests made to the API too quickly
     	} catch (\Stripe\Error\InvalidRequest $e) {
     		$body = $e->getJsonBody();
     		$err  = $body['error'];
     		return 'Message is:' . $err['message'];
     		// Invalid parameters were supplied to Stripe's API
     	} catch (\Stripe\Error\Authentication $e) {
     		$body = $e->getJsonBody();
     		$err  = $body['error'];
     		return 'Message is:' . $err['message'];
     		// Authentication with Stripe's API failed
     		// (maybe you changed API keys recently)
     	} catch (\Stripe\Error\ApiConnection $e) {
     		$body = $e->getJsonBody();
     		$err  = $body['error'];
     		return 'Message is:' . $err['message'];
     		// Network communication with Stripe failed
     	} catch (\Stripe\Error\Base $e) {
     		// Display a very generic error to the user, and maybe send
     		// yourself an email
     		$body = $e->getJsonBody();
     		$err  = $body['error'];
     		return 'Message is:' . $err['message'];
     	} catch (Exception $e) {
     		$body = $e->getJsonBody();
     		$err  = $body['error'];
     		return 'Message is:' . $err['message'];
     		// Something else happened, completely unrelated to Stripe
     	}

     }

     /**
      * planRetrieve
      */
     public function planRetrieve($planId)
     {
     	try {

     		if (empty($planId)) return null;

     		$plan = Plan::retrieve($planId);

     	} catch(\Stripe\Error\Card $e) {
     		return null;
     	} catch (\Stripe\Error\RateLimit $e) {
     		return null;
     	} catch (\Stripe\Error\InvalidRequest $e) {
     		return null;
     	} catch (\Stripe\Error\Authentication $e) {
     		return null;
     	} catch (\Stripe\Error\ApiConnection $e) {
     		return null;
     	} catch (\Stripe\Error\Base $e) {
     		return null;
     	} catch (Exception $e) {
     		return null;
     	}

     	return $plan;
     }

     /**
      * RemoveAllPlan
      */
     public function RemoveAllPlan(){
        $listPlan =  Plan::all();
        if (count($listPlan->data) > 0) {
        	foreach($listPlan->data as $p){
        		$p->delete();
        	}
        }
     }

     /**
      * createCustomer
      *
      * @param $tok_id
      * @param $email
      * @return Object
      */
     public function createCustomer($tok_id, $email)
     {
        return Customer::create(array(
            "description" => "Customer",
            "source" => $tok_id,
        	"email"	=> $email
       ));
     }

     /**
      * updateCustomer
      *
      * @param $cus_id
      * @param $tok_id
      * @param $des
      * @return Object
      */
     public function updateCustomer($cus_id, $tok_id, $des = '')
     {
     	$customer = $this->customerRetrieve($cus_id);
     	if ($des != '') $customer->description = $des;
     	$customer->source = $tok_id;
     	$customer->save();

     	return $customer;
     }

     /**
      * createSubscription
      * https://stripe.com/docs/api/php#create_subscription
      *
      * @param string $customerId is customerId
      * @param array $planArr['id', 'name', 'amount']
      * @param array $data
      * @param mixed $trialdays is Unix timestamp or "now" for charge immediately
      *
      * @return object
      */
     public function createSubscription($customerId, $planArr, $data, $trialdays = "now") {

     	$cu = $this->customerRetrieve($customerId);

     	if(empty($cu)) return null;

     	$couponValue = 0;
     	$coupon = $this->couponRetrieve($planArr['promo_code']);
     	if ($coupon != null) {
     		$couponValue = $coupon->percent_off;
     		$coupon = $planArr['promo_code'];
     	}

     	$plan = $this->createPlan($planArr);

        $sub = $cu->subscriptions->create(array(
            "plan" => $plan->id,
            "quantity" => 1,
        	"trial_end" => $trialdays,
        	"coupon" => $coupon,
        	"metadata" => array(
        			'username' => $data['username'],
        			'email' => $data['email'],
        			'address' => $data['address'],
        			'phone' => $data['phone'],
        			'addressstreet' => $data['addressstreet'],
        			'cityzipcode' => $data['cityzipcode'],
        			'aptsuite' => $data['aptsuite'],
        			//'card_number' => $data['card_number'],
        			'item'	=> $data['item'],
        			'userid' => $data['userid'],
        			'orderId' => $data['orderId'],
        			'listitems' => $data['listitems']
        		)
        ));

        return empty($sub) ? null : array($couponValue, $sub);
     }

     /**
      * retrieveCustomer
      */
     /* private function retrieveCustomer($tok_id){
        return Customer::create(array(
             	"description" => "Customer",
             	"source" => $tok_id
       ));
     } */

     /**
      * Charge
      * scanned_flag for cron task payment
      */
     public function Charge($charge, $orderBins, $user, $dropoffDate, $dropoffTime){

     	$orderId = $this->getOrderId();

     	if ($orderId == null) return ['error' => 'Can not create Order'];

     	// Create customer
        $token = $this->createToken($charge);

        if (empty($token)) return ['error' => 'Can not create token'];

        $cus = $this->customerRetrieve($charge['user_stripe_id']);

        if (empty($cus)) {
        	$cus = $this->createCustomer($token->id, $user['email']);
        } else {
        	$cus = $this->updateCustomer($cus->id, $token->id);
        }

        $payment = array();
        $payment['type'] = 'stripe';
        $payment['token_id'] = $token->id;
        $payment['customer_id'] = $cus->id;
        $payment['total'] = 0;
        $payment['orderId'] = $orderId;
        $payment['promo_code'] = $charge['promo_code'];

        foreach($orderBins as $bin) {
            if($bin['quantity'] > 0){
            	$itemCode =  ($bin['type'] == Items::ItemTypeLarge) ? 'Large' : ( $bin['type'] == Items::ItemTypeExtraLarge ? 'Extra' : 'Other' );
                $payment[$itemCode] = [
                    'price' => $bin['price'],
                    'quantity' => $bin['quantity'],
                	'scanned' => 0,
                	'scanned_flag' => 0
                ];
            }
        }

        $payment['user'] = $user;
        $payment['dropoffDate'] = $dropoffDate;
        $payment['dropoffTime'] = $dropoffTime;

        return $payment;
     }

     /**
      * getOrderId
      */
     private function getOrderId() {

     	for($i = 0; $i < 10; $i ++) {
     		$orderId = CommonHelper::generateStrongCode(7);
     		$trans = Transactions::where('transcode', $orderId)->first();
     		if (empty($trans)) return $orderId;
     	}

     	return null;
     }

     /**
      * cancelOrder
      * https://stripe.com/docs/api/php#cancel_subscription
      *
      * @param $planId
      * @param $subcriptionId
      * @return mixed
      */
     public function cancelOrder( $planId, $subcriptionId ) {

     	try {

     		// Remove plan
     		$plan = $this->planRetrieve($planId);
     		if (!empty($plan)) $plan->delete();
     		// Remove subscription
     		$subscription = $this->subscriptionRetrieve($subcriptionId);
     		//$subscription->cancel(array('at_period_end' => true));
     		if (!empty($subscription)) $subscription->cancel();

     	}
     	catch (Exception $e) {
     		$body = $e->getJsonBody();
     		$err  = $body['error'];
     		return ['error' => $err];
     	}

     	return 1;
     }

     /**
      * updatePlanSubscription
      * https://stripe.com/docs/api/php#update_subscription
      *
      * @param string $customerId
      * @param string $subcriptionId
      * @param array $planArr
      * @param string $planId
      * @param integer $trialday Unix timestamp
      *
      * @return object
      */
     public function updatePlanSubscription($customerId, $subcriptionId, $planArr, $oldPlanId, $data, $trialday) {

     	// Delete old plan
     	$old = $this->planRetrieve($oldPlanId);
     	if (!empty($old)) $old->delete();

     	// Create new plan
     	$plan = $this->createPlan($planArr);
     	$cu = $this->customerRetrieve($customerId);

     	$subscription = $cu->subscriptions->retrieve($subcriptionId);
     	// Coupon
     	/* $coupon = $this->couponRetrieve($planArr['promo_code']);
     	if ($coupon != null && empty($subscription->coupon)) {
     		$subscription->coupon = $planArr['promo_code'];
     	} */
     	$subscription->plan = $plan->id;
     	$subscription->trial_end = $trialday;
     	$listitems = $subscription->metadata['listitems'];
     	$arr = (strpos($listitems, '-') !== false) ? explode('-', $listitems) : array($listitems);
     	// Re-do Items
     	if (count($arr) > 0) {
     		$newList = '';
     		// Process Item list
     		if (isset($data['item'])) {
     			$newarr = array();
     			$arr1 = (strpos($data['item'], '-') !== false) ? explode('-', $data['item']) : array($data['item']);
     			foreach ($arr1 as $foo) {
     				//$temp = explode(':', $foo);
     				if ($foo[1] == 'L') {
     					$newarr['L'] = $foo[0];
     				} else if ($foo[1] == 'E') {
     					$newarr['E'] = $foo[0];
     				} else {
     					$newarr['O'] = $foo[0];
     				}
     			}
     		}

     		foreach ($arr as $f) {
     			$temp = explode(':', $f);
     			if ($temp[0] == 'L') {
     				if (isset($newarr['L'])) {
     					$temp[1] = $temp[1] + $newarr['L'];
     				} else if (isset($data['remove_items']['Large'])) {
     					$temp[1] = $temp[1] - $data['remove_items']['Large'];
     				}
     			} else if ($temp[0] == 'E') {
     				if (isset($newarr['E'])) {
     					$temp[1] = $temp[1] + $newarr['E'];
     				} else if (isset($data['remove_items']['Extra'])) {
     					$temp[1] = $temp[1] - $data['remove_items']['Extra'];
     				}
     			} else {
     				if (isset($newarr['O'])) {
     					$temp[1] = $temp[1] + $newarr['O'];
     				} else if (isset($data['remove_items']['Other'])) {
     					$temp[1] = $temp[1] - $data['remove_items']['Other'];
     				}
     			}
     			$newList .= ($newList == '') ? implode(':', $temp) : '-' . implode(':', $temp);
     		}

     		$subscription->metadata['listitems'] = $newList;
     	}
     	else
     	{
     		$subscription->metadata['listitems'] = $data['listitems'];
     	}

     	if (isset($data['item']))
     		$subscription->metadata['item'] = $data['item'];
     	// Save
     	$subscription->save();

     	return $subscription;
     }

     /**
      * UpdateSubscription
      */
     public function UpdateSubscription($paymentInfo, $itemtype){

        $key = $itemtype == Items::ItemTypeLarge ? self::large
                : ($itemtype == Items::ItemTypeExtraLarge ? self::extra : self::other );
        $cu = $this->customerRetrieve($paymentInfo['customer_id']);

        if(!isset($paymentInfo['plan'][$key]['id']))
            return [
                'error'=>true,
                'message'=>'Subscription not avaiable'
            ];

        $subscription = $cu->subscriptions->retrieve($paymentInfo['plan'][$key]['id']);
        $unitPrice = $subscription['plan']['amount'];
        // Manh - add flag detect using bin 20160310
        $paymentInfo['totalUsingBin'] = 0;

        if($subscription->quantity == 1){
            $subscription->cancel(array('at_period_end' => true));
            unset($paymentInfo['plan'][$key]);
        }else{
            $subscription->quantity --;
            $paymentInfo['totalUsingBin'] = $paymentInfo['plan'][$key]['quantity'] = $subscription->quantity;
            $subscription->save();
        }

        $paymentInfo['total'] -= ($unitPrice/100);

        return $paymentInfo;
     }

     /**
      * excuteSubscription
      * Excute a subscription immediately
      *
      * @param $paymentInfo
      *
      * @since 16/05/2016
      * @author Manh - change logic
      */
     /* public function excuteSubscription($paymentInfo) {

     	try {

     		$planArr['id'] = date('Ymdhis');
     		$planArr['name'] = $paymentInfo['orderId'] . '-' . date('Y-m-d h:i:s');
     		$planArr['amount'] = $paymentInfo['total'];
     		$subscription = $this->createSubscription($paymentInfo['customer_id'], $planArr, $paymentInfo['user'], $paymentInfo['orderId'], $trialdays = "now");

     		return $subscription;

     	} catch (Exception $e) {
     		$body = $e->getJsonBody();
     		$err  = $body['error'];
     		return [
	     		'error' => true,
	     		'message' => $err
     		];
     	}
    } */

     /**
      * customerRetrieve
      *
      * @param $customerId
      * @return mixed
      */
     public function customerRetrieve($customerId)
     {
     	if (empty($customerId)) return null;

     	try
     	{

     		$cus = Customer::retrieve($customerId);
     		return $cus->deleted ? null : $cus;

     	} catch(\Stripe\Error\Card $e) {
     		return null;
     	} catch (\Stripe\Error\RateLimit $e) {
     		return null;
     	} catch (\Stripe\Error\InvalidRequest $e) {
     		// Invalid parameters were supplied to Stripe's API
     		return null;
     	} catch (\Stripe\Error\Authentication $e) {
     		// Authentication with Stripe's API failed
     		// (maybe you changed API keys recently)
     		return null;
     	} catch (\Stripe\Error\ApiConnection $e) {
     		// Network communication with Stripe failed
     		return null;
     	} catch (\Stripe\Error\Base $e) {
     		// Display a very generic error to the user, and maybe send
     		// yourself an email
     		return null;
     	} catch (Exception $e) {
     		// Something else happened, completely unrelated to Stripe
     		return null;
     	}

     	return null;
     }

     /**
      * getEvent
      */
     public function getEvent($id)
     {
     	return Event::retrieve($id);
     }

     /**
      * createCharge
      * https://stripe.com/docs/api/php#create_charge
      *
      * @param string $customerId The ID of an existing customer that will be charged in this request
      * @param integer $amount This is dola unit type
      * @param string $des This is a description
      */
     public function createCharge($customerId, $amount, $des = '', $metaData = NULL)
     {
     	return Charge::create(array(
				"amount" => $amount * 100 ,
				"currency" => "usd",
				"customer" => $customerId, // obtained with Stripe.js
				"description" => $des,
     			"metadata" => $metaData
			));
     }

     /**
      * invoiceRetrieve
      */
     public function invoiceRetrieve($id)
     {
     	return SInvoice::retrieve($id);
     }

     /**
      * List customers
      *
      * @param $limit default = 10
      * @return object
      */
     public function listCustomers( $limit = 10 )
     {
     	return Customer::all( array( "limit" => $limit ) );
     }

     /**
      * Delete customers
      *
      *	@param $limit between 1-100
      * @return void
      */
     public function removeListCustomers( $limit = 10 )
     {
     	$limit = ($limit > 100) ? 100 : $limit;
     	$customers = Customer::all( array( "limit" => $limit ) );
     	foreach ($customers['data'] as $cus) {
     		$obj = $this->customerRetrieve( $cus->id );
     		$obj->delete();
     	}
     }

     /**
      * Delete plans
      *
      *	@param $limit between 1-100
      * @return void
      */
     public function removeListPlan( $limit = 10 )
     {
     	$limit = ($limit > 100) ? 100 : $limit;
     	$plans = Plan::all( array( "limit" => $limit ) );
     	foreach ($plans['data'] as $plan) {
     		$obj = $this->planRetrieve( $plan->id );
     		$obj->delete();
     	}
     }

     /**
      * subscriptionRetrieve
      */
     public function subscriptionRetrieve($id)
     {
     	try
     	{
     		if (empty($id)) return null;
     		return Subscription::retrieve($id);

     	} catch(\Stripe\Error\Card $e) {
     		return null;
     	} catch (\Stripe\Error\RateLimit $e) {
     		return null;
     	} catch (\Stripe\Error\InvalidRequest $e) {
     		// Invalid parameters were supplied to Stripe's API
     		return null;
     	} catch (\Stripe\Error\Authentication $e) {
     		// Authentication with Stripe's API failed
     		// (maybe you changed API keys recently)
     		return null;
     	} catch (\Stripe\Error\ApiConnection $e) {
     		// Network communication with Stripe failed
     		return null;
     	} catch (\Stripe\Error\Base $e) {
     		// Display a very generic error to the user, and maybe send
     		// yourself an email
     		return null;
     	} catch (Exception $e) {
     		// Something else happened, completely unrelated to Stripe
     		return null;
     	}

     }

     /**
      * createCoupon
      * https://stripe.com/docs/api#create_coupon
      *
      * @param string $id Unique string that will be used to identify this coupon (Code key)
      * @param integer $percent A positive integer between 1 and 100
      *
      * @return object
      */
     public function createCoupon($id, $percent)
     {
     	return Coupon::create(array(
     			"id" => $id,
     			"percent_off" => $percent,
     			"currency" => "usd",
     			"duration" => "once",
     	));
     }

     /**
      * couponRetrieve
      * https://stripe.com/docs/api#retrieve_coupon
      *
      * @param string $id Unique string that will be used to identify this coupon (Code key)
      * @return mixed
      */
     public function couponRetrieve($id)
     {
     	try
     	{
     		if (empty($id)) return null;
     		return Coupon::retrieve($id);

     	} catch(\Stripe\Error\Card $e) {
     		return null;
     	} catch (\Stripe\Error\RateLimit $e) {
     		return null;
     	} catch (\Stripe\Error\InvalidRequest $e) {
     		return null;
     	} catch (\Stripe\Error\Authentication $e) {
     		return null;
     	} catch (\Stripe\Error\ApiConnection $e) {
     		return null;
     	} catch (\Stripe\Error\Base $e) {
     		return null;
     	} catch (Exception $e) {
     		return null;
     	}
     }

     /**
      * Delete coupon
      * https://stripe.com/docs/api#delete_coupon
      *
      *	@param $id
      * @return boolean
      */
     public function deleteCoupon( $id )
     {
     	$obj = $this->couponRetrieve( $id );
     	return empty($obj) ? false : $obj->delete();
     }

     /**
      * List coupon
      */
     public function listCoupon( $limit ){
     	return Coupon::all(array('limit' => $limit));
     }
}