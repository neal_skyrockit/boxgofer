<?php
namespace App\Models;

// use Jenssegers\Mongodb\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Places extends Eloquent {

	protected $fillable = ['zipcodes', 'name','full'];

	/**
	 * getPlaceNameList
	 */
	public function getPlaceNameList() {
		return Places::all();
	}

	/**
	 * getPlaceById
	 */
	public function getPlaceById($id) {
		return Places::find($id);
	}

	/**
	 * getPlaceByZipcode
	 */
    public function getPlaceByZipcode($code) {
		return Places::where('zipcodes','all',array(''.$code) )->first();
	}

	/**
	 * getDefaultPlace
	 */
	public function getDefaultPlace() {
		return Places::first();
	}

	/**
	 * checkZipCode
	 */
	public function checkZipCode($code) {
		$result = Places::where('zipcodes','all',array(''.$code) )->first();
		return (!empty($result)) ? $result->_id : 0;
	}

    public function checkCityXZipCode($city,$code) {
		$result = Places::where('name','=',$city)->first();
        if(empty($result))
            return 0;
        if(!in_array($code,$result))
            return 0;

		return  $result->_id ;
	}

	/**
	 * Create a new places
	 *
	 * @param  array  $data
	 * @return Transactions
	 */
	public static function createPlaces()
	{
		if (1) return 'Comment this line !';

		Places::truncate();

        $data = array(
            'Addison' => ['75001'],
            'Garland' => ['75044'],
            'Richardson' => ['75080','75081','75082'],
            'Plano' => ['75023','75024','75025','75074','75075','75093'],
            'Dallas' => ['75204','75206','75209','75214','75219','75231','75248','75252','75287','75244'],
        	'Highland Park' => ['75205'],
        	'University Park' => ['75225'],
        );

		//$data = array(
			// '75001' => 'Addison',
			// '75044' => 'Garland',
			// '75080' => 'Richardson',
			// '75081' => 'Richardson',
			// '75082' => 'Richardson',
			//'75023' => 'Plano',
			// '75024' => 'Plano',
			// '75025' => 'Plano',
			// '75074' => 'Plano',
			// '75075' => 'Plano',
			// '75093' => 'Plano',
			// '75204' => 'Dallas',
			// '75205' => 'Highland Park',
			// '75206' => 'Dallas',
			// '75209' => 'Dallas',
			// '75214' => 'Dallas',
			// '75219' => 'Dallas',
			// '75225' => 'University Park',
			// '75231' => 'Dallas',
			// '75248' => 'Dallas',
			// '75252' => 'Dallas',
			// '75287' => 'Dallas',
			//'75244' => 'North Branch',
		//);

		foreach ($data as $k => $v) {
			$places = new Places();
			$places->name =$k;
			$places->zipcodes = $v;
            $f = [];
            foreach($places->zipcodes as $zip){
               $f[$zip] =  $places->name . ', ' . 'TX ' . $zip;
            }
            $places->full = $f;
			$places->save();
		}

		return 'Done !';
	}

}