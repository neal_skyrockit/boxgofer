<?php
namespace App\Models;

// use Jenssegers\Mongodb\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use \DateTime;
use App\Models\Storage;
use App\Models\Users;
use App\Http\Helpers\CommonHelper;

/**
 * Calendars class
 * Use for check datetime and show schedule in dashboard admin
 * Need create a cron task for delete old records
 */

class Calendars extends Eloquent {

	protected $fillable = [ 'userid', 'datetime', 'datesearch', 'date', 'time',
							'address', 'storagestatus', 'itemtype', 'storageid'];

	protected $guarded = [];
	//
	protected $order = 'ASC';
	protected $orderBy = 'created_at';

	protected $perPage = 15;
	protected $paginationPath = '/admin/calendars';

	/**
	 * __construct
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * search
	 */
	public function search($cons = null, $perPage = false,  $path = false, $orderBy = false, $order = false) {

		$perPage = $perPage ? $perPage : $this->perPage;
		$paginationPath = $path ? $path : $this->paginationPath;
		$order = $order ? $order : $this->order;
		$orderBy = $orderBy ? $orderBy : $this->orderBy;
		$userModel = new Users();
		$usersDeactived = $userModel::where('status', 0)->lists('_id')->toArray();

		// Build query
		$q = Calendars::query();

		$q->whereNotIn('userid', $usersDeactived);	// Chuong: Not show calendar of deactived users

		if (isset($cons['date']) && !empty($cons['date'])) {
			$start = $cons['date'] . ' - 00:00:00';
			$end = $cons['date'] . ' - 23:59:59';
			$q->where('datetime', '>=', $start);
			$q->where('datetime', '<=', $end);
		}

		$storage = $q->orderBy($orderBy, $order)->groupBy('datetime')->paginate($perPage);
		$storage->setPath($paginationPath);

		return $storage;

	}

	/**
	 * getCalendarByMonth
	 *
	 * @param string $str
	 */
	public function getDataByMonthYear($str) {
		$userModel = new Users();
		$usersDeactived = $userModel::where('status', 0)->lists('_id')->toArray();

		// Build query
		$q = Calendars::query();
		$q->whereNotIn('userid', $usersDeactived);	// Chuong: Not show calendar of deactived users
		$start = date('m/01/Y',strtotime($str));
		$end = date('m/t/Y',strtotime($str));
		$q->where('datesearch', '>=', strtotime($start));
		$q->where('datesearch', '<=', strtotime($end));
		$result = $q->orderBy('time', 'DESC')->get();

		$data = null;

		if (!empty($result)) {

			foreach ($result as $foo) {

				$getDay = explode('/', $foo->date);

				if(!isset($data[$getDay[0]]['timearr']))
				{
					$data[$getDay[0]]['timearr'] = [$foo->time];
				}
				else if ($data[$getDay[0]]['time'] != $foo->time)
				{
					array_push($data[$getDay[0]]['timearr'], $foo->time);
					usort($data[$getDay[0]]['timearr'], function ($item1, $item2) {
						return (strtotime($item1) >= strtotime($item2));
					});
				}

				$data[$getDay[0]]['date'] = $foo->date;
				$data[$getDay[0]]['time'] = $foo->time;
				if(!isset($data[$getDay[0]]['storagestatus'])) {
					$data[$getDay[0]]['storagestatus'] = [$foo->time => $foo->storagestatus];
				} else {
					$data[$getDay[0]]['storagestatus'][$foo->time] = $foo->storagestatus;
				}
			}
		}

		return $data;
	}

	/**
	 * getDataByDate
	 */
	public static function getDataByDate($date) {
		$userModel = new Users();
		$usersDeactived = $userModel::where('status', 0)->lists('_id')->toArray();

		// Build query
		$q = Calendars::query();
		$q->whereNotIn('userid', $usersDeactived);	// Chuong: Not show calendar of deactived users
		$q->where('date', '=', $date);
		$result = $q->orderBy('time', 'DESC')->get();

		$data = null;

		if (!empty($result)) {

			$obj = new Storage();
			$i = 0;

			foreach ($result as $foo) {

				// Get storage
				$storage = $obj->find($foo->storageid);

				if (($i != 0) && ($data[$i - 1]['time'] == $foo->time)
						&& ($data[$i - 1]['storagestatus'] == $foo->storagestatus))
				{
					$str = $data[$i - 1]['itemtype'];
					$data[$i - 1]['itemtype'] .= !empty($str) ? ',' . $foo->itemtype : $foo->itemtype;
					$data[$i - 1]['totalbin'] ++;
					$data[$i - 1]['itemdescript'] = CommonHelper::processSpecialStr($data[$i - 1]['itemtype']);
				}
				else
				{
					$data[$i]['userid'] = $foo->userid;
					$data[$i]['datetime'] = $foo->datetime;
					$data[$i]['time'] = $foo->time;
					$data[$i]['address'] = $foo->address;
					$data[$i]['itemtype'] = $foo->itemtype;
					$data[$i]['storagestatus'] = $foo->storagestatus;
					$data[$i]['totalbin'] = 1;
					$data[$i]['phone'] = $storage->phone;
					$data[$i]['content'] = $storage->transactions->content;
					$data[$i]['itemdescript'] = CommonHelper::processSpecialStr($data[$i]['itemtype']);
					$data[$i]['username'] = $storage->relationUser->name;
					$data[$i]['orderid'] = $storage->transactions->transcode;
					$data[$i]['otherdes'] = $storage->transactions->otherdes;
					if ($storage->item->type == 3) {
						$data[$i]['otherdes'] = $storage->transactions->otherdes;
					}

					$i ++;
				}
			}

			// Sort
			if ($data != null) {
				usort($data, function ($item1, $item2) {
					return (strtotime($item1['time']) >= strtotime($item2['time']));
				});
			}

		}

		return $data;
	}

	/**
	 * getCalendarByDate
	 * This function use in admin dashboard page
	 */
	public function getCalendarByDate($date) {
		$userModel = new Users();
		$usersDeactived = $userModel::where('status', 0)->lists('_id')->toArray();

		// Build query
		$q = Calendars::query();
		$q->whereNotIn('userid', $usersDeactived);	// Chuong: Not show calendar of deactived users
		$q->where('date', '=', $date);
		$result = $q->orderBy('time', 'DESC')->get();

		$data = null;

		if (!empty($result)) {

			$i = 0;
			foreach ($result as $foo) {

				if (($i != 0) && ($data[$i - 1]['time'] == $foo->time)
						&& ($data[$i - 1]['storagestatus'] == $foo->storagestatus))
				{
					$str = $data[$i - 1]['itemtype'];
					$data[$i - 1]['itemtype'] .= !empty($str) ? ',' . $foo->itemtype : $foo->itemtype;
					$data[$i - 1]['totalbin'] ++;
				}
				else
				{
					$data[$i]['userid'] = $foo->userid;
					$data[$i]['datetime'] = $foo->datetime;
					$data[$i]['time'] = $foo->time;
					$data[$i]['address'] = $foo->address;
					$data[$i]['itemtype'] = $foo->itemtype;
					$data[$i]['storagestatus'] = $foo->storagestatus;
					$data[$i]['totalbin'] = 1;

					$i ++;
				}
			}

			// Sort
			if ($data != null) {
				usort($data, function ($item1, $item2) {
					return (strtotime($item1['time']) >= strtotime($item2['time']));
				});
			}

		}

		return $data;
	}

	/**
	 * isAvailable
	 * Use to check Pick-up or Drop-off time is available or not
	 * Requirement:
	 * 1. During block time is one-hour
	 * 2. Greater current time
	 *
	 * @param array $post
	 * @return true|false
	 */
	public function isAvailable($post) {

		$duringTime = 3600; // 1 Hour
		$start = date('d/m/Y - g:i A', strtotime($post['date'] . ' ' . $post['time']) - $duringTime);
		$end = date('d/m/Y - g:i A', strtotime($post['date'] . ' ' . $post['time']) + $duringTime);

		$now = strtotime('now');

		if ($now > (strtotime($post['date']. ' ' . $post['time']) - $duringTime)) return ['error' => 'Must greater current time is one hour.'];

		$q = Calendars::query();
		$q->where('datetime', '>=', $start);
		$q->where('datetime', '<=', $end);
		$result = $q->get();

		return (count($result) == 0) ? 1 : ['error' => 'This time was booked.'];
	}

	/**
	 * getCalendarsByDate
	 * To check available time
	 *
	 * @param string $date
	 * @return Object
	 */
	public function getCalendarsByDate($date) {

		$date = date('d/m/Y', strtotime($date));
		$userModel = new Users();
		$usersDeactived = $userModel::where('status', 0)->lists('_id')->toArray();
		$q = Calendars::query();
		$q->whereNotIn('userid', $usersDeactived);	// Chuong: Not show calendar of deactived users
		$q->where('date', '=', $date);

		return $q->pluck('time')->toArray();
	}

	/**
	 * getCalendarById
	 *
	 * @param string $id
	 * @return mixed
	 */
	public function getCalendarById($id) {
		return Calendars::find($id);
	}

	/**
	 * createCalendar
	 *
	 * @param array $params
	 * @return mixed
	 */
	public static function createCalendar($params)
	{
		$item = new Calendars();

		$item->userid = $params['userid'];
		$item->datetime = $params['datetime'];
		// Fix for show available time
		$item->date = $params['date'];
		$item->datesearch = $params['datesearch'];
		$item->time = $params['time'];
		$item->address = $params['address'];
		$item->storagestatus = $params['storagestatus'];
		$item->itemtype = $params['itemtype'];
		$item->storageid = $params['storageid'];

		$item->save();

		return $item;
	}

	/**
	 * sampleCalendars
	 *
	 * @return mixed
	 */
	public function sampleCalendars()
	{
		if (1 == 1) return 'Comment this line!';

		$item = new Calendars();

		$item->userid = '123';
		$item->datetime = '20/03/2016 - 8:00 PM';
		$item->date = '20/03/2016';
		$item->time = '8:00 PM';
		$item->address = '2627 Live Oak Dr1, TX 75204, Dallas, Texas';
		$item->storagestatus = 'Drop-off';
		// LG: Large Bin, XL: Extra Large Bin, OS: Other Bin
		$item->itemtype = 'LG';
		$item->storageid = '012345678';

		$item->save();
	}

	/**
	 * deleteCalendarByStorageId
	 *
	 * @param string $storageId
	 * @return true
	 */
	public function deleteCalendarByStorageId( $storageId ) {
		$calen = Calendars::where('storageid', $storageId)->get();
		if (count($calen)) {
			foreach ($calen as $foo) {
				$foo->delete();
			}
		}

		return true;
	}

	/**
	 * getDisableDate
	 * To check disable date
	 * @param String $date
	 * @return array of disable date string('YYYY-MM-DD')
	 */
	public function getDisableDate($date = false) {
		$avaibleTimeOfDate = [];
		$fromDate = $date ? date('m/d/Y', strtotime($date)) : date('m/d/Y', strtotime("midnight"));
		$toDate = date('m/d/Y', strtotime($fromDate ."+45 days"));
		$userModel = new Users();
		$usersDeactived = $userModel::where('status', 0)->lists('_id')->toArray();
		$q = Calendars::query();
		$q->whereNotIn('userid', $usersDeactived);	// Chuong: Not show calendar of deactived users
		$q->where('datesearch', '>=', strtotime($fromDate));
		$q->where('datesearch', '<=', strtotime($toDate));
		$q->orderBy('datesearch', 'ASC');

		foreach ($q->get() as $foo) {
			$dataToArray = explode('/', $foo->date);
			$formatDate = $dataToArray[2] .'-'. $dataToArray[1] .'-'. $dataToArray[0] ;	// Format to string "YYYY-MM-DD"

			if (! isset($avaibleTimeOfDate[$formatDate])) {
				$avaibleTimeOfDate[$formatDate] = [$foo->time];
			} else {
				if (! in_array($foo->time, $avaibleTimeOfDate[$formatDate])) {
					$avaibleTimeOfDate[$formatDate][] = $foo->time;
				}
			}
		}

		$disableDate = $this->findDisableDate($avaibleTimeOfDate);
		return $disableDate;
	}

	/**
	 * findDisableDate
	 * @param array $avaibleTimeOfDate
	 * @return array of disable date
	 */
	private function findDisableDate($avaibleTimeOfDate) {
		$avaibleTime = CommonHelper::timeDefined();
		$result = [];

		foreach ($avaibleTimeOfDate as $keyDate => $listTime) {
			$avaibleTimeClone = $avaibleTime;

			// Saturday start 9 am - 5 pm
			if (date('w', strtotime($keyDate)) == 6) {
				array_shift($avaibleTimeClone);
				array_pop($avaibleTimeClone);
			}

			if (count($avaibleTimeClone) == count($listTime)) {
				$result[] = date('Y-m-d', strtotime($keyDate));
			}
		}

		return $result;
	}
}