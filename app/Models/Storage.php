<?php
namespace App\Models;

use App\Http\Helpers\CommonHelper;

use \DateTime;
use MongoDB\BSON\UTCDateTime;
// use Jenssegers\Mongodb\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
//use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
//use Jenssegers\Mongodb\Eloquent\HybridRelations;

use App\Models\Transactions;
use App\Models\Users;
use App\StripeFuns;
use App\PaypalFuns;
use App\GoferMail;

class Storage extends Eloquent {

	protected $collection = 'storage';
	/**
	 * Important
	 * processtime correspond dropofftime or pickuptime by status
	 * This is bad solution T_T
	 */
	protected $fillable = [ 'trans_id', 'user_id', 'item_id', 'barcode', 'images',
							'status', 'phone', 'zipcode', 'address', 'processtime',
							'dropofftime', 'pickuptime', 'canceltime', 'content',
							'workingstatus', 'searchdate', 'transcode', 'customername',
							'warehouse_barcode', 'warehouse_city', 'warehouse_code',
							'warehouse_aisle', 'warehouse_section', 'warehouse_shelf',
							'intransit',// Fix for customer logic - Flag to count Items which are In Transit. If == 0 not in-transit
							'addressstreet', 'cityzipcode', 'aptsuite',
							'isCancel'
							// Manh add flag for cancel bin (Scan to It4) process at end of day
							// isCancel = 1 this is empty bin
						];

	protected $guarded = [];
	//
	protected $order = 'DESC';
	protected $orderBy = 'created_at';
	protected $perPage = 10;
	protected $paginationPath = '/mystuff';

	protected $perAdminPage = 15;
	protected $paginationAdminPath = '/admin/bintracking';
	protected $paginationWarehousePath = '/admin/warehouse';

	/**
	 * __construct
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * search
	 */
	public function search($cons = null, $perPage = false,  $path = false, $orderBy = false, $order = false) {

		$perPage = $perPage ? $perPage : $this->perPage;
		$paginationPath = $path ? $path : $this->paginationPath;
		$order = $order ? $order : $this->order;
		$orderBy = $orderBy ? $orderBy : $this->orderBy;
		$userModel = new Users();
		//$usersDeactived = $userModel::where('status', 0)->lists('_id')->toArray();

		// Build query
		$q = Storage::query();

		//$q->whereNotIn('user_id', $usersDeactived);	// Chuong: Not show storage of deactived users

		if (!empty($cons['userId']) && !empty($cons['userId'])) {
			$q->where('user_id', '=', $cons['userId']);
		}

		if (!empty($cons['from']) && !empty($cons['from'])) {
			$q->where('datetime', '>', $cons['from'] . ' 00:00:00');
			$q->where('datetime', '<', $cons['to'] . ' 23:59:59');
		}

		if (isset($cons['status']) && is_numeric($cons['status'])) {
			$q->where('status', '=', $cons['status']);
		}

		if (!empty($cons['search'])) {
			$search = $cons['search'];
			$q->where(function($query) use ($search) {
				$query->where('_id', '=', $search)
					->orWhere('barcode', '=', $search)
					->orWhere('address', 'like', '%' . $search . '%')
					->orWhere('phone', '=', $search);
			});
		}

		$storage = $q->orderBy($orderBy, $order)->paginate($perPage);
		$storage->setPath($paginationPath);

		return $storage;

	}

	/**
	 * adminSearch
	 */
	public function adminSearch($cons = null, $perAdminPage = false,  $path = false, $orderBy = false, $order = false) {

		$perPage = $perAdminPage ? $perAdminPage : $this->perAdminPage;
		$paginationPath = $path ? $path : $this->paginationAdminPath;
		$order = $order ? $order : $this->order;
		$orderBy = $orderBy ? $orderBy : $this->orderBy;
		$userModel = new Users();
		$usersDeactived = $userModel::where('status', 0)->lists('_id')->toArray();

		// Build query
		$q = Storage::query();

		$q->whereNotIn('user_id', $usersDeactived);	// Chuong: Not show storage of deactived users

		if (isset($cons['search']) && !empty($cons['search'])) {
			$search = $cons['search'];
			$q->where(function($query) use ($search) {
				$query->where('barcode', 'like', '%' . $search . '%')
					->orWhere('transcode', 'like', '%' . $search . '%')
					->orWhere('customername', 'like', '%' . $search . '%');
			});
		}

		// For now: Get store by current date or date from request
		if (isset($cons['date']) && !empty($cons['date'])) {
			$date = date('m/d/Y', strtotime($cons['date']));
			$q->where('searchdate', '=', $date);
		}

		if (isset($cons['warehouse']) && !empty($cons['warehouse']) && ($cons['warehouse'] != 'all')) {
			$q->where('workingstatus', '=', $cons['warehouse']);
		}

		/* $result = $q->toSql();
		dd($result); */

		$storageBase = $q->orderBy($orderBy, $order)->get();
		$storage = $q->orderBy($orderBy, $order)->paginate($perPage);
		$storage->setPath($paginationPath);
		$storage->appends($cons)->links();

		return [$storage, $storageBase];

	}

	/**
	 * warehouseSearch
	 */
	public function warehouseSearch($cons = null, $perAdminPage = false, $path = false, $orderBy = false, $order = false) {
		$perPage = $perAdminPage ? $perAdminPage : $this->perAdminPage;
		$paginationPath = $path ? $path : $this->paginationWarehousePath;
		$order = $order ? $order : $this->order;
		$orderBy = $orderBy ? $orderBy : $this->orderBy;
		$userModel = new Users();
		$usersDeactived = $userModel::where('status', 0)->lists('_id')->toArray();
		// Build query
		$q = Storage::query();

		$q->whereNotIn('user_id', $usersDeactived);	// Chuong: Not show storage of deactived users

		if (isset($cons['search']) && !empty($cons['search'])) {
			$search = $cons['search'];
			$q->where(function($query) use ($search) {
				$query->where('barcode', 'like', '%' . $search . '%')
				->orWhere('transcode', 'like', '%' . $search . '%')
				->orWhere('customername', 'like', '%' . $search . '%')
				->orWhere('warehouse_barcode', 'like', '%' . $search . '%');
			});
		}

		if (isset($cons['city_code']) && !empty($cons['city_code'])) {
			$q->where('warehouse_city', '=', $cons['city_code']);
		}

		if (isset($cons['warehouse_code']) && !empty($cons['warehouse_code'])) {
			$q->where('warehouse_code', '=', $cons['warehouse_code']);
		}

		if (isset($cons['aisle_num']) && !empty($cons['aisle_num'])) {
			$q->where('warehouse_aisle', '=', $cons['aisle_num']);
		}

		if (isset($cons['section_num']) && !empty($cons['section_num'])) {
			$q->where('warehouse_section', '=', $cons['section_num']);
		}

		if (isset($cons['shelf_num']) && !empty($cons['shelf_num'])) {
			$q->where('warehouse_shelf', '=', $cons['shelf_num']);
		}

		$q->where(function($query) {
			$query->orWhere('status', '=', 0)
			->orWhere('status', '=', 1)
			->orWhere('status', '=', 2)
			->orWhere('status', '=', 3)
			->orWhere('status', '=', 4)
			->orWhere('status', '=', 5);
		});

		$storageBase = $q->orderBy($orderBy, $order)->get();
		$storage = $q->orderBy($orderBy, $order)->paginate($perPage);
		$storage->setPath($paginationPath);
		$storage->appends($cons)->links();

		return [$storage, $storageBase];
	}

	/**
	 * getTotalInTransitAndDoorstepByUser
	 * Manh - repleace totalStorageByUserAndStatus function by getTotalInTransitAndDoorstepByUser
	 * for fix Client logic get in-transit and in-storage
	 */
	public function getTotalInTransitAndDoorstepByUser($userId)
	{
		$inTransit = Storage::where('user_id', '=', $userId)
					->where(function($q) {
						$q->where('intransit', '!=', 0)
							->orWhere('status', '=', 3);
					})
					->where('status', '<>', 9)->count();
		// Build query for get in storage
		$q = Storage::query();
		$q->where('user_id', '=', $userId);
		$q->where(function($query) {
			$query->where(function($q0) {
				$q0->where('status', '=', 1)
				   ->where('intransit', '=', 0);
			});
			/* $query->orWhere(function($q1) {
				$q1->where('status', '=', 3)
				   ->where('intransit', '=', 0);
			}); */
			$query->orWhere(function($q2) {
				$q2->where('status', '=', 4)
				   ->where('intransit', '=', 0);
			});
		});
		$doorStep = $q->count();

		// In storage
		$inStorage = Storage::where('user_id', '=', $userId)->where('status', '=', 2)->count();

		return [$inTransit, $doorStep, $inStorage];
	}

	/**
	 * getStorageById
	 */
	public function getStorageById($id)
	{
		return Storage::find($id);
	}

	/**
	 * getListBarcodeByTransId
	 */
	public function getListBarcodeByTransId($id)
	{
		$data = null;
		$result = Storage::where('trans_id', '=', $id)->where('status', '<>', 9)->get();

		foreach ($result as $foo) {
			$data[] = CommonHelper::generateBarcode($foo->barcode);
		}

		return $data;
	}

	/**
	 * getCustomStorageById
	 */
	public function getCustomStorageById($id)
	{
		$result = Storage::find($id);

		if (empty($result)) return null;

		// Get stripe customerId
		$paymentkey = $result->transactions->users->stripe_id;
		$warehouse_barcode = isset($result->warehouse_barcode) ? $result->warehouse_barcode : '';

		return array(
				'phone' => $result->phone,
				'address' => $result->address,
				'status' => $result->status,
				'workingstatus' => $result->workingstatus,
				'created_at' => date('Y-m-d H:i:s', strtotime($result->created_at)),
				'dropofftime' => $result->dropofftime,
				'pickuptime' => $result->pickuptime,
				'canceltime' => $result->canceltime,
				'barcode' => $result->barcode,
				'images' => $result->images,
				'transcode' => $result->transactions->transcode,
				'barcodeimage' => CommonHelper::generateBarcode($result->barcode),
				'paymentkey' => $paymentkey,
				'paymenttype' => $result->transactions->type,
				'trans_id' => $result->trans_id,
				'warehouse_barcode' => $warehouse_barcode,
				'content' => $result->content,
				'warehouse_city' => isset($result->warehouse_city) ? $result->warehouse_city : '',
				'warehouse_code' => isset($result->warehouse_code) ? $result->warehouse_code : '',
				'warehouse_aisle' => isset($result->warehouse_aisle) ? $result->warehouse_aisle : '',
				'warehouse_section' => isset($result->warehouse_section) ? $result->warehouse_section : '',
				'warehouse_shelf' => isset($result->warehouse_shelf) ? $result->warehouse_shelf : '',
				'zipcode' => $result->zipcode,
				'cuskey' => $result->transactions->type == 0 ? $result->transactions->paymentInfo['customer_id'] : '',
				'userid' => $result->user_id,
				'list_scan' => $this->getAvailableScan($result),
				'item_type' => $result->item->type,

		);
	}

	/**
	 * cancelStorageByIdAjax
	 * Use scan IT4
	 */
	public function cancelStorageByIdAjax($arr) {
		return $this->cancelStorage($arr, $arr['storagedkey']);
	}

	/**
	 * cancelStorageUserAjax
	 */
	/* public function cancelStorageUserAjax($arr, $userid) {

		$cons['userId'] = $userid;
		$cons['status'] = 2;
		$result = $this->search($cons);

		if (count($result) > 0) {

			$return = null;

			foreach ($result as $foo) {
				$value = $this->cancelStorage($arr, $foo->_id);
				if (isset($value['error'])) return $value;
				$return[] = $value;
			}

			return $return;
		}

		return ['error' => 'Got error'];
	} */

	/**
	 * cancelStorage
	 */
	public function cancelStorage($arr, $id)
	{
		$storage = Storage::find($id);

		if (! $storage || $storage->status == 5) return ['error' => 'This storage is not exist'];

        $trans = Transactions::find($storage->trans_id);
        $paymentInfo = $trans->paymentInfo;
        $totalPayment = 0;

        // Stripe - removed paypal
        //if($trans->type == 0) {

        	$payment = new StripeFuns();
        	$newPaymentInfo = $payment->UpdateSubscription($paymentInfo, $storage->item->type);
        	if(isset($newPaymentInfo['error'])) return $newPaymentInfo;
        	// Change status of transaction to done - Manh 20160310
        	$trans->status = ($newPaymentInfo['totalUsingBin'] == 0) ? 2 : $trans->status;
        //}

        // Update transaction
        $trans->paymentInfo = $newPaymentInfo;
        $trans->save();

        // Storage
		$storage->address = (isset($arr['addressca']) && $arr['addressca'] != '') ? $arr['addressca'] : $storage->address;
		$storage->canceltime = $arr['dateca'] . ' ' . $arr['timeca'];
		$storage->phone = (isset($arr['phoneca']) && $arr['phoneca'] != '') ? $arr['phoneca'] : $storage->phone;
		$storage->status = 5; // Cancel requesting
		// Move to in-transit - Manh fix
		$storage->intransit = 4;
		$storage->save();

		// Create Calendar
		$params['userid'] = isset($arr['userid']) ? $arr['userid'] : $storage->relationUser->userid;
		$params['address'] = $storage->address;
		$params['itemtype'] = ($storage->item->type == Items::ItemTypeLarge) ? 'LG' : ( $storage->item->type == Items::ItemTypeExtraLarge ? 'XL' : 'OS' );
		$params['storageid'] = $id;

		$params['datetime'] = date('d/m/Y', strtotime($arr['dateca'])) . ' - ' . trim($arr['timeca']);
		$params['date'] = date('d/m/Y', strtotime($arr['dateca']));
		$params['datesearch'] = strtotime($arr['dateca']);
		$params['time'] = trim($arr['timeca']);
		$params['storagestatus'] = 'Pick-up';

		Calendars::createCalendar($params);

		return $storage->transactions->transcode;
	}

	/**
	 * updateStorageById
	 */
	public function updateStorageByIdAjax($arr)
	{
		$result = $this->updateStorage($arr, $arr['storagedkey']);

		return isset($result['error']) ? $result : array($result);
	}

	/**
	 * updateStorageUserAjax
	 */
	public function updateStorageUserAjax($arr, $userid)
	{
		$cons['userId'] = $userid;
		$cons['status'] = 2;
		$result = $this->search($cons);

		if (count($result) > 0) {

			$return = null;

			foreach ($result as $foo) {
				$value = $this->updateStorage($arr, $foo->_id);
				if (isset($value['error'])) return $value;
				$return[] = $value;
			}

			return $return;
		}

		return [
				'error' => true,
				'message' => 'Got error'
			];
	}

	/**
	 * updateStorage
	 */
	public function updateStorage($arr, $id)
	{
		$storage = Storage::find($id);

		if (! $storage) return [
							'error' => true,
							'message' => 'This storage is not exist'
						];

		$storage->address = (isset($arr['addressfull']) && $arr['addressfull'] != '') ? $arr['addressfull'] : $storage->address;

		$temp = $storage->dropofftime;
		$arr2 = $storage->pickuptime;
		array_push($temp, $arr['date'] . ' - ' . $arr['time'] . ' - IS');

		$storage->dropofftime = $temp;
		$storage->processtime = trim($arr['date']) . ' - ' . trim($arr['time']);
		$storage->searchdate = trim($arr['date']);
		$storage->phone =  (isset($arr['phone']) && $arr['phone'] != '') ? $arr['phone'] : $storage->phone;
		$storage->status = 3;// Pickup-delivery
		// Move to in-transit - Manh fix
		//$storage->intransit = 3;
		//$storage->workingstatus = 'IT3 - In Transit';

		// Create Calendar
		$params['userid'] = $arr['userid'];
		$params['address'] = $storage->address;
		$params['itemtype'] = ($storage->item->type == Items::ItemTypeLarge) ? 'LG' : ( $storage->item->type == Items::ItemTypeExtraLarge ? 'XL' : 'OS' );
		$params['storageid'] = $id;
		$params['datetime'] = date('d/m/Y', strtotime($arr['date'])) . ' - ' . trim($arr['time']);
		$params['date'] = date('d/m/Y', strtotime($arr['date']));
		$params['time'] = trim($arr['time']);
		$params['datesearch'] = strtotime($arr['date']);
		$params['storagestatus'] = 'Drop-off';
		// Create drop-off calendar
		Calendars::createCalendar($params);

		// If have pick-up date => create pick-up calendar
		if (!empty($arr['date2']) && !empty($arr['time2'])) {
			array_push($arr2, $arr['date2'] . ' - ' . $arr['time2'] . ' - IS');
			$params['datetime'] = date('d/m/Y', strtotime($arr['date2'])) . ' - ' . trim($arr['time2']);
			$params['date'] = date('d/m/Y', strtotime($arr['date2']));
			$params['time'] = trim($arr['time2']);
			$params['datesearch'] = strtotime($arr['date2']);
			$params['storagestatus'] = 'Pick-up';
			Calendars::createCalendar($params);
		} else {
			array_push($arr2, $arr['date'] . ' - ' . $arr['time'] . ' - IS');
		}

		$storage->pickuptime = $arr2;
		$storage->save();

		//return $storage->transactions->transcode;
		return $storage;
	}

	/**
	 * getStorageAddressById
	 * Relationship
	 */
	public function getStorageAddressById($id) {

		$result = Storage::find($id);

		return ($result) ? $result->address : ['error' => 'Can not found this storage.'];
	}

	/**
	 * transactions
	 * Relationship
	 */
	public function transactions()
	{
		return $this->belongsTo('App\Models\Transactions', 'trans_id');
	}
	/**
	 * relationUser
	 * Relationship
	 * @author : hong
	 */
	public function relationUser()
	{
		return $this->belongsTo('App\Models\Users', 'user_id');
	}

	/**
	 * Get the item record
	 */
	public function item()
	{
		return $this->belongsTo('App\Models\Items', 'item_id');
	}


	/**
	 * Create a new storage
	 * @author: DDD
	 * @return storage
	 */
	public static function createStorage($user_id, $trans_id, $item_id, $barcode, $dropofftime,
			$address = "", $phone = "", $zipcode, $transcode = '', $cusname = '', $addressstreet = '',
            $cityzipcode = '', $aptsuite = '')
	{
		$storage = new Storage();
        $storage->user_id = $user_id;
		$storage->trans_id = $trans_id;
        $storage->item_id = $item_id;
		$storage->barcode = $barcode;
		$storage->status = 0;
		// Manh add a flag to check in-transit 20160516
		$storage->intransit = 1;

		$storage->phone = $phone;
		$storage->zipcode = $zipcode;
        $storage->address = $address;
        $storage->addressstreet = $addressstreet;
        $storage->cityzipcode = $cityzipcode;
        $storage->aptsuite = $aptsuite;
        $storage->dropofftime = [$dropofftime];
        // Manh add processtime - 20160318 (commented in Modal this field)
        $storage->processtime = $dropofftime;
        $storage->searchdate = CommonHelper::processDate($dropofftime);
        $storage->pickuptime = [];
        $storage->images = [];
		$storage->content = '';
		$storage->workingstatus = '';
		$storage->canceltime = '';
		$storage->transcode = $transcode;
		$storage->customername = $cusname;
		$storage->save();
// Manh change return $barcode to $storage - 20160321
		return $storage;
	}

    /**
     * DDD - 20160216
	 * getStorageByUserIds
	 */
	public function getStorageByUserIds($ids)
	{
		return Storage::whereIn('user_id', $ids)->where('status', '<>', 9)->get();
	}

    /**
     * DDD - 20160216
	 * getStorageByUserId
	 */
	public static function getStorageByUserId($id)
	{
		return Storage::where('user_id', '=', $id)->where('status', '<>', 9)->get();
	}

	/**
	 * getStorageByUserIdAdmin
	 * Get all storage include status = 9 use in admin page
	 */
	public static function getStorageByUserIdAdmin($id)
	{
		return Storage::where('user_id', '=', $id)->get();
	}

     /**
     * DDD - 20160217
	 * getStorageByTransIds
	 */
	public function getStorageByTransIds($transids)
	{
		return Storage::whereIn('trans_id',$transids)->where('status', '<>', 9)->get();
	}

    /**
     * DDD - 20160217
	 * getStorageByTransIds
	 */
	public static function getStorageByTransId($transid)
	{
		return Storage::where('trans_id','=',$transid)->where('status', '<>', 9)->orderBy('barcode')->get();
	}

    /**
     * DDD - 20160217
	 * getStorageByTransIds
	 */
	public static function getByYear($year,$half = 0)
	{
        $start = new MongoDate(strtotime($year."-".( (6 * $half) + 1 )."-1 00:00:00"));
        $end = new MongoDate(strtotime($year."-".( (6 * $half) + 6 )."-31 00:00:00"));
		return Storage::whereBetween('create_at',array($start, $end))
					->where('status', '<>', 9)
					->orderBy('create_at')->get();
	}

	/**
	 * getStorageByBarcode
	 */
	public static function getStorageByBarcode($barcode)
	{
		return Storage::where('barcode','=',$barcode)->first();
	}

	/**
	 * totalProcessing
	 */
	public static function totalProcessing($type, $month)
	{
		$result = null;
		$data = Storage::where('created_at', '>=', new DateTime(date('Y') . '/' . $month . '/1' . ' 00:00:00'))
				->where('created_at', '<=', new DateTime(date('Y') . '/' . $month .'/31' . ' 23:59:59'))
				->where('status', '<>', 9)->get();

		if ($data) {
			$date = new DateTime();
			$count = 0;
			$changes = array();
			$current_total = null;
			foreach ($data as $foo) {
				if ($foo->item->type == $type) {
					$count ++;
					$changes = $foo->item->changes;
					$current_total = $foo->item->total;
				}
			}
			$result['ordered'] = $count;

			foreach($changes as $change) {
				if ($change['date'] == $date->format('Ym')) {
					$current_total = $change['current_total'];
				}
			}

			$result['unused'] = $current_total - $count;
		}

		return $result;
	}

	public static function totalByZipcodeAndType($zipcode, $type) {
		$result = 0;
		$storages = Storage::all();
		foreach ($storages as $storage) {
			if ((string) $storage->zipcode == (string) $zipcode && $storage->item->type == $type) {
				$result++;
			}
		}

		return $result;
	}

	/*
	 * updateDateTime: Admin change drop-off or pick-up datetime
	 * param $post array
	 * $post params [storage_id, storage_status, date(m/d/Y), time(10:00 am)]
	 */
	public static function updateDateTime($post) {
		$storage = Storage::find($post['storage_id']);

		if (empty($storage)) return ['error' => 'Cannot found this Storage.', 'success' => 0];

		if ($post['storage_status'] == "Drop-off") {
			$dropofftime = $storage->dropofftime;
			$codereadr = explode(' ', $dropofftime[count($dropofftime) - 1]);
			$dropofftime[count($dropofftime) - 1] = $post['date'] . ' - ' . $post['time'] . (isset($codereadr[5]) ? ' - ' . $codereadr[5] : '');

			if ($storage->status != 4) {
				$storage->processtime = $post['date'] . ' - ' . $post['time'];
				$storage->searchdate = $post['date'];
			}

			$storage->dropofftime = $dropofftime;

		} elseif ($post['storage_status'] == 'Pick-up') {

			$pickuptime = $storage->pickuptime;

			if (! empty($pickuptime)) {
				$codereadr = explode(' ', $pickuptime[count($pickuptime) - 1]);
				//$pickuptime[count($pickuptime) - 1] = $post['date'] . ' - ' . $post['time'];
				$pickuptime[count($pickuptime) - 1] = $post['date'] . ' - ' . $post['time'] . (isset($codereadr[5]) ? ' - ' . $codereadr[5] : '');
			} else {
				$pickuptime[] = $post['date'] . ' - ' . $post['time'];
			}

			if ($storage->status == 4) {
				$storage->processtime = $post['date'] . ' - ' . $post['time'];
				$storage->searchdate = $post['date'];
			}

			$storage->pickuptime = $pickuptime;
		}

		$calendar = Calendars::where('storageid', '=', $storage->_id)
							 ->where('storagestatus', '=', $post['storage_status'])
							 ->get();
		if (count($calendar)) {
			$calendar = $calendar[0];
			$calendar->datetime = date('d/m/Y', strtotime($post['date'])) . ' - ' . trim($post['time']);
			$calendar->date = date('d/m/Y', strtotime($post['date']));
			$calendar->datesearch = strtotime($post['date']);
			$calendar->time = trim($post['time']);

			$calendar->save();
		}
		$storage->save();

		$data = array(
				'cusname' 			=> $storage->relationUser->name,
				'address' 			=> $storage->address,
				'phone'   			=> $storage->phone,
				'status'			=> $post['storage_status'],
				'formatDateEmail' 	=> CommonHelper::formatDateForEmail($post['date'] . ' - ' . $post['time'])
		);

		return ['success' => 1, 'email' => $storage->relationUser->email, 'transcode' => $storage->transactions->transcode, 'email_data' => $data];
	}

	public static function formartWorkingStatusShortly($workingStatus) {

		if ($workingStatus == 'Empty - Inventory') return 'Returned';

		return (strpos($workingStatus, ' - ') != false) ? explode('-', $workingStatus)[0] : '';
	}

	/*
	 * getAvailableScan: Return list available codereadr scan option of bin
	 * param $storage: storage obj
	 */
	private function getAvailableScan($storage) {
		$listBinScan = [
				'0' => ['IT1 - In Transit', 'AD1 - At Doorstep'],
				'1' => ['IT2 - In Transit', 'IS - In Storage'],
				'2' => [],
				'3' => ['IT3 - In Transit', 'AD2 - At Doorstep'],
				'4' => ['IT2 - In Transit', 'IS - In Storage', 'IT4 - In Transit'],
				'5' => [],
				'9' => ['Empty - Inventory'],
		];

		$listOtherStuffScan = [
				'0' => ['IT2 - In Transit', 'IS - In Storage'],
				'2' => [],
				'3' => ['IT3 - In Transit', 'AD2 - At Doorstep'],
				'4' => ['IT2 - In Transit', 'IS - In Storage', 'IT4 - In Transit'],
				'5' => [],
				'9' => ['Empty - Inventory']
		];

		if ($storage->status == 9 && $storage->intransit == 0) {
			$listBinScan[9] = [];
			$listOtherStuffScan[9] = [];
		}

		if ($storage->intransit == 3) {
			$listBinScan[3] = ['AD2 - At Doorstep', 'IT4 - In Transit'];
			$listOtherStuffScan[3] = ['AD2 - At Doorstep', 'IT4 - In Transit'];
		}
		else if ($storage->intransit == 4) {
			$listBinScan[3] = [];
			$listOtherStuffScan[3] = [];
			$listBinScan[4] = [];
			$listOtherStuffScan[4] = [];
		}

		if ($storage->item->type != 3 && array_key_exists($storage->status, $listBinScan)) {
			if ($storage->status == 1 && $storage->intransit == 2) {
				$listBinScan[1] = ['IS - In Storage'];
			}
			return $listBinScan[$storage->status];
		} else if ($storage->item->type == 3 && array_key_exists($storage->status, $listOtherStuffScan)){
			if ($storage->status == 0 && $storage->intransit == 2) {
				$listOtherStuffScan[0] = ['IS - In Storage'];
			}
			return $listOtherStuffScan[$storage->status];
		}

		return [];
	}

	/**
	 * getStorageByTransIdAndNotCharge
	 * Manh created for Cron - 20160812
	 *
	 * @param $transid
	 * @return mixed
	 */
	public static function getStorageByTransIdAndNotCharge( $transid )
	{
		return Storage::where('trans_id', '=', $transid)
					->where('isCharged', '=', 2)
					->where('isCancel', '<>', 1)
					->where('status', '<>', 9)->get();
	}

	/**
	 * getCancelStorage
	 *	isCancel = 1 is IT4
	 * @return mixed
	 */
	public static function getCancelStorage() {
		return Storage::where('isCancel', '=', 1)->where('status', '<>', 9)->get();
	}

	/**
	 * getDoneStorageByUser
	 * @return mixed
	 */
	public static function getDoneStorageByUser( $userId ) {
		return Storage::where('user_id', '=', $userId)
						->where('status', '=', 9)->get();
	}

	/**
	 * getDeliveriesWithDay
	 * Manh created for Cron - 20161004
	 *
	 * @param integer $intdate is strtotime('m/d/Y')
	 * @return mixed
	 */
	public static function getDeliveriesWithDay( $intdate )
	{
		// Build query
		return Storage::where('searchdate', '=', date('m/d/Y', $intdate))
				->where('status', '=', 3)->orderBy('created_at', 'desc')->get();
	}
}