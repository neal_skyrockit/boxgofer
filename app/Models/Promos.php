<?php
namespace App\Models;

use App\StripeFuns;

class Promos {

	private $stripe;

	/**
	 * contruct
	 */
	public function __construct() {
		$this->stripe = new StripeFuns();
	}

    /**
     * getById
     */
    public function getById( $id ) {
    	return $this->stripe->couponRetrieve( $id );
    }

    /**
     * getPromosList
     */
    public function getPromosList() {
    	return $this->stripe->listCoupon( 100 );
    }

    /**
     * Create a new promos
     *
     * @return object
     */
    public function createPromo( $name, $percent )
	{
		if ($this->isExistedCode( $name )) {
			return ['error' => 'This code is existed'];
		}

		return $this->stripe->createCoupon( $name, $percent );
	}

	/**
	 * Edit promos
	 *
	 * @return object
	 */
	public function editPromo( $data )
	{
		/* if ($this->isExistedCode($data['code'])) {
			return ['error' => 'This code is existed'];
		}

		$promo = Promos::find($data['id']);
    	if ($promo) {

   			$promo->name = $data['code'];
        	$promo->percent = $data['percent'];
        	$promo->status = $data['status'];
    		return $promo->save();
    	}
    	return ['error' => 'Cannot found this Promotion']; */
	}

    /**
     * deleteById
     */
    public function deleteById( $id )
    {
    	return $this->stripe->deleteCoupon( $id );
    }

    /**
     * isExistedCode
     */
    public function isExistedCode( $id ) {

    	$promo = $this->stripe->couponRetrieve( $id );

    	return empty($promo) ? false : true;
    }
}