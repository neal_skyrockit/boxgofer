<?php
namespace App\Models;

use \DateTime;
// use Jenssegers\Mongodb\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Bginvoices extends Eloquent {

	protected $fillable = ['clientname', 'clientemail', 'clientaddress', 'items', 'payby',
							'dayspay', 'currency', 'amount', 'invoice_id', 'invoice_num',
							'clientphone', 'user_id', 'last4', 'listitems' ];

	protected $guarded = [];
	//
	protected $order = 'desc';
	protected $orderBy = 'created_at';

	protected $perPage = 15;
	protected $paginationPath = '/admin/invoices';

	/**
	 * search
	 */
	public function search($cons = null, $perPage = false,  $path = false, $orderBy = false, $order = false) {

		$perPage = $perPage ? $perPage : $this->perPage;
		$paginationPath = $path ? $path : $this->paginationPath;
		$order = $order ? $order : $this->order;
		$orderBy = $orderBy ? $orderBy : $this->orderBy;

		// Build query
		$q = Bginvoices::query();

		if (isset($cons['from']) && !empty($cons['from'])) {
			$start = date("Y-m-d", strtotime($cons['from'])) . ' 00:00:00';
			$q->where('created_at', '>', new DateTime($start));
		}

		if (isset($cons['to']) && !empty($cons['to'])) {
			$end = date("Y-m-d", strtotime($cons['to'])) . ' 23:59:59';
			$q->where('created_at', '<', new DateTime($end));
		}

		if (!empty($cons['search'])) {
			$search = $cons['search'];
			$q->where(function($query) use ($search) {
				$query->where('invoice_num', 'like', '%' . $search . '%')
				->orWhere('clientname', 'like', '%' . $search . '%');
			});
		}

		$storage = $q->orderBy($orderBy, $order)->paginate($perPage);
		$storage->setPath($paginationPath);

		return $storage;

	}

	/**
	 * getInvoiceById
	 *
	 * @param string $id
	 * @return mixed
	 */
	public function getInvoiceById($id) {

		$foo = Bginvoices::find($id);

		if (empty($foo)) return null;

		$result['_id'] = $foo->_id;
		$result['dayspay'] = $foo->dayspay;
		$result['invoice_id'] = $foo->invoice_id;
		$result['invoice_num'] = $foo->invoice_num;
		$result['clientname'] = $foo->clientname;
		$result['payby'] = $foo->payby;
		$result['items'] = $foo->items;
		$result['clientaddress'] = $foo->clientaddress;
		$result['clientphone'] = $foo->clientphone;
		$result['user_id'] = $foo->user_id;
		$result['last4'] = $foo->last4;
		$result['amount'] = $foo->amount;

		if (isset($foo->listitems)) {
			$listitems = $foo->listitems;
			$result['formatType'] = 'invoicesuccessnew';
			$result['totalAmountNoFormat'] = !empty($foo->totalAmountNoFormat) ? $foo->totalAmountNoFormat : 0;
			$result['discount'] = !empty($foo->discount) ? $foo->discount : 0;
			$result['discount_key'] = !empty($foo->discount_key) ? $foo->discount_key : '';
			$result['data'] = null;
			$arr = (strpos($listitems, '-') !== false) ? explode('-', $listitems) : array($listitems);
			if (count($arr) > 0) {
				$j = 0;
				foreach ($arr as $f) {
					$temp = explode(':', $f);
					if ($temp[1] > 0) {
						$result['data'][$j]['itemType'] = $temp[0];
						$result['data'][$j]['quantity'] = $temp[1];
						$result['data'][$j]['amountOneItemFormat'] = sprintf('$%0.2f', $temp[2]);
						$result['data'][$j]['amount'] = sprintf('$%0.2f', ($temp[2] * $temp[1]));
						$j ++;
					}
				}
			}
		} else {
			$result['formatType'] = 'invoicesuccesscharge';
			$result['amountNoTax'] = isset($foo->amountNoTax) ? $foo->amountNoTax : 0;
			$result['tax'] = isset($foo->tax) ? $foo->tax : 0;
			$result['totalAmount'] = isset($foo->totalAmount) ? $foo->totalAmount : 0;
			$result['description'] = $foo->description;
			$result['quantity'] = $foo->quantity;
			$result['amount'] = $foo->amount;
			$result['itemType'] = $foo->itemType;
		}

		return $result;
	}

	/**
	 * createBginvoice
	 *
	 * @param array $params
	 * @return mixed
	 */
	public function createBginvoice($params)
	{
		try {
			$item = new Bginvoices();
			$item->clientname = $params['clientName'];
			$item->clientemail = $params['clientEmail'];
			$item->clientaddress = $params['clientAddress'];
			$item->items = isset($params['item']) ? $params['item'] : '';
			$item->payby = $params['payby'];
			$item->dayspay = $params['dayspay'];
			$item->currency = $params['currency'];
			$item->amount = $params['amount'];
			$item->invoice_id = $params['invoice_id'];
			$item->invoice_num = $params['invoice_num'];
			$item->clientphone = $params['phone'];
			$item->last4 = $params['last4'];
			$item->user_id = $params['user_id'];

			// Discount
			$item->discount = isset($params['discount']) ? $params['discount'] : 0;
			$item->discount_key = isset($params['discount_key']) ? $params['discount_key'] : '';
			$item->totalAmountNoFormat = isset($params['org_amount']) ? $params['org_amount'] : 0;

			if(isset($params['listitems'])) {
				$item->listitems = $params['listitems'];
			}
			if(isset($params['amountNoTax'])) {
				$item->amountNoTax = $params['amountNoTax'];
			}
			if(isset($params['tax'])) {
				$item->tax = $params['tax'];
			}
			if(isset($params['totalAmount'])) {
				$item->totalAmount = $params['totalAmount'];
			}
			if(isset($params['description'])) {
				$item->description = $params['description'];
			}
			if(isset($params['addressstreet'])) {
				$item->addressstreet = $params['addressstreet'];
			}
			if(isset($params['cityzipcode'])) {
				$item->cityzipcode = $params['cityzipcode'];
			}
			if(isset($params['aptsuite'])) {
				$item->aptsuite = $params['aptsuite'];
			}
			if(isset($params['card_number'])) {
				$item->card_number = $params['card_number'];
			}
			if(isset($params['itemType'])) {
				$item->itemType = $params['itemType'];
			}
			if(isset($params['quantity'])) {
				$item->quantity = $params['quantity'];
			}

			return $item->save();

		} catch (Exception $e) {

		}

		return null;
	}
}