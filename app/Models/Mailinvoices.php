<?php
namespace App\Models;

// use Jenssegers\Mongodb\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Mailinvoices extends Eloquent {

	protected $fillable = ['stripeInvoiceId', 'orderId', 'userId', 'email', 'content', 'invoicedata'];

	/**
	 * getListAll
	 *
	 * @return mixed
	 */
	public function getListAll() {
		return Mailinvoices::all();
	}

	/**
	 * removeAll
	 *
	 * @return mixed
	 */
	public function removeAll() {
		return Mailinvoices::truncate();
	}

	/**
	 * upsertMailinvoice
	 *
	 * @param array $params
	 * @return mixed
	 */
	public function upsertMailinvoice($params, $invoice = array())
	{
		if (empty($params['orderId']) || empty($params['userid'])) return false;

		$mailInvoice = Mailinvoices::where('orderId', $params['orderId'])
						->where('userId', $params['userid'])->first();

		if (empty($mailInvoice)) {
			$mailInvoice = new Mailinvoices();
			$mailInvoice->orderId = $params['orderId'];
			$mailInvoice->userId = $params['userid'];
			$mailInvoice->email = $params['email'];
			$content = array($params);
			if (!empty($invoice)) {
				$mailInvoice->invoicedata = $invoice;
			}
		} else {
			// Content
			$content = $mailInvoice->content;
			array_unshift($content, $params);
			// Invoice
			$invoicedata = $mailInvoice->invoicedata;
			if (empty($invoicedata) && !empty($invoice)) {
				$mailInvoice->invoicedata = $invoice;
			}
		}

		$mailInvoice->content = $content;

		return $mailInvoice->save();
	}

	/**
	 * createMailinvoice
	 *
	 * @param array $params
	 * @return mixed
	 */
	public function createMailinvoice($params, $invoicedata)
	{
		if (empty($params['orderId']) || empty($params['userid'])) return false;

		$mailInvoice = Mailinvoices::where('stripeInvoiceId', $params['invoiceId'])->first();

		if (empty($mailInvoice)) {

			$obj = new Mailinvoices();
			$obj->stripeInvoiceId = $params['invoiceId'];
			$obj->orderId = $params['orderId'];
			$obj->userId = $params['userid'];
			$obj->content = $params;
			$obj->invoicedata = $invoicedata;

			return $obj->save();
		}
	}
}