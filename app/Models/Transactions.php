<?php
namespace App\Models;

// use Jenssegers\Mongodb\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use \DateTime;
use Carbon\Carbon;
use App\Models\Users;
use App\StripeFuns;
use App\Models\Calendars;
use Config;
use App\Http\Helpers\CommonHelper;

class Transactions extends Eloquent {

	protected $order = 'DESC';
	protected $orderBy = 'updated_at';

	protected $perPage = 15;
	protected $paginationPath = '/admin/orders';

	protected $guarded = [];
	// Manh add rule status = 9 is deleted
	protected $fillable = ['user_id', 'transcode','paymentInfo', 'plan_id', 'type', 'status', 'content',
							'sentmaildate', 'otherdes', 'sentmailtemplate', 'sub_id', 'trial_end', 'start_sub'];

	/**
	 * search
	 */
	public function search($cons = null, $perPage = false,  $path = false, $orderBy = false, $order = false) {

		$perPage = $perPage ? $perPage : $this->perPage;
		$paginationPath = $path ? $path : $this->paginationPath;
		$order = $order ? $order : $this->order;
		$orderBy = $orderBy ? $orderBy : $this->orderBy;
		$userModel = new Users();
		//$usersDeactived = $userModel::where('status', 0)->lists('_id')->toArray();

		// Build query
		$q = Transactions::query();

		//$q->whereNotIn('user_id', $usersDeactived);	// Chuong: Not show transaction of deactived users
		// Status = 9 is deleted
		//$q->where('status', '<>', 9);

		if (isset($cons['userid'])) {
			$q->where('user_id', '=', $cons['userid']);
		}

		if (!empty($cons['from']) && !empty($cons['to'])) {
			$q->where('created_at', '>', new DateTime($cons['from'] . ' 00:00:00'));
			$q->where('created_at', '<', new DateTime($cons['to'] . ' 23:59:59'));
		}

		if (isset($cons['status']) && is_numeric($cons['status'])) {
			$q->where('status', '=', $cons['status']);
		}

		if (!empty($cons['search'])) {
			$search = $cons['search'];
			$q->where(function($query) use ($search) {
				$query->where('_id', '=', $search)
					->orWhere('transcode', '=', $search);
			});
		}

		$trans = $q->orderBy($orderBy, $order)->paginate($perPage);
		$trans->setPath($paginationPath);
		$trans->appends($cons)->links();

		return $trans;

	}

	/**
	 * getTransactionListByStatus
	 */
	public static function getTransactionListByStatus( $status )
	{
		return Transactions::where('status', '=', $status)
								->orderBy('user_id', 'desc')->get();
	}

	/**
	 * getTransactionListByTrialEnd
	 */
	public static function getTransactionListByTrialEnd()
	{
		$today = date('Y-m-d');
		$start = strtotime($today . ' 00:00:00');
		$end = strtotime($today . ' 23:59:59');
		return Transactions::where('trial_end', '>=', $start)
							->where('trial_end', '<=', $end)
							->where('status', '<>', 9)
							->orderBy('user_id', 'desc')->get();
	}

	/**
	 * storage
	 * Relationship
	 */
	public function storage()
	{
		return $this->hasMany('App\Models\Storage', '_id');
	}

	/**
	 * users
	 * Relationship
	 */
	public function users()
	{
		return $this->belongsTo('App\Models\Users', 'user_id');
	}

	/**
	 * Create a new transactions
	 *
	 * @param  array  $data
	 * @return Transactions
	 */
	/* protected function createTrans(array $data)
	{
		$trans = new Transactions();
		$trans->user_id = $data['userId'];
		$trans->paymentToken = $data['paymentToken'];
		$trans->paymentEmail = $data['paymentEmail'];
		$trans->paymentArgs = $data['paymentArgs'];
		$trans->type = $data['type'];
		$trans->status = $data['status'];

		return $trans->save();
	} */

	/**
	 * updateTransactionsAD1
	 * Update transactions when scanned bin for AD1
	 *
	 * @param string $transactionsId
	 * @param string $type = 'Large' or 'Extra' or 'Other'
	 *
	 * @return object
	 */
	public static function updateTransactionsAD1($transactionsId, $type)
	{
		$trans = Transactions::find( $transactionsId );

		if (empty($trans)) return null;

		$paymentInfo = $trans->paymentInfo;
		$key = ($type == Items::ItemTypeLarge) ? 'Large'
		: ($type == Items::ItemTypeExtraLarge ? 'Extra' : 'Other' );

		if (!isset($paymentInfo[$key]['ad1'])) {
			$paymentInfo[$key]['ad1'] = 1;
		} else {
			$paymentInfo[$key]['ad1'] ++;
		}

		$trans->paymentInfo = $paymentInfo;
		$trans->save();

		return $trans;
	}

	/**
	 * updateScanned
	 * Update transactions when scanned bin
	 *
	 * @param string $transactionsId
	 * @param string $type = 'Large' or 'Extra' or 'Other'
	 * @param string $isCharged = 1 is charged, default = 0
	 *
	 * @return object
	 */
	public static function updateScannedTransactions($transactionsId, $type, $isCharged = 0)
	{
		$trans = Transactions::find( $transactionsId );

		if (!empty($trans) && ($isCharged != 1)) {

			$paymentInfo = $trans->paymentInfo;
			$key = ($type == Items::ItemTypeLarge) ? 'Large'
					: ($type == Items::ItemTypeExtraLarge ? 'Extra' : 'Other' );

			if (!isset($paymentInfo[$key]['scanned_flag']))
			{
				$paymentInfo[$key]['scanned_flag'] = 1;
			}
			else
			{
				$paymentInfo[$key]['scanned_flag'] ++;
			}

			$trans->paymentInfo = $paymentInfo;
			// Need update Stripe
			$trans->status = 1;
			$trans->save();
		}

		return $trans;
	}

    /**
	 * Create a new storage
	 * Manh - edited 20160309
	 * status = 0 - pending, 1 - need update on Stripe, 2 - working
	 *
	 * @param string $user_id
	 * @param string $content
	 * @param object $paymentInfo
	 * @param string $otherDes
	 *
	 * @return Transactions
	 */
	public static function createTransactions($user_id, $content, $paymentInfo, $otherDes)
	{
		$setingday = Config::get('services.paymentday');
		$transac = new Transactions();
		$transac->user_id = $user_id;
        $transac->transcode = $paymentInfo['orderId'];
        $transac->content = $content;
        $transac->otherdes = $otherDes;
        //$transac->sub_id = '';// Keep at users table
		//$transac->paymentEmail = '';
        $transac->paymentInfo = $paymentInfo;
        // Trial_end will be added when scan
        //$transac->trial_end = strtotime('+' . $setingday['trailend'] . ' days', strtotime($paymentInfo['dropoffDate']));
        //$transac->start_sub = '';// This is start subcription date // Keep at users table
		$transac->status = 0;
		$transac->save();

		return $transac;
	}

	/**
	 * getTransByPaypalToken
	 *
	 * @param string $paypalToken
	 * @return mixed
	 */
	public function getTransByPaypalToken($paypalToken)
	{
		return Transactions::where('paypalToken', '=', $paypalToken)
				->where('status', '=', 0)->first();
	}

    /**
	 * DDD - 20160216
	 * getByUserId
	 */
	function getByUserId($userid)
	{
		return Transactions::where('user_id','=',$userid)->where('status', '<>', 9)->get;
	}

    /**
	 * DDD - 20160216
	 * getLastpayment
	 */
	function getLastpayment($userid)
	{
		return Transactions::where('user_id', '=', $userid)->where('status', '<>', 9)->orderBy('updated_at', 'desc')->first();
	}

     /**
	 * DDD - 20160304
	 * getById
	 */
	function getById($transid)
	{
		return Transactions::find($transid);
	}

    /**
	 * DDD - 20160304
	 * getByYear
	 */
	function getByYear($year,$half = 0)
	{
        if (!empty($cons['from']) && !empty($cons['to'])) {
			$q->where('created_at', '>', new DateTime($cons['from'] . ' 00:00:00'));
			$q->where('created_at', '<', new DateTime($cons['to'] . ' 23:59:59'));
		}
        $month = $half == 0 ? [1,6] : [7,12];
        $day = $half == 0 ? 30 : 31;
		return Transactions::where('created_at','>',new DateTime($year . '/' . $month[0] . '/1' . ' 00:00:00'))
                  	->where('created_at','<',new DateTime($year . '/' . $month[1] .'/' . $day . ' 23:59:59'))
					->where('status', '<>', 9)->get();
	}

	/**
	 * getTransByUserIdAndCurrentMonth
	 */
	public static function getTransByUserIdAndCurrentMonth($userId)
	{
		// Build query
		$q = Transactions::query();
		$q->where('user_id', '=', $userId);
		$trans = $q->where('updated_at', '>=', Carbon::now()->startOfMonth())
					->where('status', '<>', 9)->get();

		return $trans;
	}

	/**
	 * deleteTransactionById
	 * Just change status = 9
	 *
	 * @param string $id
	 * @return true|false
	 */
	public function deleteTransactionById($id)
	{
		$obj = new Storage();
		$calen = new Calendars();
		$storages = $obj->getStorageByTransId($id);
		if (!empty($storages)) {
			foreach ($storages as $storage) {
				$storage->item->total_rented --;
				$storage->item->save();
				$calen->deleteCalendarByStorageId( $storage->_id );
				$storage->canceltime = date('m/d/Y - H:00 a');
				$storage->status = 9;
				$storage->save();
				//$storage->delete();
			}
		}

		$trans = Transactions::find( $id );
		if (!empty($trans)) {
			$trans->status = 9;
			$trans->save();
		}

		return true;
	}

	/**
	 * cancelOrderById
	 * This is cancel Order or Transactions
	 */
	public function cancelOrderById( $id ) {

		$stripe = new StripeFuns();
		$trans = Transactions::find( $id );
		$result = null;

		if (empty($trans)) return ['error' => 'Not found'];

		$paymentInfo = $trans->paymentInfo;

		if (empty($trans->users->sub_total) || $trans->users->sub_total == $paymentInfo['total'])
		{
			$result = $stripe->cancelOrder( $trans->users->plan_id, $trans->users->sub_id );

			if (isset($result['error'])) return $result;

			// Update user
			$trans->users->sub_total = 0;
			$trans->users->sub_date = '';
			$trans->users->sub_id = '';
			$trans->users->plan_id = '';
			$trans->users->save();

		}
		else if ($paymentInfo['total'] > 0)
		{
			$trans->users->sub_total -= $paymentInfo['total'];
			// Update subcription
			$planArr['id'] = date('Ymdhis');
			$planArr['name'] = $paymentInfo['orderId'] . '-' . date('Y-m-d h:i:s');
			$planArr['amount'] = $trans->users->sub_total;
			$trialDay = CommonHelper::getTrialday($trans->users->sub_date);
			// Update subcription
			$data['remove_items'] = array(
					'Large' => isset($paymentInfo['Large']['scanned']) ? $paymentInfo['Large']['scanned'] : 0,
					'Extra' => isset($paymentInfo['Extra']['scanned']) ? $paymentInfo['Extra']['scanned'] : 0,
					'Other' => isset($paymentInfo['Other']['scanned']) ? $paymentInfo['Other']['scanned'] : 0
				);
			$sub = $stripe->updatePlanSubscription($trans->users->stripe_id, $trans->users->sub_id, $planArr, $trans->users->plan_id, $data, $trialDay);
			// Update user
			$trans->users->plan_id = $planArr['id'];
			$trans->users->save();
		}

		// Delete this transactions
		$this->deleteTransactionById($id);

		return $result;
	}

	/**
	 * getTransByOrderId
	 */
	public static function getTransByOrderId( $orderId )
	{
		return Transactions::where( 'transcode', '=', $orderId )
							->where('status', '<>', 9)->first();
	}
}