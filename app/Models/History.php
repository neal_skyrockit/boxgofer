<?php
/**
 * Use for statistic chartDailySchedule
 */
namespace App\Models;

use PhpParser\Node\Expr\Cast\Object_;

// use Jenssegers\Mongodb\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use \DateTime;


class History extends Eloquent {

	protected $collection = 'history';

    protected $fillable = ['L', 'XL', 'Other'];

    /**
     * getHistory
     */
    public function getHistory() {
    	return History::find(1);
    }

    /**
     * getHistoryByCurrentDay
     */
    public static function getHistoryByCurrentDay()
    {
    	$start = date('Y/m/d') . ' 00:00:00';
    	$end = date('Y/m/d') . ' 23:59:59';

    	return History::where('updated_at', '>=', new DateTime($start))
    					->where('updated_at', '<=', new DateTime($end))
    					->first();
    }

    /**
     * upsertHistory
     *
     * @param integer $itemType
     * @param string $option
     * @param integer $val
     *
     * @return Object
     */
    public static function upsertHistory($itemType, $option, $val)
    {
    	if (!in_array($option, array('DropoffEmpty', 'PickupFull', 'DropoffFull', 'PickupEmpty')))
    		return false;

    	$history = History::getHistoryByCurrentDay();

    	if (! $history) $history = new History();

    	$type = ($itemType == 1) ? 'L' : (($itemType == 2) ? 'XL' : 'Other');

    	$a = $history->{$type};
    	$a[$option] = isset($a[$option]) ? $a[$option] + $val : $val;
    	$history->{$type} = $a;

    	$history->save();

    	return $history;
    }

    /**
     * Create a new items
     *
     * @return void
     */
    public function createHistory($arr)
	{
		/* $item = new History();
        $item->total = $arr;
        $item->save(); */
	}

}