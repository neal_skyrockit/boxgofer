<?php
namespace App\Models;

// use Jenssegers\Mongodb\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
class Warehouses extends Eloquent {

    protected $fillable = ['city_code', 'warehouse_code'];

    /* getAll
     * Author Chuong 20160516
     * */
	public static function getAll() {
		return Warehouses::all();
	}

	public static function createWarehouse () {
		$item = new Warehouses();
		$item->query()->delete();

		$item->city_code = array('RCH');
		$item->warehouse_code = array('0001');
		$item->save();
	}
}