<?php
namespace App\Models;

// use Jenssegers\Mongodb\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\StripeFuns;
class Items extends Eloquent {

    protected $fillable = ['name', 'type', 'price', 'description', 'total', 'total_rented', 'StripePlanId', 'PaypalPlanId', 'changes'];

    const ItemTypeLarge = 1;
    const ItemTypeExtraLarge = 2;
    const ItemTypeOtherStuff = 3;
    const InitItemTotal = 99;

    /**
     * getItemsList
     */
    public function getItemsList() {
    	return Items::orderBy('type', 'asc')->get();
    }

    /**
     * removeAll
     */
    public function removeAll(){
        $listItem = Items::orderBy('type', 'asc')->get();
        foreach($listItem as $item){
            Items::destroy($item->_id);
        }
    }

    /**
     * remainLarge
     */
    public function remainLarge($large) {
        return $large['total'] - $large['total_rented'];
    }

    /**
     * remainExtLarge
     */
    public function remainExtLarge($extlarge) {
        return $extlarge['total'] - $extlarge['total_rented'];
    }

    /**
     * otherstuff
     */
    public function otherstuff($otherstuff) {
        return $otherstuff['total'] - $otherstuff['total_rented'];
    }

	/**
	 * Manh - 20160308
	 * Remove trial day 20160628
	 */
    public function initItems() {

    	if (1 == 1) return 'Comment this line!!!';

    	$stripe = new StripeFuns();

    	// Remove
    	$this->removeAll();
    	$stripe->RemoveAllPlan();

    	// Init Items
        $this->createItems("Large", "Large Bin", self::ItemTypeLarge, 4);
        $this->createItems("Extra", "Extra Bin", self::ItemTypeExtraLarge, 6);
        $this->createItems("Other", "Other Stuff", self::ItemTypeOtherStuff, 6);

        return "Done !";
    }

    /**
     * Create a new items
     *
     * @author: hong
     * @return void
     * @edited Manh -20160308
     */
    public function createItems($name, $description, $type, $price)
	{
        //$stripe = new StripeFuns();

		$item = new Items();
        $item->total = self::InitItemTotal;
        $item->total_rented = 0;
        $item->name = $name;
        $item->type = $type;
        $item->price = $price;
        $item->description = $description;
        $firstChange[] = array(
           'total'=>self::InitItemTotal,
           'date'=> intval((date("Y") .''. date("m"))),
           'year'=>date("Y"),
           'month'=>date("M"),
           'current_total'=>self::InitItemTotal
        );
        $item->changes = $firstChange;

        try {
            //$StripePlan = $stripe->CreatePlan($name, $description, ($price*100));
            // Add to Items
            //$item->StripePlanId = $StripePlan->id;
            $item->save();
        }
        catch(Exception $e)
        {

        }
	}

    /**
     * Get storage
     */
    public function storage()
    {
        return $this->hasOne('App\Models\Storage', '_id');
    }


    /**
     * DDD - 20160219
     * updateBin
     */
    public function getById($binid) {
        return Items::find($binid);
    }

    /**
     * getItemByType
     *
     * @param integer $type
     * @return mixed
     */
    public function getItemByType($type) {
    	return Items::where('type', $type)->first();
    }

    /**
     * getChanges
     */
    public function getChanges() {
    	return Items::where('type', 1)
    				->orWhere('type', 2)->select('changes')->get();
    }
}