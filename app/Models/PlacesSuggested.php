<?php
namespace App\Models;

// use Jenssegers\Mongodb\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class PlacesSuggested extends Eloquent {

	protected $collection = 'placessuggested';
	protected $fillable = ['zipcode', 'emails'];

	/**
	 * sendSuggestion
	 */
	public function sendSuggestion($code, $email) {

		$obj = PlacesSuggested::where('zipcode', $code)->first();

		if (empty($obj)) {
			$obj = new PlacesSuggested();
			$obj->emails = $email;
		}
		else if ($obj->emails != '') {
			$arr = (strpos($obj->emails, '|') !== false) ? explode('|', $obj->emails) : array($obj->emails);
			if (in_array($email, $arr)) return ['code' => 0, 'error' => 'This email is existed.'];
			$obj->emails = $obj->emails . '|' . $email;
		}
		else
		{
			$obj->emails = $email;
		}

		$obj->zipcode = $code;
		$obj->save();

		return (!empty($obj)) ? 1 : ['code' => 0, 'error' => 'Error !!!'];
	}
}