<?php
namespace App\Models;

use \DateTime;
// use Jenssegers\Mongodb\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Users extends Eloquent {

	protected $order = 'ASC';
	protected $orderBy = 'created_at';

	protected $perPage = 15;
	protected $paginationPath = '/admin/userlist';

	// Manh add stripe_id (customer_id on stripe) 20160720
	// Manh add sub_date, sub_id (subscription date, subscription id) to keep that day for renewal 20160818
	protected $fillable = ['userid', 'city', 'full', 'zipcode', 'email', 'password',
							'name', 'avatar', 'phone', 'physicalAddress', 'status',
							'authData', 'referral', 'point', 'totalorder', 'ipaddress',
							'stripe_id', 'stripe_token', 'sub_date', 'sub_id', 'sub_total', 'plan_id'];
	protected $guarded = [];

	/**
	 * search
	 */
	public function search($cons = null, $perPage = false,  $path = false, $orderBy = false, $order = false) {

		$perPage = $perPage ? $perPage : $this->perPage;
		$paginationPath = $path ? $path : $this->paginationPath;
		$order = $order ? $order : $this->order;
		$orderBy = $orderBy ? $orderBy : $this->orderBy;

		// Build query
		$q = Users::query();

		if (isset($cons['withoutadmin']) && ($cons['withoutadmin'])) {
			$q->where('_id', '!=', 1);
		}

		if (isset($cons['from']) && !empty($cons['from'])) {
			$start = date("Y-m-d", strtotime($cons['from'])) . ' 00:00:00';
			$q->where('created_at', '>', new DateTime($start));
		}

		if (isset($cons['to']) && !empty($cons['to'])) {
			$end = date("Y-m-d", strtotime($cons['to'])) . ' 23:59:59';
			$q->where('created_at', '<', new DateTime($end));
		}

		if (isset($cons['status']) && is_numeric($cons['status'])) {
			$q->where('status', '=', $cons['status']);
		}

		if (isset($cons['search']) && $cons['search'] != '') {
			$search = $cons['search'];
			$q->where(function($query) use ($search) {
				$query->where('_id', '=', $search)
						->orWhere('zipcode', '=', $search)
						->orWhere('name', 'like', '%' . $search . '%')
						->orWhere('email', 'like', '%' . $search . '%')
						->orWhere('phone', 'like', '%' . $search . '%');
			});
		}

		$users = $q->orderBy($orderBy, $order)->paginate($perPage);
		$users->setPath($paginationPath);
		$users->appends($cons)->links();

		return $users;

	}

	/**
	 * paginate
	 * Get list all users with paginate
	 *
	 * @return array
	 */
	public function paginate($perPage = false,  $path = false, $orderBy = false, $order = false)
	{
		$perPage = $perPage ? $perPage : $this->perPage;
		$paginationPath = $path ? $path : $this->paginationPath;

		$order = $order ? $order : $this->order;
		$orderBy = $orderBy ? $orderBy : $this->orderBy;

		$users = Users::orderBy($orderBy, $order)->paginate($perPage);
		$users->setPath($paginationPath);

		return $users;
	}

	/**
	 * isExistedUserId
	 */
	public function isExistedUserId($userId)
	{
		$result = Users::find($userId);
		return ($result) ? true : false;
	}

	/**
	 * isExistedEmail
	 */
	public static function isExistedEmail($email)
	{
		$result = Users::where('email', '=', $email)->first();

		return ($result) ? true : false;
	}

	/**
	 * createUser
	 */
	public function createUser($data)
	{
		/* $authUser = new Users();
    	$authUser->zipcode = $data['zipcode'];
    	$authUser->name = $data['name'];
    	$authUser->email = $data['email'];
    	$authUser->password = bcrypt($data['password']);
    	$authUser->authData = $data['fbId'];
    	$authUser->avatar = $data['avatar'];
    	$authUser->status = 1;
    	$authUser->save();

    	return $authUser; */
	}

	/**
	 * trans
	 * Relationship
	 */
	public function trans()
	{
		return $this->hasMany('App\Models\Transactions', '_id');
	}

	/**
	 * getUserFieldById
	 */
	public static function getUserFieldById($uid)
	{
		$uid = ($uid == 1) ? (int)$uid : $uid;
		return Users::select('userid', 'city',
				'zipcode', 'email',
				'name', 'avatar',
				'phone', 'physicalAddress',
				'status', 'authData',
				'referral', 'point',
				'totalorder', 'full',
				'created_at', 'updated_at',
				'ipaddress', 'ipaddress'
			)->find($uid);
	}

    /**
     * DDD - 20160216
	 * getUserById
	 */
	public static function getUserById($uid)
	{
		$uid = ($uid == 1) ? (int)$uid : $uid;
		return Users::find($uid);
	}
    /**
     * DDD - 20160216
	 * getUserByIds
	 */
	public static function getUserByIds($uids)
	{
		return Users::whereIn('_id',$uids)->get();
	}

	/**
	 * updateUserTotal
	 *
	 * @param $user
	 * @param $total This is amount payment
	 */
	public static function updateUserTotal($user, $total)
	{
		if ($total <= 0) return false;

		$total = $total / 100;

		$obj = Users::where('userid', '=', $user)->first();
		if( $obj ) {
			$obj->totalorder += number_format($total, 2, '.', '');
			$obj->save();
		}
	}

}