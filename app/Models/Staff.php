<?php
namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use \DateTime;
// use Jenssegers\Mongodb\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Staff extends Eloquent implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

    protected $order = 'ASC';
    protected $orderBy = 'updated_at';

    protected $perPage = 15;
    protected $paginationPath = '/admin/staff';

    protected $fillable = ['first_name', 'name', 'email', 'password', 'role', 'birthday', 'ss', 'driverlicense', 'phone', 'salary', 'address', 'city', 'zipcode', 'position', 'datehired', 'comments', 'status'];
    protected $typeProperties = [
        'salary' =>'string',
    ];
    protected $guarded = [];

    /**
     * search
     */
    public function search($cons = null, $perPage = false, $path = false, $orderBy = false, $order = false) {

        $perPage = $perPage ? $perPage : $this->perPage;
        $paginationPath = $path ? $path : $this->paginationPath;
        $order = $order ? $order : $this->order;
        $orderBy = $orderBy ? $orderBy : $this->orderBy;
        // Build query
        $q = Staff::query();

        $q->where('role', '!=', 'owner');	// Not query owner role

        if (isset($cons['status']) && is_numeric($cons['status'])) {
            $q->where('status', '=', $cons['status']);
        }

        if (!empty($cons['search'])) {
            $search = $cons['search'];
            $q->where(function($query) use($search) {
                $query->where('_id', '=', $search)
                ->orWhere('name', 'like', '%'.$search.'%')
                ->orWhere('first_name', 'like', '%'.$search.'%')
                ->orWhere('ss', 'like', '%'.$search.'%')
                ->orWhere('driverlicense', 'like', '%'.$search.'%');
            });
        }

        $users = $q->orderBy($orderBy, $order)->paginate($perPage);
        $users->setPath($paginationPath);

        return $users;
    }

    /**
     * paginate
     * Get list all users with paginate
     *
     * @return array
     */
    public function paginate($perPage = false, $path = false, $orderBy = false, $order = false) {
        $perPage = $perPage ? $perPage : $this->perPage;
        $paginationPath = $path ? $path : $this->paginationPath;

        $order = $order ? $order : $this->order;
        $orderBy = $orderBy ? $orderBy : $this->orderBy;

        $users = Users::orderBy($orderBy, $order)->paginate($perPage);
        $users->setPath($paginationPath);

        return $users;
    }

    /**
     * DDD - 20160217
     * mappingValues
     */
    public function mappingValues($model, $data) {
        foreach($this->fillable as $field) {
            if (isset($data[$field]))
                $model->{$field} = (string) $data[$field];
        }
        return $model;
    }

    /**
     * DDD - 20160217
     * upsert
     */
    public function upsert($data) {
        if (isset($data['_id'])) {
            $staff = Staff::find($data['_id']);
            if ($staff === null) return false;
        } else {
            $staff = new Staff();
            $staff->status = 1;
        }

        $newModel = $this->mappingValues($staff, $data);
        $newModel->save();

        return $newModel;
    }

    /**
     * DDD - 20160218
     * getAll
     */
    public function getAll() {
        return Staff::orderBy('created_at', 'desc')->get();
    }

    /**
     * DDD - 20160218
     * getById
     */
    public function getById($staffid) {
    	$staffid = ($staffid == 1) ? (int) $staffid : $staffid;
        return Staff::find($staffid);
    }

    /**
     * DDD - 20160218
     * IsNullOrEmptyString
     */
    function IsNullOrEmptyString($str) {
        return (!isset($str) || trim($str) === '');
    }

    /**
     * DDD - 20160218
     * validateDate
     */
    public function validateDate($date, $format) {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * DDD - 20160218
     * validModel
     */
    public function validModel($data) {

        if ($this->IsNullOrEmptyString($data['name'])) return "Name is require";

        if ($this->IsNullOrEmptyString($data['email'])) return "Email is require";

        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) return "Email invalid format";

        if ($this->IsNullOrEmptyString($data['salary'])) return "Salary is require";

        if (!is_numeric($data['salary'])) return "Salary invalid";

        if (!$this->IsNullOrEmptyString($data['birthday']))
            if (!$this->validateDate($data['birthday'], 'Y/m/d'))
                return "Birthday invalid format";

        if (!$this->IsNullOrEmptyString($data['datehired']))
            if (!$this->validateDate($data['datehired'], 'Y/m/d'))
                return "Date hired invalid format";

        return true;
    }

    public function validExcelPost($data) {
        if ($this->IsNullOrEmptyString($data['name']))
            return "Name is require";

        if ($this->IsNullOrEmptyString($data['email']))
            return "Email is require";

        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL))
            return "Email invalid format";

        if ($this->IsNullOrEmptyString($data['salary']))
            return "Salary is require";

        if (!is_numeric($data['salary']))
            return "Salary invalid";

        if (!$this->IsNullOrEmptyString($data['birthday']))
            if (!$this->validateDate($data['birthday'], 'n/d/Y'))
                return "Birthday invalid format";

        if (!$this->IsNullOrEmptyString($data['datehired']))
            if (!$this->validateDate($data['datehired'], 'n/d/Y'))
                return "Date hired invalid format";

                return true;
    }

    /**
     * DDD - 20160223
     * getListEmail
     */
    public function getListEmail() {
        return Staff::distinct()->get(['email']);
    }

}